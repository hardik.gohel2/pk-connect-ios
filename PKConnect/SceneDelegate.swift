//
//  SceneDelegate.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 10/30/22.


//https://stackoverflow.com/questions/58973143/method-scene-openurlcontexts-is-not-called

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FacebookCore
import OtplessSDK

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    var dynamicLinkParams = [URLQueryItem]()
    var dynamicLinkValue = ""
    
    
    //Deeplink or Universial Link Open when app is start.
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.makeKeyAndVisible()
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let splashVC = storyBoard.instantiateViewController(withIdentifier: "splashVC")
        let initialNavigationController = UINavigationController(rootViewController: splashVC)
        window?.rootViewController = initialNavigationController
        
        DispatchQueue.main.asyncAfter(deadline: .now()+2){
            
            // Universal link Open
            if let userActivity = connectionOptions.userActivities.first,
               userActivity.activityType == NSUserActivityTypeBrowsingWeb,
               let urlinfo = userActivity.webpageURL{
                
                print ("Universial Link Open at SceneDelegate on App Start ::::::: \(urlinfo)")
                self.HandleDynamicURL(url: urlinfo)
                
                
            }
            
            //deeplink Open
            if connectionOptions.urlContexts.first?.url != nil {
                //                 let urlinfo = connectionOptions.urlContexts.first?.url
                
                if let url = connectionOptions.userActivities.first?.webpageURL {
                    // ... or might have to cycle thru multiple activities
                    print ("Deeplink Open at SceneDelegate on App Start ::::::: \(String(describing: url))")
                    self.HandleDynamicURL(url: url)
                    
                    
                    if DynamicLinks.dynamicLinks().shouldHandleDynamicLink(fromCustomSchemeURL: url) {
                        let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url)
                        self.handleDynamicLink(dynamicLink)
                    }
                    
                }
            }
        }
    }
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let url = URLContexts.first?.url else {
            return
        }
        for context in URLContexts {
          if Otpless.sharedInstance.isOtplessDeeplink(url: context.url.absoluteURL) {
           Otpless.sharedInstance.processOtplessDeeplink(url: context.url.absoluteURL)
          }
          break
         }
        if let url = URLContexts.first?.url {
             print ("Deeplink Open on SceneDelegate at App Pause :::::::: \(String(describing: url))")
             self.HandleDynamicURL(url: url)
         }
        if (url.scheme == "fb592025922652694") {
            let _ = ApplicationDelegate.shared.application(
                UIApplication.shared,
                open: url,
                sourceApplication: nil,
                annotation: [UIApplication.OpenURLOptionsKey.annotation])
        }else{
            //            if let openURLContext = URLContexts.first{
            //              let url = openURLContext.url
            //              let options: [AnyHashable : Any] = [
            //                UIApplication.OpenURLOptionsKey.openInPlace : openURLContext.options.openInPlace
            //              ]
            //              TWTRTwitter.sharedInstance().application(UIApplication.shared, open: url, options: options)
            //            }
        }
    }
    
    func handleDynamicLink(_ dynamicLink: DynamicLink?) {
        guard let dynamicLink = dynamicLink else { return }
        guard let deepLink = dynamicLink.url else { return }
        let queryItems = URLComponents(url: deepLink, resolvingAgainstBaseURL: true)?.queryItems
        let invitedBy = queryItems?.filter({(item) in item.name == "referral_code"}).first?.value
        let user = Auth.auth().currentUser
        // If the user isn't signed in and the app was opened via an invitation
        // link, sign in the user anonymously and record the referrer UID in the
        // user's RTDB record.
        if user == nil && invitedBy != nil {
            
            UserDefaults.standard.set(invitedBy, forKey: "refered_by")
            
            Auth.auth().signInAnonymously() { (user, error) in
                if let user = user {
                    let userRecord = Database.database().reference().child("users").child(user.user.uid)
                    userRecord.child("referred_by").setValue(invitedBy)
                }
            }
        }
    }
    
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    
    
    // Deeplink Open when app in onPause
    
//    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
//        if let url = URLContexts.first?.url {
//
//            print ("Deeplink Open on SceneDelegate at App Pause :::::::: \(String(describing: url))")
//            self.HandleDynamicURL(url: url)
//
//
//        }
//    }
    
    
    // Universial link Open when app is onPause
    
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        print("userActivity url: \(userActivity.webpageURL!)")
        
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
              let url = userActivity.webpageURL else {
            return
        }
        
        print ("Universial Link Open at SceneDelegate on App Pause  ::::::: \(url)")
        
        self.HandleDynamicURL(url: url)
        
    }
    
   
    
    
    func HandleDynamicURL(url: URL){
        
        if let retrievedUser = getRetrievedUser() {
            if retrievedUser.is_registered == "1" {
                
                // CHECK USER IS LOGIN OR NOT
                
                let timerValue = 1.0
                
                DynamicLinks.dynamicLinks().handleUniversalLink(url) { [self] dynamicLink, error in
                    
                    guard error == nil,
                          let dynamicLink = dynamicLink,
                          let urlString = dynamicLink.url?.absoluteString else {
                        return
                    }
                    self.dynamicLinkValue = urlString
                    print("Dyanmic link url: \(urlString)")
                    print("Dynamic link match type: \(dynamicLink.matchType.rawValue)")
                    
                    // Get URL components from the incoming user activity.
                    guard let incomingURL = dynamicLink.url,
                          let components = NSURLComponents(url: incomingURL, resolvingAgainstBaseURL: true) else {
                        return
                    }
                    // Check for specific URL components that you need.
                    guard let path = components.path,
                          let params = components.queryItems else {
                        return
                    }
                    dynamicLinkParams = params
                    self.HandleDynamicLinks(params: params, timerValue: timerValue)
                    
                    print("params = \(params)")
                    print("path = \(path)")
                    
                }
            }
        }
    }
    
    func HandleDynamicLinks(params: [URLQueryItem], timerValue: Double)
    {
        print(params)
        
        let items: [String: Any] = params.reduce(into: [:]) {
            params, queryItem in
            params[queryItem.name] = queryItem.value
        }
        
        print(items)
        
        var info : [String:Any] = [:]
        info["news_id"] = items["news_id"]
        info["link_type"] = items["item_type"]
        info["referral_code"] = items["referral_code"]
        info["quiz_id"] = items["quiz_id"]
        
        if let topView = topMostViewController(){
            
            (topView is UINavigationController) ? print("TYPE - UINavigationController") : print("TYPE - UIViewController")
            
            if let nav : UINavigationController = topView as? UINavigationController
            {
                LinkManager.shared.delegate = nav.topViewController
            }else{
                LinkManager.shared.delegate = topView
            }
        }
        LinkManager.shared.handle(info)
    }
    
    
    func getRetrievedUser() -> VerifiedUser? {
        if let userData = UserDefaults.standard.object(forKey: "User") as? Data,
           let retrievedUser = try? JSONDecoder().decode(VerifiedUser.self, from: userData) {
            return retrievedUser
        }
        return nil
    }
    
    
    func topMostViewController() -> UIViewController? {
        
        let vc = UIApplication.shared.connectedScenes.filter {
            $0.activationState == .foregroundActive
        }.first(where: { $0 is UIWindowScene })
            .flatMap( { $0 as? UIWindowScene })?.windows
            .first(where: \.isKeyWindow)?
            .rootViewController?
            .topMostViewController()
        
        return vc
    }
    
}

