//
//  RoundedButton.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 10/30/22.
//

import UIKit

@IBDesignable

class RoundedButton: UIButton {

    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
           self.layer.cornerRadius = cornerRadius
        }
    }

    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
           self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
           self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var shadowColor: UIColor = .clear {
          didSet {
              self.layer.shadowColor = shadowColor.cgColor
          }
      }
      
      @IBInspectable var shadowRadius: CGFloat = 0 {
          didSet {
              self.layer.shadowRadius = shadowRadius
          }
      }
      
      @IBInspectable var shadowOffsetX: CGFloat = 0 {
          didSet {
              self.layer.shadowOffset.width = shadowOffsetX
          }
      }
      
      @IBInspectable var shadowOffsetY: CGFloat = 0 {
          didSet {
              self.layer.shadowOffset.height = shadowOffsetY
          }
      }
    
    @IBInspectable var localizableText: String = "" {
        didSet {
            let attributedTitle = NSAttributedString(string: localizableText.localize())
            self.setAttributedTitle(attributedTitle, for: .normal)
        }
    }
    
    override func layoutSubviews() {
           super.layoutSubviews()
          // applyShadow()
       }
       
       private func applyShadow() {
           self.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
           self.layer.shadowOpacity = 1.0
           self.layer.masksToBounds = false
       }

}
