//
//  CustomView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 10/30/22.
//

import UIKit

@IBDesignable
class CustomView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var addShadow: Bool = false {
        didSet {
            self.layer.shadowColor = UIColor.lightGray.cgColor
            self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
            self.layer.shadowOpacity = 1.0
            self.layer.shadowRadius = 2
        }
    }

}
