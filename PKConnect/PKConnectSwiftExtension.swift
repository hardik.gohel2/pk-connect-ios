//
//  PKConnectSwiftExtension.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 12/6/22.


import UIKit


extension UIImageView {
    
    func addInitials(first: String) {
        self.image = UIImage()
        self.backgroundColor = appYelloColor
        let initials = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height))
        initials.center = CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2)
        initials.textAlignment = .center
        initials.text = first
        initials.textColor = .black
        self.addSubview(initials)
    }
}


extension UITableView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "NunitoSans-Bold", size: 20.0)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}

extension String {
    
    var initialsTwo: String {
        return self.components(separatedBy: " ").filter { !$0.isEmpty }.reduce("") { ($0 == "" ? "" : "\($0.first!)") + "\($1.first!)" }
    }
    
    var initialsOne: String {
        
        var finalString = String()
        var words = components(separatedBy: .whitespacesAndNewlines)
        
        if let firstCharacter = words.first?.first {
            finalString.append(String(firstCharacter))
            words.removeFirst()
        }
        
        return finalString.uppercased()
    }
}

extension UIColor {
    static let pkConnectYellow = UIColor(red: 255/255, green: 206/255, blue: 26/255, alpha: 1)
}

extension String {
    func localize() -> String {
        if let lang = UserDefaults.standard.object(forKey: "selectedLang") {
            let path = Bundle.main.path(forResource: lang as? String, ofType: "lproj")
            let bundle = Bundle(path: path!)
            return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        }
        return self
    }
    
    var htmlToAttributedString: NSMutableAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSMutableAttributedString(data: data,
                                                 options: [.documentType: NSMutableAttributedString.DocumentType.html,
                                                           .characterEncoding: String.Encoding.utf8.rawValue],
                                                 documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    
    func removeHTMLTag() -> String {
        let str = self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        let str1 = str.replacingOccurrences(of: "&nbsp;", with: " ")
        return str1.replacingOccurrences(of: "&#39;", with: "'")
    }
}

extension UIImageView {
    func setLoadedImage(_ urlStr: String) {
        guard let imageUrl = URL(string: urlStr) else { return }
        self.loadImage(url: imageUrl) { data, error in
            if let data = data, let img = UIImage(data: data) {
                DispatchQueue.main.async { [self] in
                    self.image = img
                }
            }
        }
    }
    
    func loadImage(url: URL, completion: @escaping (Data?, Error?) -> Void) {
        // Compute a path to the URL in the cache
        let fileCachePath = FileManager.default.temporaryDirectory
            .appendingPathComponent(url.lastPathComponent, isDirectory: false)
        
        // If the image exists in the cache, load the image from the cache and exit
        do {
            let data = try Data(contentsOf: fileCachePath)
            completion(data, nil)
            return
        } catch {}
        
        // If the image does not exist in the cache, download the image to the cache
        downloadImageFrom(url: url, toFile: fileCachePath) { (error) in
            do {
                let data = try Data(contentsOf: fileCachePath)
                completion(data, error)
            } catch {}
        }
    }
    
    func downloadImageFrom(url: URL, toFile file: URL, completion: @escaping (Error?) -> Void) {
        // Download the remote URL to a file
        let task = URLSession.shared.downloadTask(with: url) {
            (tempURL, response, error) in
            guard let tempURL = tempURL else {
                completion(error)
                return
            }
            
            do {
                // Remove any existing document at file
                if FileManager.default.fileExists(atPath: file.path) {
                    try FileManager.default.removeItem(at: file)
                }
                
                // Copy the tempURL to file
                try FileManager.default.copyItem(at: tempURL, to: file)
                completion(nil)
            } catch {}
        }
        task.resume()
    }
    
}

extension UIViewController {
    
    func topMostViewController() -> UIViewController {
        if self.presentedViewController == nil {
            return self
        }
        
        if let navigation = self.presentedViewController as? UINavigationController {
            return navigation.visibleViewController!.topMostViewController()
        }
        
        if let tab = self.presentedViewController as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        
        return self.presentedViewController!.topMostViewController()
    }
    
}

extension UIView {
    
    @IBInspectable var ViewBorderColor:UIColor? {
        set {
            layer.borderColor = newValue?.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            else {
                return nil
            }
        }
    }
    
    @IBInspectable var ViewBorderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var ViewCornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var enableShadow: Bool {
        get {
            return layer.shadowOpacity > 0
        }
        set{
            self.addShadow()
        }
    }
    
    func addShadow() {
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowOpacity = 1
        layer.shadowRadius = 5.0
        clipsToBounds = false
    }
    
}
