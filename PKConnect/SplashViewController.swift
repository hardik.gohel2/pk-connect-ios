

import UIKit

class SplashViewController: BaseViewController {

    @IBOutlet weak var pkConnectLogo: UIImageView!
    var isLoginUser:Bool? = false
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        var initialVC: UIViewController = LanguageSelectionVC()
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        
        if let retrievedUser = self.getRetrievedUser() {
            if retrievedUser.is_registered == "1" {
                self.isLoginUser = true
                let slidingVC = storyBoard.instantiateViewController(withIdentifier: "slidingVC") as! SlidingViewController
                initialVC = slidingVC
            } else { }
        }
        
        self.pkConnectLogo.transform = CGAffineTransform(scaleX: 0.1,y: 0.1)
        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear, animations: { [self] in
            pkConnectLogo.transform = CGAffineTransform(scaleX: 1,y: 1)
        }) { (success: Bool) in
            if (self.isLoginUser ?? false) {
//                self.getStoriLists {
                    self.navigationController?.pushViewController(initialVC, animated: false)
//                }
            } else {
                self.navigationController?.pushViewController(initialVC, animated: false)
            }
        }
    }
    
    

}
