//
//  CustomTextField.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 10/30/22.
//

import UIKit

@IBDesignable
class CustomTextField: UITextField {
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        if leftView != nil {
            return CGRectInset(bounds, 50, 0)
        }
        return CGRectInset(bounds, 20, 0)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        if leftView != nil {
            return CGRectInset(bounds, 50, 0)
        }
        return CGRectInset(bounds, 20, 0)
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
           self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
           self.layer.borderWidth = borderWidth
        }
    }
    
}
