//
//  LinkManager.swift
//  PKConnect
//
//  Created by Yatindra on 09/02/23.
//

import Foundation
import UIKit

protocol LinkManagerDelegate: AnyObject {
    func present(vc: UIViewController)
}


extension UIViewController: LinkManagerDelegate {
    func present(vc: UIViewController) {
        DispatchQueue.main.async {
            
            
            (vc is UINavigationController) ? print("TYPE - UINavigationController") : print("TYPE - UIViewController")
            
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
}

final class LinkManager {
    
    static let shared = LinkManager()
    
    private var deeplinkInfo: [String : Any]?
    
    public weak var delegate: LinkManagerDelegate? {
        didSet {
            self.deeplinkAction()
        }
    }
    
    private init() { }
    
    private func deeplinkAction() {
        guard self.delegate != nil,
              let userInfo = self.deeplinkInfo
        else {
            return
        }
        self.deeplinkInfo = nil
        self.handle(userInfo)
    }
    
    
    public func handle(_ userInfo: [String : Any]) {
        
        guard self.delegate != nil else {
            self.deeplinkInfo = userInfo
            return
        }
        
        self.deeplinkInfo = nil
        
        do {
            let userInfoData = try JSONSerialization.data(withJSONObject: userInfo, options: [])
            let notificationData = try JSONDecoder().decode(LinkData.self, from: userInfoData)
            print(notificationData)
            do {
                let data = try NSKeyedArchiver.archivedData(withRootObject: userInfo, requiringSecureCoding: false)

                // Save the Data to UserDefaults
                UserDefaults.standard.set(data, forKey: "myDictionaryKey")
            } catch {
                print("Error archiving data: \(error.localizedDescription)")
            }

            // Retrieve the Data from UserDefaults and convert it back to a dictionary
            if let savedData = UserDefaults.standard.data(forKey: "myDictionaryKey") {
                do {
                    if let savedDictionary = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(savedData) as? [String: String] {
                        print("Saved Referral Code: \(savedDictionary["referral_code"] ?? "")")
                        if let referralCode = savedDictionary["referral_code"] {
                                     print("Saved Referral Code: \(referralCode)")
                            let defaults = UserDefaults.standard
                            defaults.set(referralCode, forKey: "referral_code")
                     if let referralCode = UserDefaults.standard.value(forKey: "referral_code") as? String {
                                print(referralCode)
                            } else {
                                print("Gaurav (Default Value)")
                            }

                            defaults.synchronize()
                                 } else {
                                     print("Referral code not found in the saved dictionary.")
                                 }
                        
                    
                    }
                } catch {
                    print("Error unarchiving data: \(error.localizedDescription)")
                }
            } else {
                print("Dictionary not found in UserDefaults.")
            }
            self.setupController(for: notificationData)
        }
        catch {
            print(error)
        }
    }
    
    private func setupController(for notificationData: LinkData) {
        
        switch notificationData.type {
        case .news:
            self.getNewsDetail(for: notificationData)
        case .referral_code:
            self.getReferralCode(for: notificationData)
        case .none:
            break
        }
    }
    
    public func getReferralCode(for notificationData: LinkData) {
        guard let referralCode = notificationData.referral_code else{ return }
        UserDefaults.standard.set(referralCode, forKey: "referralCode")
        UserDefaults.standard.synchronize()
    }
    
    public func getNewsDetail(for notificationData: LinkData) {
        
        guard let newsId = notificationData.id
        else{ return }
        
        let storyBoard = UIStoryboard(name: "Other", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "newsDetailView") as! NewsDetailView
        vc.newsId = newsId
        //        let nvc = UINavigationController(rootViewController: vc)
        //        nvc.isNavigationBarHidden = true
        self.delegate?.present(vc: vc)
    }
    
    
//    public func getQuizDetail(for notificationData: LinkData) {
//
//        guard let newsId = notificationData.id
//        else{ return }
//
//        let storyBoard = UIStoryboard(name: "Quiz", bundle: nil)
//        let vc = storyBoard.instantiateViewController(withIdentifier: "newsDetailView") as! quizde
//        vc.newsId = newsId
//        //        let nvc = UINavigationController(rootViewController: vc)
//        //        nvc.isNavigationBarHidden = true
//        self.delegate?.present(vc: vc)
//    }
    
}
