//
//  File.swift
//  PKConnect
//
//  Created by Yatindra on 09/02/23.
//


import Foundation

public enum LinkType: String, Codable {
    case news = "news"
    case referral_code = "referral_code"
    case none
    
}

struct LinkData: Codable {
    
    public private(set) var id: String?
    public private(set) var quiz_id: String?
    public private(set) var type: LinkType
    public private(set) var referral_code: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case newsID = "news_id"
        case type = "link_type"
        case referral_code = "referral_code"
        case quiz_id = "quiz_id"
    }
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.type = (try? container.decode(LinkType.self, forKey: .type)) ?? .none
        
        switch self.type {
        case .news:
            self.id = try? container.decode(String.self, forKey: .newsID)
        case .referral_code:
            self.referral_code = try? container.decode(String.self, forKey: .referral_code)
        case .none:
            self.id = nil
        
        }
        
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try? container.encode(self.id, forKey: .id)
        try? container.encode(self.type, forKey: .type)
        try? container.encode(self.quiz_id, forKey: .quiz_id)
        try? container.encode(self.referral_code, forKey: .referral_code)
    }
}
