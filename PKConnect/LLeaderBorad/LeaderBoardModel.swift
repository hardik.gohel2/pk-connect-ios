//
//  LeaderBoardModel.swift
//  PKConnect
//
//  Created by apple on 11/01/24.
//

import Foundation



struct LeaderBoardModel: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: LeaderBoardResultModel
    let STATUS: String
}

struct LeaderBoardResultModel: Codable {
    let leaderBoard: [LeaderBoardDataModel]
}

struct LeaderBoardDataModel: Codable {
    let rank: Int
    let user_id: String
    let full_name: String
    let user_image: String
    let total_points: String
}

