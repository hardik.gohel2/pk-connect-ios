//
//  SlidingViewController.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 10/30/22.
//   


import UIKit

class SlidingViewController: BaseViewController, ContainerToMaster {

    @IBOutlet private var slidingView: UIView!
    private var VerifyPopUpView: VerifyPopUpView!

    @IBOutlet private var slidingViewLeading: NSLayoutConstraint!
    @IBOutlet private var slidingViewTrailing: NSLayoutConstraint!
    @IBOutlet private var slidingViewTop: NSLayoutConstraint!

    @IBOutlet private var containerView: UIView!
    @IBOutlet private var backViewForHamburger: UIView!

    @IBOutlet private var screenTitle: UILabel!

    private var currentMenuItemIndex: Int = 0,
                isHamburgerMenuShown = false,
                currentVC: UIViewController?,
                logOutVC: LogOutView!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .pkConnectYellow

        backViewForHamburger.isHidden = true
        view.bringSubviewToFront(slidingView)

        if let tabBarVC = self.children.first as? TabBarBaseController {
            currentVC = tabBarVC
            screenTitle.text = String(format: "Hi %@".localize(), (getRetrievedUser()?.user_name ?? ""))
        }
        
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let tabBarVC = segue.destination as? TabBarBaseController {
            tabBarVC.containerToMaster = self
        } else if let hamburgerVC = segue.destination as? HamburgerViewController {
            hamburgerVC.hamburgerToMaster = self
        }
    }

    func showHamburgerMenu() {
        for vc in self.children {
            if let hamburgerVC = vc as? HamburgerViewController, let savedUser = getRetrievedUser() {
                hamburgerVC.retrievedUser = savedUser
            }
        }
        UIView.animate(withDuration: 0.3) { [self] in
            slidingViewLeading.constant = 180
            slidingViewTrailing.constant = -180
            slidingViewTop.constant = 20
            self.view.layoutIfNeeded()
        } completion: { [self] _ in
            self.backViewForHamburger.alpha = 0.75
            self.backViewForHamburger.isHidden = false
            self.view.bringSubviewToFront(backViewForHamburger)
        }
    }

    func changeTopViewTitleTo(newTitle: String) {
        screenTitle.text = newTitle
    }

    @IBAction func tappedOutsideHamburgerView(_ sender: Any) {
        hideHamburgerMenu()
    }

    private func hideHamburgerMenu() {
        UIView.animate(withDuration: 0.3) { [self] in
            slidingViewLeading.constant = 0
            slidingViewTrailing.constant = 0
            slidingViewTop.constant = 0
            self.view.layoutIfNeeded()
        } completion: { [self] _ in
            self.backViewForHamburger.isHidden = true
        }
    }

    @IBAction private func changeLanguage(_ sender: UIButton) {
        changeAppLanguage()
    }

    @IBAction func notificationButtonTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "notificationView") as! NotificationView
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction private func hamburgerMenuTapped(_ sender: Any) {
        showHamburgerMenu()
    }
}

extension SlidingViewController: HamburgerToMaster {

    func menuItemSelected(index: Int) {
        //Remove Current View from SuperView
        if index != 9 {
            slidingView.subviews[1].removeFromSuperview()
            currentVC?.removeFromParent()
            currentVC?.willMove(toParent: nil)
        }

        var viewIdentifier = ""
        switch index {
        case 0: //Home
            screenTitle.text = String(format: "Hi %@".localize(), (getRetrievedUser()?.user_name ?? ""))
            viewIdentifier = "tabBarController"
        case 1:
            screenTitle.text = "Profile"
            viewIdentifier = "profileView"
        case 2:
            screenTitle.text = "Task"
            viewIdentifier = "TaskViewController"
            if UserDefaults.standard.value(forKey: "UserVerification") != nil, UserDefaults.standard.value(forKey: "UserVerification") as! String == "0" || UserDefaults.standard.value(forKey: "UserVerification") as! String == "2" || UserDefaults.standard.value(forKey: "UserVerification") as! String == "3"{
                let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                self.VerifyPopUpView = storyBoard.instantiateViewController(withIdentifier: "VerifyPopUpView") as? VerifyPopUpView
                self.addChild(self.VerifyPopUpView)
                self.view.addSubview(self.VerifyPopUpView.view)
                self.VerifyPopUpView.view.frame = self.view.bounds

                self.view.bringSubviewToFront(self.VerifyPopUpView.view)
            }
        case 3:
            screenTitle.text = "My Rewards"
            viewIdentifier = "myRewardsView"
//        case 4:
//            screenTitle.text = "Frame It Up"
//            viewIdentifier = "frameItUpView"
        case 4:
            screenTitle.text = "Bookmarks"
            viewIdentifier = "bookmarksView"
//        case 6:
//            screenTitle.text = "Join Padyatra"
//            viewIdentifier = "joinPadyatraView"
//        case 6:
//            screenTitle.text = "Walkthrough"
//            viewIdentifier = "walkthroughView"
        case 5:
            screenTitle.text = "Referral"
            viewIdentifier = "referralView"
        case 6:
            screenTitle.text = "FAQs"
        case 7:
            screenTitle.text = "Contact Us"
            viewIdentifier = "contactUsView"
        case 8:
            screenTitle.text = "Privacy Policy"
        case 9:
            hideHamburgerMenu()
            self.showLogOutAlert()
            return
        default:
            viewIdentifier = "tabBarController"
        }
        screenTitle.text = screenTitle.text?.localize()
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)//privacy
        
        if index == 6 || index == 8 {
            let webview = WebViewController(with: (index == 8 ? "privacy" : "faq"), true)
            currentVC = webview
        } else if let vc = storyBoard.instantiateViewController(withIdentifier: viewIdentifier) as? TabBarBaseController {
            vc.containerToMaster = self
            currentVC = vc
        } else {
            currentVC = storyBoard.instantiateViewController(withIdentifier: viewIdentifier)
        }
        addChild(currentVC!)
        currentVC?.view.frame = containerView.frame
        slidingView.addSubview(currentVC!.view)
        currentVC?.didMove(toParent: self)

        hideHamburgerMenu()
    }

    func showLogOutAlert() {
        self.view.addSubview(backgroundView)
        backgroundView.frame = self.view.frame

        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        logOutVC = storyBoard.instantiateViewController(withIdentifier: "logOutVC") as? LogOutView
        logOutVC.noButtonAction = { self.dismissLogOutAlert() }
        logOutVC.yesButtonAction = { self.logOutUser() }
        addChild(logOutVC)
        view.addSubview(logOutVC.view)
        logOutVC.view.frame = view.bounds

        view.bringSubviewToFront(logOutVC.view)
    }

    func dismissLogOutAlert() {
        backgroundView.removeFromSuperview()
        logOutVC?.removeFromParent()
        logOutVC?.view.removeFromSuperview()
        logOutVC?.willMove(toParent: nil)
    }

    func logOutUser() {
        UserDefaults.standard.removeObject(forKey: "User")
        UserDefaults.standard.removeObject(forKey: "fbname")
     //   UserDefaults.standard.removeObject(forKey: "selectedLang")
        UserDefaults.standard.set("3", forKey: "UserVerification")
        UserDefaults.standard.removeObject(forKey: "twittername")
        UserDefaults.standard.removeObject(forKey: "twitterOAuthToken")
        UserDefaults.standard.removeObject(forKey: "twitterOAuthSecret")
        UserDefaults.standard.removeObject(forKey: "isPassedJourney")
        dismissLogOutAlert()
        self.goToWelcomeView()
    }
}
