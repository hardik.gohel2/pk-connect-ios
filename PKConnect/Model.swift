//
//  Model.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 10/30/22.
//

import Foundation

struct User: Codable {
    var contactNumber: String
    var isRegistered: String
    var accessToken: String?
    var fullName: String?
    var id: String
    var email: String?
    var gender: String?
    var dob: String?
    var district: String?
    var constituency: String?
}

struct UserWhatsapp: Codable {
    let name: String
    let first_name: String
    let phone_number: String
    let national_phone_number: String
    let country_code: String
    let email_verified: Bool
    let auth_time: String
    let authentication_details: AuthenticationDetails?
}

struct AppVersion : Codable {
    let CODE: Int?
    let MESSAGE: String?
    let RESULT: AppVersionResult
    
}
struct AppVersionResult: Codable {
    let vid: String?
    let version_name: String?
    let versiob_title: String?
    let version_desc: String?
    let platform: String?
    let update_type: String?
    let is_cur_version: String?
    let created_date: String?
}
struct AuthenticationDetails: Codable {
    let phone: PhoneAuthentication?
    let email: EmailAuthentication? = nil
}
struct PhoneAuthentication: Codable {
    let mode: String
    let phone_number: String
    let country_code: String
    let auth_state: String
}
struct EmailAuthentication: Codable {
   
}

struct GetCampaignsResponse: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: [Campaign]?
}

struct Campaign: Codable {
    let id: String
    let title: String
    let image: String
    let url: String
    let type: String
}

struct GetContactUsResponse: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: [ContactUsCategory]?
}

struct GetBlockResponse: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: [BlockList]
}

struct ContactUsCategory: Codable {
    let category_id: String
    let category_name: String
    let category_name_tn: String
}

struct BlockList: Codable {
    let id: String
    let district_id: String
    let block_name: String
}

struct DefaultAPIResponse: Codable {
    let CODE: Int
    let MESSAGE: String
}

struct GetStateResponse: Codable {
    let status: String
    let message: String
    let data: [State]
}

struct State: Codable {
    let state_id: String
    let state_name_en: String
}

struct GetDistrictsResponse: Codable {
    let status: String
    let message: String
    let data: [District]
}

struct District: Codable {
    let district_id: String
    let district_name: String
}

struct GetACResponse: Codable {
    let status: String
    let message: String
    let data: [AC]
}

struct AC: Codable {
    let id: String
    let ac: String
}

struct GetProfileResponse: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: UserProfile?
}

struct UserProfile: Codable {
    var user_image: String?
    var full_name: String?
    var email_id: String?
    var whatsup_number: String?
    var phone_number: String?
    var district: String?
    var ac: String?
    var state_name: String?
    var district_name: String?
    var ac_name: String?
    var education: String?
    var occupation: String?
    var referal_code: String?
    var fb_username: String?
    var twitter_id: String?
    var twitter_username: String?

}

struct UpdatedProfile: Codable {
    let full_name: String?
    let whatsup_number: String?
    let email_id: String?
    let ac: String?
    let education: String?
    let occupation: String?
}

struct SendOTPResponse: Codable {
    let status: String
    let message: String
    let data: OTPObj
}

struct OTPObj: Codable {
    let otp: String?
    let contact_number: String?
    let message: String
}

struct Normal: Codable {
    let status: String
    let message: String
}

struct VerifyOTPResponse: Codable {
    let status: String
    let message: String
    let data: VerifiedUser
}

struct VerifiedUser: Codable {
    let user_id: String?
    var is_registered: String?
    var user_name: String?
    let accessToken: String?
    var contactNumber: String?
    var full_name: String?
//    let message: String
    var referal_code: String?
    var user_image: String?
    var fb_username: String?

}
struct VerifyOTPResponseProfile: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: VerifiedUser
}

struct MobileVerifiedResponse: Codable {
    let status: String
    let message: String
    let data: MobileVerifiedUser
}
struct MobileVerifiedUser: Codable {
    @StringForcible var user_id: String?
    var is_registered: String?
    let accessToken: String?
    let message: String
    var user_name:String?
}
struct RegisterUserResponse: Codable {
    let status: String
    let message: String
    let data: RegisteredUser
}

struct RegisteredUser: Codable {
    var full_name: String? = nil
    var email_id: String? = nil
    var gender: String? = nil
    var dob: String? = nil
    var state: String? = nil
    var district: String
    var ac: String? = nil
    var is_registered: String? = nil
    var registered_on: String? = nil
    var refered_by: String? = nil
}
struct NewRegisterUserResponse: Codable {
    let status: String
    let message: String
    let data: NewRegisteredUser
}

struct NewRegisteredUser: Codable {
    let full_name: String
    let is_registered: String
    let registeration_no: String
    let refered_by: String
}
struct chatModel: Codable {
    var status: String?
    var message: String?
    var data: [chatDetails]
}


struct chatDetails: Codable {
    var id: String?
    var user_id: String?
    var sender_type: String?
    var message: String?
    var media_type: String?
    var read_status: String?
    var send_timestamp: String?
    var media_thumb: String?
}

struct FeedItem: Codable {
    let news_id: String
    let news_title: String
    let news_description: String
    var created_date: String
    var home_section: String
    let content_media_set: [Media]?
    let is_like: String
    let is_bookmarked: String
    let total_comments: String
    let total_likes: String
    let total_votes: String
    let clicks_user_list: [ClickUser]
}

struct ClickUser: Codable {
    let full_name: String
    let user_image: String?
}

struct GetHomeData: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: [FeedItem]?
    @StringForcible var TOTAL: String?
}

struct GetComments: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: [Comment]?
    @StringForcible var NEXT: String?
    let TOTAL: String
}

struct Comment: Codable {
    let full_name: String
    let user_id: String
    let user_comment: String
    let user_image: String?
    let submitted_timestamp: String
    var is_like: String
    let parent_id: String
    let comment_id: String
    var comments: [Comment]?
}

struct GetCommentsShorts: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: [ShortComment]?
}

struct ShortComment: Codable {
    let id: String
    let short_id: String
    let user_id: String
    let comment: String
    let is_deleted: String
    let created_on: String
    let updated_on: String?
    let createdBy: CommentUser
    let comments: [Comment]?
}

struct CommentUser: Codable {
    let user_id: String
    let full_name: String
    let email_id: String?
    let phone_number: String
    let user_image: String?
}

struct Media: Codable {
    let media_url: String
    let media_type: String
    let media_thumb: String
}

struct RewardTransactionResponse: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: [RewardTransaction]
}

struct RewardTransaction: Codable {
    let point: String
    let title: String
    let description: String
    let type: String
    var created_date: String
    let updated_date: String
}

struct LevelResponse: Codable {
    let CODE: Int
    let MESSAGE: String
    //EDIT SHUBHENDU
    //let total_wallet_amount: String
    let total_wallet_amount: String?
    let levels: [Level]
}

struct Level: Codable {
    let name: String
    let name_tn: String
    let level_tag_name: String
    let incentive: String
    let description: String
    let description_tn: String
    var is_unlocked: Bool
    var image: String
}

struct LeaderboardResponse: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: LeaderboardResult
}

struct LeaderboardResult: Codable {
    @StringForcible var state_rank: String?
    @StringForcible var district_rank: String?
    @StringForcible var state_total: String?
    @StringForcible var district_total: String?
    var level_id: String?
    @StringForcible var points_earned: String?
    let joined_date: String
    let topleaders: [LeaderboardUserEntry]?
    let districtTopleaders: [LeaderboardUserEntry]?
    let userInfo: UserInfo?
    let task: [Task]?
}

struct LeaderboardUserEntry: Codable {
    let full_name: String?
    let user_image: String?
    let ranking: Int?
}

struct UserInfo: Codable {
    let full_name: String?
    let user_image: String?
    let registered_on: String?
    let points_earned: String?
    let rank: Int?
}

struct MessageModel: Codable {
    let code: Int?
    let message: String?
    var result: [ResultMsg]?
}

struct HomeGridModel: Codable {
    let CODE: Int?
    let MESSAGE: String?
    var RESULT: [ResultGrid]?
}

// MARK: - Result

struct ResultGrid: Codable {
        let title: String?
       let image: String?
       let type: String?
       let url: String?
       let pta: String?
       let district: String?
       let ac: String?
       let block: String?
       let priority: String?
       let status: String?
       let createdDate: String?
}

struct ResultMsg: Codable {
    let video_id, video_title, video_desc: String?
    let media_url: String?
    let media_thumb: String?
    let created_time, is_liked: String?
    var isPlaying:Bool? = false
}

struct Task: Codable {
    let added_on: String
    let total_task: String
    let completed_task: String
    let total_fb_task: String
    let completed_fb_task: String
    let total_twitter_task: String
    let completed_twitter_task: String
    let total_whatsapp_task: String
    let completed_whatsapp_task: String
    let total_youtube_task: String
    let completed_youtube_task: String
    let total_form_task: String
    let completed_form_task: String
    let total_offline_task: String
    let completed_offline_task: String
    let reward_points: String
}

struct TaskModel {
    let code: Int?
    let message: String?
    let result: [String]?
    let isProfileEdited: String?
//    let termCondition: JSONNull?
    let total: String?
    let totalCompleteTask: Int?
    let isCampaignFormFilled: String?
    let campaignFormURL: String?
    let paymentDetailAdded: Int?
    let isIDProofAdded: String?
    
}

struct MediaSet: Encodable {
    let mediaThumb: String?
    let mediaType: String?
    let mediaUrl: String?
}

struct InputUserDetails: Codable {
    var name: String?
    var email: String?
    var gender: String?
    var dob: String?
    var state_name: String?
    var district_name: String?
    var ac_name: String?
    var refered_by: String?
}

struct BellNotificationResponse: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: [BellNotification]?
}

struct BellNotification: Codable {
    let notification_id: String
    let user_id: String
    let notification_type: String
    let notification_text: String
    let is_read: String
    let inserted_on: String
}

struct GetReferralListResponse: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: [Referral]?
}

struct Referral: Codable {
    let full_name: String
    let created_date: String
    let user_image: String
}

extension Encodable {
    var dictionary: [String: String]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: String] }
    }
}

enum HttpMethodType: String {
    case GET = "GET"
    case POST = "POST"
}

public enum RequestStatus: Int{
    case OK             = 200
    case noAccount      = 201
    case error          = 202
    case notVerified    = 204
    case tokenExpire    = 300
    case ipFailed       = 302
    case requestDenied  = 401
    case invalidRequest = 400
}

struct GetAPeerGroupResponse: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: [ClickUser]?
}
@propertyWrapper
struct StringForcible: Codable {
    
    var wrappedValue: String?
    
    enum CodingKeys: CodingKey {}
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let string = try? container.decode(String.self) {
            wrappedValue = string
        } else if let integer = try? container.decode(Int.self) {
            wrappedValue = "\(integer)"
        } else if let double = try? container.decode(Double.self) {
            wrappedValue = "\(double)"
        } else if container.decodeNil() {
            wrappedValue = nil
        }
        else {
            throw DecodingError.typeMismatch(String.self, .init(codingPath: container.codingPath, debugDescription: "Could not decode incoming value to String. It is not a type of String, Int or Double."))
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(wrappedValue)
    }
    
    init() {
        self.wrappedValue = nil
    }
    
}



// MARK: - Welcome short video stroies
struct VideoModel: Codable {
    let code: Int?
    let message: String?
    var result: [VideosData]?
    var next, total: Int?

    enum CodingKeys: String, CodingKey {
        case code = "CODE"
        case message = "MESSAGE"
        case result = "RESULT"
        case next = "NEXT"
        case total = "TOTAL"
    }
}

struct VideoModel1: Codable {
    let code: Int?
    let message: String?
    var result: VideosData?
    var next, total: Int?

    enum CodingKeys: String, CodingKey {
        case code = "CODE"
        case message = "MESSAGE"
        case result = "RESULT"
        case next = "NEXT"
        case total = "TOTAL"
    }
}

// MARK: - Result
struct VideosData: Codable {
    let id, channelID, shortTitle, shortDesc: String?
    let s3URL: String?
    let videoURL: String?
    let videoThumb: String?
    var likeCount, commentCount, shareCount, downloadCount: String?
    let viewCount, status: String?
    let createdAt: String?
    var isLiked, isSubscribed: Bool?
    let createdBy: CreatedBy?

    enum CodingKeys: String, CodingKey {
        case id
        case channelID = "channel_id"
        case shortTitle = "short_title"
        case shortDesc = "short_desc"
        case s3URL = "s3_url"
        case videoURL = "video_url"
        case videoThumb = "video_thumb"
        case likeCount = "like_count"
        case commentCount = "comment_count"
        case shareCount = "share_count"
        case downloadCount = "download_count"
        case viewCount = "view_count"
        case status
        case createdAt = "created_at"
        case isLiked = "is_liked"
        case isSubscribed = "is_subscribed"
        case createdBy
    }
}

// MARK: - CreatedBy
struct CreatedBy: Codable {
    let channelID: String?
    let title: String?
    let desc: String?
    let image: String?
    let subscribers: String?
    let createdAt: String?

    enum CodingKeys: String, CodingKey {
        case channelID = "channel_id"
        case title, desc, image, subscribers
        case createdAt = "created_at"
    }
}

 
struct ChannelDetails:Codable
{
    var CODE:Int?
    var MESSAGE:String?
    var RESULT:ChannelInfimation?
}

struct ChannelInfimation:Codable
{
    var channel_id,title,desc,image,subscribers,created_at:String?
    var is_subscribed:Bool?
    var shorts:[AllShorts]?
}


struct AllShorts:Codable{
    var id,channel_id,short_title,image_url,s3_url,video_url,video_thumb:String?
    var comment_count,share_count,download_count,view_count,status:String?
    var is_subscribed,is_liked:Bool?
    var createdBy:CreatedBy?
    var like_count:String?
}

struct ShortsModel : Codable {
    var CODE:Int?
    var MESSAGE:String?
    var RESULT:[AllShorts]?
}

struct AllShortsResult : Codable {
        let id: String?
        let channel_id: String?
        var suid: String? = nil
        let short_title: String?
        let short_desc: String?
        var image_url: String? = nil
        let readytostream: String?
        let s3_url: String?
        let video_url: String?
        let video_thumb: String?
        let pinned: String?
        var like_count: String?
        let comment_count: String?
        let share_count: String?
        let download_count: String?
        let view_count: String?
        let status: String?
        let start_date: String?
        var end_date: String? = nil
        let created_at: String?
        var is_liked: Bool?
        var is_subscribed: Bool?
        let createdBy: CreatedByShorts?
}

struct CreatedByShorts: Codable {
    let channel_id: String?
    let title: String?
    let desc: String?
    let image: String?
    let subscribers: String?
    let created_at: String?
}

struct NewsResponse: Codable {
    let CODE: Int?
    let MESSAGE: String?
    let RESULT: [NewsItem]?
    @StringForcible var NEXT: String?
    @StringForcible var TOTAL: String?
}

struct NewsItem: Codable {
    let id: String?
    let title: String?
    let description: String?
    let description_trimmed: String?
    let share_count: String?
    let view_count: String?
    let url: String?
    let image: String?
    var created_date: String?
    var video_thumb : String?
    var video_url : String?
}
