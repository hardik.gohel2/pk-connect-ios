//
//  LocationVC.swift
//  PKConnect
//
//  Created by admin on 26/10/23.
//

import UIKit



class LocationVC: BaseViewController {
    
    @IBOutlet weak var viewULB: UIView!
    @IBOutlet weak var viewBlock: UIView!
    @IBOutlet weak var viewComplete: UIView!
    @IBOutlet weak var constViewComplete: NSLayoutConstraint!
    @IBOutlet weak var txtULB: CustomTextField!{
        didSet{
            txtULB.placeholder = "Select your ULB".localize()
            txtULB.delegate = self
            txtULB.inputView = pickerView
        }
    }
    @IBOutlet weak var txtBlock: CustomTextField!{
        didSet {
            txtBlock.delegate = self
            txtBlock.inputView = pickerView
            txtBlock.placeholder = "Select your Block".localize()
        }
    }
    @IBOutlet weak var btnContinue: RoundedButton!{
        didSet {
           // btnContinue.isEnabled = false
        }
    }
    @IBOutlet weak var btUrban: RoundedButton!
    @IBOutlet weak var btnRural: RoundedButton!
    var pickerView: UIPickerView!{
        didSet {
            
            pickerView.delegate = self
            pickerView.dataSource = self
        }
    }
    @IBOutlet weak var txtContituency: CustomTextField!{
        didSet {
            txtContituency.delegate = self
            txtContituency.inputView = pickerView
            txtContituency.placeholder = "Select your Contituency".localize()
        }
    }
    @IBOutlet weak var txtDistrict: CustomTextField!{
        didSet {
            txtDistrict.delegate = self
            txtDistrict.inputView = pickerView
            txtDistrict.placeholder = "Select Your District".localize()
        }
    }
    @IBOutlet weak var txtState: CustomTextField!{
        didSet {
            txtState.delegate = self
            txtState.inputView = pickerView
            txtState.placeholder = "Select Your State".localize()
        }
    }
    
    var stateList: [State]?,  districtList: [District]?, acList: [AC]?, blockList: [BlockList]?
    var state_id = "", district_id = "", currentPicker = "", currentArea = ""
    var verifiedUser: VerifiedUser?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        showLoadingIndicator(in: self.view)
        getStateList { stateList in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                self.stateList = stateList
                self.pickerView = UIPickerView()
                self.pickerView.reloadAllComponents()
                self.pickerView.selectedRow(inComponent: 0)
                self.state_id = stateList[0].state_id
                self.txtState.text = stateList[0].state_name_en
                
                self.getDistrictList(for:self.state_id ) { districtList in
                    DispatchQueue.main.async {
                        self.districtList = districtList
                        self.pickerView.reloadAllComponents()
                        self.pickerView.selectedRow(inComponent: 0)
                        self.district_id = districtList[0].district_id
                        self.txtDistrict.text = districtList[0].district_name
                    }
                }
                
                self.getACList(for:self.district_id ) { acList in
                    DispatchQueue.main.async {
                        self.acList = acList
                        self.pickerView.reloadAllComponents()
                        self.pickerView.selectedRow(inComponent: 0)
                        self.txtContituency.text = acList[0].ac
                        
                        self.getBlocks(district_id: self.district_id)
                    }
                }
                
            }
        }
    }
    
    @IBAction func oClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickArea(_ sender: UIButton) {
        
        if sender.tag == 1 {
            btnRural.backgroundColor = appYelloColor
            btUrban.backgroundColor = UIColor.systemGroupedBackground
            currentArea = "rural"
            viewBlock.isHidden = false
            viewULB.isHidden = true
            
        }else{
            btUrban.backgroundColor = appYelloColor
            btnRural.backgroundColor = UIColor.systemGroupedBackground
            currentArea = "urban"
            viewULB.isHidden = false
            viewBlock.isHidden = true
        }
    }
    
    
    func getBlocks(district_id: String) {
        
        getBlockList(for: district_id) { blocks in
            DispatchQueue.main.async {
                self.blockList = blocks
                DispatchQueue.main.async {
                    self.pickerView.reloadAllComponents()
                    self.pickerView.selectedRow(inComponent: 0)
                   self.txtBlock.text = self.blockList?[0].block_name
                   self.txtULB.text = self.blockList?[0].block_name
                    
                }
                
            }
        }
    }
    
    @IBAction func onClickSubmit(_ sender: Any) {
        if txtState.text == "" {
            let alertController = UIAlertController(title: "Error", message: "please select state", preferredStyle: .alert)
            
            
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                self.dismiss(animated: true)
                print("OK button tapped")
            }
            
            
            alertController.addAction(okAction)
            
            
            present(alertController, animated: true, completion: nil)
        }else if txtDistrict.text == "" {
            let alertController = UIAlertController(title: "Error", message: "please select district", preferredStyle: .alert)
            
            
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                self.dismiss(animated: true)
                print("OK button tapped")
            }
            
            
            alertController.addAction(okAction)
            
            
            present(alertController, animated: true, completion: nil)
        }else if txtContituency.text == "" {
            let alertController = UIAlertController(title: "Error", message: "please select contituency", preferredStyle: .alert)

                
                let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    self.dismiss(animated: true)
                    print("OK button tapped")
                }

                
                alertController.addAction(okAction)

                
                present(alertController, animated: true, completion: nil)
        }else if currentArea == "" {
            let alertController = UIAlertController(title: "Error", message: "please select any one area", preferredStyle: .alert)

                
                let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    self.dismiss(animated: true)
                    print("OK button tapped")
                }

                
                alertController.addAction(okAction)

                
                present(alertController, animated: true, completion: nil)
        }
        else if txtBlock.text == "" && currentArea == "rural" {
            let alertController = UIAlertController(title: "Error", message: "please select block", preferredStyle: .alert)

                
                let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    self.dismiss(animated: true)
                    print("OK button tapped")
                }

                
                alertController.addAction(okAction)

                
                present(alertController, animated: true, completion: nil)
        }else if txtULB.text == ""  && currentArea == "urbann" {
            let alertController = UIAlertController(title: "Error", message: "Tplease seelct ULB", preferredStyle: .alert)

                
                let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    self.dismiss(animated: true)
                    print("OK button tapped")
                }

                
                alertController.addAction(okAction)

                
                present(alertController, animated: true, completion: nil)
        }
        else{
            userinputDetails.district_name = txtDistrict.text
            userinputDetails.state_name = txtState.text
            userinputDetails.ac_name = txtContituency.text
            if  let user = getRetrievedUser() {
                registerUser(user, userinputDetails) { errorMsg in
                    if errorMsg == nil {
                        DispatchQueue.main.async {
                            self.view.addSubview(self.backgroundView)
                            self.backgroundView.frame = self.view.frame
                            
                            let alertView = UINib(nibName: "SuccessView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SuccessView
                            alertView.layer.cornerRadius = 32
                            alertView.translatesAutoresizingMaskIntoConstraints = false
                            alertView.delegate = self
                            self.view.addSubview(alertView)
                            NSLayoutConstraint.activate([
                                alertView.heightAnchor.constraint(equalToConstant: 311),
                                alertView.widthAnchor.constraint(equalToConstant: 328),
                                alertView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 1),
                                alertView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 1)
                            ])
                        }
                    }
                }
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension LocationVC : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtState {
            currentPicker = "state"
        }else if textField == txtDistrict {
            currentPicker = "district"
        }else if textField == txtContituency {
            currentPicker = "ac"
        }else if textField == txtBlock || textField == txtULB {
            currentPicker = "block"
        }
        textField.inputView = pickerView
        // self.pickerView.reloadAllComponents()
        return true
    }
}

extension LocationVC : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if currentPicker == "state" {
            return stateList?.count ?? 0
        }else if currentPicker == "district" {
            return districtList?.count ?? 0
        }else if currentPicker == "ac"{
            return acList?.count ?? 0
        }else if currentPicker == "block" {
            return blockList?.count ?? 0
        }else{
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if currentPicker == "state" {
            return stateList?[row].state_name_en
        }else if currentPicker == "district" {
            return districtList?[row].district_name
        }else if currentPicker == "ac"{
            return acList?[row].ac
        }else if currentPicker == "block" {
            return blockList?[row].block_name
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if currentPicker == "state" {
            state_id = stateList?[row].state_id ?? ""
            DispatchQueue.main.async {
                self.txtState.text = self.stateList?[row].state_name_en
                self.getDistrictList(for:self.state_id ) { districtList in
                    DispatchQueue.main.async {
                        self.districtList = districtList
                        self.pickerView.reloadAllComponents()
                        self.pickerView.selectedRow(inComponent: 0)
                        self.district_id = districtList[0].district_id
                        self.txtDistrict.text = districtList[0].district_name
                    }
                }
            }
        }else if currentPicker == "district" {
            district_id = districtList?[row].district_id ?? ""
            DispatchQueue.main.async {
                self.txtDistrict.text = self.districtList?[row].district_name ?? ""
                self.getACList(for:self.district_id ) { acList in
                    DispatchQueue.main.async {
                        self.acList = acList
                        self.pickerView.reloadAllComponents()
                        self.pickerView.selectedRow(inComponent: 0)
                        self.txtContituency.text = acList[0].ac
                    }
                }
            }
        }else if currentPicker == "ac" {
            self.txtContituency.text = acList?[row].ac
            getBlocks(district_id: district_id)
        }else if currentPicker == "block" {
            if currentArea == "rural" {
                self.txtBlock.text = blockList?[row].block_name
            }else{
                self.txtULB.text = blockList?[row].block_name
            }
            if txtContituency.text != "" && txtState.text != "" && txtDistrict.text != "" {
               // btnContinue.isEnabled = true
            }
        }
    }
}
extension LocationVC : SuccessViewDelegate {
    func startJourneyTapped() {
        constViewComplete.constant = viewComplete.frame.width
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let slidingVC = storyboard.instantiateViewController(withIdentifier: "slidingVC") as! SlidingViewController
        let initialNavigationController = UINavigationController(rootViewController: slidingVC)
        self.view.window?.rootViewController = initialNavigationController
    }
}
