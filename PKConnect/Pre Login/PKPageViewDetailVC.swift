//
//  PKPageViewDetailVC.swift
//  Team Jagananna
//
//  Created by MAC on 05/10/23.
//

import UIKit
import WebKit


class PKPageViewDetailVC: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var webView: WKWebView!
    var campaignDetail: Campaign?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTitle.text = campaignDetail?.title
        if  let link = URL(string: campaignDetail?.url ?? "")
       {
            let request = URLRequest(url: link)
            webView.load(request)
        }
        
    }
    

    @IBAction func onClickBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
