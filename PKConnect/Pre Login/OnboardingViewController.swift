//
//  OnboardingViewController.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/21/22.
//

import UIKit

class OnboardingViewController: BaseViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func onClickArrow(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginOPTVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
