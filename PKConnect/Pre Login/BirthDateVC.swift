//
//  BirthDateVC.swift
//  PKConnect
//
//  Created by admin on 26/10/23.
//

import UIKit
import IQKeyboardManagerSwift

class BirthDateVC: BaseViewController {
    
    @IBOutlet weak var btContinue: RoundedButton!{
        didSet{
           // btContinue.isEnabled = false
        }
    }
    @IBOutlet weak var txtDate: UITextField!{
        didSet{
            txtDate.placeholder = "Day".localize()
        }
    }
    @IBOutlet weak var txtMonth: UITextField!{
        didSet{
            txtMonth.placeholder = "Month".localize()
        }
    }
    @IBOutlet weak var txtYear: UITextField!{
        didSet{
            txtYear.placeholder = "Year".localize()
        }
    }
    
    var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
                txtDate.delegate = self
                txtMonth.delegate = self
                txtYear.delegate = self
               
                datePicker = UIDatePicker()
                datePicker.maximumDate = Date()
                datePicker.datePickerMode = .date
                datePicker.preferredDatePickerStyle = .wheels
                datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)

                txtDate.inputView = datePicker
                txtMonth.inputView = datePicker
                txtYear.inputView = datePicker
        
    }
    
    @IBAction func oClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickContinnue(_ sender: Any) {
        if txtDate.text == "" {
            let alertController = UIAlertController(title: "Error".localize(), message: "please select birth date".localize(), preferredStyle: .alert)

                
            let okAction = UIAlertAction(title: "OK".localize(), style: .default) { (action) in
                    self.dismiss(animated: true)
                    print("OK button tapped")
                }

                
                alertController.addAction(okAction)

                
                present(alertController, animated: true, completion: nil)
        }else{
            userinputDetails.dob = "\(txtYear.text ?? "")-\(txtMonth.text ?? "")-\(txtDate.text ?? "")"
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "GenderVC")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM"
        var selectedDate = dateFormatter.string(from: sender.date)
        self.txtMonth.text = selectedDate
        DispatchQueue.main.async {
            dateFormatter.dateFormat = "dd"
            selectedDate = dateFormatter.string(from: sender.date)
            self.txtDate.text = selectedDate
        }
        DispatchQueue.main.async {
            dateFormatter.dateFormat = "yyyy"
            selectedDate = dateFormatter.string(from: sender.date)
            self.txtYear.text = selectedDate
        }
        //self.btContinue.isEnabled = true
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension BirthDateVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
          
               textField.inputView = datePicker
           
           return true
       }
}
