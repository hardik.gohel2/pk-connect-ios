//
//  RegistrationCell.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 10/30/22.
//

import UIKit

class RegistrationCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var arrow: UIImageView!
    @IBOutlet weak var textField: CustomTextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
