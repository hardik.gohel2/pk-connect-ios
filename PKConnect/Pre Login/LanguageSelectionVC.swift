//
//  LanguageSelectionVC.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 10/30/22.
//

import UIKit

class LanguageSelectionVC: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    private var tableView: UITableView!
    private let primaryTextArray = ["हिन्दी", "English"],
                secondaryTextArray = ["Hindi", "English"]
    var nextButton: RoundedButton?
    var headerLabel: UILabel!
    private var selectedLang = "hi",
                selectedCellIndex = 0
    var isLangChange = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        view.backgroundColor = .white
        if !isLangChange {
            addDashedPageControl(with: 0)
        }
        if let lang = UserDefaults.standard.object(forKey: "selectedLang") as? String {
            selectedLang = lang
            selectedCellIndex = (lang == "en" ? 1 : 0)
        }
        createTableView()
        
        nextButton = getButton()
        nextButton?.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func createTableView() {
        tableView = UITableView()
        self.view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableFooterView = UIView()

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 72),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100)
        ])
        
        tableView.register(UINib(nibName: "LanguageSelectionCell", bundle: nil), forCellReuseIdentifier: "languageSelectionCell")
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.allowsMultipleSelection = false
        tableView.isScrollEnabled = false
    }
    
    @objc func nextButtonTapped() {
        UserDefaults.standard.set(selectedLang, forKey: "selectedLang")
        
        if isLangChange {
            self.dismiss(animated: true) {
                let slidingVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "slidingVC") as! SlidingViewController
                let navigationVC = UINavigationController(rootViewController: slidingVC)
                guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
                      let delegate = windowScene.delegate as? SceneDelegate else { return }
                delegate.window?.rootViewController = navigationVC
            }
        } else {
            if let isOnboardingShown = UserDefaults.standard.object(forKey: "isOnboardingShown") as? Bool, isOnboardingShown == true {
                let welcomeView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "welcomeVC")
                self.navigationController?.pushViewController(welcomeView, animated: false)
            } else {
                //Show Onboarding Screens
                let loginotpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginOPTVC")
                self.navigationController?.pushViewController(loginotpVC, animated: false)
            }
        }
    }
    
    
    func getButton() -> RoundedButton {
        let button = RoundedButton()
        self.view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            button.topAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -100),
            button.heightAnchor.constraint(equalToConstant: 40),
            button.widthAnchor.constraint(greaterThanOrEqualToConstant: 200)
        ])
        button.cornerRadius = 20
        button.backgroundColor = .pkConnectYellow
        button.setAttributedTitle(NSAttributedString(string: (selectedCellIndex == 0 ? "अगला" : "NEXT"), attributes: [.font: UIFont(name: "NunitoSans-SemiBold", size: 16)!]), for: .normal)
        
        return button
    }
    
    //MARK: TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        primaryTextArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "languageSelectionCell", for: indexPath) as! LanguageSelectionCell
        
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 4
        
        let attributedTitle = NSAttributedString(string: primaryTextArray[indexPath.section], attributes: [.font: UIFont(name: "NunitoSans-Regular", size: 16)!])
        cell.titleLabel.attributedText = attributedTitle
        
        let attributedSubtitle = NSAttributedString(string: secondaryTextArray[indexPath.section], attributes: [.font: UIFont(name: "NunitoSans-Regular", size: 14)!, .foregroundColor: UIColor.pkConnectYellow])
        cell.subtitleLabel.attributedText = attributedSubtitle
        
        let bgView = UIView()
        bgView.backgroundColor = .pkConnectYellow
        
        if indexPath.section == selectedCellIndex {
            cell.backgroundView = bgView
            cell.subtitleLabel.textColor = .black
//            cell.langCheckButton.imageView?.image = UIImage(named: "checkmark.circle.black")
            cell.langCheckButton.setImage(UIImage(named: "checkmark.circle.black"), for: .normal)

            cell.layer.borderColor = UIColor.pkConnectYellow.cgColor

        } else {
            cell.backgroundView = nil
            cell.subtitleLabel.textColor = .pkConnectYellow
            cell.langCheckButton.setImage(UIImage(systemName: "circle"), for: .normal)

            //            cell.langCheckButton.imageView?.image = UIImage(systemName: "circle")
            cell.layer.borderColor = UIColor.black.cgColor
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        selectedCellIndex = indexPath.section
//        selectedLang = (selectedCellIndex == 0 ? "hi" : "en")
//        self.nextButton?.localizableText = (selectedCellIndex == 0 ? "अगला" : "NEXT")
//        tableView.reloadData()
        
        selectedCellIndex = indexPath.section
                selectedLang = (selectedCellIndex == 0 ? "hi" : "en")

                let txt = (selectedLang == "hi" ? "अगला" : "NEXT")
                let attributedTitle = NSAttributedString(string: txt, attributes: [.font: UIFont(name: "NunitoSans-SemiBold", size: 16)!])
                nextButton?.setAttributedTitle(attributedTitle, for: .normal)
                tableView.reloadData()


    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let containerView = UIView()
            headerLabel = UILabel()
            let attributedText = NSAttributedString(string: (selectedCellIndex == 0 ? "भाषा चुनें" : "Choose Language"), attributes: [.font: UIFont(name: "NunitoSans-Bold", size: 24)!])
            headerLabel.attributedText = attributedText
            headerLabel.textAlignment = .center
            
            containerView.addSubview(headerLabel)
            headerLabel.translatesAutoresizingMaskIntoConstraints = false
            headerLabel.numberOfLines = 0
            NSLayoutConstraint.activate([
                headerLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
                headerLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
                headerLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 20),
                headerLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -100)
            ])
            
            return containerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return UITableView.automaticDimension
        }
        return 0
    }
    
}
