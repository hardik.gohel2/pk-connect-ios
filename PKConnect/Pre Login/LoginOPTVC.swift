//
//  LoginOPTVC.swift
//  PKConnect
//
//  Created by admin on 26/10/23.
//

import UIKit
import OtplessSDK

class LoginOPTVC: BaseViewController {

    @IBOutlet private var btnLoginMobile: UIButton!
    @IBOutlet private var btnLoginWhatsApp: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        Otpless.sharedInstance.delegate = self
        Otpless.sharedInstance.eventDelegate = self
    }
    

    
    
    @IBAction private func onClickMobile(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "welcomeVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction private func onClickWhatsApp  (_ sender: Any) {
        var initialParams = [String:Any]()
        initialParams["cid"] = "idsep1h1"
        var params =  [String:Any]()
        params["method"] = "get"
        params["params"] = initialParams
        Otpless.sharedInstance.startwithParams(vc: self, params: params)
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginOPTVC : onResponseDelegate, onEventCallback {
    func onResponse(response: OtplessSDK.OtplessResponse?) {
        if (response?.errorString != nil) {
            print(response?.errorString ?? "no value in erro")
        } else {
            if (response != nil && response?.responseData != nil
                && response?.responseData?["data"] != nil){
                if let data = response?.responseData?["data"] as? [String: Any] {
                    let token = data["token"]
                    print(token ?? "no token")
                    whatsappLogin(token: "\(token ?? "")") { response in
                        DispatchQueue.main.async {
                            self.verifyOTP(response.phone_number)
                        }
                       
                    }
                }
            }
            
        }
        
    }
    
    func verifyOTP(_ mobile_no : String) {
        
        verifyOTPWhatsapp(mobile_no) { verifiedUser in
            if verifiedUser != nil {
                DispatchQueue.main.async {
                    self.hideLoadingIndicator()
                    UserDefaults.standard.set("1", forKey: "UserVerification")
                    self.goToNextView(mobile_no, verifiedUser!)
                }
            } else {
                DispatchQueue.main.async { [self] in
                    self.hideLoadingIndicator()
                    
                    
                }
            }
        }
    }
    
    private func goToNextView(_ mobile_no : String ,_ verifiedUser: VerifiedUser?) {
        var signedInUser = verifiedUser
        signedInUser?.contactNumber = mobile_no
        UserDefaults.standard.removeObject(forKey: "User")
        if verifiedUser?.is_registered == "1" {
            UserDefaults.standard.set(verifiedUser?.fb_username, forKey: "fbname")
            if let userData = try? JSONEncoder().encode(signedInUser)  {
                UserDefaults.standard.set(userData, forKey: "User")
            }
            let slidingView = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "slidingVC") as! SlidingViewController
            self.navigationController?.pushViewController(slidingView, animated: false)
        } else {
            UserDefaults.standard.set(verifiedUser?.fb_username, forKey: "fbname")
            if let userData = try? JSONEncoder().encode(signedInUser)  {
                UserDefaults.standard.set(userData, forKey: "User")
            }
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NameVC") as! NameVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    
    func onEvent(eventCallback: OtplessSDK.OtplessEventResponse?) {
        guard let eventCodeInstance = eventCallback?.eventCode else {
            print("Event callback or event code is missing.")
            return
        }

        if let responseString = eventCallback?.responseString {
            switch eventCodeInstance {
            case .networkFailure:
                print("networkFailure - EventCallback: \(responseString)")
                // Handle network failure case using responseString

            case .userDismissed:
                print("userDismissed - EventCallback: \(responseString)")
                // Handle user dismissed case using responseString

            // You can add more cases if needed

            @unknown default:
                print("Unknown case.")
                // Handle any unknown cases that might occur
            }
        } else {
            print("No response string provided.")
        }
    }
    
    
}
