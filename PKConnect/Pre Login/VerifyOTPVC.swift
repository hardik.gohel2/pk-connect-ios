//
//  VerifyOTPVC.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 10/30/22.
//

import UIKit

class VerifyOTPVC: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var lblNumber: CustomLabel!{
        didSet {
            lblNumber.text = "Change Number".localize()
        }
    }
    @IBOutlet private var contactNumber: UIButton!
    @IBOutlet private var textFieldArray: [UITextField]!
    @IBOutlet private var viewArray: [UIView]!
    @IBOutlet private var verifyButton: RoundedButton!
    
    @IBOutlet weak var resendOTPButton: UIButton!{
        didSet {
            resendOTPButton.titleLabel?.font = UIFont(name: "OpenSans-Semibold", size: 14.0)
        }
    }
    @IBOutlet private var otpTimerLabel: UILabel!
//    @IBOutlet private var otpFieldBottomConstraint: NSLayoutConstraint!
    var UpdateAll = false
    private var timer: Timer?,
                counter = 60,
                otpEntered = ""
    
    var mobileNo = "",
    state = "",
    District = "",
    Aseembly = ""
    var currentOTP = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
     //   addDashedPageControl(with: 1)
        sendOTP(for: mobileNo) { responseObj in
            if responseObj!.status == "200" {
                DispatchQueue.main.async {
                    self.currentOTP = responseObj!.data.otp!
                    self.initialSetUp()
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func initialSetUp() {
        for i in 0..<textFieldArray.count {
            let textField = textFieldArray[i]
            
            textField.delegate = self
            textField.tag = i
            textField.tintColor = .clear
            textField.text = " "
        }
        for i in 0..<viewArray.count {
            viewArray[i].tag = i
        }
        
        textFieldArray[0].becomeFirstResponder()
        
//        let attributedString = NSAttributedString(string: " \(mobileNo)", attributes: [.font: UIFont(name: "OpenSans-Regular", size: 14) ?? UIFont.systemFont(ofSize: 14.0), .foregroundColor: UIColor.pkConnectYellow])
//        contactNumber.setAttributedTitle(attributedString, for: .normal)
        
        contactNumber.setTitle("\(mobileNo)", for: .normal)
        contactNumber.setTitleColor(appYelloColor, for: .normal)
        
        //Timer
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc private func timerAction() {
        if counter != 0 {
            counter -= 1
            if counter <= 9 {
                otpTimerLabel.text = "\("Your OTP will expire in".localize()) 00:0\(counter)"
            } else {
                otpTimerLabel.text = "\("Your OTP will expire in".localize()) 00:\(counter)"
            }
        } else {
            currentOTP = ""
            otpTimerLabel.text = "\("Your OTP has Expired".localize())"
            resendOTPButton.titleLabel?.textColor = .`pkConnectYellow`
            timer?.invalidate()
        }
    }

    @IBAction func oClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func editNumberTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let welcomeView = storyBoard.instantiateViewController(withIdentifier: "welcomeVC")
        self.navigationController?.pushViewController(welcomeView, animated: true)
        
       
    }
    
    @IBAction private func resendOTP(_ sender: Any) {
        if counter == 0 {
//            otpFieldBottomConstraint.constant = 15
            resendOTPButton.titleLabel?.textColor = .label
            verifyButton.backgroundColor = .pkConnectYellow
            counter = 59
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
            for textField in textFieldArray {
                textField.text = " "
            }
            sendOTP(for: mobileNo) { responseObj in
                if responseObj!.status == "200" {
                    DispatchQueue.main.async {
                        self.currentOTP = responseObj!.data.otp!
                    }
                }
            }
            
            textFieldArray[0].becomeFirstResponder()
        }
    }

    
    @IBAction private func verifyButtonTapped(_ sender: Any) {
        otpEntered = ""
        for textField in textFieldArray {
            if textField.text != " " {
                otpEntered.append(textField.text!)
            }
        }
        if otpEntered.count == 4 {
            showLoadingIndicator(in: view)
            if otpEntered == currentOTP{
                if UpdateAll{
                    verifyOTPProfile(otpEntered, mobileNo, state, District, Aseembly) { verifiedUser in
                        if verifiedUser != nil {
                            DispatchQueue.main.async {
                                self.hideLoadingIndicator()
                                UserDefaults.standard.set("1", forKey: "UserVerification")
                                self.goToNextView(verifiedUser!)
                            }
                        } else {
                            DispatchQueue.main.async { [self] in
                                self.hideLoadingIndicator()
                                verifyButton.layer.borderColor = UIColor.label.cgColor
                                let alertController = UIAlertController(title: "Error".localize(), message: "Please Invalid OTP".localize(), preferredStyle: .alert)

                                    
                                let okAction = UIAlertAction(title: "OK".localize(), style: .default) { (action) in
                                        self.dismiss(animated: true)
                                        print("OK button tapped")
                                    }

                                    
                                    alertController.addAction(okAction)

                                    
                                    present(alertController, animated: true, completion: nil)

                                for textField in textFieldArray {
                                    textField.text = " "
                                }
                                textFieldArray[0].becomeFirstResponder()
                            }
                        }
                    }
                }else{
                    verifyOTP(otpEntered, mobileNo) { verifiedUser in
                        if verifiedUser != nil {
                            DispatchQueue.main.async {
                                self.hideLoadingIndicator()
                                UserDefaults.standard.set("1", forKey: "UserVerification")
                                self.goToNextView(verifiedUser!)
                            }
                        } else {
                            DispatchQueue.main.async { [self] in
                                self.hideLoadingIndicator()
                                verifyButton.layer.borderColor = UIColor.label.cgColor
                                let alertController = UIAlertController(title: "Error".localize(), message: "Invalid OTP".localize(), preferredStyle: .alert)

                                    
                                let okAction = UIAlertAction(title: "OK".localize(), style: .default) { (action) in
                                        self.dismiss(animated: true)
                                        print("OK button tapped")
                                    }

                                    
                                    alertController.addAction(okAction)

                                    
                                    present(alertController, animated: true, completion: nil)

                                for textField in textFieldArray {
                                    textField.text = " "
                                }
                                textFieldArray[0].becomeFirstResponder()
                            }
                        }
                    }
                }
            }else{
                DispatchQueue.main.async { [self] in
                    self.hideLoadingIndicator()
                    
                    verifyButton.layer.borderColor = UIColor.label.cgColor
                    let alertController = UIAlertController(title: "Error", message: "Invalid OTP".localize(), preferredStyle: .alert)

                        
                    let okAction = UIAlertAction(title: "OK".localize(), style: .default) { (action) in
                            self.dismiss(animated: true)
                            print("OK button tapped")
                        }

                        
                        alertController.addAction(okAction)

                        
                        present(alertController, animated: true, completion: nil)

                    for textField in textFieldArray {
                        textField.text = " "
                    }
                    textFieldArray[0].becomeFirstResponder()
                }
            }
        }else{
            let alertController = UIAlertController(title: "Error".localize(), message: "Please Eter 4 digit OTP".localize(), preferredStyle: .alert)

                
            let okAction = UIAlertAction(title: "OK".localize(), style: .default) { (action) in
                    self.dismiss(animated: true)
                    print("OK button tapped")
                }

                
                alertController.addAction(okAction)

                
                present(alertController, animated: true, completion: nil)
        }
    }
    
    private func goToNextView(_ verifiedUser: VerifiedUser) {
        var signedInUser = verifiedUser
        signedInUser.contactNumber = mobileNo
        UserDefaults.standard.removeObject(forKey: "User")
        if verifiedUser.is_registered == "1" {
            UserDefaults.standard.set(verifiedUser.fb_username, forKey: "fbname")
            if let userData = try? JSONEncoder().encode(signedInUser)  {
                UserDefaults.standard.set(userData, forKey: "User")
            }
            let slidingView = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "slidingVC") as! SlidingViewController
            self.navigationController?.pushViewController(slidingView, animated: false)
        } else {
            UserDefaults.standard.set(verifiedUser.fb_username, forKey: "fbname")
            if let userData = try? JSONEncoder().encode(signedInUser)  {
                UserDefaults.standard.set(userData, forKey: "User")
            }
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NameVC") as! NameVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    //MARK: UITextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let otp = textField.text else { return false }
//        otpFieldBottomConstraint.constant = 15
        if string == "" {
            
            for i in stride(from: 3, through: 0, by: -1) {
                if textFieldArray[i].text != " " {
                    textFieldArray[i].text = " "
                    viewArray[i].layer.backgroundColor = UIColor.black.cgColor
                    textFieldArray[i].becomeFirstResponder()
                    return false
                } else {
                    viewArray[i].layer.backgroundColor = (i == 0) ? UIColor.black.cgColor : UIColor.systemGray4.cgColor
                }
            }
            return false
        }
        
        if otp.count == 1 {
            textField.text = string
            
            let nextIndex = textField.tag + 1
            if nextIndex >= textFieldArray.count {
                viewArray[textField.tag].layer.backgroundColor = UIColor.systemGray4.cgColor
                textField.resignFirstResponder()
                verifyButton.backgroundColor = .pkConnectYellow
                verifyButton.setTitleColor(.black, for: .normal)
                verifyButton.layer.borderColor = UIColor.pkConnectYellow.cgColor
            } else {
                viewArray[nextIndex].layer.backgroundColor = UIColor.black.cgColor
                viewArray[textField.tag].layer.backgroundColor = UIColor.systemGray4.cgColor
                textFieldArray[nextIndex].becomeFirstResponder()
            }
        }
        return false
    }
    
}
