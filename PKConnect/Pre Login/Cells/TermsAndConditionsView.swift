//
//  TermsAndConditionsView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//
import UIKit

class TermsAndConditionsView: UIView, UITextViewDelegate {
    
    @IBOutlet weak var checkBox: UIButton!
    @IBOutlet weak var registerButton: RoundedButton!
    @IBOutlet weak var textView: UITextView!
    
    weak var delegate: TermsAndConditionsViewDelegate?
    
    var isChecked: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let attributedString =
        NSMutableAttributedString(string: "I accept the Terms and Privacy Policy".localize(), attributes: [.font : UIFont(name: "NunitoSans-Regular", size: 14)!])
        attributedString.addAttribute(.link, value: "", range: NSRange(location: 13, length: 5))
        attributedString.addAttribute(.link, value: "", range: NSRange(location: 23, length: 14))
        
        let linkAttributes: [NSAttributedString.Key : Any] = [
            .foregroundColor: UIColor.pkConnectYellow,
            .underlineColor: UIColor.pkConnectYellow,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        textView.attributedText = attributedString
        textView.linkTextAttributes = linkAttributes
        textView.delegate = self
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {

        let range1 = NSRange(location: 13, length: 5)
        let range2 = NSRange(location: 23, length: 14)
        if characterRange == range1 {
            delegate?.navigateToWeb(path: "term")
        } else if characterRange == range2 {
            delegate?.navigateToWeb(path: "privacy")
        }
        
        return false
    }
    
    @IBAction func checkBoxTapped(_ sender: Any) {
        isChecked.toggle()
        let image = isChecked ? UIImage(systemName: "checkmark.square.fill") : UIImage(systemName: "square")
        checkBox.setImage(image, for: .normal)
        
        registerButton.backgroundColor = isChecked ? .pkConnectYellow : .systemGray6
        registerButton.layer.borderColor = isChecked ? UIColor.pkConnectYellow.cgColor : UIColor.label.cgColor
        
        if let delegate = delegate {
            delegate.checkBoxTapped()
        }
    }
    
    @IBAction func registerTapped(_ sender: Any) {
        delegate?.registerButtonTapped(termsAccepted: isChecked)
    }
    
}

protocol TermsAndConditionsViewDelegate: AnyObject {
    func checkBoxTapped()
    func registerButtonTapped(termsAccepted: Bool)
    func navigateToWeb(path: String)
}
