//
//  SuccessView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit
import SwiftGifOrigin

class SuccessView: UIView {

    @IBOutlet weak var startJourneyButton: UIButton!
  
    @IBOutlet weak var imgSuccess: UIImageView!
    weak var delegate: SuccessViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgSuccess.image = UIImage.gif(name: "green_gif_success")

        startJourneyButton.layer.cornerRadius = 25
        startJourneyButton.layer.borderWidth = 1
        startJourneyButton.layer.borderColor = UIColor.pkConnectYellow.cgColor
        startJourneyButton.backgroundColor = .pkConnectYellow
    }
    
    @IBAction func startButtonSelected(_ sender: Any) {
        if let delegate = delegate {
            delegate.startJourneyTapped()
        }
    }
    
}

protocol SuccessViewDelegate: AnyObject {
    func startJourneyTapped()
}
