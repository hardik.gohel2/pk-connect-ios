//
//  NameVC.swift
//  PKConnect
//
//  Created by admin on 26/10/23.
//

import UIKit

var userinputDetails = InputUserDetails()
class NameVC: BaseViewController {

    
    @IBOutlet weak var btnConntinue: RoundedButton!{
        didSet{
           // btnConntinue.isEnabled = false
        }
    }
    @IBOutlet weak var txtName: CustomTextField!{
        didSet{
            txtName.delegate = self
            txtName.placeholder = "Enter Your Name".localize()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func oClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickContinue(_ sender: UIButton) {
        if txtName.text == "" {
            let alertController = UIAlertController(title: "Error".localize(), message: "please enter your name".localize(), preferredStyle: .alert)

                
            let okAction = UIAlertAction(title: "OK".localize(), style: .default) { (action) in
                    self.dismiss(animated: true)
                    print("OK button tapped")
                }

                
                alertController.addAction(okAction)

                
                present(alertController, animated: true, completion: nil)
        }else{
            userinputDetails.name = txtName.text
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "BirthDateVC")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NameVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.letters
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}
