//
//  WelcomeVC.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//  swift

import UIKit

class WelcomeVC: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet private var proceedButton: RoundedButton!
    @IBOutlet private var mobileTextField: CustomTextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        view.backgroundColor = .white
        
        // addDashedPageControl(with: 1)
        self.initialSetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func initialSetUp() {
        //TextField
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        label.attributedText = NSAttributedString(string: "  +91 |  ", attributes: [.font: UIFont(name: "OpenSans-Regular", size: 14) ?? UIFont.systemFont(ofSize: 14.0)])
        mobileTextField.leftView = label
        mobileTextField.leftViewMode = .always
        
        mobileTextField.placeholder = "Enter a valid phone number".localize()
        mobileTextField.delegate = self
        
        
    }
    
    @IBAction func oClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction private func proceedButtonTapped(_ sender: Any) {
        
        if mobileTextField.text?.count != 10 {
            
            let alertController = UIAlertController(title: "Error", message: "Please enter a valid 10-digit mobile number", preferredStyle: .alert)
            
            
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                self.dismiss(animated: true)
                print("OK button tapped")
            }
            
            
            alertController.addAction(okAction)
            
            
            present(alertController, animated: true, completion: nil)
            
        }
        else if let firstDigit = mobileTextField.text?.first, let digitValue = Int(String(firstDigit)), digitValue >= 0 && digitValue <= 5 {
            let alertController = UIAlertController(title: "Error", message: "Please enter a number with first digit between 6 and 9", preferredStyle: .alert)
            
            
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                self.dismiss(animated: true)
                print("OK button tapped")
            }
            
            
            alertController.addAction(okAction)
            
            
            present(alertController, animated: true, completion: nil)
            
        }else {
            
            // Mobile number is valid and user entered a number between 6 and 9, proceed with verification
            showLoadingIndicator(in: view)
            mobileVerify(for: mobileTextField.text!) { VerifyUser in
                
                if VerifyUser != nil {
                    DispatchQueue.main.async {
                        self.hideLoadingIndicator()
                        if VerifyUser?.is_registered == "1"{
                            UserDefaults.standard.set("3", forKey: "UserVerification")
                        }else{
                            UserDefaults.standard.set(VerifyUser?.is_registered, forKey: "UserVerification")
                        }
                      
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let VerifyOTPVC = storyboard.instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
                            
                            VerifyOTPVC.mobileNo = self.mobileTextField.text ?? ""
                            VerifyOTPVC.UpdateAll = false
                            self.navigationController?.pushViewController(VerifyOTPVC, animated: false)
//                        }else if VerifyUser?.is_registered == "1"{
//                            let storyboard = UIStoryboard(name: "Home", bundle: nil)
//                            let registeredUser = VerifiedUser(user_id: VerifyUser?.user_id, is_registered:  "1", user_name: VerifyUser?.user_name, accessToken: VerifyUser?.accessToken ?? "")
//                            //                        registeredUser.user_name = inputDetails.name
//                            //                        registeredUser.is_registered = "1"
//                            if let userData = try? JSONEncoder().encode(registeredUser)  {
//                                UserDefaults.standard.set(userData, forKey: "User")
//                            }
//                            let slidingVC = storyboard.instantiateViewController(withIdentifier: "slidingVC") as! SlidingViewController
//                            let initialNavigationController = UINavigationController(rootViewController: slidingVC)
//                            self.view.window?.rootViewController = initialNavigationController
//                        }else if VerifyUser?.is_registered == "2"{
//                            let storyboard = UIStoryboard(name: "Home", bundle: nil)
//                            let registeredUser = VerifiedUser(user_id: VerifyUser?.user_id, is_registered:  "1", user_name: VerifyUser?.user_name, accessToken: VerifyUser?.accessToken ?? "")
//                            if let userData = try? JSONEncoder().encode(registeredUser)  {
//                                UserDefaults.standard.set(userData, forKey: "User")
//                            }
//                            let slidingVC = storyboard.instantiateViewController(withIdentifier: "slidingVC") as! SlidingViewController
//                            let initialNavigationController = UINavigationController(rootViewController: slidingVC)
//                            self.view.window?.rootViewController = initialNavigationController
//                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        self.hideLoadingIndicator()
                    }
                }
            }
        }
    }
    
    
    //MARK: UITextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.layer.borderColor = UIColor.pkConnectYellow.cgColor
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard var number = textField.text else { return false }
        
        if string == "" {
            number.removeLast(0)
            textField.text = number
            proceedButton.backgroundColor = .pkConnectYellow
            proceedButton.borderColor = .label
            return true
        }
        
        if number.count > 9 {
            return false
        }
        
        if number.count == 9 {
            proceedButton.backgroundColor = .pkConnectYellow
            proceedButton.borderColor = .pkConnectYellow
        }
        return true
    }
    
}
