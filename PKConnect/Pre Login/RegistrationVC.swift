//
//  RegistrationVC.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 10/30/22.
//

import UIKit

class RegistrationVC: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var verifiedUser: VerifiedUser?
    var userProfile: UserProfile?,
        refreshProfileAction: (()->())?
    private lazy var dropdownTable = UITableView()
    private let rowTitles = ["Name", "Email", "Gender", "Date of Birth", "State", "District", "Assembly Constituency"],
                placeholderArray = ["Please Enter Your Name", "Please Enter Your Email", "Please Select Your Gender", "Please Select Your DOB", "Please Select Your State", "Please Select Your District", "Please Select Your AC"],
                errorTitles = ["Please Enter Your Name", "Please Enter a Valid Email", "Please Select Your Gender", "Please Enter Valid DOB", "Please Select Your State", "Please Select Your District", "Please Select Your Assembly Constituency"],
                toolBar = UIToolbar()
    private var datePicker = UIDatePicker(),
                dropdownItems = ["Male", "Female", "Other"],
                stateList: [State]?,
                districtList: [District]?,
                acList: [AC]?,
                indexOfDropdown = 88,
                updatedProfile: UserProfile!,
                headerView: UITableViewHeaderFooterView?,
                errorValues = [true, true, true, true, true, true, true, true],
                inputDetails = InputUserDetails(),
                state_id = "0",
                district_id = "0",
                ac_id = "0"
    let containerView = {
        let shadowView = UIView()
        shadowView.layer.shadowColor = UIColor.darkGray.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        shadowView.layer.shadowOpacity = 1.0
        shadowView.layer.shadowRadius = 2
        return shadowView
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addDashedPageControl(with: 2)
        createDatePicker()
        
        let nib = UINib(nibName: "TermsAndConditionsView", bundle: nil)
        let tcView = nib.instantiate(withOwner: self, options: nil).first as! TermsAndConditionsView
        tcView.delegate = self
        view.addSubview(tcView)
        tcView.layer.shadowColor = UIColor.darkGray.cgColor
        tcView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        tcView.layer.shadowOpacity = 1.0
        tcView.layer.shadowRadius = 2
        tcView.frame = CGRect(x: 0, y: view.frame.size.height - 150, width: view.frame.size.width, height: 150)
        
        updatedProfile = UserProfile()
        setupHideKeyboardOnTap()
        if let userProfile = userProfile {
            updatedProfile = userProfile
            self.tableView.reloadData()
        }
        
        dropdownTable.register(UITableViewCell.self, forCellReuseIdentifier: "dropdownCell")
        dropdownTable.dataSource = self
        dropdownTable.delegate = self
        
        getStateList { stateList in
            DispatchQueue.main.async {
                self.stateList = stateList
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        if let refered_by = UserDefaults.standard.object(forKey: "refered_by") as? String {
            inputDetails.refered_by = refered_by
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            let insets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
//            tableView.contentInset = insets
//            tableView.scrollIndicatorInsets = insets
        }
    }
//
    @objc private func keyboardWillHide(_ notification: NSNotification) {
//        tableView.contentInset = .zero
//        tableView.scrollIndicatorInsets = .zero
    }
    
    //MARK: DOB
    private func createDatePicker() {
        toolBar.sizeToFit()
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()

        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneTapped))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelTapped))
        toolBar.setItems([doneButton,spaceButton,cancelButton], animated: false)
    }
    
    @objc private func doneTapped() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium

        let cell = self.tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! RegistrationCell
        cell.textField.text = dateFormatter.string(from: datePicker.date)
        if errorValues[3] == false {
            cell.errorLabel.isHidden = true
            errorValues[3] = true
            resetTableCell()
        }
        inputDetails.dob = cell.textField.text
        view.endEditing(true)
    }
    
    @objc private func cancelTapped() {
        view.endEditing(true)
    }
    
    //MARK:
    private func validateFields() -> Bool {
        //Name
        if userinputDetails.name == nil || userinputDetails.name?.trimmingCharacters(in: .whitespaces) == "" {
            errorValues[0] = false
        }
        //Email
        if let email = userinputDetails.email, email.trimmingCharacters(in: .whitespaces) != "" {
            if !isValidEmail(email: email) {
                errorValues[1] = false
            }
        }
        //Gender
        if userinputDetails.gender == nil {
            errorValues[2] = false
        }
        //DOB
        if userinputDetails.dob == nil {
            errorValues[3] = false
        }
        //State
        if userinputDetails.state_name == nil {
            errorValues[4] = false
        }
        //DOB
        if userinputDetails.district_name == nil {
            errorValues[5] = false
        }
        //DOB
        if userinputDetails.ac_name == nil {
            errorValues[6] = false
        }
        
        if errorValues.compactMap({ $0 }).contains(false) {
            tableView.reloadData()
            return false
        } else {
            return true
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == dropdownTable {
            if indexOfDropdown == 4 {
                return stateList?.count ?? 0
            } else if indexOfDropdown == 5 {
                return districtList?.count ?? 0
            } else if indexOfDropdown == 6 {
                return acList?.count ?? 0
            }
            return dropdownItems.count
        }
        return rowTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "registrationCell", for: indexPath) as! RegistrationCell

            let attributedString = NSMutableAttributedString(attributedString: NSAttributedString(string: " *", attributes: [.foregroundColor: UIColor.systemRed]))
            let combinedText = NSMutableAttributedString(attributedString: NSAttributedString(string: rowTitles[indexPath.row].localize()))
            if indexPath.row != 1 {
                combinedText.append(attributedString)
            }

            cell.label.attributedText = combinedText
            cell.textField.tag = indexPath.row
            cell.textField.placeholder = placeholderArray[indexPath.row].localize()
            cell.textField.delegate = self
            cell.errorLabel.text = errorTitles[indexPath.row] .localize()
            cell.errorLabel.isHidden = errorValues[indexPath.row]
            cell.textField.inputAccessoryView = nil
            cell.textField.inputView = nil
            
            switch indexPath.row {
            case 0:
                cell.textField.text = inputDetails.name
            case 1:
                cell.textField.text = inputDetails.email
                cell.textField.keyboardType = .emailAddress
            case 2:
                cell.textField.text = inputDetails.gender
            case 3:
                cell.textField.text = inputDetails.dob
                cell.textField.inputAccessoryView = toolBar
                cell.textField.inputView = datePicker
            case 4:
                cell.textField.text = inputDetails.state_name
            case 5:
                cell.textField.text = inputDetails.district_name
            case 6:
                cell.textField.text = inputDetails.ac_name
            default:
                cell.textField.text = ""
            }
            
            return cell

        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell", for: indexPath)
            var config = cell.defaultContentConfiguration()
            config.textProperties.font = UIFont(name: "NunitoSans-Regular", size: 14)!
            if indexOfDropdown == 2 {
                config.text = dropdownItems[indexPath.row].localize()
            } else if indexOfDropdown == 4 {
                config.text = stateList?[indexPath.row].state_name_en
            } else if indexOfDropdown == 5 {
                config.text = districtList?[indexPath.row].district_name
            } else if indexOfDropdown == 6 {
                config.text = acList?[indexPath.row].ac
            }
            
            cell.contentConfiguration = config

            return cell
        }
    }
    
    @objc func tappedOutsideDropdown() {
        backgroundView.removeFromSuperview()
        containerView.removeFromSuperview()
    }

    @objc func didSwipe(_ sender: UISwipeGestureRecognizer) {
        backgroundView.removeFromSuperview()
        containerView.removeFromSuperview()
    }
  
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            print("Main Table Scrolled")
            if self.view.subviews.contains(containerView) {
                containerView.removeFromSuperview()
            }
        } else if scrollView == self.dropdownTable {
            print("Dropdown Table")
        }
    }
    
    private func setUpDropDownTable(indexPath: IndexPath) {
//        self.view.addSubview(self.backgroundView)
//        self.backgroundView.frame = self.view.frame
//        self.backgroundView.backgroundColor = .clear
//        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedOutsideDropdown)))
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(didSwipe(_:)))
        swipeUp.direction = .up
        backgroundView.addGestureRecognizer(swipeUp)
        var tableHeight: CGFloat = 0, yPos: CGFloat = 0
        let cell = tableView.cellForRow(at: indexPath) as! RegistrationCell
        let currentTextFieldFrame = cell.textField.frame
        
        view.addSubview(containerView)

        containerView.addSubview(dropdownTable)
        if indexPath.row == 2 {
            tableHeight = CGFloat((dropdownItems.count) * 44)
        } else if indexPath.row == 4 {
            tableHeight = CGFloat((stateList?.count ?? 0) * 44)
        } else if indexPath.row == 5 {
            tableHeight = CGFloat((districtList?.count ?? 0) * 44)
        } else if indexPath.row == 6 {
            tableHeight = CGFloat((acList?.count ?? 0) * 44)
        }
        
        let h1 = abs(tableView.frame.origin.y + cell.frame.origin.y + 5 + currentTextFieldFrame.origin.y - self.tableView.contentOffset.y)
        let h2 = abs(view.frame.size.height - h1 - currentTextFieldFrame.height - view.safeAreaInsets.bottom)
        if h1 > h2 {
            tableHeight = min(CGFloat(Int(h1) - 50 - 10), tableHeight)
            yPos = (h1 > tableHeight) ? (h1 - tableHeight) : (view.safeAreaInsets.top)
        } else {
            tableHeight = min(CGFloat(Int(h2)), tableHeight)
            yPos = h1 + 50
        }
        containerView.frame = CGRect(x: 15, y: yPos, width: self.view.frame.size.width - 30, height: CGFloat(tableHeight))
        dropdownTable.frame = containerView.bounds
        if #available(iOS 15.0, *) {
            dropdownTable.sectionHeaderTopPadding = 0
        } else {
            // Fallback on earlier versions
        }
        dropdownTable.reloadData()

        view.bringSubviewToFront(dropdownTable)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (tableView == self.tableView ? 60 : 0)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == self.tableView {
            return getTableHeaderWith(title: "Registration".localize())
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var newVal = ""
        if indexOfDropdown == 2 { //Gender
            inputDetails.gender = dropdownItems[indexPath.row]
            newVal = inputDetails.gender!
        } else if indexOfDropdown == 4 { //State
            resetCellFor(index1: 5, index2: 6)
            district_id = "0"
            inputDetails.district_name = nil
            ac_id = "0"
            inputDetails.ac_name = nil
            state_id = stateList![indexPath.row].state_id
            inputDetails.state_name = stateList![indexPath.row].state_name_en
            newVal = inputDetails.state_name!
        } else if indexOfDropdown == 5 { //District
            resetCellFor(index1: nil, index2: 6)
            ac_id = "0"
            inputDetails.ac_name = nil
            district_id = districtList![indexPath.row].district_id
            inputDetails.district_name = districtList![indexPath.row].district_name
            newVal = inputDetails.district_name!
        } else if indexOfDropdown == 6 { //AC
            ac_id = acList![indexPath.row].id
            inputDetails.ac_name = acList![indexPath.row].ac
            newVal = inputDetails.ac_name!
        }
        containerView.removeFromSuperview()

        let cell = self.tableView.cellForRow(at: IndexPath(row: indexOfDropdown, section: 0)) as! RegistrationCell
        cell.textField.text = newVal.localize()
        errorValues[indexOfDropdown] = true
        cell.errorLabel.isHidden = errorValues[indexOfDropdown]
        resetTableCell()
    }
    
    private func resetCellFor(index1: Int?, index2: Int?) {
        if index1 != nil {
            if (tableView.indexPathsForVisibleRows?.contains(IndexPath(row: index1!, section: 0)) == true) {
                let cell = self.tableView.cellForRow(at: IndexPath(row: index1!, section: 0)) as! RegistrationCell
                cell.textField.text = ""
            }
        }
        if index2 != nil {
            if (tableView.indexPathsForVisibleRows?.contains(IndexPath(row: index2!, section: 0)) == true) {
                let cell = self.tableView.cellForRow(at: IndexPath(row: index2!, section: 0)) as! RegistrationCell
                cell.textField.text = ""
            }
        }
    }
}

extension RegistrationVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
      
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 0 {
            inputDetails.name = textField.text
        } else if textField.tag == 1 {
            inputDetails.email = textField.text
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 0 {
            if !string.isEmpty && string.rangeOfCharacter(from: .letters.union(.whitespaces)) == nil {
                return false
            }
        }
        if errorValues[textField.tag] == false {
            let cell = self.tableView.cellForRow(at: IndexPath(row: textField.tag, section: 0)) as! RegistrationCell
            cell.errorLabel.isHidden = true
            errorValues[textField.tag] = true
            resetTableCell()
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if ![0, 1, 3].contains(textField.tag) {
            view.endEditing(true)
        }
        if textField.tag == indexOfDropdown, view.subviews.contains(containerView) {
            containerView.removeFromSuperview()
            indexOfDropdown = 88
            return false
        }
        let row = textField.tag
        containerView.removeFromSuperview()
        indexOfDropdown = 88
        switch row {
        case 2: //Gender
            indexOfDropdown = textField.tag
            setUpDropDownTable(indexPath: IndexPath(row: row, section: 0))
            return false
        case 4: //State
            indexOfDropdown = textField.tag
            setUpDropDownTable(indexPath: IndexPath(row: row, section: 0))
            return false
        case 5: //District
            indexOfDropdown = textField.tag
            if state_id != "0" {
                getDistrictList(for: state_id) { districtList in
                    DispatchQueue.main.async {
                        self.districtList = districtList
                        self.setUpDropDownTable(indexPath: IndexPath(row: row, section: 0))
                    }
                }
            }
            return false
        case 6: //AC
            indexOfDropdown = textField.tag
            if district_id != "0" {
                getACList(for: district_id, completion: { acList in
                    DispatchQueue.main.async {
                        self.acList = acList
                        self.setUpDropDownTable(indexPath: IndexPath(row: row, section: 0))
                    }
                })
            }
            return false
        default:
            return true
        }
    }
    
    func setupHideKeyboardOnTap() {
        self.view.addGestureRecognizer(self.endEditingRecognizer())
        self.navigationController?.navigationBar.addGestureRecognizer(self.endEditingRecognizer())
    }
    
    //Dismiss keyboard if shown
    private func endEditingRecognizer() -> UIGestureRecognizer {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        return tap
    }
    
    private func resetTableCell() {
        UIView.setAnimationsEnabled(false)
        tableView.beginUpdates()
        tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func getTableHeaderWith(title: String) -> UITableViewHeaderFooterView {
        headerView = UITableViewHeaderFooterView()
        
        var config = UIListContentConfiguration.groupedHeader()
        config.attributedText = NSAttributedString(string: title, attributes: [.font: UIFont(name: "NunitoSans-SemiBold", size: 22)!, .foregroundColor: UIColor.label])
        config.textProperties.alignment = .center
        headerView?.contentConfiguration = config
        
        return headerView!
    }
}

extension RegistrationVC: TermsAndConditionsViewDelegate, SuccessViewDelegate {
    func checkBoxTapped() {
        //
    }

    func registerButtonTapped(termsAccepted: Bool) {
        backgroundView.removeFromSuperview()
        containerView.removeFromSuperview()
        let isMandatoryFieldsFilled = validateFields()
        if !termsAccepted {
            showDisappearingMessage("Please accept our Terms and Conditions")
            return
        }
        
        if isMandatoryFieldsFilled && termsAccepted, let user = verifiedUser {
//            registerUser(user, inputDetails) { errorMsg in
//                if errorMsg == nil {
//                    DispatchQueue.main.async {
//                        self.view.addSubview(self.backgroundView)
//                        self.backgroundView.frame = self.view.frame
//                        
//                        let alertView = UINib(nibName: "SuccessView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SuccessView
//                        alertView.layer.cornerRadius = 32
//                        alertView.translatesAutoresizingMaskIntoConstraints = false
//                        alertView.delegate = self
//                        self.view.addSubview(alertView)
//                        NSLayoutConstraint.activate([
//                            alertView.heightAnchor.constraint(equalToConstant: 311),
//                            alertView.widthAnchor.constraint(equalToConstant: 328),
//                            alertView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 1),
//                            alertView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 1)
//                        ])
//                    }
//                }
//            }
        }
    }
    
    private func showDisappearingMessage(_ msg: String) {
        let label = UILabel()
        label.textAlignment = .center
        label.attributedText = NSAttributedString(string: msg.localize(), attributes: [.font: UIFont(name: "NunitoSans-SemiBold", size: 16)!, .foregroundColor: UIColor.label])
        label.backgroundColor = .white
        label.layer.cornerRadius = 20
        label.layer.masksToBounds = true
        view.addSubview(label)
        label.frame = CGRect(x: 20, y: view.frame.maxY - 95, width: view.frame.size.width - 40, height: 40)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            label.removeFromSuperview()
        }
    }

    func navigateToWeb(path: String) {
        let webview = WebViewController(with: path, false)
        self.navigationController?.pushViewController(webview, animated: true)
    }
    
    func startJourneyTapped() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let slidingVC = storyboard.instantiateViewController(withIdentifier: "slidingVC") as! SlidingViewController
        let initialNavigationController = UINavigationController(rootViewController: slidingVC)
        self.view.window?.rootViewController = initialNavigationController
    }
}

