//
//  GederVC.swift
//  PKConnect
//
//  Created by admin on 26/10/23.
//

import UIKit

class GenderVC: BaseViewController {

    var selectedGen = ""
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func oClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func onClickGender(_ sender: UIButton) {
        sender.backgroundColor = appYelloColor
        switch sender.tag {
        case 1:
          selectedGen = "Male"
            userinputDetails.gender = "1"
        case 2:
          selectedGen = "Female"
            userinputDetails.gender = "2"
            
        default:
            selectedGen = "Others"
            userinputDetails.gender = "3"
            
        }
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "LocationVC")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
