


import UIKit
import SDWebImage

class PKPageViewController: BaseViewController {
    
    @IBOutlet weak var scrollAuto: UICollectionView!
    @IBOutlet weak var pagecontrols: UIPageControl!
    
    var timer: Timer?
    var xValue = 1
    
    public var isBannerEmpty: Bool {
           return campaigns.isEmpty
       }
       

     var currentIndex = 0,
                nextIndex = 0,
                prevIndex = 0,
                campaigns = [Campaign]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollAuto.isPagingEnabled = true
        self.scrollAuto.register(UINib(nibName: "BannerCell", bundle: nil), forCellWithReuseIdentifier: "BannerCell")
        
        let cellWidth : CGFloat = screenWidth - 40
        let cellheight : CGFloat = 180
        let cellSize = CGSize(width: cellWidth , height:cellheight)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        scrollAuto.setCollectionViewLayout(layout, animated: true)
    }
    
    
    func setTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 7.0, target: self, selector: #selector(PKPageViewController.autoScroll), userInfo: nil, repeats: true)
    }


    @objc func autoScroll() {
        self.pagecontrols.currentPage = self.xValue
        if self.xValue < self.campaigns.count {
            let indexPath = IndexPath(item: xValue, section: 0)
            let rect = self.scrollAuto.layoutAttributesForItem(at: indexPath)?.frame
            self.scrollAuto.scrollRectToVisible(rect!, animated: true)
            self.xValue = self.xValue + 1
        }else{
            self.xValue = 0
            self.pagecontrols.currentPage = self.xValue
            let rect = self.scrollAuto.layoutAttributesForItem(at: IndexPath(item: 0, section: 0))?.frame
            self.scrollAuto.scrollRectToVisible(rect!, animated: true)
        }
    }
}


// CollectionView Delegates

extension PKPageViewController : UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return campaigns.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : BannerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCell", for: indexPath) as! BannerCell
        if let imageURL = URL(string: campaigns[indexPath.row].image) {
            // Download image using SDWebImage
            SDWebImageManager.shared.loadImage(with: imageURL, options: [], progress: nil) { (image, _, _, _, _, _) in
                if let downloadedImage = image {
                    // Compress the image here
                    if let compressedImageData = downloadedImage.jpegData(compressionQuality: 0.5) {
                        // Set the compressed image to the cell
                        let compressedImage = UIImage(data: compressedImageData)
                        cell.imgBanner.image = compressedImage
                    }
                }
            }
        }
       return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch(campaigns[indexPath.row].type) {
            
        case "1" :
            let vc = UIStoryboard(name: "Home", bundle: nil)
            let liveVC = vc.instantiateViewController(withIdentifier: "LiveViewController") as! LiveViewController
            liveVC.session_ID = campaigns[indexPath.row].id
            liveVC.videoID = campaigns[indexPath.row].id
            liveVC.modalPresentationStyle = .fullScreen
            self.navigationController?.present(liveVC, animated: true)
        case "2" :
           print("2")
        case"3" :
            let vc = UIStoryboard(name: "Home", bundle: nil)
            let taskDetailVC = vc.instantiateViewController(withIdentifier: "PKPageViewDetailVC") as! PKPageViewDetailVC
            taskDetailVC.campaignDetail = campaigns[indexPath.row]
            self.navigationController?.pushViewController(taskDetailVC, animated: true)
        case "5" :
            let vc = UIStoryboard(name: "Home", bundle: nil)
            let taskDetailVC = vc.instantiateViewController(withIdentifier: "ManifestoVC") as! ManifestoVC
            taskDetailVC.campaignDetail = campaigns[indexPath.row]
            self.navigationController?.pushViewController(taskDetailVC, animated: true)
            
        case "4" :
            if let whatsappURL = URL(string: campaigns[indexPath.row].url) {
                
                print(whatsappURL)
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    UIApplication.shared.open(whatsappURL, options: [: ], completionHandler: nil)
                } else {
                    debugPrint("please install WhatsApp")
                }
            }
        default:
            print("")
        }
        print("Click event call ---------->>>>>>>>>> ")
        let selectedCampaign = campaigns[indexPath.row]
        print(selectedCampaign)
       
           
        }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 200, height: 120)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 15, bottom: 5, right: 15)
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if (context.nextFocusedItem != nil) {
            scrollAuto.scrollToItem(at: context.nextFocusedItem as! IndexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    
    
}

extension PKPageViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let visibleRect = CGRect(origin: scrollAuto.contentOffset, size: scrollAuto.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = scrollAuto.indexPathForItem(at: visiblePoint)
        
        if let indexPathRow = visibleIndexPath?.row {
            self.pagecontrols.currentPage = indexPathRow
        }
        
        
    }
    
}
