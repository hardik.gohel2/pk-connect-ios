

import UIKit
import AWSS3
import AWSCore
import FacebookCore
import IQKeyboardManagerSwift
import FirebaseCore
import FirebaseDynamicLinks
import Firebase
import FirebaseMessaging
import UserNotifications
import Swifter
import OtplessSDK


let appDelegate = UIApplication.shared.delegate as! AppDelegate
public typealias dataResponse = [String: Any]

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate , MessagingDelegate , UNUserNotificationCenterDelegate {
    
    var dynamicLinkValue = ""
    var dynamicLinkParams = [URLQueryItem]()
    var strFCMToken : String?

    static var standard: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
    FirebaseApp.configure()
        var swifter: Swifter!
        // Instantiation using Twitter's OAuth Consumer Key and secret
        swifter = Swifter(consumerKey: "7dfm8RoglWqOMUbgz3ebbz38g", consumerSecret: "SuFQzZoF8DTX4YUi1HShqxZhyLDKKMoWPapeEcmathY6QkqxCj")
        
        
        
      
        // Specify the desired permissions as an array
       

        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 10.0

        AWSController.setupAmazonS3(withPoolID: "ap-south-1:49dc3297-c56e-4e91-b10a-220eb52a72e0")
        
        DynamicLinks.performDiagnostics(completion: nil)
        
        DispatchQueue.main.async {
            Messaging.messaging().delegate = self
        }
        
        let notificationContent = UNMutableNotificationContent()
        notificationContent.sound = UNNotificationSound(named:UNNotificationSoundName(rawValue: "default_c4i_notification_tone.mp3"))

        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
          options: authOptions,
          completionHandler: { _, _ in }
        )

        DispatchQueue.main.async {
            UIApplication.shared.registerForRemoteNotifications()
        }
       

        return true
    }
    
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        
        ApplicationDelegate.shared.application(
            application,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        if Otpless.sharedInstance.isOtplessDeeplink(url: url){
         Otpless.sharedInstance.processOtplessDeeplink(url: url) }
        
        if (url.scheme == "fb592025922652694") {
            return ApplicationDelegate.shared.application(application, open: url, options: options)
        }else if (url.scheme == "twitterkit-7dfm8RoglWqOMUbgz3ebbz38g://"){
            Swifter.handleOpenURL(url, callbackURL: URL(string: "twitterkit-7dfm8RoglWqOMUbgz3ebbz38g://")!)
            
        }
    
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            guard let urlString = dynamicLink.url?.absoluteString  else {
                return false
            }
            
            print("Dyanmic link url: \(urlString)")
            print("Dynamic link match type: \(dynamicLink.matchType.rawValue)")
            
            return true
        }
        
        return false
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            // Handle the deep link. For example, show the deep-linked content or
            // apply a promotional offer to the user's account.
            // ...
            print("sourceApplication")
            print(dynamicLink)
            return true
        }
        return false
    }
    
    // MARK: FCM Methods
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken ?? "")")
        strFCMToken = fcmToken ?? ""
        
        if let strFCM = strFCMToken {
            print("FCM token:-",strFCM);
            // 3.Save  FCM Device Token
            let defaults = UserDefaults.standard
            defaults.set(strFCM, forKey: "FCMToken")
            defaults.synchronize()
         
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("didRegisterForRemoteNotificationsWithDeviceToken:-\(deviceToken)")
        Messaging.messaging().apnsToken = deviceToken
        
       
        
    }
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError
                     error: Error) {
        print("didFailToRegisterForRemoteNotificationsWithError:-")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("didReceiveRemoteNotification Notification userInfo:- \(userInfo)\n")
        completionHandler(UIBackgroundFetchResult.newData)

    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void){
        print("willPresent notification :- \(notification)")
        print("\nwillPresent userInfo :- \(notification.request.content.userInfo)")
        
        completionHandler([.banner, .badge, .sound])
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("didReceiveRemoteNotification Notification userInfo:- \(userInfo)\n")
        
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
       
        print("Notification response:- \(response)\n")
        print("Notification userInfo:- \(response.notification.request.content.userInfo)")
        completionHandler()
    }

    func registerFirebaseToken(completion: @escaping (Bool) -> Void) {
        Messaging.messaging().token { token, error in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
                completion(false)
            }else{
                self.strFCMToken = token
                completion(true)
            }
        }
    }
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
        var dictonary:NSDictionary?
        if let data = text.data(using: String.Encoding.utf8) {
            
            do {
                dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject] as NSDictionary?
                
                if let myDictionary = dictonary
                {
                    return myDictionary as! [String : Any]
                }
            } catch let error as NSError {
                return nil
            }
        }
        return nil
    }
  
}










