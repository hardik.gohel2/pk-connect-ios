

import UIKit
import AVKit
import AVFoundation
import FirebaseDynamicLinks

class MyShotsVC: BaseViewController {
    
    var avPlayer: AVPlayer!
    var visibleIP : IndexPath?
    var aboutToBecomeInvisibleCell = -1
    var avPlayerLayer: AVPlayerLayer!
    var paused: Bool = false
    var videoURLs = Array<URL>()
    var firstLoad = true
    var videoArray = [String]()
    var userLiked = ""
    var likesCount = "0"
    var shortList = [AllShorts]()
    var rowIndex = 0
    var comingFrom = true
    var videoUrl :VideoModel?
    var isVideoPlay:Bool = true
    var isFromDeepLink:Bool? = false
    var shortId:String? = ""
    
    @IBOutlet weak var videoTableVw:UITableView!
    
    var playerCache = NSCache<NSString, AVPlayer>()
    var visibleIndexPaths = Set<IndexPath>()
    var objPlayer = AVPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.videoTableVw.delegate = self
        self.videoTableVw.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFromDeepLink ?? false) {
            shortsById(short_id: shortId ?? "")
        } else {
            getShortsData()
        }
    }
    
    func getShortsData() {
        showLoadingIndicator(in: self.view)
        getHomeShorts { response in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                self.shortList = response.RESULT ?? []
                self.videoTableVw.reloadData()
                let indexPath = IndexPath(row: self.rowIndex, section:0)
                DispatchQueue.main.async {
                    self.videoTableVw.scrollToRow(at: indexPath, at: .none, animated: true)
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        let indexPath = IndexPath(row: rowIndex, section:0)
        if let videoCell = videoTableVw.cellForRow(at: indexPath) as? HomeVideoTableViewCell {
            DispatchQueue.main.async {
                videoCell.player?.pause()
                self.visibleIndexPaths.removeAll()
            }
        }
    }
    
    func shortsById(short_id: String) {
        getShortsById(short_id: short_id) { response in
            let obj: VideosData = response.result!
            self.videoUrl = VideoModel(code: response.code,
                                       message: response.message,
                                       result: [obj],
                                       next: response.next,
                                       total: response.total)
        
            
            self.videoUrl?.result?.append(contentsOf: SessionManager.shared.shortsVideos?.result ?? [])
            
            DispatchQueue.main.async {
                self.videoTableVw.reloadData()
                let indexPath = IndexPath(row: 0, section:0)
//                self.videoTableVw.scrollToRow(at: indexPath, at: .none, animated: true)
                if let cell = self.videoTableVw.cellForRow(at: indexPath) as? HomeVideoTableViewCell {
                    let videoRect = self.videoTableVw.convert(cell.videoLayerView.bounds, from: cell.videoLayerView)
                    // Calculate the visible portion of the video
                    let intersection = self.videoTableVw.bounds.intersection(videoRect)
                    let visiblePercentage = (intersection.width * intersection.height) / (cell.videoLayerView.bounds.width * cell.videoLayerView.bounds.height)
                    // If more than 80% of the video is visible, play it; otherwise, pause it
                    DispatchQueue.main.async {
                        cell.player?.play()
                        self.isVideoPlay = true
                        self.objPlayer = cell.player!
                    }
                }
            }
        }
    }
//
//    @IBAction private func back_Btn(_ sender :UIButton) {
//        self.objPlayer.pause()
//        for indexPath in videoTableVw.indexPathsForVisibleRows ?? [] {
//            if let videoCell = videoTableVw.cellForRow(at: indexPath) as? HomeVideoTableViewCell {
//                videoCell.player?.pause()
//                visibleIndexPaths.removeAll()
//
//               // self.navigationController?.popViewController(animated: true)
//
//            }
//        }
//        playerCache.removeAllObjects() // Assuming playerCache is used for caching AVPlayers
//
//        self.navigationController?.popViewController(animated: true)
//    }
    @IBAction private func back_Btn(_ sender: UIButton) {
        // Pause the main player
        self.objPlayer.pause()
        
        // Pause visible video cells in the tbl view and clear cache
        for indexPath in videoTableVw.indexPathsForVisibleRows ?? [] {
            if let videoCell = videoTableVw.cellForRow(at: indexPath) as? HomeVideoTableViewCell {
                // Pause the video player in the cell
                videoCell.player?.pause()
                if (videoCell.player?.currentItem) != nil {
                    // Remove observer for playerItem if added
                    // videoCell.player?.removeObserver(self, forKeyPath: "status")
                }
                // Dealloacte player
                videoCell.player = nil
            }
        }
        playerCache.removeAllObjects()
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension MyShotsVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if comingFrom == true{
            return self.shortList.count
        }else{
            return self.videoUrl?.result?.count ?? 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeVideoTableViewCell") as! HomeVideoTableViewCell
        cell.img_Play.isHidden = true
        if comingFrom == true {
            var videoRelatedData = self.shortList[indexPath.row]
            cell.videoLayerView.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
            guard  let path = URL(string:videoRelatedData.s3_url ?? "") else {
                debugPrint("video.mp4 not found")
                return cell
            }
            
            if let cachedPlayer = playerCache.object(forKey: (videoRelatedData.s3_url ?? "") as NSString) {
                cell.player = cachedPlayer
            } else {
                cell.player = AVPlayer(url: path)
                playerCache.setObject(cell.player!, forKey: (videoRelatedData.s3_url ?? "") as NSString)
            }
            
            let playerLayer = AVPlayerLayer(player: cell.player)
            playerLayer.frame = cell.videoLayerView.bounds
            playerLayer.backgroundColor = UIColor.clear.cgColor
            cell.videoLayerView.layer.addSublayer(playerLayer)
            self.objPlayer = cell.player!
            
            visibleIP = IndexPath.init(row: indexPath.row, section: 0)
            cell.userNameBtn.setTitle(videoRelatedData.createdBy?.title ?? "", for: .normal)
            cell.videoTitleLbl.text = videoRelatedData.short_title ?? ""
            cell.numberOfCommentsLbl.text = videoRelatedData.comment_count ?? ""
            let likeImaage = videoRelatedData.is_liked == true ? "liked_small" : "heart_icon"
            cell.btnLike.setImage(UIImage(named:likeImaage), for: .normal)
            let subscription = videoRelatedData.is_subscribed == true
            ? "Subscribed" : "Subscribe"
            cell.subscribeBtn.setTitle(subscription, for: .normal)
            if videoRelatedData.is_subscribed == true{
                cell.subscribeBtn.backgroundColor = .lightGray
            }else{
                cell.subscribeBtn.backgroundColor = UIColor(hexString: "#0000FF")
            }
            cell.viewCountLbl.text = videoRelatedData.view_count
            cell.numberOfLikes.text = videoRelatedData.like_count ?? ""
            cell.onClickLike = {
                if videoRelatedData.is_liked == true{
//                    videoRelatedData.is_liked = false
//                    let likeValue = (Int(videoRelatedData.like_count ?? "") ?? 0) - 1
//                    videoRelatedData.like_count = "\(likeValue)"
//                    self.deletelikeshorts(shortId: videoRelatedData.id ?? "") { [weak self] (result) in
//                        guard let self else {return}
//                        DispatchQueue.main.async {
//                            print(self)
//                            cell.numberOfLikes.text = videoRelatedData.like_count
//                            let likeImaage = videoRelatedData.is_liked == true ? "liked_small" : "heart_icon"
//                            cell.btnLike.setImage(UIImage(named:likeImaage), for: .normal)
//                        }
//                    }
                } else {
                    let likeValue = (Int(videoRelatedData.like_count ?? "") ?? 0) + 1
                    videoRelatedData.like_count = "\(likeValue)"
                    self.likeshorts(shortId: videoRelatedData.id ?? "") { [weak self](result) in
                        guard let self else {return}
                        DispatchQueue.main.async {
                            videoRelatedData.is_liked = true
                            cell.numberOfLikes.text = videoRelatedData.like_count
                            let likeImaage = videoRelatedData.is_liked == true ? "liked_small" : "heart_icon"
                            cell.btnLike.setImage(UIImage(named:likeImaage), for: .normal)
                        }
                    }
                }
            }
            cell.onClickComment = {
                cell.player?.pause()
                let storyBoard = UIStoryboard(name: "Other", bundle: nil)
                let commentVC = storyBoard.instantiateViewController(withIdentifier: "ShortsCommetsVC") as! ShortsCommetsVC
                commentVC.short_id = videoRelatedData.id ?? ""
                commentVC.short_title = videoRelatedData.short_title ?? ""
                self.navigationController?.pushViewController(commentVC, animated: true)
            }
            cell.onClickUserTitle = { [weak self] in
                guard let self else {return}
                DispatchQueue.main.async {
                    cell.player?.pause()
                    let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                    let commentVC = storyBoard.instantiateViewController(withIdentifier: "ChannelDetailsVC") as! ChannelDetailsVC
                    commentVC.channel_id = videoRelatedData.channel_id ?? ""
                    self.navigationController?.pushViewController(commentVC, animated: true)
                }
            }
            
            cell.shareBtn.addTarget(self, action:#selector(shareBtn(_ :)), for: .touchUpInside)
            cell.shareBtn.tag = indexPath.row
            cell.subscribeBtn.addTarget(self, action:#selector(subscribeBtnAction(_ :)), for: .touchUpInside)
            cell.subscribeBtn.tag = indexPath.row
            
            
            if let imageStr = videoRelatedData.createdBy?.image{
                cell.profilePic.setLoadedImage(imageStr)
            }
        } else {
            if  var  videoRelatedData = self.videoUrl?.result?[indexPath.row] {
                cell.videoLayerView.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                guard  let path = URL(string:videoRelatedData.s3URL ?? "") else {
                    debugPrint("video.mp4 not found")
                    return cell
                }
                
                if let cachedPlayer = playerCache.object(forKey: (videoRelatedData.videoURL ?? "") as NSString) {
                    cell.player = cachedPlayer
                } else {
                    cell.player = AVPlayer(url: path)
                    playerCache.setObject(cell.player!, forKey: (videoRelatedData.videoURL ?? "") as NSString)
                }
                
                let playerLayer = AVPlayerLayer(player: cell.player)
                playerLayer.frame = cell.videoLayerView.bounds
                playerLayer.backgroundColor = UIColor.clear.cgColor
                cell.videoLayerView.layer.addSublayer(playerLayer)
                self.objPlayer = cell.player!
                visibleIP = IndexPath.init(row: indexPath.row, section: 0)
                cell.userNameBtn.setTitle(videoRelatedData.createdBy?.title ?? "", for: .normal)
                cell.videoTitleLbl.text = videoRelatedData.shortTitle ?? ""
                cell.numberOfCommentsLbl.text = videoRelatedData.commentCount ?? ""
                let likeImaage = videoRelatedData.isLiked == true ? "liked_small" : "heart_icon"
                let subscription = videoRelatedData.isSubscribed == true
                ? "Subscribed" : "Subscribe"
                cell.subscribeBtn.setTitle(subscription, for: .normal)
                if videoRelatedData.isSubscribed == true{
                    cell.subscribeBtn.backgroundColor = .lightGray
                }else{
                    cell.subscribeBtn.backgroundColor = UIColor(hexString: "#0000FF")
                }
                cell.viewCountLbl.text = videoRelatedData.viewCount
                cell.numberOfLikes.text = videoRelatedData.likeCount ?? ""
                cell.onClickLike = {
                    if videoRelatedData.isLiked == true{
                        videoRelatedData.isLiked = false
                        let likeValue = (Int(videoRelatedData.likeCount ?? "") ?? 0) - 1
                        videoRelatedData.likeCount = "\(likeValue)"
                        self.deletelikeshorts(shortId: videoRelatedData.id ?? "") { [weak self] (result) in
                            guard let self else {return}
                            DispatchQueue.main.async {
                                print(self)
                                cell.numberOfLikes.text = videoRelatedData.likeCount
                                let likeImaage = videoRelatedData.isLiked == true ? "liked_small" : "heart_icon"
                                cell.btnLike.setImage(UIImage(named:likeImaage), for: .normal)
                            }
                        }
                    } else {
                        videoRelatedData.isLiked = true
                        let likeValue = (Int(videoRelatedData.likeCount ?? "") ?? 0) + 1
                        videoRelatedData.likeCount = "\(likeValue)"
                        self.likeshorts(shortId: videoRelatedData.id ?? "") { [weak self](result) in
                            guard let self else {return}
                            DispatchQueue.main.async {
                                print(self)
                                cell.numberOfLikes.text = videoRelatedData.likeCount
                                let likeImaage = videoRelatedData.isLiked == true ? "liked_small" : "heart_icon"
                                cell.btnLike.setImage(UIImage(named:likeImaage), for: .normal)
                            }
                        }
                    }
                }
                cell.onClickComment = {
                    DispatchQueue.main.async {
                        cell.player?.pause()
                        let storyBoard = UIStoryboard(name: "Other", bundle: nil)
                        let commentVC = storyBoard.instantiateViewController(withIdentifier: "commentVC") as! CommentVC
                        commentVC.newsId = videoRelatedData.channelID ?? ""
                        commentVC.headline = videoRelatedData.shortTitle ?? ""
                        self.navigationController?.pushViewController(commentVC, animated: true)
                    }
                }
                cell.onClickUserTitle = { [weak self] in
                    guard let self else {return}
                    DispatchQueue.main.async {
                        cell.player?.pause()
                        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                        let commentVC = storyBoard.instantiateViewController(withIdentifier: "ChannelDetailsVC") as! ChannelDetailsVC
                        commentVC.channel_id = videoRelatedData.channelID ?? ""
                        self.navigationController?.pushViewController(commentVC, animated: true)
                    }
                }
                cell.btnLike.setImage(UIImage(named:likeImaage), for: .normal)
                cell.shareBtn.addTarget(self, action:#selector(shareBtn(_ :)), for: .touchUpInside)
                cell.shareBtn.tag = indexPath.row
                cell.subscribeBtn.addTarget(self, action:#selector(subscribeBtnAction(_ :)), for: .touchUpInside)
                cell.subscribeBtn.tag = indexPath.row
                if let imageStr = videoRelatedData.createdBy?.image{
                    cell.profilePic.setLoadedImage(imageStr)
                }
            }
        }
        
        return cell
    }
    
    @objc func subscribeBtnAction(_ sender :UIButton){
        var channelId:String?
        if comingFrom == true{
            self.shortList[sender.tag].is_subscribed = true
            channelId = self.shortList[sender.tag].channel_id ?? ""
        }else{
            self.videoUrl?.result?[sender.tag].isSubscribed = true
            channelId = self.videoUrl?.result?[sender.tag].channelID ?? ""
        }
        
        addSubscriptionStatus(channel_id:channelId ?? "") { [weak self](result) in
            guard let self else {return}
            DispatchQueue.main.async {
                debugPrint("Done")
            }
        }
        self.videoTableVw.reloadData()
    }
    
    @objc func shareBtn(_ sender :UIButton) {
            self.showLoadingIndicator(in: self.view)
            var videoId = ""
            var title = ""
            if comingFrom == true {
                videoId = (self.shortList[sender.tag].id ?? "")
                title = (self.shortList[sender.tag].short_title ?? "") + "Shared Via Team Jagananna"
            } else {
                videoId = (self.videoUrl?.result?[sender.tag].id ?? "")
                title = (self.videoUrl?.result?[sender.tag].shortTitle ?? "") + "Shared Via Team Jagananna"
            }
            
            self.shareDeepLink(controller: self, videoId: videoId, message: title) {
                self.hideLoadingIndicator()
            }
        }
        
        func shareDeepLink(controller:UIViewController, videoId:String, message:String, handler: @escaping () -> Void) {
            let shareLink = "https://pkconnect.page.link/?item_type=live_short&short_video_id=" + videoId
            
            let linkParameter = URL(string: shareLink)!
            guard let dynamiclink = DynamicLinkComponents.init(link: linkParameter, domainURIPrefix: "https://pkconnect.page.link") else {
                return
            }
            dynamiclink.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.pk.connect")
            dynamiclink.iOSParameters?.appStoreID = "6448717716"
            dynamiclink.androidParameters = DynamicLinkAndroidParameters(packageName: DynamicLinkParams.packageName)
            dynamiclink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
            dynamiclink.socialMetaTagParameters?.title = DynamicLinkParams.packageName
            
            dynamiclink.shorten {[weak controller] (url, warnings, error) in
                if let url = url {
                    var shareText = "\(message)" + "\n\n\(url.absoluteString)"
                    DispatchQueue.main.async {
                        let activityVC = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
                        activityVC.popoverPresentationController?.sourceView = controller?.view
                        controller?.present(activityVC, animated: true, completion: nil)
                        handler()
                    }
                } else {
                    handler()
                }
            }
        }
    
    @objc func likeBtnAction(_ sender :UIButton){
        if comingFrom == true{
            if self.shortList[sender.tag].is_liked == true{
                self.shortList[sender.tag].is_liked = false
                let likeValue = (Int(self.shortList[sender.tag].like_count ?? "") ?? 0) - 1
                self.shortList[sender.tag].like_count = "\(likeValue)"
                deletelikeshorts(shortId: self.shortList[sender.tag].id ?? "") { [weak self] (result) in
                    guard let self else {return}
                    DispatchQueue.main.async {
                        print(self)
                    }
                }
            }else{
                self.shortList[sender.tag].is_liked = true
                let likeValue = (Int(self.shortList[sender.tag].like_count ?? "") ?? 0) + 1
                self.shortList[sender.tag].like_count = "\(likeValue)"
                likeshorts(shortId: self.shortList[sender.tag].id ?? "") { [weak self](result) in
                    guard let self else {return}
                    DispatchQueue.main.async {
                        print(self)
                    }
                }
            }
        }else{
            if self.videoUrl?.result?[sender.tag].isLiked == true{
                self.videoUrl?.result?[sender.tag].isLiked = false
                let likeValue = (Int(self.videoUrl?.result?[sender.tag].likeCount ?? "") ?? 0) - 1
                self.self.videoUrl?.result?[sender.tag].likeCount = "\(likeValue)"
                deletelikeshorts(shortId:self.videoUrl?.result?[sender.tag].id ?? "") { [weak self] (result) in
                    guard let self else {return}
                    DispatchQueue.main.async {
                        print(self)
                    }
                }
            }else{
                self.videoUrl?.result?[sender.tag].isLiked = true
                let likeValue = (Int(self.videoUrl?.result?[sender.tag].likeCount ?? "") ?? 0) + 1
                self.videoUrl?.result?[sender.tag].likeCount = "\(likeValue)"
                likeshorts(shortId: self.videoUrl?.result?[sender.tag].id ?? "") { [weak self](result) in
                    guard let self else {return}
                    DispatchQueue.main.async {
                        print(self)
                    }
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return videoTableVw.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let videoCell = cell as? HomeVideoTableViewCell {
            DispatchQueue.main.async {
                //                videoCell.player?.play()
               // self.objPlayer = videoCell.player!
                if let player = videoCell.player {
                        self.objPlayer = player
                    }
                self.visibleIndexPaths.insert(indexPath)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let videoCell = cell as? HomeVideoTableViewCell {
            DispatchQueue.main.async {
                videoCell.player?.pause()
                self.visibleIndexPaths.removeAll()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = videoTableVw.cellForRow(at: indexPath) as? HomeVideoTableViewCell
        if isVideoPlay {
            cell?.player?.pause()
            isVideoPlay = false
            cell?.img_Play.isHidden = false
            
        } else {
            isVideoPlay = true
            cell?.img_Play.isHidden = true
            cell?.player?.play()
            self.objPlayer = (cell?.player)!
        }
    }
  
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let visibleIndexPaths = videoTableVw.indexPathsForVisibleRows ?? []
//        for indexPath in visibleIndexPaths {
//            if let cell = videoTableVw.cellForRow(at: indexPath) as? HomeVideoTableViewCell {
//                let videoRect = videoTableVw.convert(cell.videoLayerView.bounds, from: cell.videoLayerView)
//
//                // Calculate the visible portion of the video
//                let visibleHeight = min(videoRect.maxY, scrollView.contentOffset.y + scrollView.bounds.height) - max(videoRect.minY, scrollView.contentOffset.y)
//                let visiblePercentage = visibleHeight / videoRect.height
//
//                // Pause the player if more than 80% of the video is hidden
//                if visiblePercentage < 1.0 {
//                    DispatchQueue.main.async {
//                        cell.player?.pause()
//                        self.objPlayer = cell.player!
//                    }
//                } else {
//                    DispatchQueue.main.async {
//                        cell.player?.play()
//                        self.isVideoPlay = true
//                        self.objPlayer = cell.player!
//                    }
//                }
//            }
//        }
//    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleIndexPaths = videoTableVw.indexPathsForVisibleRows ?? []
        for indexPath in visibleIndexPaths {
            if let cell = videoTableVw.cellForRow(at: indexPath) as? HomeVideoTableViewCell {
                let videoRect = videoTableVw.convert(cell.videoLayerView.bounds, from: cell.videoLayerView)

                // Calculate the visible portion of the video
                let intersection = videoTableVw.bounds.intersection(videoRect)
                let visiblePercentage = (intersection.width * intersection.height) / (cell.videoLayerView.bounds.width * cell.videoLayerView.bounds.height)

                // If more than 80% of the video is visible, play it; otherwise, pause it
                if visiblePercentage > 0.9 {
                    DispatchQueue.main.async {
                        cell.player?.play()
                        self.isVideoPlay = true
                        if let player = cell.player
                        {
                            self.objPlayer = player
                        }
                       // self.objPlayer = cell.player!
                    }
                } else {
                    DispatchQueue.main.async {
                        cell.player?.pause()
                        if let player = cell.player
                        {
                            self.objPlayer = player
                        }
                        // self.objPlayer = cell.player!
                    }
                }
            }
        }
    }
}
