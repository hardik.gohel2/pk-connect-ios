//
//  ServiceManager.swift
//  YoutubeShotsDemo


import Foundation
import Foundation


enum ApiError: Error {
    case badURL
    case internetError
    case timeOut
    case technicalError
    case parsingError
    
    
    var label: String {
        switch self {
        case .badURL:
            return "Not a valid UR"
        case .internetError:
            return "Slow or no internet connection. Please check your internet settings."
        case .timeOut:
            return "request time out please try again."
        case .technicalError:
            return "Something Went Wrong! Our system may be having some trouble, Please try again later. We apologize for the inconvenience."
        case .parsingError:
            return "No data recivied from server."
        }
    }
}

 class ServiceManager:BaseViewController {
    static var shared = ServiceManager()
     
//     let baseURL = "https://stage.pkconnect.com",
//                 username = "admin",
//                 password = "12345"
//     
        let baseURL = "https://pkconnect.com/dashboard/",
                 username = "admin",
                 password = "12345"



    private func parse<T:Codable>(data: Data,  type: T.Type) -> T? {

            do{
                let json = try JSONDecoder().decode(type, from: data)
                return json
            }catch {
                return nil
            }
            return nil
        }
     
     
     func getRequestWithArr<T: Codable>(endpoint: String, type: T.Type, completionHandler: @escaping (T?, Error?) -> ()) {
         print("URL is ================= \(endpoint)")
         
         // ... (previous code)
         showLoadingIndicator(in: view)
         let url = URL(string: endpoint)
         var request = URLRequest(url: url!, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 40.0) // Use cached data if available
         
         request.httpMethod = "GET"
         
         guard let data = "\(username):\(password)".data(using: .utf8) else { return }
         let base64 = data.base64EncodedString()
         
         guard let lang = UserDefaults.standard.object(forKey: "selectedLang") as? String else {
             return
         }
         
         request.setValue("Basic \(base64)", forHTTPHeaderField: "Authorization")
         request.setValue((lang == "hi" ? "hi" : "en"), forHTTPHeaderField: "lang")
         request.setValue(getRetrievedUser()?.accessToken ?? "", forHTTPHeaderField: "Accesstoken")
         
         let config = URLSessionConfiguration.default
         let cache = URLCache(memoryCapacity: 50 * 1024 * 1024, diskCapacity: 100 * 1024 * 1024, diskPath: "videosCache") // Create a custom cache
         
         config.urlCache = cache
         config.requestCachePolicy = .returnCacheDataElseLoad
         
         let session = URLSession(configuration: config)
         
         // Check if video response is already cached
         if let cachedResponse = cache.cachedResponse(for: request) {
             if let result = self.parse(data: cachedResponse.data, type: type) {
                 completionHandler(result, nil)
                 return
             }
         }
         
         let dataTask = session.dataTask(with: request) { (data, response, error) in
             DispatchQueue.main.async {
                 // Hide loader here
                 self.hideLoadingIndicator()
                 if error == nil {
                     if let info = data {
                         do {
                             // If response is a video, handle caching accordingly
                             if let mimeType = response?.mimeType, mimeType.contains("video") {
                                 // Store video data in cache
                                 let cachedResponse = CachedURLResponse(response: response!, data: info)
                                 cache.storeCachedResponse(cachedResponse, for: request)
                             }
                             
                             let result = self.parse(data: info, type: type)
                             completionHandler(result, nil)
                         } catch let jsonErr {
                             completionHandler(nil, jsonErr)
                         }
                     }
                 } else {
                     // Handle error and hide loader here
                     completionHandler(nil, error)
                 }
             }
         }
         dataTask.resume()
     }

     
     
     
     
}


