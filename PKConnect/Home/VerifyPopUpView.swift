//
//  VerifyPopUpView.swift
//  PKConnect
//
//  Created by Navdip on 17/04/23.
//

import UIKit

class VerifyPopUpView: BaseViewController {
    @IBOutlet weak var contentView: CustomView!
    
    @IBOutlet weak var btnVerifyNow: RoundedButton!
    @IBOutlet weak var btnVerifyLater: RoundedButton!
    var mobileNo = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        getProfile { profile in
            DispatchQueue.main.async { [self] in
                mobileNo = profile.phone_number!
            }
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnNowClick(_ sender: UIButton) {
        if UserDefaults.standard.value(forKey: "UserVerification") != nil, UserDefaults.standard.value(forKey: "UserVerification") as! String == "3"{
            sendOTP(for: mobileNo) {  responseObj in
                if responseObj!.status == "200" {
                    DispatchQueue.main.async {
                        let VerifyOTPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "verifyOTPVC") as! VerifyOTPVC
                        
                        VerifyOTPVC.mobileNo = self.mobileNo
                        VerifyOTPVC.UpdateAll = false
                        VerifyOTPVC.currentOTP = responseObj!.data.otp!
                        self.navigationController?.pushViewController(VerifyOTPVC, animated: false)
                        
                    }
                }
            }
        }else{
            let NewVerifyProfileVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "NewVerifyProfileVC") as! NewVerifyProfileVC
            self.navigationController?.pushViewController(NewVerifyProfileVC, animated: false)
        }
    }
    
    @IBAction func btnLaterClick(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let slidingVC = storyboard.instantiateViewController(withIdentifier: "slidingVC") as! SlidingViewController
        let initialNavigationController = UINavigationController(rootViewController: slidingVC)
        self.view.window?.rootViewController = initialNavigationController
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
