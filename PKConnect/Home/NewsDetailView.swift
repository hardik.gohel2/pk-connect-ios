//
//  NewsDetailView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit
import AVKit

class NewsDetailView: BaseViewController {
    
    @IBOutlet private var mediaContainerView: UIView!
    @IBOutlet private var newsImage: UIImageView!
    @IBOutlet private var newsTitle: UILabel!
    @IBOutlet private var newsCreatedDate: UILabel!
    @IBOutlet private var newsDescription: UILabel!
    
    @IBOutlet private var likeButton: UIButton!
    @IBOutlet private var commentButton: UIButton!
    @IBOutlet private var bookmarkButton: UIButton!
    
    @IBOutlet weak var imageContainerHeight: NSLayoutConstraint!
    
    private var shareAction: (()->())?,
                downloadAction: (()->())?,
                feedItem: FeedItem?,
                player: AVPlayer?,
                avpController = AVPlayerViewController()
    
    var newsId = "0",
        img: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        likeButton.setImage(UIImage(named: "like"), for: .normal)
        likeButton.setImage(UIImage(named: "liked"), for: .selected)
        
        bookmarkButton.setImage(UIImage(named: "bookmark"), for: .normal)
        bookmarkButton.setImage(UIImage(named: "bookmarked"), for: .selected)
        
        if let img = img {
            self.imageContainerHeight.constant = img.size.height
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showLoadingIndicator(in: view)
        getNewsDetailFor(newsId) { feedItems in
            guard let feedItem = feedItems.first else { return }
            self.feedItem = feedItem
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                self.setUpView()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        player?.pause()
        avpController.removeFromParent()
    }
    
    func setDynamicImageFrom(url: String, completion: @escaping (UIImage?)->()) {
        
        guard let imageUrl = URL(string: url) else { return }
        self.loadImage(url: imageUrl) { data, error in
            DispatchQueue.main.async { [self] in
                if let data = data, let customImage = UIImage(data: data) {
                    let resizedImage = self.resizeImage(image: customImage, newWidth: self.view.frame.size.width)
                    self.imageContainerHeight.constant = resizedImage.size.height
                    self.newsImage.image = resizedImage
                    completion(resizedImage)
                }
            }
        }
    }
    
    private func setUpView() {
        guard let feedItem = feedItem else { return }
        self.newsTitle.text = feedItem.news_title
        self.newsCreatedDate.text = getFormattedDate(from: feedItem.created_date, inputFormat: "yyyy-MM-dd HH:mm:ss", outputFormat: 0)
        if let description = feedItem.news_description.htmlToAttributedString {
            description.addAttributes([.font: UIFont(name: "NunitoSans-Light", size: 13)!], range: NSRange(location: 0, length: description.length))
            self.newsDescription.attributedText = description
        }
        
        //Like, Comment, Share & Download
        self.likeButton.isSelected = (feedItem.is_like == "dislike") ? false : true
        self.bookmarkButton.isSelected = (feedItem.is_bookmarked == "yes") ? true : false
        
        if feedItem.total_likes != "0" {
            let totalLikesAttrText = NSAttributedString(string: feedItem.total_likes, attributes: [.font: UIFont(name: "NunitoSans-Regular", size: 14)!])
            likeButton.setAttributedTitle(totalLikesAttrText, for: .normal)
        }
        
        if feedItem.total_comments != "0" {
            let totalCommentsAttrText = NSAttributedString(string: feedItem.total_comments, attributes: [.font: UIFont(name: "NunitoSans-Regular", size: 14)!])
            commentButton.setAttributedTitle(totalCommentsAttrText, for: .normal)
        }
        
        //Media
        if let mediaSet = feedItem.content_media_set {
            if mediaSet.first?.media_type == "1" {
                //Image
                
                if let urlStr = mediaSet.first?.media_url {
                    self.setDynamicImageFrom(url: urlStr) { img in
                        
                    }
                }
                
                if let img = img {
                    self.newsImage.image = img
                    self.downloadAction = {
                        self.downloadTapped(img)
                    }
                    self.shareAction = { self.shareTapped(img, feedItem: feedItem) }
                    reloadInputViews()
                }
            } else if mediaSet.first?.media_type == "2" {
                //Video
                
                if let urlStr = mediaSet.first?.media_thumb {
                    self.setDynamicImageFrom(url: urlStr) { img in
                        
                    }
                }
                
                self.downloadAction = {
                    filedownLoad(view: self,mediaFile: mediaSet.first!.media_url,fileName: feedItem.news_title)
                }
                
                self.shareAction = { self.shareVideoURL(url: mediaSet.first!.media_url,feedItem: feedItem) }

                if let videoUrl = mediaSet.first?.media_url {
                    self.setVideoURL(urlStr: videoUrl)
                }
            }
        }
    }
    
    private func setVideoURL(urlStr: String) {
        newsImage.isHidden = true
        
        let videoUrl = URL(string: urlStr)
        player = AVPlayer(url: videoUrl!)
        avpController.player = player
        avpController.view.frame = mediaContainerView.bounds
        self.mediaContainerView.addSubview(avpController.view)
        player?.play()
    }
    
    @IBAction private func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func changeLanguage(_ sender: Any) {
        changeAppLanguage()
    }
    
    @IBAction private func likeTapped(_ sender: Any) {
        if !likeButton.isSelected {
            postLike(newsId: newsId) {
                DispatchQueue.main.async {
                    self.likeButton.isSelected.toggle()
                }
            }
        } else {
            unlikeNews(newsId: newsId) {
                DispatchQueue.main.async {
                    self.likeButton.isSelected.toggle()
                }
            }
        }
    }
    
    @IBAction private func commentTapped(_ sender: Any) {
        self.player?.pause()
        let storyBoard = UIStoryboard(name: "Other", bundle: nil)
        let commentVC = storyBoard.instantiateViewController(withIdentifier: "commentVC") as! CommentVC
        commentVC.newsId = newsId
        commentVC.headline = feedItem?.news_title ?? ""
        self.navigationController?.pushViewController(commentVC, animated: false)
    }
    
    @IBAction private func shareTapped(_ sender: Any) {
        self.shareAction?()
    }
    
    @IBAction private func downloadTapped(_ sender: Any) {
        self.downloadAction?()
    }
    
    @IBAction private func bookmarkTapped(_ sender: Any) {
        postBookmark(newsId: newsId) {
            DispatchQueue.main.async {
                self.bookmarkButton.isSelected.toggle()
            }
        }
    }
    
    private func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth/image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.draw(in: CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}

