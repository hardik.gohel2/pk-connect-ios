//
//  NewsListingVC.swift
//  PKConnect
//
//  Created by admin on 01/11/23.
//

import UIKit

class NewsListingVC: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblNews: UITableView!{
        didSet {
            tblNews.delegate = self
            tblNews.dataSource = self
            let cell = UINib(nibName: LoadMoreCell.identifier, bundle: nil)
            tblNews.register(cell, forCellReuseIdentifier: LoadMoreCell.identifier)
            if isfromMedia {
                tblNews.register(UINib(nibName: "NewsMediaListCell", bundle: nil), forCellReuseIdentifier: "NewsMediaListCell")
            }else{
                tblNews.register(UINib(nibName: "FeedTableCell", bundle: nil), forCellReuseIdentifier: "feedTableCell")
            }
        }
    }
    
    var homeFeedItems = [FeedItem](),
        refreshControl = UIRefreshControl(),
        section = "",
        pageIndex = 1
    private var haveNextItems: Bool = false
    var mediaArray = [NewsItem]()
    var  isfromMedia = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isfromMedia {
            lblTitle.text = "News and Media".localize()
            getNewsMediaData()
        }else{
            lblTitle.text = section
            callHomeFeedAPI()
        }
        
        
    }
    
    
    func getNewsMediaData() {
        showLoadingIndicator(in: self.view)
        getNewsMedia { response in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                self.mediaArray = response.RESULT ?? []
                self.tblNews.reloadData()
            }
            
        }
    }
    
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func callHomeFeedAPI() {
        showLoadingIndicator(in: view)
        getHomeData(section: section, count: "\(pageIndex)") { feedItems in
            if let feedItems = feedItems {
                
                if(self.pageIndex == 1){
                    self.homeFeedItems.removeAll()
                }
                
                self.homeFeedItems = feedItems.RESULT ?? []
                self.haveNextItems = (feedItems.RESULT?.count ?? 0 > 0)
                DispatchQueue.main.async {
                    self.hideLoadingIndicator()
                    self.refreshControl.endRefreshing()
                    
                    self.tblNews.reloadData()
                    //self.feedTable.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                }
            }
        }
    }
    
    
}
extension NewsListingVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? FeedTableCell else { return }
        DispatchQueue.main.async {
            cell.player?.pause()
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isfromMedia {
            mediaArray.count + (haveNextItems ? 1 : 0)
        }else{
            homeFeedItems.count + (haveNextItems ? 1 : 0)
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !isfromMedia {
            if(indexPath.row == homeFeedItems.count){
                let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreCell.identifier) as! LoadMoreCell
                cell.tapLoadMore = {
                    self.showLoadingIndicator(in: self.view)
                    self.pageIndex += 1
                    //API Call
                    self.getHomeData(section: self.section, count: "\(self.pageIndex * 10)") { feedItems in
                        if let feedItems = feedItems {
                            if(self.pageIndex == 1){
                                self.homeFeedItems.removeAll()
                            }
                            
                            self.haveNextItems = (feedItems.RESULT?.count ?? 0 > 0)
                            self.homeFeedItems.append(contentsOf: feedItems.RESULT ?? [])
                            DispatchQueue.main.async {
                                self.hideLoadingIndicator()
                                self.tblNews.reloadData()
                            }
                        }
                    }
                }
                return cell
            } else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "feedTableCell", for: indexPath) as! FeedTableCell
                
                var feedItem = homeFeedItems[indexPath.row]
                feedItem.created_date = getFormattedDate(from: feedItem.created_date, inputFormat: "yyyy-MM-dd HH:mm:ss", outputFormat: 0)
                
                cell.tableDelegate = self
                cell.setUpContent(feedItem)
                
                cell.likeAction = { self.likeTapped(cell: cell, newsId: feedItem.news_id) }
                cell.commentAction = {
                    self.commentTapped(entry: feedItem, newsId: feedItem.news_id)
                }
                cell.bookmarkAction = {self.bookmarkTapped(cell: cell, newsId: feedItem.news_id) {}}
                
                cell.downloadAction = {
                    
                    if let mediaSet = feedItem.content_media_set {
                        if mediaSet.first?.media_type == "1" {
                            //
                        } else if mediaSet.first?.media_type == "2" {
                            filedownLoad(view: self,mediaFile: mediaSet.first!.media_url,fileName: feedItem.news_title)
                        }
                    }
                }
                
                if let mediaSet = feedItem.content_media_set {
                    if mediaSet.first?.media_type == "1" {
                        //
                    } else if mediaSet.first?.media_type == "2" {
                        cell.shareAction = { self.shareVideoURL(url: mediaSet.first!.media_url,feedItem: feedItem) }
                    }
                }
                
                cell.viewLikers = {
                    let vc = LikesListVC(nibName: LikesListVC.identifier, bundle: nil)
                    vc.news_id = feedItem.news_id
                    vc.clicks_user_list = feedItem.clicks_user_list
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    self.present(vc, animated: true, completion: nil)
                }
                
                return cell
            }
        }else {
            if(indexPath.row == mediaArray.count){
                let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreCell.identifier) as! LoadMoreCell
                cell.tapLoadMore = {
                    self.showLoadingIndicator(in: self.view)
                    self.pageIndex += 1
                    self.getNewsMedia { response in
                        
                        if let feedItems = response.RESULT {
                            if(self.pageIndex == 1){
                                self.mediaArray.removeAll()
                            }
                            
                            self.haveNextItems = (feedItems.count > 0)
                            self.mediaArray.append(contentsOf: feedItems )
                            DispatchQueue.main.async {
                                self.hideLoadingIndicator()
                                self.tblNews.reloadData()
                            }
                        }
                    }
                }
                return cell
            }else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "NewsMediaListCell", for: indexPath) as! NewsMediaListCell
                
                var feedItem = mediaArray[indexPath.row]
                feedItem.created_date = getFormattedDate(from: feedItem.created_date ?? "", inputFormat: "yyyy-MM-dd HH:mm:ss", outputFormat: 0)
                
                cell.imgNews.sd_setImage(with: URL(string: mediaArray[indexPath.row].image ?? ""))
                
                cell.selectionStyle = .none
                cell.lblTitle.text = feedItem.title
                cell.lblDuration.text = getFormattedDate(from: feedItem.created_date ?? "", inputFormat: "yyyy-MM-dd HH:mm:ss", outputFormat: 0)
                let htmlString = feedItem.description
                
                if let data = htmlString?.data(using: .utf8),
                   let attributedString = try? NSAttributedString(data: data,
                                                                  options: [.documentType: NSAttributedString.DocumentType.html],
                                                                  documentAttributes: nil) {
                    
                    let labelWidth = cell.lblDesc.bounds.width
                    let labelFont = cell.lblDesc.font
                    
                    let maxSize = CGSize(width: labelWidth, height: CGFloat.infinity)
                    let textHeight = attributedString.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).height
                    
                    
                    let maxLines: CGFloat = 3
                    
                    if textHeight > ((labelFont?.lineHeight ?? 0) * maxLines) {
                        let nonNewlineAttributedString = NSMutableAttributedString(attributedString: attributedString)
                        nonNewlineAttributedString.mutableString.replaceOccurrences(of: "\n", with: "", options: .literal, range: NSRange(location: 0, length: nonNewlineAttributedString.length))
                        
                        let viewMoreString = NSAttributedString(string: "...View More", attributes: [
                            .foregroundColor: UIColor.black,
                            .font: UIFont.boldSystemFont(ofSize: 14)
                        ])
                        
                        nonNewlineAttributedString.append(viewMoreString)
                        cell.lblDesc.attributedText = nonNewlineAttributedString
                        cell.lblDesc.numberOfLines = Int(maxLines)
                    } else {
                        cell.lblDesc.attributedText = attributedString
                        cell.lblDesc.numberOfLines = 0
                    }
                    
                    cell.btnShare.tag = indexPath.row
                    cell.btnShare.addTarget(self, action:#selector(shareBtn(_ :)), for: .touchUpInside)
                }
                
                cell.btnShare.tag = indexPath.row
                cell.btnShare.addTarget(self, action:#selector(shareBtn(_ :)), for: .touchUpInside)
                
                
                // cell.setupReadMore()
                
                cell.readMoreTapped = {
                    let storyBoard = UIStoryboard(name: "Other", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "NewsMediaDetailVC") as! NewsMediaDetailVC
                    vc.arrayDetail = self.mediaArray
                    vc.scrollIndex = indexPath.row
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isfromMedia {
            let storyBoard = UIStoryboard(name: "Other", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "NewsMediaDetailVC") as! NewsMediaDetailVC
            vc.arrayDetail = mediaArray
            vc.scrollIndex = indexPath.row
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func shareBtn(_ sender: UIButton) {
        let url = URL(string: mediaArray[sender.tag].url ?? "")!
        
        let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        
        activityViewController.completionWithItemsHandler = { (activityType, completed, returnedItems, error) in
            if completed {
                
                let inputString = activityType?.rawValue ?? "Unknown App"
                let components = inputString.components(separatedBy: ".")
                if components.count > 1 {
                    let extractedValue = components[1]
                    print(extractedValue)
                    self.shareNewsMedia(newsId: self.mediaArray[sender.tag].id ?? "", platform: extractedValue, completion: {
                        print("shared success")
                    })
                } else {
                    print("The string doesn't have enough components.")
                }
            } else {
                print("Sharing was canceled or failed.")
            }
        }
        
        if let popoverController = activityViewController.popoverPresentationController {
            popoverController.sourceView = self.view
            
        }
        
        self.present(activityViewController, animated: true, completion: nil)
    }
}
extension UILabel {
    func numberOfVisibleLines() -> Int {
        let maxSize = CGSize(width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let textHeight = self.sizeThatFits(maxSize).height
        let lineHeight = self.font.lineHeight
        return Int(textHeight / lineHeight)
    }
}
extension NewsListingVC: FeedTableCellDelegate {
    
    func loadImg(urlStr: String, completion: @escaping (Data?, Error?) -> Void) {
        guard let imageUrl = URL(string: urlStr) else { return }
        self.loadImage(url: imageUrl) { data, error in
            completion(data, error)
        }
    }
    
    func updateTableView() {
        UIView.setAnimationsEnabled(false)
        tblNews.beginUpdates()
        tblNews.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func downloadMedia(_ img: UIImage) {
        self.downloadTapped(img)
    }
    
    func shareMedia(_ img: UIImage,feedItem:FeedItem) {
        self.shareTapped(img,feedItem:feedItem)
    }
    
    func showNewsDetailFor(_ id: String, _ img: UIImage?) {
        self.showNewsDetailView(id, img)
    }
}
