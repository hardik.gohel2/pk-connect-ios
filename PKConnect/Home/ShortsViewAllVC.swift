//
//  ShortsViewAllVC.swift
//  PKConnect
//
//  Created by admin on 16/12/23.
//

import UIKit

class ShortsViewAllVC: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!{
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
    var shortsArray : [AllShorts]?
    let numberOfColumns: CGFloat = 3
       let spacing: CGFloat = 1
       
    override func viewDidLoad() {
        super.viewDidLoad()
        let layout = UICollectionViewFlowLayout()
                layout.minimumInteritemSpacing = 1
                layout.minimumLineSpacing = 1
                
                collectionView.collectionViewLayout = layout
    }
    
    @IBAction func onClickBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ShortsViewAllVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shortsArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShortsDetailCVCell", for: indexPath) as! ShortsDetailCVCell
        cell.imgShorts.sd_setImage(with: URL(string: shortsArray?[indexPath.row].video_thumb ?? ""))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let width = (collectionView.bounds.width - 4) / 3
           let height = width * 1.5
           return CGSize(width: width, height: height)
       }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "MyShotsVC") as! MyShotsVC
        vc.shortList = self.shortsArray ?? []
        vc.rowIndex = indexPath.row
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
