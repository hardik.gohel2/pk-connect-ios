

import UIKit

protocol ContainerToMaster {
    func changeTopViewTitleTo(newTitle: String)
}

class TabBarBaseController: BaseViewController, UITabBarDelegate  {
    
//    @IBOutlet weak var Shorts: UIView!
    @IBOutlet private var homeView: UIView!
    @IBOutlet private var YSRSToriesView: UIView!
    @IBOutlet private var taskView: UIView!
    @IBOutlet private var dashboardView: UIView!
    @IBOutlet private var connectView: UIView!
    
    @IBOutlet private var tabBar: UITabBar!
    
    private var isHamburgerMenuShown = false
    
    var containerToMaster: ContainerToMaster?
    var storyFeed = [FeedItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        
       // view.backgroundColor = UIColor(hexString: "062D8C")
        view.backgroundColor = .blue
        
        for tabBarItem in tabBar.items! {
            tabBarItem.title = tabBarItem.title?.localize()
        }
        self.tabBar.selectedItem = tabBar.items?.first
        
        homeView.isHidden = false
        YSRSToriesView.isHidden = true
        taskView.isHidden = true
        dashboardView.isHidden = true
        connectView.isHidden = true
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        switch item.title {
        case "Home".localize():
            containerToMaster?.changeTopViewTitleTo(newTitle: String(format: "Hi %@".localize(), (getRetrievedUser()?.user_name ?? "")))
            homeView.isHidden = false
            YSRSToriesView.isHidden = true
            taskView.isHidden = true
            dashboardView.isHidden = true
            connectView.isHidden = true
            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: "0")

            //YSR Stories
        case "Shorts".localize():
            containerToMaster?.changeTopViewTitleTo(newTitle: item.title ?? "")
            homeView.isHidden = true
            YSRSToriesView.isHidden = false
            taskView.isHidden = true
            dashboardView.isHidden = true
            connectView.isHidden = true
            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifierStories"), object: "1")

        case "Task".localize():
            containerToMaster?.changeTopViewTitleTo(newTitle: item.title ?? "")
            homeView.isHidden = true
            YSRSToriesView.isHidden = true
            taskView.isHidden = false
            dashboardView.isHidden = true
            connectView.isHidden = true
            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: "2")

           
        case "Leaderboard".localize():
            containerToMaster?.changeTopViewTitleTo(newTitle: item.title ?? "")
            YSRSToriesView.isHidden = true
            homeView.isHidden = true
            taskView.isHidden = true
            dashboardView.isHidden = false
            connectView.isHidden = true
            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: "3")
         //YSR Connect
        case "PK Connect".localize():
            containerToMaster?.changeTopViewTitleTo(newTitle: item.title ?? "")
            homeView.isHidden = true
            YSRSToriesView.isHidden = true
            taskView.isHidden = true
            dashboardView.isHidden = true
            connectView.isHidden = false
            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: "4")

        default:
            homeView.isHidden = false
            YSRSToriesView.isHidden = true
            taskView.isHidden = true
            dashboardView.isHidden = true
            connectView.isHidden = true
            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: "5")

        }
    }
    
}


