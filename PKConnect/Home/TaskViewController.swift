//
//  TaskViewController.swift
//  KCR Sainyam
//
//  Created by Poshitha Gajjala on 5/28/22.
//

import UIKit
import FBSDKShareKit
 import Swifter
import AuthenticationServices

class TaskViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, instaPopupDismiss, SharingDelegate, ASWebAuthenticationPresentationContextProviding  {
    
    func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        return self.view.window ?? ASPresentationAnchor()
    }
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var imgpend: UIImageView!
    @IBOutlet weak var imgcomp: UIImageView!
    
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var completedView: UIView!
    @IBOutlet weak var completedBtn: UIButton!
    @IBOutlet weak var pendingBtn: UIButton!
    @IBOutlet weak var pendingView: UIView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var myTable: UITableView!
    //    let userCalendar = Calendar.current
    //    let requestedComponent: Set<Calendar.Component> = [.day,.hour,.minute]
    //    var isPendingTask = true
    //
    //    var once = true
    //    var pageIndex = 1
    //    var completedTasks: Int?
    //    var totalTasks: Int?
    //    var allTasks: AllTasksModel?
    //    var overallTasks: AllTasksModel?
    //    var task111: [TaskResult]?
    //    var facebookTaskID: String?
    //    var documentInteractionController: UIDocumentInteractionController = UIDocumentInteractionController()
    //
    //    private var commentList = [Comment]()
    //    var task: TaskResult?
    //    var today: String?
    //    var userProfile: UserProfile?
    //
    //        override func viewDidLoad() {
    //        super.viewDidLoad()
    //
    //            //LinkManager.shared.delegate = self
    //
    //            pendingBtn.setTitle("Pending".localize(), for: .normal)
    //            completedBtn.setTitle("Completed".localize(), for: .normal)
    //
    //            let date = Date()
    //            // Create Date Formatter
    //            let dateFormatter = DateFormatter()
    //
    //            // Set Date Format
    //            dateFormatter.dateFormat = "yyyy-MM-dd"
    //
    //            // Convert Date to String
    //            dateFormatter.string(from: date)
    //
    //            today = dateFormatter.string(from: date)
    //            print("asssaasdsa", today)
    //        completedView.backgroundColor = .white
    //        pendingView.backgroundColor = UIColor(hexString: "#FFCE1A")
    //        //
    //        //        completedTasks = 2
    //        //        totalTasks = 3
    //        //
    //        //        var value3 = completedTasks/totalTasks
    //
    //
    //
    //            getAllPendingTasks(date: today ?? "2022-11-09", count: "1") { fullresponse in
    //            DispatchQueue.main.async {
    //
    ////                let pendingTasks = fullresponse.RESULT?.map({$0.task_id == "0"})
    ////                print(("sadghsad: ", pendingTasks.count))
    //                self.allTasks = fullresponse
    //                let pendingTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "0"})
    //                let completedTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "1" || $0.task_completed == "2"})
    //                print(("sadghsad: ", pendingTasks?.count))
    //                self.task111 = pendingTasks
    //
    //                let convertDoubleValue = self.allTasks?.TOTAL ?? 10
    //                let totalvalue = Double(convertDoubleValue).removeZerosFromEnd()
    //                let counts = completedTasks?.count ?? 0
    //                let complete: Double?
    //                complete = Double(counts)/(Double(totalvalue ?? "10") ?? 10)
    //                self.progressView.progress = Float(complete ?? 0)
    //                self.progressView.layer.cornerRadius = 5
    //                self.progressView.clipsToBounds = true
    //                self.progressView.layer.sublayers![1].cornerRadius = 5
    //                self.progressView.subviews[1].clipsToBounds = true
    //                self.progressLabel.text = "\(counts)/\(totalvalue ?? "10")"
    //
    //               // self.allTasks = fullresponse.RESULT?.map({$0.task_completed == "0"})
    //                self.myTable.reloadData()
    //                UIApplication.shared.endIgnoringInteractionEvents()
    //
    //            }
    //        }
    //
    //        myTable.dataSource = self
    //        myTable.delegate = self
    //
    //        // Do any additional setup after loading the view.
    //    }
    //    @objc func callClickTab(){
    //     //   showLoadingIndicator(in: view)
    //        viewWillAppear(true)
    //
    //        if isPendingTask{
    //            pageIndex = 1
    //            completedView.backgroundColor = .white
    //            pendingView.backgroundColor = UIColor(hexString: "#FFCE1A")
    //
    //            //        allTasks = [Any]
    //            getAllPendingTasks(date: today ?? "2022-11-09", count: "1") { fullresponse in
    //                DispatchQueue.main.async {
    //             //       self.hideLoadingIndicator()
    //
    //                    self.allTasks = fullresponse
    //                    let pendingTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "0"})
    //                    print(("sadghsad: ", pendingTasks?.count))
    //                    self.task111 = pendingTasks
    //
    //                    let completedTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "1" || $0.task_completed == "2"})
    //                    print(("sadghsad: ", pendingTasks?.count))
    //
    //                    let convertDoubleValue = self.allTasks?.TOTAL ?? 10
    //                    let totalvalue = Double(convertDoubleValue).removeZerosFromEnd()
    //                    let counts = completedTasks?.count ?? 0
    //                    let complete: Double?
    //                    complete = Double(counts)/(Double(totalvalue ) ?? 10)
    //                    self.progressView.progress = Float(complete ?? 0)
    //                    self.progressView.layer.cornerRadius = 5
    //                    self.progressView.clipsToBounds = true
    //                    self.progressView.layer.sublayers![1].cornerRadius = 5
    //                    self.progressView.subviews[1].clipsToBounds = true
    //                    self.progressLabel.text = "\(counts)/\(totalvalue )"
    //                    UIApplication.shared.endIgnoringInteractionEvents()
    //
    //                    // self.allTasks = fullresponse.RESULT?.map({$0.task_completed == "0"})
    //                    self.myTable.reloadData()
    //
    //                }
    //            }
    //        }else{
    //            pageIndex = 1
    //            completedView.backgroundColor = UIColor(hexString: "#FFCE1A")
    //            pendingView.backgroundColor = .white
    //
    //            //        self.allTasks = []
    //            getAllCompletedTasks(date: today ?? "2022-11-09", count: "1") { fullresponse in
    //                DispatchQueue.main.async {
    //                    self.allTasks = fullresponse
    //                    let pendingTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "2" || $0.task_completed == "1"})
    //                    print(("sadghsad: ", pendingTasks?.count))
    //                    self.task111 = pendingTasks
    //                    let completedTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "1" || $0.task_completed == "2"})
    //                    print(("sadghsad: ", pendingTasks?.count))
    //
    //                    let convertDoubleValue = self.allTasks?.TOTAL ?? 10
    //                    let totalvalue = Double(convertDoubleValue).removeZerosFromEnd()
    //                    let counts = completedTasks?.count ?? 0
    //                    let complete: Double?
    //                    complete = Double(counts)/(Double(totalvalue ) ?? 10)
    //                    self.progressView.progress = Float(complete ?? 0)
    //                    self.progressView.layer.cornerRadius = 5
    //                    self.progressView.clipsToBounds = true
    //                    self.progressView.layer.sublayers![1].cornerRadius = 5
    //                    self.progressView.subviews[1].clipsToBounds = true
    //                    self.progressLabel.text = "\(counts)/\(totalvalue )"
    //
    //                    UIApplication.shared.endIgnoringInteractionEvents()
    //
    //                    self.myTable.reloadData()
    //
    //                }
    //            }
    //        }
    //
    //    }
    //
    //    @objc func refresh() {
    //        if isPendingTask{
    //            pageIndex = 1
    //            completedView.backgroundColor = .white
    //          //  pendingView.backgroundColor = UIColor.darkBlueColor
    //
    //            //        allTasks = [Any]
    //            getAllPendingTasks(date: today ?? "2022-11-09", count: "1") { fullresponse in
    //                DispatchQueue.main.async {
    //                    self.refreshControl.endRefreshing()
    //
    //                    self.allTasks = fullresponse
    //                    let pendingTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "0"})
    //                    print(("sadghsad: ", pendingTasks?.count))
    //                    self.task111 = pendingTasks
    //
    //                    let completedTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "1" || $0.task_completed == "2"})
    //                    print(("sadghsad: ", pendingTasks?.count))
    //
    //                    let convertDoubleValue = self.allTasks?.TOTAL ?? 10
    //                    let totalvalue = Double(convertDoubleValue).removeZerosFromEnd()
    //                    let counts = completedTasks?.count ?? 0
    //                    let complete: Double?
    //                    complete = Double(counts)/(Double(totalvalue ) ?? 10)
    //                    self.progressView.progress = Float(complete ?? 0)
    //                    self.progressView.layer.cornerRadius = 5
    //                    self.progressView.clipsToBounds = true
    //                    self.progressView.layer.sublayers![1].cornerRadius = 5
    //                    self.progressView.subviews[1].clipsToBounds = true
    //                    self.progressLabel.text = "\(counts)/\(totalvalue )"
    //
    //                    // self.allTasks = fullresponse.RESULT?.map({$0.task_completed == "0"})
    //                    self.myTable.reloadData()
    //                }
    //            }
    //        }else{
    //            pageIndex = 1
    //            //completedView.backgroundColor = UIColor.darkBlueColor
    //            pendingView.backgroundColor = .white
    //
    //            //        self.allTasks = []
    //            getAllCompletedTasks(date: today ?? "2022-11-09", count: "1") { fullresponse in
    //                DispatchQueue.main.async {
    //                    self.refreshControl.endRefreshing()
    //
    //                    //                let pendingTasks = fullresponse.RESULT?.map({$0.task_id == "0"})
    //                    //                print(("sadghsad: ", pendingTasks.count))
    //                    self.allTasks = fullresponse
    //                    let pendingTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "2" || $0.task_completed == "1"})
    //                    print(("sadghsad: ", pendingTasks?.count))
    //                    self.task111 = pendingTasks
    //                    let completedTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "1" || $0.task_completed == "2"})
    //                    print(("sadghsad: ", pendingTasks?.count))
    //
    //                    let convertDoubleValue = self.allTasks?.TOTAL ?? 10
    //                    let totalvalue = Double(convertDoubleValue).removeZerosFromEnd()
    //                    let counts = completedTasks?.count ?? 0
    //                    let complete: Double?
    //                    complete = Double(counts)/(Double(totalvalue ) ?? 10)
    //                    self.progressView.progress = Float(complete ?? 0)
    //                    self.progressView.layer.cornerRadius = 5
    //                    self.progressView.clipsToBounds = true
    //                    self.progressView.layer.sublayers![1].cornerRadius = 5
    //                    self.progressView.subviews[1].clipsToBounds = true
    //                    self.progressLabel.text = "\(counts)/\(totalvalue )"
    //
    //
    //                    // self.allTasks = fullresponse.RESULT?.map({$0.task_completed == "0"})
    //
    //                    self.myTable.reloadData()
    //                }
    //            }
    //        }
    //    }
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //        print("<<<--viewWillAppear-->>>")
    //        NotificationCenter.default.removeObserver(self, name: Notification.Name("APICALL"), object: nil)
    //        NotificationCenter.default.addObserver(self, selector: #selector(callClickTab), name: Notification.Name("APICALL"), object: nil)
    //
    //        DispatchQueue.main.async {
    //                NotificationCenter.default.addObserver(self, selector: #selector(self.DirectTask), name: Notification.Name("DirectTask"), object: nil)
    //        }
    //    }
    //    override func viewDidDisappear(_ animated: Bool) {
    //        super.viewDidDisappear(animated)
    //        self.once = true
    //        NotificationCenter.default.removeObserver(self, name: Notification.Name("DirectTask"), object: nil)
    //    }
    //    @objc func DirectTask(notification:Notification){
    //        if let id = (notification.object as? NSArray)?[0]{
    //            print(id)
    //            if self.task111?.count != 0{
    //                for i in 0..<(self.task111?.count ?? 0){
    //                    if self.task111?[i].task_id == "\(id)"{
    //                        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
    //                        guard self.navigationController?.viewControllers.contains(where: {$0.isKind(of: TaskDetailViewController.self)}) == false else { return }
    //
    //                        let taskDetailVC = storyBoard.instantiateViewController(withIdentifier: "TaskDetailViewController") as! TaskDetailViewController
    //                        taskDetailVC.recievetask = self.task111?[i]
    //                        self.navigationController?.pushViewController(taskDetailVC, animated: true)
    //                        NotificationCenter.default.removeObserver(self, name: Notification.Name("DirectTask"), object: nil)
    //
    //                        return
    //                    }
    //                }
    //            }
    //        }
    //    }
    //    @IBAction func pendingClicked(_ sender: Any) {
    //        isPendingTask = true
    //        pageIndex = 1
    //        completedView.backgroundColor = .white
    //        pendingView.backgroundColor = UIColor(hexString: "#FFCE1A")
    //
    //        //        allTasks = [Any]
    //        getAllPendingTasks(date: today ?? "2022-11-09", count: "1") { fullresponse in
    //            DispatchQueue.main.async {
    //
    //                self.allTasks = fullresponse
    //                let pendingTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "0"})
    //                print(("sadghsad: ", pendingTasks?.count))
    //                self.task111 = pendingTasks
    //
    //                let completedTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "1" || $0.task_completed == "2"})
    //                print(("sadghsad: ", pendingTasks?.count))
    //
    //                let convertDoubleValue = self.allTasks?.TOTAL ?? 10
    //                let totalvalue = Double(convertDoubleValue).removeZerosFromEnd()
    //                let counts = completedTasks?.count ?? 0
    //                let complete: Double?
    //                complete = Double(counts)/(Double(totalvalue ?? "10") ?? 10)
    //                self.progressView.progress = Float(complete ?? 0)
    //                self.progressView.layer.cornerRadius = 5
    //                self.progressView.clipsToBounds = true
    //                self.progressView.layer.sublayers![1].cornerRadius = 5
    //                self.progressView.subviews[1].clipsToBounds = true
    //                self.progressLabel.text = "\(counts)/\(totalvalue ?? "10")"
    //
    //
    //
    //               // self.allTasks = fullresponse.RESULT?.map({$0.task_completed == "0"})
    //                self.myTable.reloadData()
    //
    //            }
    //        }
    //
    //    }
    //    @IBAction func completedClicked(_ sender: Any) {
    //        isPendingTask = false
    //
    //        pageIndex = 1
    //        completedView.backgroundColor = UIColor(hexString: "#FFCE1A")
    //        pendingView.backgroundColor = .white
    //
    //        //        self.allTasks = []
    //        getAllCompletedTasks(date: today ?? "2022-11-09", count: "1") { fullresponse in
    //            DispatchQueue.main.async {
    //
    //                //                let pendingTasks = fullresponse.RESULT?.map({$0.task_id == "0"})
    //                //                print(("sadghsad: ", pendingTasks.count))
    //                self.allTasks = fullresponse
    //                let pendingTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "2" || $0.task_completed == "1"})
    //                print(("sadghsad: ", pendingTasks?.count))
    //                self.task111 = pendingTasks
    //                let completedTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "1" || $0.task_completed == "2"})
    //                print(("sadghsad: ", pendingTasks?.count))
    //
    //                let convertDoubleValue = self.allTasks?.TOTAL ?? 10
    //                let totalvalue = Double(convertDoubleValue).removeZerosFromEnd()
    //                let counts = completedTasks?.count ?? 0
    //                let complete: Double?
    //                complete = Double(counts)/(Double(totalvalue) ?? 10)
    //                self.progressView.progress = Float(complete ?? 0)
    //                self.progressView.layer.cornerRadius = 5
    //                self.progressView.clipsToBounds = true
    //                self.progressView.layer.sublayers![1].cornerRadius = 5
    //                self.progressView.subviews[1].clipsToBounds = true
    //                self.progressLabel.text = "\(counts)/\(totalvalue )"
    //
    //
    //                // self.allTasks = fullresponse.RESULT?.map({$0.task_completed == "0"})
    //                self.myTable.reloadData()
    //
    //            }
    //        }
    //    }
    //    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //
    //        if self.task111?.count == 0 {
    //            self.myTable.setEmptyMessage("No record found.".localize())
    //        } else {
    //            self.myTable.restore()
    //        }
    //
    //        return self.task111?.count ?? 0
    //    }
    //
    //    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskPendingCell", for: indexPath) as! TaskPendingCell
    //
    //        cell.taskTitle.text = self.task111?[indexPath.row].task_title ?? ""
    //        cell.taskFullDesc.text = self.task111?[indexPath.row].task_description ?? ""
    //        cell.taskRewards.text = self.task111?[indexPath.row].points ?? ""
    //        cell.taskDate.text = calculateFormat(date : self.task111?[indexPath.row].start_date , formatterReq : "dd.MM.yyyy")
    //
    //        let formatter = DateFormatter()
    //        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    //        let startTime = Date()
    //        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    //        formatter.timeZone = TimeZone.current
    //        let current_date = formatter.string(from: startTime)
    //        let SnewDate = formatter.date(from: current_date)!
    //
    //        let EnewDate = formatter.date(from: (self.task111?[indexPath.row].end_date)!) ?? Date()
    //
    ////        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    ////        let sDate = formatter.string(from: newDate)
    ////        let sDate = formatter.string(from: newDate)
    //
    //
    //        let timeDifference = userCalendar.dateComponents(requestedComponent, from: SnewDate, to: EnewDate)
    //        print(timeDifference)
    //        cell.taskExpire.text = "\(timeDifference.day!.addLeadingZero())d:\(timeDifference.hour!.addLeadingZero())h:\(timeDifference.minute!.addLeadingZero())m"
    //
    //        if self.task111?[indexPath.row].task_type == .facebook {
    //            cell.taskIcon.image = UIImage.init(named: "facebook")
    //            cell.taskShareBtn.setTitle(self.task111?[indexPath.row].action?.capitalizedSentence.localize() ?? "", for: .normal)
    //        } else  if self.task111?[indexPath.row].task_type == .whatsapp {
    //            cell.taskIcon.image = UIImage.init(named: "whatsapp")
    //            cell.taskShareBtn.setTitle("Perform".localize(), for: .normal)
    //        } else  if self.task111?[indexPath.row].task_type == .instagram {
    //            cell.taskIcon.image = UIImage.init(named: "instagram")
    //            cell.taskShareBtn.setTitle(self.task111?[indexPath.row].action?.capitalizedSentence.localize() ?? "", for: .normal)
    //        } else  if self.task111?[indexPath.row].task_type == .twitter {
    //            cell.taskIcon.image = UIImage.init(named: "twitter")
    //            cell.taskShareBtn.setTitle(self.task111?[indexPath.row].action?.capitalizedSentence.localize() ?? "", for: .normal)
    //        } else  if self.task111?[indexPath.row].task_type == .youtube {
    //            cell.taskIcon.image = UIImage.init(named: "youtube_new")
    //            cell.taskShareBtn.setTitle(self.task111?[indexPath.row].action?.capitalizedSentence.localize() ?? "", for: .normal)
    //        }else  if self.task111?[indexPath.row].task_type == .offline {
    //            cell.taskIcon.image = UIImage.init(named: "offline")
    //            cell.taskShareBtn.setTitle("Perform".localize(), for: .normal)
    //        }
    //
    //        if self.task111?[indexPath.row].task_completed_user_list?.count == 0 || self.task111?[indexPath.row].task_completed_user_list?.count ?? 0 <= 2 {
    //            cell.completedNumbers.isHidden = true
    //            cell.like1.isHidden = true
    //            cell.like2.isHidden = true
    //
    //        } else {
    //            cell.like1.isHidden = false
    //            cell.like2.isHidden = false
    //            cell.completedNumbers.isHidden = false
    //
    //            cell.completedNumbers.text = "+ \(String(describing: self.task111?[indexPath.row].task_completed_user_list?.count ?? 0)) Completed task".localize()
    //
    //            downloadImageFrom(urlString: self.task111?[indexPath.row].task_completed_user_list?[0].user_image ?? "") { downloadedImage in
    //                DispatchQueue.main.async {
    //                    cell.like1.layer.cornerRadius = 15
    //                    cell.like1.clipsToBounds = true
    //                    cell.like1.image = downloadedImage
    //                }
    //            }
    //            downloadImageFrom(urlString: self.task111?[indexPath.row].task_completed_user_list?[1].user_image ?? "") { downloadedImage in
    //                DispatchQueue.main.async {
    //                    cell.like2.layer.cornerRadius = 15
    //                    cell.like2.clipsToBounds = true
    //                    cell.like2.image = downloadedImage
    //                }
    //            }
    //        }
    //
    //        if self.task111?[indexPath.row].task_completed == "0" {
    //            cell.taskShareBtn.setTitle(self.task111?[indexPath.row].action?.capitalizedSentence.localize(), for: .normal)
    //            cell.taskShareBtn.backgroundColor = UIColor(hexString: "#FFCE1A")
    //            cell.taskShareBtn.setTitleColor(UIColor(hexString: "#000000"), for: .normal)
    //            cell.imgTimer.isHidden = false
    //            cell.constWidthTimer.constant = 15
    //        } else {
    //            cell.taskShareBtn.setTitle("Completed".localize(), for: .normal)
    //            cell.taskShareBtn.backgroundColor = UIColor(hexString: "#087126")
    //            cell.taskShareBtn.setTitleColor(UIColor(hexString: "#FFFFFF"), for: .normal)
    //            cell.taskExpire.text = calculateFormat(date : self.task111?[indexPath.row].completed_time , formatterReq : "dd.MM.yyyy hh:mm a")
    //            cell.imgTimer.isHidden = true
    //            cell.constWidthTimer.constant = 0
    //        }
    //
    //
    //        cell.taskShareNormal.tag = indexPath.row
    //        //    cell.imageTableView.image = UIImage(named: name[indexPath.row])
    //        cell.taskShareNormal.isUserInteractionEnabled = true
    //        cell.taskShareNormal.addTarget(self, action: #selector(shareClicked), for: .touchUpInside)
    //
    //        cell.taskShareBtn.tag = indexPath.row
    //        cell.taskShareBtn.isUserInteractionEnabled = true
    //        cell.taskShareBtn.addTarget(self, action: #selector(shareClickedTask), for: .touchUpInside)
    //
    //        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(likesClicked))
    //
    //        cell.completedNumberView.isUserInteractionEnabled = true
    //        cell.completedNumberView.tag = indexPath.row
    //        cell.completedNumberView.addGestureRecognizer(tapGestureRecognizer)
    //
    //        return cell
    //    }
    //
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //
    //        let pendingTasks =  self.task111?.filter({$0.task_completed == "0"})
    //        if indexPath.row == (pendingTasks?.count ?? 0) - 1 {
    //            pageIndex += 1
    //
    //            if(isPendingTask == true){
    //
    //                getAllPendingTasks(date: today ?? "2022-11-09", count: String(pageIndex)) { fullresponse in
    //
    //                    if fullresponse.RESULT?.count == 0 {
    //
    //                    } else {
    //                        let pendingTasks = fullresponse.RESULT?.filter({$0.task_completed == "0"})
    //                        self.task111?.append(contentsOf: pendingTasks ?? [])
    //                        DispatchQueue.main.async {
    //                            self.myTable.reloadData()
    //                        }
    //                    }
    //                }
    //
    //            } else{
    //                getAllCompletedTasks(date: today ?? "2022-11-09", count: String(pageIndex)) { fullresponse in
    //
    //                    if fullresponse.RESULT?.count == 0 {
    //
    //                    } else {
    //                        let pendingTasks =  fullresponse.RESULT?.filter({$0.task_completed == "2" || $0.task_completed == "1"})
    //
    //                        self.task111?.append(contentsOf: pendingTasks ?? [])
    //                        DispatchQueue.main.async {
    //                            self.myTable.reloadData()
    //                        }
    //                    }
    //                }
    //            }
    //
    //        }
    //    }
    //
    //    @objc func likesClicked(_ sender:AnyObject){
    //        print("you tap image number:", sender.view.tag)
    //
    ////        self.task111?.comleted
    //        if self.task111?[sender.view.tag].task_completed_user_list?.count == 0 {
    //
    //        } else {
    //            let vc = UIStoryboard(name: "Home", bundle: nil)
    //            let taskDetailVC = vc.instantiateViewController(withIdentifier: "TaskCompletedVC") as! TaskCompletedVC
    //            taskDetailVC.task = self.task111?[sender.view.tag]
    //            self.navigationController?.present(taskDetailVC, animated: true)
    //        }
    //
    //    }
    //
    //    @objc func shareClickedTask(sender: UIButton) {
    //        if self.task111?[sender.tag].task_completed == "0" {
    //            self.share(indexPath: sender.tag)
    //        }
    //    }
    //
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return 222
    //    }
    //
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //
    //        if self.task111?[indexPath.row].task_completed == "0" {
    //            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
    //            let taskDetailVC = storyBoard.instantiateViewController(withIdentifier: "TaskDetailViewController") as! TaskDetailViewController
    //            taskDetailVC.indexpath = indexPath.row
    //            taskDetailVC.recievetask = self.task111?[indexPath.row]
    //            self.navigationController?.pushViewController(taskDetailVC, animated: true)
    //        } else {
    //
    //        }
    //
    //    }
    //
    //
    //    @objc func shareClicked(sender: UIButton){
    //
    //        shareMediaforDeeplinkTask(view: self, TaskResult: (self.task111?[sender.tag])!)
    //
    ////        let firstActivityItem = self.task111?[sender.tag].task_title ?? ""
    ////        let secondActivityItem = self.task111?[sender.tag].task_description ?? ""
    ////        let activityViewController : UIActivityViewController = UIActivityViewController(
    ////            activityItems: [firstActivityItem, secondActivityItem], applicationActivities: nil)
    ////        // This lines is for the popover you need to show in iPad
    ////        activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)
    ////        // This line remove the arrow of the popover to show in iPad
    ////        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
    ////        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
    ////        // Pre-configuring activity items
    ////        activityViewController.activityItemsConfiguration = [
    ////            UIActivity.ActivityType.message
    ////        ] as? UIActivityItemsConfigurationReading
    ////
    ////        // Anything you want to exclude
    ////        activityViewController.excludedActivityTypes = [
    ////            UIActivity.ActivityType.postToWeibo,
    ////            UIActivity.ActivityType.print,
    ////            UIActivity.ActivityType.assignToContact,
    ////            UIActivity.ActivityType.saveToCameraRoll,
    ////            UIActivity.ActivityType.addToReadingList,
    ////            UIActivity.ActivityType.postToFlickr,
    ////            UIActivity.ActivityType.postToVimeo,
    ////            UIActivity.ActivityType.postToTencentWeibo,
    ////            UIActivity.ActivityType.postToFacebook
    ////        ]
    ////
    ////        activityViewController.isModalInPresentation = true
    ////        self.present(activityViewController, animated: true, completion: nil)
    //    }
    //
    //    func dismisss(type: String,TaskData: TaskResult) {
    //
    //        print("sahjshjkashd: ", type)
    //
    //        if type == "instagram" {
    //            //SHUBHENDU CODE INSTAGRAM
    //            guard let instagram = URL(string: TaskData.post_url ?? "https://www.instagram.com/") else { return }
    //            UIApplication.shared.open(instagram)
    //
    //        } else if type == "whatsapp" {
    //            self.showLoadingIndicator(in: self.view)
    //
    //            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
    //                let urlWhats = "whatsapp://app"
    //                if TaskData.action == "video"{
    //                    if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
    //                        if let whatsappURL = NSURL(string: urlString) {
    //
    //                            if UIApplication.shared.canOpenURL(whatsappURL as URL) {
    //                                let fileExt = ".mp4"
    //                                let videoFileName = TaskData.task_title.removeSpace() + (fileExt)
    //                                let tmpPathOriginal = NSTemporaryDirectory() as String
    //                                let filePathOriginal = tmpPathOriginal + videoFileName
    //
    //                                DispatchQueue.global(qos: .background).async {
    //                                    let url = URL(string: TaskData.task_media_set?[0].media_url ?? "")!
    //                                    var request = URLRequest(url: url)
    //                                    request.httpMethod = "GET"
    //                                    let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
    //
    //                                        DispatchQueue.main.async {
    //                                        }
    //
    //                                        if(error != nil){
    //                                            print("\n\nsome error occured\n\n")
    //                                            self.hideLoadingIndicator()
    //                                            self.showToast(message: "An error occurred. Please try again".localize())
    //                                            return
    //                                        }
    //                                        if let response = response as? HTTPURLResponse{
    //                                            if response.statusCode == 200{
    //                                                DispatchQueue.main.async {
    //                                                    if let data = data{
    //                                                        if let _ = try? data.write(to: URL(fileURLWithPath: filePathOriginal), options: Data.WritingOptions.atomic){
    //                                                            print("\n\nurl data written\n\n")
    //                                                            UISaveVideoAtPathToSavedPhotosAlbum(filePathOriginal,nil,nil,nil)
    //
    //                                                            let tempFile = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/whatsAppTmp.wam")
    //                                                            do {
    //                                                                do {
    //                                                                    let videoURL = URL(fileURLWithPath: filePathOriginal)
    //                                                                    self.hideLoadingIndicator()
    //
    //                                                                    self.documentInteractionController = UIDocumentInteractionController(url: videoURL)
    //                                                                    self.documentInteractionController.uti = "net.whatsapp.movie"
    //                                                                    self.documentInteractionController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
    //
    //                                                                }
    //                                                                catch let error {
    //                                                                    print(error)
    //                                                                    self.hideLoadingIndicator()
    //
    //                                                                }
    //
    //                                                            } catch {
    //                                                                self.hideLoadingIndicator()
    //
    //                                                                print(error)
    //                                                            }
    //                                                        }
    //                                                        else{
    //                                                            self.hideLoadingIndicator()
    //
    //                                                            print("\n\nerror again\n\n")
    //                                                        }
    //                                                    }//end if let data
    //                                                }//end dispatch main
    //                                            }//end if let response.status
    //                                        }
    //                                    })
    //                                    task.resume()
    //                                }
    //                            }
    //                        }
    //                    }
    //                }else if TaskData.action == "link"{
    //                    let message = TaskData.post_url
    //                     var queryCharSet = NSCharacterSet.urlQueryAllowed
    //
    //                     // if your text message contains special char like **+ and &** then add this line
    //                     queryCharSet.remove(charactersIn: "+&")
    //
    //                    if let escapedString = message!.addingPercentEncoding(withAllowedCharacters: queryCharSet) {
    //                         if let whatsappURL = URL(string: "whatsapp://send?text=\(escapedString)") {
    //                             if UIApplication.shared.canOpenURL(whatsappURL) {
    //                                 UIApplication.shared.open(whatsappURL, options: [: ], completionHandler: nil)
    //                                 self.hideLoadingIndicator()
    //                             } else {
    //                                 debugPrint("please install WhatsApp")
    //                             }
    //                         }
    //                     }
    //                }else{
    //                    if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
    //                        if let whatsappURL = NSURL(string: urlString) {
    //
    //                            if UIApplication.shared.canOpenURL(whatsappURL as URL) {
    //
    //                                let imgURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    //                                let fileExt = ".jpg"
    //
    //                                let fileName = TaskData.task_title.removeSpace() + (fileExt)
    //                                let fileURL = imgURL.appendingPathComponent(fileName)
    //
    //                                if let url = URL(string: TaskData.task_media_set?[0].media_url ?? ""),
    //                                   let data = try? Data(contentsOf: url),
    //                                   let image = UIImage(data: data) {
    //                                    if let image = UIImage(data: data)  {
    //                                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
    //                                        if let imageData = image.jpegData(compressionQuality: 0.75) {
    //                                            let tempFile = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("/Documents/whatsAppTmp.wai")
    //                                            do {
    //                                                try imageData.write(to: tempFile, options: .atomic)
    //                                                self.hideLoadingIndicator()
    //
    //                                                self.documentInteractionController = UIDocumentInteractionController(url: tempFile)
    //                                                self.documentInteractionController.uti = "public.image"
    //                                                self.documentInteractionController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
    //
    //                                            } catch {
    //                                                print(error)
    //                                                DispatchQueue.main.async {
    //                                                    self.hideLoadingIndicator()
    //                                                }
    //                                            }
    //                                        }
    //                                    }
    //                                } else {
    //                                    DispatchQueue.main.async {
    //                                        self.hideLoadingIndicator()
    //                                    }
    //                                    // Cannot open whatsapp
    //                                }
    //                            }
    //
    //                        }
    //                    }
    //                }
    //            }
    //        } else {
    //            guard let url  = URL(string: "youtube://app") else {return}
    //            if UIApplication.shared.canOpenURL(url) {
    //
    //
    //            }else{
    //
    //            }
    //        }
    //    }
    //
    //    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
    //        print("sddsddsd")
    //        showLoadingIndicator(in: view)
    //        self.postfacebookTwitter(task_id: self.facebookTaskID ?? "") {
    //            DispatchQueue.main.async {
    //                self.hideLoadingIndicator()
    //                self.callClickTab()
    //                self.dismiss(animated: true)
    //            }
    //        }
    //    }
    //
    //    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
    //        print("dsdsdsdd")
    //
    //    }
    //
    //    func sharerDidCancel(_ sharer: Sharing) {
    //        print("sddsdssssssssdsd")
    //
    //    }
    //}
    //extension TaskViewController {
    //    func calculateFormat(date : String? , formatterReq : String) -> String{
    //        let formatter = DateFormatter()
    //        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    //        let newDate = formatter.date(from: date!) ?? Date()
    //        formatter.dateFormat = formatterReq
    //        return formatter.string(from: newDate)
    //    }
    //
    //    func share(indexPath: Int){
    //        let task = self.task111?[indexPath]
    //        self.task = task
    //        switch task?.task_type {
    //        case .online:
    //            print("online")
    //
    //        case .offline:
    //            print("offline")
    //            let storyboard = UIStoryboard(name: "Home", bundle: nil)
    //            let controller = storyboard.instantiateViewController(withIdentifier: "UploadScreenshotVC") as! UploadScreenshotVC
    //            controller.taskid = task?.task_id ?? ""
    //            controller.modalPresentationStyle = .overCurrentContext
    //            controller.modalTransitionStyle = .crossDissolve
    //            self.present(controller, animated: true, completion: nil)
    //
    //        case .whatsapp:
    //            print("whatsapp")
    //            let vc = UIStoryboard(name: "Home", bundle: nil)
    //            let taskDetailVC = vc.instantiateViewController(withIdentifier: "InstagramPopUP") as! InstagramPopUP
    //            taskDetailVC.delegate = self
    //            taskDetailVC.tasktype = "whatsapp"
    //            taskDetailVC.taskData = task
    //
    //            //            vc.task = task
    //            self.navigationController?.present(taskDetailVC, animated: true)
    //
    //        case .facebook:
    //            print("facebook")
    //            self.facebookTaskID = self.task?.task_id ?? ""
    //
    //            if self.task?.action == "post" {
    //                downloadImageFrom(urlString: self.task?.task_media_set?[0].media_url ?? "https://pkconnect.s3.ap-south-1.amazonaws.com/Media/63763f85de0b4.314658624_659474292302575_3356630214966091798_n.jpg") { downloadedImage in
    //                    DispatchQueue.main.async {
    //                        let photoss = downloadedImage
    //                        let photo = SharePhoto(image: photoss, userGenerated: true)
    //                        let content2 = SharePhotoContent()
    //                        content2.photos = [photo]
    //                        let dialog = ShareDialog(
    //                            fromViewController: self,
    //                            content: content2,
    //                            delegate: self
    //                        )
    //                        do {
    //                            try dialog.validate()
    //                            dialog.show()
    //                        } catch {
    //                            self.dismiss(animated: true)
    //                        }
    //                    }
    //                }
    //
    //            } else {
    //                var postTaskURL: String?
    //                if self.task?.post_url == "" || self.task?.post_url == nil {
    //                    postTaskURL = "https://www.facebook.com/jansuraajofficial/"
    //                } else {
    //                    postTaskURL = self.task?.post_url ?? ""
    //                }
    //
    //                guard let url = URL(string: postTaskURL ?? "https://www.facebook.com/jansuraajofficial/") else {
    //                    return dismiss(animated: true)
    //                }
    //
    //                let content = ShareLinkContent()
    //                content.contentURL = url
    //                let dialog = ShareDialog(
    //                    fromViewController: self,
    //                    content: content,
    //                    delegate: self
    //                )
    //                do {
    //                    try dialog.validate()
    //                    dialog.show()
    //                } catch {
    //                    dismiss(animated: true)
    //                }
    //            }
    //
    //        case .instagram:
    //            print("instagram")
    //            let vc = UIStoryboard(name: "Home", bundle: nil)
    //            let taskDetailVC = vc.instantiateViewController(withIdentifier: "InstagramPopUP") as! InstagramPopUP
    //            taskDetailVC.delegate = self
    //            taskDetailVC.tasktype = "instagram"
    //            taskDetailVC.taskData = task
    //
    //            //            vc.task = task
    //            self.navigationController?.present(taskDetailVC, animated: true)
    //
    //        case .twitter:
    //            print("twitter")
    //            showLoadingIndicator(in: view)
    //
    //            getProfile { profile in
    //                DispatchQueue.main.async { [self] in
    //                    userProfile = profile
    //
    //                    let post_URL = self.task?.post_url
    //                    let components = URLComponents(string: post_URL!)
    //                    let id = components?.path.split(separator: "/").last
    //                    print(id)
    //                    DispatchQueue.main.async {
    //                        self.hideLoadingIndicator()
    //                    }
    //
    //                    if userProfile?.twitter_username != "" || userProfile?.twitter_username != nil {
    //                        var swifter: Swifter!
    //                        if UserDefaults.standard.string(forKey: "twitterOAuthToken") == nil {
    //                            swifter = Swifter(consumerKey: "7dfm8RoglWqOMUbgz3ebbz38g", consumerSecret: "SuFQzZoF8DTX4YUi1HShqxZhyLDKKMoWPapeEcmathY6QkqxCj")
    //                            if let url = URL(string: "twitterkit-7dfm8RoglWqOMUbgz3ebbz38g://") {
    //                                swifter.authorize(withProvider: self, callbackURL: url) { (token, response) in
    //                                    UserDefaults.standard.set(token?.key, forKey: "twitterOAuthToken")
    //                                    UserDefaults.standard.set(token?.secret, forKey: "twitterOAuthSecret")
    //                                    print("signed in!!")
    //                                }
    //                            }
    //                        }
    //                        swifter = Swifter(consumerKey: "7dfm8RoglWqOMUbgz3ebbz38g", consumerSecret: "SuFQzZoF8DTX4YUi1HShqxZhyLDKKMoWPapeEcmathY6QkqxCj", oauthToken: UserDefaults.standard.string(forKey: "twitterOAuthToken") ?? "", oauthTokenSecret: UserDefaults.standard.string(forKey: "twitterOAuthSecret") ?? "")
    //                        if let url = URL(string: "twitterkit-7dfm8RoglWqOMUbgz3ebbz38g://") {
    //                            swifter.retweetTweet(forID: String(id!), success: {success in
    //                                print(success)
    //                                self.showToast(message: "Retweet success".localize())
    //                                self.postTask(task_id: self.task?.task_id ?? "") {
    //                                    DispatchQueue.main.async {
    //                                        self.callClickTab()
    //                                    }
    //                                }
    //                            },failure: {error in
    //                                print(error)
    //                                if error.localizedDescription.contains("You have already retweeted this Tweet."){
    //                                    self.showToast(message: "You have already retweeted this Tweet.".localize())
    //                                }else{
    //                                    self.showToast(message: error.localizedDescription)
    //
    //                                }
    //                            })
    //                        }
    //                    }else{
    //                        self.showToast(message: "Please login through twitter first".localize())
    //                    }
    //                }
    //            }
    //        case .youtube:
    //            print("youtube")
    //            let vc = UIStoryboard(name: "Home", bundle: nil)
    //            let taskDetailVC = vc.instantiateViewController(withIdentifier: "InstagramPopUP") as! InstagramPopUP
    //            taskDetailVC.delegate = self
    //            taskDetailVC.tasktype = "youtube"
    //            taskDetailVC.taskData = task
    //
    //            //            vc.task = task
    //            self.navigationController?.present(taskDetailVC, animated: true)
    //        default:
    //            print(task?.task_type ?? "")
    //
    //        }
    //    }
    //}
    //
    //extension Double {
    //    func removeZerosFromEnd() -> String {
    //        let formatter = NumberFormatter()
    //        let number = NSNumber(value: self)
    //        formatter.minimumFractionDigits = 0
    //        formatter.maximumFractionDigits = 16 //maximum digits in Double after dot (maximum precision)
    //        return String(formatter.string(from: number) ?? "")
    //    }
    //}
    //
    //extension String {
    //    var capitalizedSentence: String {
    //        // 1
    //        let firstLetter = self.prefix(1).capitalized
    //        // 2
    //        let remainingLetters = self.dropFirst().lowercased()
    //        // 3
    //        return firstLetter + remainingLetters
    //    }
    //}
    //extension Int {
    //    func addLeadingZero(_ leadingZeroCount:Int = 2)->String {
    //        return String(format: "%0\(leadingZeroCount)d", self)
    //    }
    //}

        let userCalendar = Calendar.current
        let requestedComponent: Set<Calendar.Component> = [.day,.hour,.minute]
        var isPendingTask = true
        
        var once = true
        var pageIndex = 1
        var completedTasks: Int?
        var totalTasks: Int?
        var allTasks: AllTasksModel?
        var overallTasks: AllTasksModel?
        var task111: [TaskResult]?
        var facebookTaskID: String?
        var documentInteractionController: UIDocumentInteractionController = UIDocumentInteractionController()
        
        var task: TaskResult?
        var today: String?
    //    var userProfile: UserProfile?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            
            
            //LinkManager.shared.delegate = self
            imgcomp.isHidden = true
            pendingBtn.backgroundColor = .black
            pendingBtn.titleLabel?.textColor = .white
            completedBtn.setTitleColor(UIColor.lightGray, for: .normal)
            //setTitleColor(UIColor(hexString: "#00862D"), for: .normal)
            
            
            pendingBtn.setTitle("Pending".localize(), for: .normal)
            completedBtn.setTitle("Completed".localize(), for: .normal)
            
            let date = Date()
            // Create Date Formatter
            let dateFormatter = DateFormatter()
            
            // Set Date Format
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            // Convert Date to String
            dateFormatter.string(from: date)
            
            today = dateFormatter.string(from: date)
            print("asssaasdsa", today)
            completedView.backgroundColor = .white
            pendingView.backgroundColor = UIColor(hexString: "#FFCE1A")
            
            
            
            getAllPendingTasks(date: today ?? "2022-11-09", count: "1") { fullresponse in
                DispatchQueue.main.async {
                    print(fullresponse)
                    self.allTasks = fullresponse
                    let pendingTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "0"})
                    let completedTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "1" || $0.task_completed == "2"})
                    print(("sadghsad: ", pendingTasks?.count))
                    self.task111 = pendingTasks
                    
                    let convertDoubleValue = self.allTasks?.TOTAL ?? 10
                    let totalvalue = Double(convertDoubleValue).removeZerosFromEnd()
                    let counts = completedTasks?.count ?? 0
                    let complete: Double?
                    complete = Double(counts)/(Double(totalvalue ?? "10") ?? 10)
                    self.progressView.progress = Float(complete ?? 0)
                    self.progressView.layer.cornerRadius = 5
                    self.progressView.clipsToBounds = true
                    self.progressView.layer.sublayers![1].cornerRadius = 5
                    self.progressView.subviews[1].clipsToBounds = true
                    self.progressLabel.text = "\(counts)/\(totalvalue ?? "10")"
                    
                    // self.allTasks = fullresponse.RESULT?.map({$0.task_completed == "0"})
                    self.myTable.reloadData()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                }
            }
            
            myTable.dataSource = self
            myTable.delegate = self
            
            // Do any additional setup after loading the view.
            
            refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
            myTable.addSubview(refreshControl)
            
        }
        
        @objc func callClickTab() {
            viewWillAppear(true)
            
            if isPendingTask{
                pageIndex = 1
                completedView.backgroundColor = .white
                pendingView.backgroundColor = UIColor(hexString: "#FFCE1A")
                
                
                getAllPendingTasks(date: today ?? "2022-11-09", count: "1") { fullresponse in
                    DispatchQueue.main.async {
                        //       self.hideLoadingIndicator()
                        
                        print(fullresponse)
                        self.allTasks = fullresponse
                        let pendingTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "0"})
                        print(("sadghsad: ", pendingTasks?.count))
                        self.task111 = pendingTasks
                        
                        let completedTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "1" || $0.task_completed == "2"})
                        print(("sadghsad: ", pendingTasks?.count))
                        
                        let convertDoubleValue = self.allTasks?.TOTAL ?? 10
                        let totalvalue = Double(convertDoubleValue).removeZerosFromEnd()
                        let counts = completedTasks?.count ?? 0
                        let complete: Double?
                        complete = Double(counts)/(Double(totalvalue ) ?? 10)
                        self.progressView.progress = Float(complete ?? 0)
                        self.progressView.layer.cornerRadius = 5
                        self.progressView.clipsToBounds = true
                        self.progressView.layer.sublayers![1].cornerRadius = 5
                        self.progressView.subviews[1].clipsToBounds = true
                        self.progressLabel.text = "\(counts)/\(totalvalue )"
                        UIApplication.shared.endIgnoringInteractionEvents()
                        
                        // self.allTasks = fullresponse.RESULT?.map({$0.task_completed == "0"})
                        self.myTable.reloadData()
                        
                    }
                }
            }else{
                pageIndex = 1
                completedView.backgroundColor = UIColor(hexString: "#FFCE1A")
                pendingView.backgroundColor = .white
                
                //        self.allTasks = []
                getAllCompletedTasks(date: today ?? "2022-11-09", count: "1") { fullresponse in
                    DispatchQueue.main.async {
                        self.allTasks = fullresponse
                        let pendingTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "2" || $0.task_completed == "1"})
                        print(("sadghsad: ", pendingTasks?.count))
                        self.task111 = pendingTasks
                        let completedTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "1" || $0.task_completed == "2"})
                        print(("sadghsad: ", pendingTasks?.count))
                        
                        let convertDoubleValue = self.allTasks?.TOTAL ?? 10
                        let totalvalue = Double(convertDoubleValue).removeZerosFromEnd()
                        let counts = completedTasks?.count ?? 0
                        let complete: Double?
                        complete = Double(counts)/(Double(totalvalue ) ?? 10)
                        self.progressView.progress = Float(complete ?? 0)
                        self.progressView.layer.cornerRadius = 5
                        self.progressView.clipsToBounds = true
                        self.progressView.layer.sublayers![1].cornerRadius = 5
                        self.progressView.subviews[1].clipsToBounds = true
                        self.progressLabel.text = "\(counts)/\(totalvalue )"
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        
                        self.myTable.reloadData()
                    }
                }
            }
        }
        
        
        @objc func refresh() {
            if isPendingTask{
                pageIndex = 1
                completedView.backgroundColor = .white
                //  pendingView.backgroundColor = UIColor.darkBlueColor
                
                getAllPendingTasks(date: today ?? "2022-11-09", count: "1") { fullresponse in
                    DispatchQueue.main.async {
                        self.refreshControl.endRefreshing()
                        
                        self.allTasks = fullresponse
                        let pendingTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "0"})
                        print(("sadghsad: ", pendingTasks?.count))
                        self.task111 = pendingTasks
                        
                        let completedTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "1" || $0.task_completed == "2"})
                        print(("sadghsad: ", pendingTasks?.count))
                        
                        let convertDoubleValue = self.allTasks?.TOTAL ?? 10
                        let totalvalue = Double(convertDoubleValue).removeZerosFromEnd()
                        let counts = completedTasks?.count ?? 0
                        let complete: Double?
                        complete = Double(counts)/(Double(totalvalue ) ?? 10)
                        self.progressView.progress = Float(complete ?? 0)
                        self.progressView.layer.cornerRadius = 5
                        self.progressView.clipsToBounds = true
                        self.progressView.layer.sublayers![1].cornerRadius = 5
                        self.progressView.subviews[1].clipsToBounds = true
                        self.progressLabel.text = "\(counts)/\(totalvalue )"
                        
                        // self.allTasks = fullresponse.RESULT?.map({$0.task_completed == "0"})
                        self.myTable.reloadData()
                    }
                }
            }else{
                pageIndex = 1
                //completedView.backgroundColor = UIColor.darkBlueColor
                pendingView.backgroundColor = .white
                
                //        self.allTasks = []
                getAllCompletedTasks(date: today ?? "2022-11-09", count: "1") { fullresponse in
                    DispatchQueue.main.async {
                        self.refreshControl.endRefreshing()
                        
                        //                let pendingTasks = fullresponse.RESULT?.map({$0.task_id == "0"})
                        //                print(("sadghsad: ", pendingTasks.count))
                        self.allTasks = fullresponse
                        let pendingTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "2" || $0.task_completed == "1"})
                        print(("sadghsad: ", pendingTasks?.count))
                        self.task111 = pendingTasks
                        
                        self.task111?.sort(by: { $0.end_date! < $1.end_date!})
                        
                        let completedTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "1" || $0.task_completed == "2"})
                        print(("sadghsad: ", pendingTasks?.count))
                        
                        let convertDoubleValue = self.allTasks?.TOTAL ?? 10
                        let totalvalue = Double(convertDoubleValue).removeZerosFromEnd()
                        let counts = completedTasks?.count ?? 0
                        let complete: Double?
                        complete = Double(counts)/(Double(totalvalue ) ?? 10)
                        self.progressView.progress = Float(complete ?? 0)
                        self.progressView.layer.cornerRadius = 5
                        self.progressView.clipsToBounds = true
                        self.progressView.layer.sublayers![1].cornerRadius = 5
                        self.progressView.subviews[1].clipsToBounds = true
                        self.progressLabel.text = "\(counts)/\(totalvalue )"
                        
                        
                        
                        
                        self.myTable.reloadData()
                    }
                }
            }
        }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            print("<<<--viewWillAppear-->>>")
            NotificationCenter.default.removeObserver(self, name: Notification.Name("APICALL"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(callClickTab), name: Notification.Name("APICALL"), object: nil)
            
            DispatchQueue.main.async {
                NotificationCenter.default.addObserver(self, selector: #selector(self.DirectTask), name: Notification.Name("DirectTask"), object: nil)
            }
        }
        override func viewDidDisappear(_ animated: Bool) {
            super.viewDidDisappear(animated)
            self.once = true
            NotificationCenter.default.removeObserver(self, name: Notification.Name("DirectTask"), object: nil)
        }
        @objc func DirectTask(notification:Notification){
            if let id = (notification.object as? NSArray)?[0]{
                print(id)
                if self.task111?.count != 0{
                    for i in 0..<(self.task111?.count ?? 0){
                        if self.task111?[i].task_id == "\(id)"{
                            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                            guard self.navigationController?.viewControllers.contains(where: {$0.isKind(of: TaskDetailViewController.self)}) == false else { return }
                            
                            let taskDetailVC = storyBoard.instantiateViewController(withIdentifier: "TaskDetailViewController") as! TaskDetailViewController
                            taskDetailVC.recievetask = self.task111?[i]
                            self.navigationController?.pushViewController(taskDetailVC, animated: true)
                            NotificationCenter.default.removeObserver(self, name: Notification.Name("DirectTask"), object: nil)
                            
                            return
                        }
                    }
                }
            }
        }
        @IBAction func pendingClicked(_ sender: Any) {
            imgpend.isHidden = false
            imgcomp.isHidden = true
            pendingBtn.backgroundColor = .black
            
            completedBtn.backgroundColor = .white
            completedBtn.setTitleColor(UIColor.lightGray, for: .normal)
            //setTitleColor(UIColor(hexString: "#00862D"), for: .normal)
            pendingBtn.setTitleColor(.white, for: .normal)
            pendingBtn.titleLabel?.textColor = .white
            
            isPendingTask = true
            pageIndex = 1
            completedView.backgroundColor = .white
            pendingView.backgroundColor = UIColor(hexString: "#FFCE1A")
            
            getAllPendingTasks(date: today ?? "2022-11-09", count: "1") { fullresponse in
                DispatchQueue.main.async {
                    self.allTasks = fullresponse
                    let pendingTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "0"})
                    self.task111 = pendingTasks
                    
                    let completedTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "1" || $0.task_completed == "2"})
                    print(("sadghsad: ", pendingTasks?.count))
                    let convertDoubleValue = self.allTasks?.TOTAL ?? 10
                    let totalvalue = Double(convertDoubleValue).removeZerosFromEnd()
                    let counts = completedTasks?.count ?? 0
                    let complete: Double?
                    complete = Double(counts)/(Double(totalvalue) ?? 10)
                    self.progressView.progress = Float(complete ?? 0)
                    self.progressView.layer.cornerRadius = 5
                    self.progressView.clipsToBounds = true
                    self.progressView.layer.sublayers![1].cornerRadius = 5
                    self.progressView.subviews[1].clipsToBounds = true
                    self.progressLabel.text = "\(counts)/\(totalvalue)"
                    self.myTable.reloadData()
                }
            }
            
            
        }
        @IBAction func completedClicked(_ sender: Any) {
            imgpend.isHidden = true
            imgcomp.isHidden = false
            
            isPendingTask = false
            pageIndex = 1
            
            //pendingBtn.setTitleColor(UIColor(hexString: "#00862D"), for: .normal)
            pendingBtn.setTitleColor(UIColor.lightGray, for: .normal)
            pendingBtn.backgroundColor = .white
            completedBtn.backgroundColor = .black
            completedBtn.setTitleColor(.white, for: .normal)
            completedBtn.titleLabel?.textColor = .white
            completedView.backgroundColor = UIColor(hexString: "#FFCE1A")
            pendingView.backgroundColor = .white
            
            getAllCompletedTasks(date: today ?? "2022-11-09", count: "1") { fullresponse in
                DispatchQueue.main.async {
                    self.allTasks = fullresponse
                    let pendingTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "2" || $0.task_completed == "1"})
                    self.task111 = pendingTasks
                    let completedTasks =  self.allTasks?.RESULT?.filter({$0.task_completed == "1" || $0.task_completed == "2"})
                    let convertDoubleValue = self.allTasks?.TOTAL ?? 10
                    let totalvalue = Double(convertDoubleValue).removeZerosFromEnd()
                    let counts = completedTasks?.count ?? 0
                    let complete: Double?
                    complete = Double(counts)/(Double(totalvalue) ?? 10)
                    self.progressView.progress = Float(complete ?? 0)
                    self.progressView.layer.cornerRadius = 5
                    self.progressView.clipsToBounds = true
                    self.progressView.layer.sublayers![1].cornerRadius = 5
                    self.progressView.subviews[1].clipsToBounds = true
                    self.progressLabel.text = "\(counts)/\(totalvalue )"
                    self.myTable.reloadData()
                }
            }
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if self.task111?.count == 0 {
                self.myTable.setEmptyMessage("No record found.".localize())
            } else {
                self.myTable.restore()
            }
            
            return self.task111?.count ?? 0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TaskPendingCell", for: indexPath) as! TaskPendingCell
            
            cell.taskTitle.text = self.task111?[indexPath.row].task_title ?? ""
            cell.taskFullDesc.text = self.task111?[indexPath.row].task_description ?? ""
            cell.taskRewards.text = self.task111?[indexPath.row].points ?? ""
            cell.taskDate.text = calculateFormat(date : self.task111?[indexPath.row].start_date , formatterReq : "dd.MM.yyyy")
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let startTime = Date()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            formatter.timeZone = TimeZone.current
            let current_date = formatter.string(from: startTime)
            let SnewDate = formatter.date(from: current_date)!
            
            let EnewDate = formatter.date(from: (self.task111?[indexPath.row].end_date)!) ?? Date()
            let timeDifference = userCalendar.dateComponents(requestedComponent, from: SnewDate, to: EnewDate)
            print(timeDifference)
            cell.taskExpire.text = "\(timeDifference.day!.addLeadingZero())d:\(timeDifference.hour!.addLeadingZero())h:\(timeDifference.minute!.addLeadingZero())m"
            
            if self.task111?[indexPath.row].task_type == .facebook {
                cell.taskIcon.image = UIImage.init(named: "facebook")
                cell.taskShareBtn.setTitle(self.task111?[indexPath.row].action?.capitalizedSentence.localize() ?? "", for: .normal)
            } else  if self.task111?[indexPath.row].task_type == .whatsapp {
                cell.taskIcon.image = UIImage.init(named: "whatsapp")
                cell.taskShareBtn.setTitle("Perform".localize(), for: .normal)
            } else  if self.task111?[indexPath.row].task_type == .instagram {
                cell.taskIcon.image = UIImage.init(named: "instagram")
                cell.taskShareBtn.setTitle(self.task111?[indexPath.row].action?.capitalizedSentence.localize() ?? "", for: .normal)
            } else  if self.task111?[indexPath.row].task_type == .twitter {
                cell.taskIcon.image = UIImage.init(named: "twitter")
                cell.taskShareBtn.setTitle(self.task111?[indexPath.row].action?.capitalizedSentence.localize() ?? "", for: .normal)
            } else  if self.task111?[indexPath.row].task_type == .youtube {
                cell.taskIcon.image = UIImage.init(named: "youtube")
                cell.taskShareBtn.setTitle(self.task111?[indexPath.row].action?.capitalizedSentence.localize() ?? "", for: .normal)
            }else  if self.task111?[indexPath.row].task_type == .offline {
                cell.taskIcon.image = UIImage.init(named: "offline")
                cell.taskShareBtn.setTitle("Perform".localize(), for: .normal)
            }
            
            if self.task111?[indexPath.row].task_completed_user_list?.count == 0 || self.task111?[indexPath.row].task_completed_user_list?.count ?? 0 <= 2 {
                cell.completedNumbers.isHidden = true
                cell.like1.isHidden = true
                cell.like2.isHidden = true
                
            } else {
                cell.like1.isHidden = false
                cell.like2.isHidden = false
                cell.completedNumbers.isHidden = false
                
                cell.completedNumbers.text = "+ \(String(describing: self.task111?[indexPath.row].task_completed_user_list?.count ?? 0)) Completed task".localize()
                
                downloadImageFrom(urlString: self.task111?[indexPath.row].task_completed_user_list?[0].user_image ?? "") { downloadedImage in
                    DispatchQueue.main.async {
                        cell.like1.layer.cornerRadius = 15
                        cell.like1.clipsToBounds = true
                        cell.like1.image = downloadedImage
                    }
                }
                downloadImageFrom(urlString: self.task111?[indexPath.row].task_completed_user_list?[1].user_image ?? "") { downloadedImage in
                    DispatchQueue.main.async {
                        cell.like2.layer.cornerRadius = 15
                        cell.like2.clipsToBounds = true
                        cell.like2.image = downloadedImage
                    }
                }
            }
            
            if self.task111?[indexPath.row].task_completed == "0" {
                cell.taskShareBtn.setTitle(self.task111?[indexPath.row].action?.capitalizedSentence.localize(), for: .normal)
                cell.taskShareBtn.backgroundColor = UIColor(red: 255/255, green: 202/255, blue: 0/255, alpha: 1)
                cell.imgTimer.isHidden = false
                cell.constWidthTimer.constant = 15
            } else {
                cell.taskShareBtn.setTitle("Completed".localize(), for: .normal)
                cell.taskShareBtn.backgroundColor = UIColor(red: 255/255, green: 202/255, blue: 0/255, alpha: 1)
                cell.taskExpire.text = calculateFormat(date : self.task111?[indexPath.row].completed_time , formatterReq : "dd.MM.yyyy hh:mm a")
                cell.imgTimer.isHidden = true
                cell.constWidthTimer.constant = 0
            }
            
            
            cell.taskShareNormal.tag = indexPath.row
            cell.taskShareNormal.isUserInteractionEnabled = true
            cell.taskShareNormal.addTarget(self, action: #selector(shareClicked), for: .touchUpInside)
            
            cell.taskShareBtn.tag = indexPath.row
            cell.taskShareBtn.isUserInteractionEnabled = true
            cell.taskShareBtn.addTarget(self, action: #selector(shareClickedTask), for: .touchUpInside)
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(likesClicked))
            cell.completedNumberView.isUserInteractionEnabled = true
            cell.completedNumberView.tag = indexPath.row
            cell.completedNumberView.addGestureRecognizer(tapGestureRecognizer)
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            
            let pendingTasks =  self.task111?.filter({$0.task_completed == "0"})
            if indexPath.row == (pendingTasks?.count ?? 0) - 1 {
                pageIndex += 1
                
                if(isPendingTask == true){
                    
                    getAllPendingTasks(date: today ?? "2022-11-09", count: String(pageIndex)) { fullresponse in
                        
                        if fullresponse.RESULT?.count == 0 {
                            
                        } else {
                            let pendingTasks = fullresponse.RESULT?.filter({$0.task_completed == "0"})
                            self.task111?.append(contentsOf: pendingTasks ?? [])
                            DispatchQueue.main.async {
                                self.myTable.reloadData()
                            }
                        }
                    }
                    
                } else{
                    getAllCompletedTasks(date: today ?? "2022-11-09", count: String(pageIndex)) { fullresponse in
                        
                        if fullresponse.RESULT?.count == 0 {
                            
                        } else {
                            let pendingTasks =  fullresponse.RESULT?.filter({$0.task_completed == "2" || $0.task_completed == "1"})
                            
                            self.task111?.append(contentsOf: pendingTasks ?? [])
                            DispatchQueue.main.async {
                                self.myTable.reloadData()
                            }
                        }
                    }
                }
                
            }
        }
        
        @objc func likesClicked(_ sender:AnyObject){
            print("you tap image number:", sender.view.tag)
            
            //        self.task111?.comleted
            if self.task111?[sender.view.tag].task_completed_user_list?.count == 0 {
                
            } else {
                let vc = UIStoryboard(name: "Home", bundle: nil)
                let taskDetailVC = vc.instantiateViewController(withIdentifier: "TaskCompletedVC") as! TaskCompletedVC
                taskDetailVC.task = self.task111?[sender.view.tag]
                self.navigationController?.present(taskDetailVC, animated: true)
            }
            
        }
        
        @objc func shareClickedTask(sender: UIButton) {
            if self.task111?[sender.tag].task_completed == "0" {
                self.share(indexPath: sender.tag, action_type: sender.title(for: .normal) ?? "")
            }
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 222
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            if self.task111?[indexPath.row].task_completed == "0" {
                let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                let taskDetailVC = storyBoard.instantiateViewController(withIdentifier: "TaskDetailViewController") as! TaskDetailViewController
                taskDetailVC.indexpath = indexPath.row
                taskDetailVC.recievetask = self.task111?[indexPath.row]
                taskDetailVC.isCompleted = false
                self.navigationController?.pushViewController(taskDetailVC, animated: true)
            } else {
                let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                let taskDetailVC = storyBoard.instantiateViewController(withIdentifier: "TaskDetailViewController") as! TaskDetailViewController
                taskDetailVC.indexpath = indexPath.row
                taskDetailVC.recievetask = self.task111?[indexPath.row]
                taskDetailVC.isCompleted = true
                self.navigationController?.pushViewController(taskDetailVC, animated: true)
            }
            
        }
        
        
        @objc func shareClicked(sender: UIButton){
            
            shareMediaforDeeplinkTask(view: self, TaskResult: (self.task111?[sender.tag])!)
            
            
        }
        
        func dismisss(type: String,TaskData: TaskResult,videoPath: String) {
            
            print("type: ", type)
            
            if type == "instagram" {
                //SHUBHENDU CODE INSTAGRAM
                if TaskData.action == "video" {
                    if let url = URL(string: videoPath) {
                        let fileName = url.lastPathComponent
                        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                        let videoURL = documentsDirectory.appendingPathComponent(fileName)
                        
                        if FileManager.default.fileExists(atPath: videoURL.path) {
                            DispatchQueue.main.async {
                                if let url = URL(string: "instagram://library?LocalIdentifier=\(videoURL.path)") {
                                    if UIApplication.shared.canOpenURL(url) {
                                        DispatchQueue.main.async {
                                            
                                            UIApplication.shared.open(url, options: [:]) { isdone in
                                                self.postfacebookTwitter(task_id: TaskData.task_id) {
                                                    DispatchQueue.main.async {
                                                        self.showToast(message: "task completed successfully")
                                                        self.navigationController?.popViewController(animated: true)
                                                    }
                                                    
                                                }
                                                
                                            }
                                        }
                                    } else {
                                    }
                                } else {
                                    print("Error constructing the URL")
                                }
                            }
                        } else {
                            print("Video file does not exist at the specified URL")
                        }
                    } else {
                        print("Invalid file path")
                    }
                    
                }else{
                    guard let instagram = URL(string: TaskData.post_url ?? "https://www.instagram.com/") else { return }
                    UIApplication.shared.open(instagram)
                    
                }
                
            } else if type == "whatsapp" {
                self.showLoadingIndicator(in: self.view)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    let urlWhats = "whatsapp://app"
                    if TaskData.action == "video"{
                        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                            if let whatsappURL = NSURL(string: urlString) {
                                
                                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                                    let fileExt = ".mp4"
                                    let videoFileName = TaskData.task_title.removeSpace() + (fileExt)
                                    let tmpPathOriginal = NSTemporaryDirectory() as String
                                    let filePathOriginal = tmpPathOriginal + videoFileName
                                    
                                    DispatchQueue.global(qos: .background).async {
                                        let url = URL(string: TaskData.task_media_set?[0].media_url ?? "")!
                                        var request = URLRequest(url: url)
                                        request.httpMethod = "GET"
                                        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                                            
                                            DispatchQueue.main.async {
                                            }
                                            
                                            if(error != nil){
                                                print("\n\nsome error occured\n\n")
                                                self.hideLoadingIndicator()
                                                self.showToast(message: "An error occurred. Please try again".localize())
                                                return
                                            }
                                            if let response = response as? HTTPURLResponse{
                                                if response.statusCode == 200{
                                                    DispatchQueue.main.async {
                                                        if let data = data{
                                                            if let _ = try? data.write(to: URL(fileURLWithPath: filePathOriginal), options: Data.WritingOptions.atomic){
                                                                print("\n\nurl data written\n\n")
                                                                UISaveVideoAtPathToSavedPhotosAlbum(filePathOriginal,nil,nil,nil)
                                                                
                                                                let tempFile = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/whatsAppTmp.wam")
                                                                do {
                                                                    do {
                                                                        let videoURL = URL(fileURLWithPath: filePathOriginal)
                                                                        self.hideLoadingIndicator()
                                                                        
                                                                        self.documentInteractionController = UIDocumentInteractionController(url: videoURL)
                                                                        self.documentInteractionController.uti = "net.whatsapp.movie"
                                                                        self.documentInteractionController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
                                                                        
                                                                    }
                                                                    catch let error {
                                                                        print(error)
                                                                        self.hideLoadingIndicator()
                                                                        
                                                                    }
                                                                    
                                                                } catch {
                                                                    self.hideLoadingIndicator()
                                                                    
                                                                    print(error)
                                                                }
                                                            }
                                                            else{
                                                                self.hideLoadingIndicator()
                                                                
                                                                print("\n\nerror again\n\n")
                                                            }
                                                        }//end if let data
                                                    }//end dispatch main
                                                }//end if let response.status
                                            }
                                        })
                                        task.resume()
                                    }
                                }
                            }
                        }
                    }else if TaskData.action == "link"{
                        let message = TaskData.post_url
                        var queryCharSet = NSCharacterSet.urlQueryAllowed
                        
                        // if your text message contains special char like **+ and &** then add this line
                        queryCharSet.remove(charactersIn: "+&")
                        
                        if let escapedString = message!.addingPercentEncoding(withAllowedCharacters: queryCharSet) {
                            if let whatsappURL = URL(string: "whatsapp://send?text=\(escapedString)") {
                                if UIApplication.shared.canOpenURL(whatsappURL) {
                                    UIApplication.shared.open(whatsappURL, options: [: ], completionHandler: nil)
                                    self.hideLoadingIndicator()
                                } else {
                                    debugPrint("please install WhatsApp")
                                }
                            }
                        }
                    }else{
                        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                            if let whatsappURL = NSURL(string: urlString) {
                                
                                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                                    
                                    let imgURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                                    let fileExt = ".jpg"
                                    
                                    let fileName = TaskData.task_title.removeSpace() + (fileExt)
                                    let fileURL = imgURL.appendingPathComponent(fileName)
                                    
                                    if let url = URL(string: TaskData.task_media_set?[0].media_url ?? ""),
                                       let data = try? Data(contentsOf: url),
                                       let image = UIImage(data: data) {
                                        if let image = UIImage(data: data)  {
                                            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                                            if let imageData = image.jpegData(compressionQuality: 0.75) {
                                                let tempFile = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("/Documents/whatsAppTmp.wai")
                                                do {
                                                    try imageData.write(to: tempFile, options: .atomic)
                                                    self.hideLoadingIndicator()
                                                    
                                                    self.documentInteractionController = UIDocumentInteractionController(url: tempFile)
                                                    self.documentInteractionController.uti = "public.image"
                                                    self.documentInteractionController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
                                                    
                                                } catch {
                                                    print(error)
                                                    DispatchQueue.main.async {
                                                        self.hideLoadingIndicator()
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        DispatchQueue.main.async {
                                            self.hideLoadingIndicator()
                                        }
                                        // Cannot open whatsapp
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
            else {
                guard let url  = URL(string: "youtube://app") else {return}
                if UIApplication.shared.canOpenURL(url) {
                    
                }else{
                    
                }
            }
        }
        
        func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
            // print("sddsddsd")
            showLoadingIndicator(in: view)
            self.postfacebookTwitter(task_id: self.facebookTaskID ?? "") {
                DispatchQueue.main.async {
                    self.hideLoadingIndicator()
                    self.callClickTab()
                    self.dismiss(animated: true)
                }
            }
        }
        
        func sharer(_ sharer: Sharing, didFailWithError error: Error) {
            print("share didfailwitherror print")
            
        }
        
        func sharerDidCancel(_ sharer: Sharing) {
            print("SHare did cancel print function")
            
        }
    }
    extension TaskViewController {
        func calculateFormat(date : String? , formatterReq : String) -> String{
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let newDate = formatter.date(from: date!) ?? Date()
            formatter.dateFormat = formatterReq
            return formatter.string(from: newDate)
        }
        
        func share(indexPath: Int, action_type:String) {
            let task = self.task111?[indexPath]
            self.task = task
            switch task?.task_type {
            case .online:
                print("online")
                
            case .offline:
                print("offline")
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "UploadScreenshotVC") as! UploadScreenshotVC
                controller.taskid = task?.task_id ?? ""
                controller.modalPresentationStyle = .overCurrentContext
                controller.modalTransitionStyle = .crossDissolve
                self.present(controller, animated: true, completion: nil)
                
            case .whatsapp:
                print("whatsapp")
                let vc = UIStoryboard(name: "Home", bundle: nil)
                let taskDetailVC = vc.instantiateViewController(withIdentifier: "InstagramPopUP") as! InstagramPopUP
                taskDetailVC.delegate = self
                taskDetailVC.tasktype = "whatsapp"
                taskDetailVC.taskData = task
                
                //            vc.task = task
                self.navigationController?.present(taskDetailVC, animated: true)
                
            case .facebook:
                print("facebook")
                self.facebookTaskID = self.task?.task_id ?? ""
                print("",  self.facebookTaskID)
                
                if self.task?.action == "post" {
                    downloadImageFrom(urlString: self.task?.task_media_set?[0].media_url ?? "") { downloadedImage in
                        DispatchQueue.main.async {
                            let photoss = downloadedImage
                            let photo = SharePhoto(image: photoss, userGenerated: true)
                            let content2 = SharePhotoContent()
                            content2.photos = [photo]
                            let dialog = ShareDialog(
                                fromViewController: self,
                                content: content2,
                                delegate: self
                            )
                            do {
                                try dialog.validate()
                                dialog.show()
                            } catch {
                                print("catch is calll====>")
                                self.dismiss(animated: true)
                            }
                        }
                    }
                    NotificationCenter.default.removeObserver(self, name: Notification.Name("APICALL"), object: nil)
                    NotificationCenter.default.post(name: Notification.Name("APICALL"), object: nil,userInfo:nil)
                    
                }
                else {
                    var postTaskURL: String?
                    if self.task?.post_url == "" || self.task?.post_url == nil {
                        postTaskURL = "https://www.facebook.com/ysrcpofficial/"
                    } else {
                        postTaskURL = self.task?.post_url ?? ""
                    }
                    
                    guard let url = URL(string: postTaskURL ?? "https://www.facebook.com/ysrcpofficial/") else {
                        return dismiss(animated: true)
                    }
                    
                    let content = ShareLinkContent()
                    content.contentURL = url
                    let dialog = ShareDialog(
                        fromViewController: self,
                        content: content,
                        delegate: self
                    )
                    do {
                        print("Validating share dialog...")
                        try dialog.validate()
                        dialog.show()
                    } catch {
                        print("catch is calll====> else")
                        dismiss(animated: true)
                    }
                }
                
            case .instagram:
                print("instagram")
                let vc = UIStoryboard(name: "Home", bundle: nil)
                let taskDetailVC = vc.instantiateViewController(withIdentifier: "InstagramPopUP") as! InstagramPopUP
                taskDetailVC.delegate = self
                taskDetailVC.tasktype = "instagram"
                taskDetailVC.taskData = task
                
                //            vc.task = task
                self.navigationController?.present(taskDetailVC, animated: true)
                
            case .twitter:
                print("twitter")
                getTwittwerAuth { fullresponse in
                    if (fullresponse.RESULT != "") {
                        DispatchQueue.main.async {
                            let vc = UIStoryboard(name: "Home", bundle: nil)
                            let taskDetailVC = vc.instantiateViewController(withIdentifier: "TwitterTaskVC") as! TwitterTaskVC
                            taskDetailVC.action_type = action_type
                            taskDetailVC.task_id = task?.task_id ?? ""
                            taskDetailVC.webUrl = fullresponse.RESULT ?? ""
                            self.navigationController?.pushViewController(taskDetailVC, animated: true)
                        }
                        
                    }
                }
            case .youtube:
                //print("youtube called",task)
                let vc = UIStoryboard(name: "Home", bundle: nil)
                let taskDetailVC = vc.instantiateViewController(withIdentifier: "TaskShareVC") as! TaskShareVC
                taskDetailVC.shareTask = task
                self.navigationController?.pushViewController(taskDetailVC, animated: true)
            default:
                print(task?.task_type ?? "")
                
            }
        }
    }

    extension Double {
        func removeZerosFromEnd() -> String {
            let formatter = NumberFormatter()
            let number = NSNumber(value: self)
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 16 //maximum digits in Double after dot (maximum precision)
            return String(formatter.string(from: number) ?? "")
        }
    }

    extension String {
        var capitalizedSentence: String {
            // 1
            let firstLetter = self.prefix(1).capitalized
            // 2
            let remainingLetters = self.dropFirst().lowercased()
            // 3
            return firstLetter + remainingLetters
        }
    }

    extension Int {
        func addLeadingZero(_ leadingZeroCount:Int = 2)->String {
            return String(format: "%0\(leadingZeroCount)d", self)
        }
    }
