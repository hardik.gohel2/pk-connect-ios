//
//  TaskPendingCell.swift
//  KCR Sainyam
//
//  Created by apple on 25/09/22.
//

import UIKit

class TaskPendingCell: UITableViewCell {

//    @IBOutlet weak var shareBtn: UIButton!
    
    @IBOutlet weak var completedNumbers: UILabel!
    @IBOutlet weak var taskExpire: UILabel!
    @IBOutlet weak var taskShareNormal: UIButton!
    @IBOutlet weak var taskShareBtn: UIButton!
    @IBOutlet weak var taskTitle: UILabel!
    @IBOutlet weak var taskFullDesc: UILabel!
    
    @IBOutlet weak var constWidthTimer: NSLayoutConstraint!
    @IBOutlet weak var imgTimer: UIImageView!
    @IBOutlet weak var completedNumberView: UIView!
    @IBOutlet weak var taskIcon: UIImageView!
    @IBOutlet weak var taskRewards: UILabel!
    @IBOutlet weak var taskDate: UILabel!
    
    
    @IBOutlet weak var like2: UIImageView!
    @IBOutlet weak var like1: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
