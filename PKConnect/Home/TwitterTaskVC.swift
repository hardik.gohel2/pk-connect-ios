//
//  TwitterTaskVC.swift
//  Team Jagananna
//
//  Created by MAC on 10/10/23.
//

import UIKit
import WebKit



class TwitterTaskVC: BaseViewController {

    
    @IBOutlet weak var webView: WKWebView!{
        didSet{
            webView.navigationDelegate = self
        }
    }
    
    var webUrl = ""
    var task_id = ""
    var action_type = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if  let link = URL(string:  webUrl) {
            let request = URLRequest(url: link)
            webView.load(request)
        }
    }
    

    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func callTwitterCreate(acess_token:String,task_id:String) {
        CreateTwitter(access_token: acess_token, task_id: task_id) {
            DispatchQueue.main.async {
                self.twitterTaskSumit()
            }
            
        }
    }
    func postCompleteTask () {
            self.showLoadingIndicator(in: self.view)
            self.postfacebookTwitter(task_id: self.task_id) {
                DispatchQueue.main.async {
                    self.hideLoadingIndicator()
                   // self.delegate?.twitterTaskVCDidDismiss(self)
                    let alertController = UIAlertController(title: "Sucess", message: "Task Completed sucessfully", preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                           print("OK button tapped")
                            self.dismiss(animated: true)
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                        alertController.addAction(okAction)
                        
                        
                            self.present(alertController, animated: true, completion: nil)
                        
                    }

                }
            }
    
//    func postCompleteTask () {
//        self.showLoadingIndicator(in: self.view)
//        self.postfacebookTwitter(task_id: self.task_id) {
//            DispatchQueue.main.async {
//                self.hideLoadingIndicator()
//               // self.delegate?.twitterTaskVCDidDismiss(self)
//                self.showToast(message: "task Submmited Successfully")
//                self.navigationController?.popViewController(animated: true)
//            }
//        }
//    }
    
    func twitterTaskSumit() {
        self.TaskSumitTwitter(task_id: task_id) {
            DispatchQueue.main.async {
               
                self.postCompleteTask()
            }
        }
    }

}
extension TwitterTaskVC : WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if let urlString = navigationAction.request.url?.absoluteString,
           urlString.contains("code=") {
            let url = navigationAction.request.url
            if let range = url?.absoluteString.range(of: "code=") {
                let extractedString = url?.absoluteString.suffix(from: range.upperBound)
                
                // Remove parentheses from the extracted string
                let resultString = extractedString?.replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")
                
                print(resultString)
                getTwittwerCallack(code: resultString ?? "") { fullresponse in
                    if fullresponse.RESULT != nil {
                        DispatchQueue.main.async {
                            let result = fullresponse.RESULT
                            let access_token = result.access_token
                            if self.action_type == "Like" {
                                self.LikeTwitter(access_token: access_token ?? "", task_id: self.task_id) {
                                    DispatchQueue.main.async {
                                        self.twitterTaskSumit()
                                    }
                                    
                                }
                            }else if self.action_type == "Retweet" {
                                self.CreateTwitterRetweet(access_token: access_token ?? "", task_id: self.task_id) {
                                    DispatchQueue.main.async {
                                        self.twitterTaskSumit()
                                    }
                                }
                            }
                            else{
                                self.callTwitterCreate(acess_token: access_token ?? "", task_id: self.task_id)
                            }
                        }
                        
                    }
                }
            } else {
                print("String not found after '='")
            }
        }
        decisionHandler(.allow)
        
        
    }
    
   
    
}
