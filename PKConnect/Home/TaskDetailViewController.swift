//
//  TaskDetailViewController.swift
//  PKConnect
//
//  Created by apple on 07/11/22.
//

import UIKit
import FBSDKShareKit
import AuthenticationServices
import Swifter
import AVKit

class TaskDetailViewController: BaseViewController, SharingDelegate, instaPopupDismiss, ASWebAuthenticationPresentationContextProviding {
   
    
    func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        return self.view.window ?? ASPresentationAnchor()
    }
    
    @IBOutlet weak var lblheadertask: UILabel!
   
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var videoView: UIView!

    @IBOutlet weak var taskImageHeight: NSLayoutConstraint!
    @IBOutlet weak var uploadScreenshotBnt: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var typeIcon: UIImageView!
    @IBOutlet weak var taskTitle: UILabel!
    @IBOutlet weak var completedNumbers: UILabel!
    @IBOutlet weak var taskImage: UIImageView!
    @IBOutlet weak var taskRewards: UILabel!
    @IBOutlet weak var taskDesc: UILabel!
    @IBOutlet weak var taskDate: UILabel!
    @IBOutlet weak var newTimer: UILabel!
    @IBOutlet weak var like2: UIImageView!
    @IBOutlet weak var like1: UIImageView!
    
    @IBOutlet weak var lbl_Completed: UILabel!
    var playAction: (()->())?
    var facebookTaskID: String?
    var recievetask: TaskResult?
    var indexpath: Int?
    var documentInteractionController: UIDocumentInteractionController = UIDocumentInteractionController()
    var userProfile: UserProfile?
    let userCalendar = Calendar.current
    let requestedComponent: Set<Calendar.Component> = [.day,.hour,.minute]
    var player: AVPlayer!
    var avpController = AVPlayerViewController()
    var isCompleted:Bool?

    @IBOutlet weak var completedNumberView: UIView!
   

    override func viewDidLoad() {
        super.viewDidLoad()
        lblheadertask.text = "Task".localize()
//
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(likesClicked))

        completedNumberView.isUserInteractionEnabled = true
        completedNumberView.addGestureRecognizer(tapGestureRecognizer)
        
        print("dadsds: ", self.recievetask?.points ?? "")
        let title = self.recievetask?.action ?? ""
        self.shareBtn.setTitle(title.capitalizedSentence.localize(), for: .normal)
        self.uploadScreenshotBnt.setTitle("Upload Screenshot".localize(), for: .normal)

        if isCompleted ?? false {
            self.lbl_Completed.isHidden = false
            self.shareBtn.isHidden = true
            self.uploadScreenshotBnt.isHidden = true
        } else {
            self.lbl_Completed.isHidden = true
            self.shareBtn.isHidden = false
            self.uploadScreenshotBnt.isHidden = false
        }
        if self.recievetask?.task_type == .facebook {
            typeIcon.image = UIImage.init(named: "facebook")
            uploadScreenshotBnt.isHidden = true
            print("Facebook")
        } else if self.recievetask?.task_type == .whatsapp {
            print("Whatsup")
            typeIcon.image = UIImage.init(named: "whatsapp")
            shareBtn.setTitle("Perform".localize(), for: .normal)
            uploadScreenshotBnt.isHidden = false
        } else if self.recievetask?.task_type == .instagram {
            print("Instagram")
            uploadScreenshotBnt.isHidden = false
            typeIcon.image = UIImage.init(named: "instagram")
        } else if self.recievetask?.task_type == .twitter {
            print("Twitter")
            typeIcon.image = UIImage.init(named: "twitter")
            uploadScreenshotBnt.isHidden = true
        } else  if self.recievetask?.task_type == .youtube {
            print("Youtube")
            typeIcon.image = UIImage.init(named: "youtube")
            uploadScreenshotBnt.isHidden = true
        }else  if self.recievetask?.task_type == .offline {
            print("Offline")
            typeIcon.image = UIImage.init(named: "offline")
            shareBtn.setTitle("Perform".localize(), for: .normal)
            uploadScreenshotBnt.isHidden = true
        } else{
            print("--> Error")
        }
        
        if self.recievetask?.task_type == .facebook {
            self.facebookTaskID = self.recievetask?.task_id ?? ""
        }
        
        
        if self.recievetask?.task_media_set?.count == 0 {
            taskImageHeight.constant = 0
        } else {
            downloadImageFrom(urlString: self.recievetask?.task_media_set?[0].media_url ?? "") { downloadedImage in
                DispatchQueue.main.async {
                    self.taskImageHeight.constant = screenWidth-60
                    self.taskImage.image = downloadedImage
                }
            }
        }
        
        taskTitle.text = self.recievetask?.task_title ?? ""
        completedNumbers.text = self.recievetask?.task_completed_user_count ?? ""
        taskRewards.text = self.recievetask?.points ?? ""
        taskDesc.text = self.recievetask?.task_description ?? ""
        taskDate.text = self.recievetask?.start_date ?? ""
        print("date is>>>>>>",taskDate.text )
        taskDate.text = "Posted on \(calculateFormat(date : self.recievetask?.start_date ?? "" , formatterReq : "dd.MM.yyyy"))"
        
        print("date is possssss>>>>>>",taskDate.text )

        
        if self.recievetask?.task_completed_user_list?.count == 0 || self.recievetask?.task_completed_user_list?.count ?? 0 <= 2 {
            completedNumbers.isHidden = true
            like1.isHidden = true
            like2.isHidden = true

        } else {
            like1.isHidden = false
            like2.isHidden = false
            completedNumbers.isHidden = false
            
            completedNumbers.text = "+ \(String(describing: self.recievetask?.task_completed_user_list?.count ?? 0)) Completed task"
            
            downloadImageFrom(urlString: self.recievetask?.task_completed_user_list?[0].user_image ?? "") { downloadedImage in
                DispatchQueue.main.async {
                    self.like1.layer.cornerRadius = 15
                    self.like1.clipsToBounds = true
                    self.like1.image = downloadedImage
                }
            }
            downloadImageFrom(urlString: self.recievetask?.task_completed_user_list?[1].user_image ?? "") { downloadedImage in
                DispatchQueue.main.async {
                    self.like2.layer.cornerRadius = 15
                    self.like2.clipsToBounds = true
                    self.like2.image = downloadedImage
                }
            }
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let startTime = Date()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = TimeZone.current
        let current_date = formatter.string(from: startTime)
        let SnewDate = formatter.date(from: current_date)!

        let EnewDate = formatter.date(from: (self.recievetask?.end_date)!) ?? Date()

        
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: SnewDate, to: EnewDate)
        print(timeDifference)
        newTimer.text = "\(timeDifference.day!.addLeadingZero())d:\(timeDifference.hour!.addLeadingZero())h:\(timeDifference.minute!.addLeadingZero())m"
        

     //   self.newTimer.text = "00d:\(calculateFormat(date : self.recievetask?.end_date ?? "" , formatterReq : "hh"))h:\(calculateFormat(date : self.recievetask?.end_date ?? "" , formatterReq : "mm"))m"
        
        if recievetask?.action == "video"{
            guard let mediaURL = self.recievetask?.task_media_set?[0].media_url else {
                return
            }
            self.playAction = { self.setVideoURL(urlStr: mediaURL)}
            btnPlay.isHidden = false
            self.taskImage.image = UIImage(named: "BKG")
            self.videoView.isHidden = true
            self.avpController.removeFromParent()
            
            if((self.player?.currentItem) != nil){
                self.player.replaceCurrentItem(with: nil)
            }
        }else{
            videoView.isHidden = true
            btnPlay.isHidden = true

        }
    

        // Do any additional setup after loading the view.
    }
    func setVideoURL(urlStr: String) {
        
        self.videoView.isHidden = false
        self.taskImage.isHidden = true
        self.btnPlay.isHidden = true

        if let videoUrl = URL(string: urlStr) {
            player = AVPlayer(url: videoUrl)
        }
       
        avpController.player = player
        avpController.view.frame = videoView.bounds
        self.videoView.addSubview(avpController.view)
        player?.play()
        
    }
    @objc func stopPlayer() {
        player?.pause()
        avpController.removeFromParent()
    }
    
    @IBAction func btnPlayvideo(_ sender: UIButton) {
        self.playAction?()
    }
    @objc func likesClicked(_ sender:AnyObject){
        print("you tap image number:", sender.view.tag)

//        self.task111?.comleted
        if self.recievetask?.task_completed_user_list?.count == 0 {

        } else {
            let vc = UIStoryboard(name: "Home", bundle: nil)
            let taskDetailVC = vc.instantiateViewController(withIdentifier: "TaskCompletedVC") as! TaskCompletedVC
            taskDetailVC.task = self.recievetask
            self.navigationController?.present(taskDetailVC, animated: true)
        }

    }
    
    func calculateFormat(date : String? , formatterReq : String) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let newDate = formatter.date(from: date!) ?? Date()
        formatter.dateFormat = formatterReq
        return formatter.string(from: newDate)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        print("sddsddsd")
        showLoadingIndicator(in: view)
        self.postfacebookTwitter(task_id: self.facebookTaskID ?? "") {
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                self.dismiss(animated: true)
            }
        }
    }
    
    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        print("dsdsdsdd")

    }
    
    func sharerDidCancel(_ sharer: Sharing) {
        print("sddsdssssssssdsd")

    }
    
    @IBAction func shareBtnClicked(_ sender: Any) {
        
        
        if self.recievetask?.task_type == .facebook {
            
            self.facebookTaskID = self.recievetask?.task_id ?? ""
        
            if self.recievetask?.action == "post" {
                downloadImageFrom(urlString: self.recievetask?.task_media_set?[0].media_url ?? "https://pkconnect.s3.ap-south-1.amazonaws.com/Media/63763f85de0b4.314658624_659474292302575_3356630214966091798_n.jpg") { downloadedImage in
                    DispatchQueue.main.async {
                        var photoss = downloadedImage
                        
                        let photo = SharePhoto(image: photoss, userGenerated: true)
                        let content2 = SharePhotoContent()
                        content2.photos = [photo]
                        let dialog = ShareDialog(
                            fromViewController: self,
                            content: content2,
                            delegate: self
                        )
                        do {
                            try dialog.validate()
                            dialog.show()
                        } catch {
                            self.dismiss(animated: true)
                        }
                    }
                }
             
            } else {
                
                
                //
                var postTaskURL: String?
                if self.recievetask?.post_url == "" || self.recievetask?.post_url == nil {
                    postTaskURL = "https://www.facebook.com/jansuraajofficial/"
                } else {
                    postTaskURL = self.recievetask?.post_url ?? ""
                }
                
                guard let url = URL(string: postTaskURL ?? "https://www.facebook.com/jansuraajofficial/") else {
                    return dismiss(animated: true)
                }
                let content = ShareLinkContent()
                content.contentURL = url
                let dialog = ShareDialog(
                    fromViewController: self,
                    content: content,
                    delegate: self
                )
                do {
                    try dialog.validate()
                    dialog.show()
                } catch {
                    dismiss(animated: true)
                }
            }
            
        } else if self.recievetask?.task_type == .whatsapp {
            
            let vc = UIStoryboard(name: "Home", bundle: nil)
            let taskDetailVC = vc.instantiateViewController(withIdentifier: "InstagramPopUP") as! InstagramPopUP
            taskDetailVC.delegate = self
            taskDetailVC.tasktype = "whatsapp"
            taskDetailVC.taskData = recievetask

            //            vc.task = task
            self.navigationController?.present(taskDetailVC, animated: true)
            
        } else if self.recievetask?.task_type == .instagram {
            
            let vc = UIStoryboard(name: "Home", bundle: nil)
            let taskDetailVC = vc.instantiateViewController(withIdentifier: "InstagramPopUP") as! InstagramPopUP
            taskDetailVC.delegate = self
            taskDetailVC.tasktype = "instagram"
            taskDetailVC.taskData = recievetask

            //            vc.task = task
            self.navigationController?.present(taskDetailVC, animated: true)
            
        } else if self.recievetask?.task_type == .youtube {
            
            let vc = UIStoryboard(name: "Home", bundle: nil)
            let taskDetailVC = vc.instantiateViewController(withIdentifier: "InstagramPopUP") as! InstagramPopUP
            taskDetailVC.delegate = self
            taskDetailVC.tasktype = "youtube"
            taskDetailVC.taskData = recievetask

            //            vc.task = task
            self.navigationController?.present(taskDetailVC, animated: true)
            
        }else if self.recievetask?.task_type == .offline {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "UploadScreenshotVC") as! UploadScreenshotVC
            controller.taskid = self.recievetask?.task_id ?? ""
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalTransitionStyle = .crossDissolve
            self.present(controller, animated: true, completion: nil)
        }else if self.recievetask?.task_type == .twitter {
            print("twitter")
            showLoadingIndicator(in: view)
            
            getProfile { profile in
                DispatchQueue.main.async { [self] in
                    userProfile = profile
                    
                    let post_URL = self.recievetask?.post_url
                    let components = URLComponents(string: post_URL!)
                    let id = components?.path.split(separator: "/").last
                    print(id)
                    DispatchQueue.main.async {
                        self.hideLoadingIndicator()
                    }
                    
                    if userProfile?.twitter_username != "" || userProfile?.twitter_username != nil {
                        var swifter: Swifter!
                        if UserDefaults.standard.string(forKey: "twitterOAuthToken") == nil {
                            swifter = Swifter(consumerKey: "7dfm8RoglWqOMUbgz3ebbz38g", consumerSecret: "SuFQzZoF8DTX4YUi1HShqxZhyLDKKMoWPapeEcmathY6QkqxCj")
                            if let url = URL(string: "twitterkit-7dfm8RoglWqOMUbgz3ebbz38g://") {
                                swifter.authorize(withProvider: self, callbackURL: url) { (token, response) in
                                    UserDefaults.standard.set(token?.key, forKey: "twitterOAuthToken")
                                    UserDefaults.standard.set(token?.secret, forKey: "twitterOAuthSecret")
                                    print("signed in!!")
                                }
                            }
                        }
                        swifter = Swifter(consumerKey: "7dfm8RoglWqOMUbgz3ebbz38g", consumerSecret: "SuFQzZoF8DTX4YUi1HShqxZhyLDKKMoWPapeEcmathY6QkqxCj", oauthToken: UserDefaults.standard.string(forKey: "twitterOAuthToken") ?? "", oauthTokenSecret: UserDefaults.standard.string(forKey: "twitterOAuthSecret") ?? "")
                        if let url = URL(string: "twitterkit-7dfm8RoglWqOMUbgz3ebbz38g://") {
                            swifter.retweetTweet(forID: String(id!), success: {success in
                                print(success)
                                self.showToast(message: "Retweet success".localize())
                                self.postTask(task_id: self.recievetask?.task_id ?? "") {
                                    DispatchQueue.main.async {
                                        //self.callClickTab()
                                    }
                                }
                            },failure: {error in
                                print(error)
                                if error.localizedDescription.contains("You have already retweeted this Tweet."){
                                    self.showToast(message: "You have already retweeted this Tweet.".localize())
                                }else{
                                    self.showToast(message: error.localizedDescription)

                                }
                            })
                        }
                    }else{
                        self.showToast(message: "Please login through twitter first".localize())
                    }
                }
            }
        }
        
        
    }
    func dismisss(type: String,TaskData: TaskResult, videoPath: String) {
        
        print("sahjshjkashd: ", type)
        
        if type == "instagram" {
            //SHUBHENDU CODE INSTAGRAM
            if TaskData.action == "video" {
                if let url = URL(string: videoPath) {
                    let fileName = url.lastPathComponent
                    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                    let videoURL = documentsDirectory.appendingPathComponent(fileName)
                    
                   if FileManager.default.fileExists(atPath: videoURL.path) {
                        DispatchQueue.main.async {
                            if let url = URL(string: "instagram://library?LocalIdentifier=\(videoURL.path)") {
                                if UIApplication.shared.canOpenURL(url) {
                                    UIApplication.shared.open(url, options: [:]) { isdone in
                                        self.postfacebookTwitter(task_id: TaskData.task_id) {
                                            DispatchQueue.main.async {
                                                self.showToast(message: "task completed successfully")
                                                self.navigationController?.popViewController(animated: true)
                                            }
                                           
                                        }
                                    }
                                } else {
                                     }
                            } else {
                                print("Error constructing the URL")
                            }
                        }
                    } else {
                        print("Video file does not exist at the specified URL")
                        }
                } else {
                    print("Invalid file path")
                }
               
            }else{
                guard let instagram = URL(string: TaskData.post_url ?? "https://www.instagram.com/") else { return }
                UIApplication.shared.open(instagram)
            }

        } else if type == "whatsapp" {
            self.showLoadingIndicator(in: self.view)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                let urlWhats = "whatsapp://app"
                if TaskData.action == "video"{
                    if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                        if let whatsappURL = NSURL(string: urlString) {
                            
                            if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                                let fileExt = ".mp4"
                                let videoFileName = TaskData.task_title.removeSpace() + (fileExt)
                                let tmpPathOriginal = NSTemporaryDirectory() as String
                                let filePathOriginal = tmpPathOriginal + videoFileName
                                
                                
                                DispatchQueue.global(qos: .background).async {
                                    let url = URL(string: TaskData.task_media_set?[0].media_url ?? "")!
                                    var request = URLRequest(url: url)
                                    request.httpMethod = "GET"
                                    let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                                        
                                        DispatchQueue.main.async {
                                        }
                                        
                                        if(error != nil){
                                            print("\n\nsome error occured\n\n")
                                            self.hideLoadingIndicator()
                                            
                                            self.showToast(message: "An error occurred. Please try again".localize())
                                            return
                                        }
                                        if let response = response as? HTTPURLResponse{
                                            if response.statusCode == 200{
                                                DispatchQueue.main.async {
                                                    if let data = data{
                                                        if let _ = try? data.write(to: URL(fileURLWithPath: filePathOriginal), options: Data.WritingOptions.atomic){
                                                            print("\n\nurl data written\n\n")
                                                            UISaveVideoAtPathToSavedPhotosAlbum(filePathOriginal,nil,nil,nil)
                                                            
                                                            let tempFile = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/whatsAppTmp.wam")
                                                            do {
                                                                do {
                                                                    //                                                                    let videoData = try Data(contentsOf: URL(fileURLWithPath: filePathOriginal))
                                                                    //                                                                    try videoData.write(to: tempFile, options: .atomic)
                                                                    let videoURL = URL(fileURLWithPath: filePathOriginal)
                                                                    self.hideLoadingIndicator()
                                                                    
                                                                    self.documentInteractionController = UIDocumentInteractionController(url: videoURL)
                                                                    self.documentInteractionController.uti = "net.whatsapp.movie"
                                                                    self.documentInteractionController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
                                                                    
                                                                }
                                                                catch let error {
                                                                    print(error)
                                                                }
                                                            } catch {
                                                                print(error)
                                                                self.hideLoadingIndicator()
                                                                
                                                            }
                                                        }
                                                        else{
                                                            self.hideLoadingIndicator()
                                                            print("\n\nerror again\n\n")
                                                        }
                                                    }//end if let data
                                                }//end dispatch main
                                            }//end if let response.status
                                        }
                                    })
                                    task.resume()
                                }
                            }
                        }
                    }
                }else if TaskData.action == "link"{
                    let message = TaskData.post_url
                    var queryCharSet = NSCharacterSet.urlQueryAllowed
                    
                    // if your text message contains special char like **+ and &** then add this line
                    queryCharSet.remove(charactersIn: "+&")
                    
                    if let escapedString = message!.addingPercentEncoding(withAllowedCharacters: queryCharSet) {
                        if let whatsappURL = URL(string: "whatsapp://send?text=\(escapedString)") {
                            if UIApplication.shared.canOpenURL(whatsappURL) {
                                UIApplication.shared.open(whatsappURL, options: [: ], completionHandler: nil)
                                self.hideLoadingIndicator()
                            } else {
                                debugPrint("please install WhatsApp")
                            }
                        }
                    }
                }else{
                    if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                        if let whatsappURL = NSURL(string: urlString) {
                            
                            if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                                
                                let imgURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                                let fileExt = ".jpg"
                                
                                let fileName = TaskData.task_title.removeSpace() + (fileExt)
                                let fileURL = imgURL.appendingPathComponent(fileName)
                                
                                DispatchQueue.main.async {
                                }
                                
                                if let url = URL(string: TaskData.task_media_set?[0].media_url ?? ""),
                                   let data = try? Data(contentsOf: url),
                                   let image = UIImage(data: data) {
                                    if let image = UIImage(data: data)  {
                                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                                        
                                        if let imageData = image.jpegData(compressionQuality: 0.75) {
                                            let tempFile = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("/Documents/whatsAppTmp.wai")
                                            do {
                                                try imageData.write(to: tempFile, options: .atomic)
                                                self.hideLoadingIndicator()
                                                
                                                self.documentInteractionController = UIDocumentInteractionController(url: tempFile)
                                                self.documentInteractionController.uti = "public.image"
                                                self.documentInteractionController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
                                                //                                                    self.postTask(task_id: TaskData.task_id ?? "") {
                                                //                                                        DispatchQueue.main.async {
                                                //
                                                //                                                        }
                                                //                                                    }
                                            } catch {
                                                print(error)
                                                self.hideLoadingIndicator()
                                                
                                            }
                                        }
                                    }
                                } else {
                                    self.hideLoadingIndicator()
                                    // Cannot open whatsapp
                                }
                            }
                            
                        }
                    }
                }
                
            }
        } else {
            guard let url  = URL(string: "youtube://app") else {return}
            if UIApplication.shared.canOpenURL(url) {
                //            UIImageView().kf.setImage(with: URL(string : /self.taskLists[(shareCellIndexPath?.row) ?? 0].task_media_set.first?.mediaUrl), placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (img, _, _, _) in
                //                self.isShare = false
                //                UIImageWriteToSavedPhotosAlbum(img ?? UIImage(), self,#selector(self.images(_:didFinishSavingWithError:contextInfo:)), nil)
                //            })
            }else{
                // CommonFunction.showToast(msg: AlertMessage.installWhatsapp.localized)
            }
        }
    }
//    func dismisss(type: String,TaskData: TaskResult) {
//
//        print("sahjshjkashd: ", type)
//
//        if type == "instagram" {
//            //SHUBHENDU CODE INSTAGRAM
//            guard let instagram = URL(string: TaskData.post_url ?? "https://www.instagram.com/") else { return }
//            UIApplication.shared.open(instagram)
//
////            let instagramHooks = self.recievetask?.post_url ?? ""
////            let instagramUrl = NSURL(string: instagramHooks)
////            if UIApplication.shared.canOpenURL(instagramUrl! as URL) {
////                UIApplication.shared.canOpenURL(instagramUrl! as URL)
////            } else {
////                //redirect to safari because the user doesn't have Instagram
////                UIApplication.shared.canOpenURL(NSURL(string: self.recievetask?.post_url ?? "")! as URL)
////            }
//        } else if type == "whatsapp" {
//            self.showLoadingIndicator(in: self.view)
//
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                let urlWhats = "whatsapp://app"
//                if TaskData.action == "video"{
//                    if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
//                        if let whatsappURL = NSURL(string: urlString) {
//
//                            if UIApplication.shared.canOpenURL(whatsappURL as URL) {
//                                let fileExt = ".mp4"
//                                let videoFileName = TaskData.task_title.removeSpace() + (fileExt)
//                                let tmpPathOriginal = NSTemporaryDirectory() as String
//                                let filePathOriginal = tmpPathOriginal + videoFileName
//
//
//                                DispatchQueue.global(qos: .background).async {
//                                    let url = URL(string: TaskData.task_media_set?[0].media_url ?? "")!
//                                    var request = URLRequest(url: url)
//                                    request.httpMethod = "GET"
//                                    let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
//
//                                        DispatchQueue.main.async {
//                                        }
//
//                                        if(error != nil){
//                                            print("\n\nsome error occured\n\n")
//                                            self.hideLoadingIndicator()
//
//                                            self.showToast(message: "An error occurred. Please try again".localize())
//                                            return
//                                        }
//                                        if let response = response as? HTTPURLResponse{
//                                            if response.statusCode == 200{
//                                                DispatchQueue.main.async {
//                                                    if let data = data{
//                                                        if let _ = try? data.write(to: URL(fileURLWithPath: filePathOriginal), options: Data.WritingOptions.atomic){
//                                                            print("\n\nurl data written\n\n")
//                                                            UISaveVideoAtPathToSavedPhotosAlbum(filePathOriginal,nil,nil,nil)
//
//                                                            let tempFile = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/whatsAppTmp.wam")
//                                                            do {
//                                                                do {
//                                                                    //                                                                    let videoData = try Data(contentsOf: URL(fileURLWithPath: filePathOriginal))
//                                                                    //                                                                    try videoData.write(to: tempFile, options: .atomic)
//                                                                    let videoURL = URL(fileURLWithPath: filePathOriginal)
//                                                                    self.hideLoadingIndicator()
//
//                                                                    self.documentInteractionController = UIDocumentInteractionController(url: videoURL)
//                                                                    self.documentInteractionController.uti = "net.whatsapp.movie"
//                                                                    self.documentInteractionController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
//
//                                                                }
//                                                                catch let error {
//                                                                    print(error)
//                                                                }
//                                                            } catch {
//                                                                print(error)
//                                                                self.hideLoadingIndicator()
//
//                                                            }
//                                                        }
//                                                        else{
//                                                            self.hideLoadingIndicator()
//                                                            print("\n\nerror again\n\n")
//                                                        }
//                                                    }//end if let data
//                                                }//end dispatch main
//                                            }//end if let response.status
//                                        }
//                                    })
//                                    task.resume()
//                                }
//                            }
//                        }
//                    }
//                }else if TaskData.action == "link"{
//                    let message = TaskData.post_url
//                    var queryCharSet = NSCharacterSet.urlQueryAllowed
//
//                    // if your text message contains special char like **+ and &** then add this line
//                    queryCharSet.remove(charactersIn: "+&")
//
//                    if let escapedString = message!.addingPercentEncoding(withAllowedCharacters: queryCharSet) {
//                        if let whatsappURL = URL(string: "whatsapp://send?text=\(escapedString)") {
//                            if UIApplication.shared.canOpenURL(whatsappURL) {
//                                UIApplication.shared.open(whatsappURL, options: [: ], completionHandler: nil)
//                                self.hideLoadingIndicator()
//                            } else {
//                                debugPrint("please install WhatsApp")
//                            }
//                        }
//                    }
//                }else{
//                    if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
//                        if let whatsappURL = NSURL(string: urlString) {
//
//                            if UIApplication.shared.canOpenURL(whatsappURL as URL) {
//
//                                let imgURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//                                let fileExt = ".jpg"
//
//                                let fileName = TaskData.task_title.removeSpace() + (fileExt)
//                                let fileURL = imgURL.appendingPathComponent(fileName)
//
//                                DispatchQueue.main.async {
//                                }
//
//                                if let url = URL(string: TaskData.task_media_set?[0].media_url ?? ""),
//                                   let data = try? Data(contentsOf: url),
//                                   let image = UIImage(data: data) {
//                                    if let image = UIImage(data: data)  {
//                                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
//
//                                        if let imageData = image.jpegData(compressionQuality: 0.75) {
//                                            let tempFile = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("/Documents/whatsAppTmp.wai")
//                                            do {
//                                                try imageData.write(to: tempFile, options: .atomic)
//                                                self.hideLoadingIndicator()
//
//                                                self.documentInteractionController = UIDocumentInteractionController(url: tempFile)
//                                                self.documentInteractionController.uti = "public.image"
//                                                self.documentInteractionController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
//                                                //                                                    self.postTask(task_id: TaskData.task_id ?? "") {
//                                                //                                                        DispatchQueue.main.async {
//                                                //
//                                                //                                                        }
//                                                //                                                    }
//                                            } catch {
//                                                print(error)
//                                                self.hideLoadingIndicator()
//
//                                            }
//                                        }
//                                    }
//                                } else {
//                                    self.hideLoadingIndicator()
//                                    // Cannot open whatsapp
//                                }
//                            }
//
//                        }
//                    }
//                }
//
//            }
//        } else {
//            guard let url  = URL(string: "youtube://app") else {return}
//            if UIApplication.shared.canOpenURL(url) {
//                //            UIImageView().kf.setImage(with: URL(string : /self.taskLists[(shareCellIndexPath?.row) ?? 0].task_media_set.first?.mediaUrl), placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (img, _, _, _) in
//                //                self.isShare = false
//                //                UIImageWriteToSavedPhotosAlbum(img ?? UIImage(), self,#selector(self.images(_:didFinishSavingWithError:contextInfo:)), nil)
//                //            })
//            }else{
//                // CommonFunction.showToast(msg: AlertMessage.installWhatsapp.localized)
//            }
//        }
//    }
    
    @IBAction func uploadScreenshotClicked(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UploadScreenshotVC") as! UploadScreenshotVC
        controller.taskid = self.recievetask?.task_id ?? ""
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
