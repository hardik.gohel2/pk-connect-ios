


import UIKit
import AVKit
import AVFoundation
import FirebaseDynamicLinks

class YSRStoriesVC: BaseViewController {
    
    @IBOutlet weak var videoTableVw: UITableView!
    
    var avPlayer: AVPlayer!
    var aboutToBecomeInvisibleCell = -1
    var avPlayerLayer: AVPlayerLayer!
    var paused: Bool = false
    var videoURLs = Array<URL>()
    var firstLoad = true
    var videoArray = [String]()
    var userLiked = ""
    var likesCount = "0"
    var videoUrl :[VideosData]?
    var isVideoPlay:Bool = true
    var refreshControl = UIRefreshControl()
    var pageIndex = 1
    var videoResponse : VideoModel?
    var playerCache = NSCache<NSString, AVPlayer>()
    var visibleIndexPaths = Set<IndexPath>()
    var objPlayer = AVPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.videoTableVw.delegate = self
        self.videoTableVw.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationStories(notification:)), name: Notification.Name("NotificationIdentifierStories"), object: nil)
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        videoTableVw.addSubview(refreshControl)
        
        let footerView = UIView()
        footerView.frame.size.height = 1
        videoTableVw.tableFooterView = footerView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.getStoriesList()
        
    }
    
    @objc func refresh() {
        getStoriesList()
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        DispatchQueue.main.async {
            self.objPlayer.pause()
        }
    }

    @objc func methodOfReceivedNotificationStories(notification: Notification) {
        self.getStoriesList()
    }
    
    func getStoriesList() {
        if self.videoResponse != nil {
            let indexPath = IndexPath(row: 0, section:0)
            if let videoCell = self.videoTableVw.cellForRow(at: indexPath) as? HomeVideoTableViewCell {
                DispatchQueue.main.async {
                    videoCell.player?.pause()
                    self.isVideoPlay = false
                    videoCell.img_Play.isHidden = false
                }
            }
        }
        showLoadingIndicator(in: self.view)
        ServiceManager.shared.getRequestWithArr(endpoint:"\(ServiceManager.shared.baseURL)/api/shortsListing", type:(VideoModel).self) { [weak self] (result, error) in
            guard let self else {return}
            self.videoResponse = result
            if let videoItems =  result?.result {
                if(self.pageIndex == 1){
                    self.videoUrl?.removeAll()
                }
                self.videoUrl = videoItems
                DispatchQueue.main.async {
                    self.hideLoadingIndicator()
                    self.refreshControl.endRefreshing()
                    self.videoTableVw.reloadData()
                }
            }
        }
    }
    
//    func getStoriesList() {
//        self.videoResponse = SessionManager.shared.videoUrl
//        if let videoItems =  SessionManager.shared.videoUrl?.result {
//            if(self.pageIndex == 1){
//                self.videoUrl?.removeAll()
//            }
//            self.videoUrl = videoItems
//            DispatchQueue.main.async {
//                self.hideLoadingIndicator()
//                self.refreshControl.endRefreshing()
//                self.videoTableVw.reloadData()
//            }
//        }
//       
//        DispatchQueue.main.async {
//            self.videoTableVw.reloadData()
//        }
//    }
}


extension YSRStoriesVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        (videoUrl?.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.videoTableVw.dequeueReusableCell(withIdentifier: "HomeVideoTableViewCell") as! HomeVideoTableViewCell
        cell.videoLayerView.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        let videoRelatedData = self.videoUrl?[indexPath.row]
        cell.displayData(data: videoRelatedData)
        cell.img_Play.isHidden = true
        guard  let path = URL(string:videoRelatedData?.s3URL ?? "") else {
            debugPrint("video.mp4 not found")
            return cell
        }
        cell.userNameBtn.isUserInteractionEnabled = false
//        cell.loaderView.startAnimating()
        if let cachedPlayer = playerCache.object(forKey: (videoRelatedData?.s3URL ?? "") as NSString) {
            cell.player = cachedPlayer
            DispatchQueue.main.async {
//                cell.loaderView.stopAnimating()
                cell.userNameBtn.isUserInteractionEnabled = true
            }
        } else {
            cell.player = AVPlayer(url: path)
            playerCache.setObject(cell.player!, forKey: (videoRelatedData?.s3URL ?? "") as NSString)
            NotificationCenter.default.addObserver(forName: .AVPlayerItemNewAccessLogEntry, object: cell.player?.currentItem, queue: .main) { _ in
                DispatchQueue.main.async {
//                    cell.loaderView.stopAnimating()
                    cell.userNameBtn.isUserInteractionEnabled = true
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: cell.player?.currentItem, queue: .main) { [weak self] _ in
            cell.player?.seek(to: CMTime.zero)
            cell.player?.play()
            DispatchQueue.main.async {
                cell.userNameBtn.isUserInteractionEnabled = true
            }
        }

        let playerLayer = AVPlayerLayer(player: cell.player)
        playerLayer.frame = cell.videoLayerView.bounds
        playerLayer.backgroundColor = UIColor.clear.cgColor
        cell.videoLayerView.layer.addSublayer(playerLayer)
        self.objPlayer = cell.player!
       
        cell.onClickComment = {
            cell.player?.pause()
            let storyBoard = UIStoryboard(name: "Other", bundle: nil)
            let commentVC = storyBoard.instantiateViewController(withIdentifier: "ShortsCommetsVC") as! ShortsCommetsVC
            commentVC.short_id = videoRelatedData?.id ?? ""
            commentVC.short_title = videoRelatedData?.shortTitle ?? ""
            self.navigationController?.pushViewController(commentVC, animated: true)
        }
        cell.numberOfCommentsLbl.text = self.videoUrl?[indexPath.row].commentCount
        let likeImaage = self.videoUrl?[indexPath.row].isLiked == true ? "liked_small" : "heart_icon"
        cell.btnLike.setImage(UIImage(named:likeImaage), for: .normal)
        
        cell.onClickLike = {
            if self.videoUrl?[indexPath.row].isLiked == true {
//                self.videoUrl?[indexPath.row].isLiked = false
//                let likeValue = (Int(self.videoUrl?[indexPath.row].likeCount ?? "") ?? 0) - 1
//                self.videoUrl?[indexPath.row].likeCount = "\(likeValue)"
//                self.deletelikeshorts(shortId: self.videoUrl?[indexPath.row].id ?? "") { [weak self] (result) in
//                    guard let self else {return}
//                    DispatchQueue.main.async {
//                        print(self)
//                        cell.numberOfLikes.text = self.videoUrl?[indexPath.row].likeCount
//                        let likeImaage = self.videoUrl?[indexPath.row].isLiked == true ? "liked_small" : "heart_icon"
//                        cell.btnLike.setImage(UIImage(named:likeImaage), for: .normal)
//                    }
//                }
            } else {
                self.videoUrl?[indexPath.row].isLiked = true
                let likeValue = (Int(self.videoUrl?[indexPath.row].likeCount ?? "") ?? 0) + 1
                self.videoUrl?[indexPath.row].likeCount = "\(likeValue)"
                self.likeshorts(shortId: self.videoUrl?[indexPath.row].id ?? "") { [weak self](result) in
                    guard let self else {return}
                    DispatchQueue.main.async {
                        print(self)
                        cell.numberOfLikes.text = self.videoUrl?[indexPath.row].likeCount
                        let likeImaage = self.videoUrl?[indexPath.row].isLiked == true ? "liked_small" : "heart_icon"
                        cell.btnLike.setImage(UIImage(named:likeImaage), for: .normal)
                    }
                }
            }
        }
        
        cell.onClickUserTitle = {
            cell.player?.pause()
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let commentVC = storyBoard.instantiateViewController(withIdentifier: "ChannelDetailsVC") as! ChannelDetailsVC
            commentVC.channel_id = self.videoUrl?[indexPath.row].channelID ?? ""
            self.navigationController?.pushViewController(commentVC, animated: true)
        }
        
        if indexPath.row == ((self.videoUrl?.count ?? 0) - 1 ) {
            self.getStoriLists(next: self.videoResponse?.next ?? 0) {
                DispatchQueue.main.async {
                    self.videoTableVw.reloadData()
                }
            }
        }
        
        if self.videoUrl?[indexPath.row].isSubscribed == true {
            cell.subscribeBtn.backgroundColor = .gray
            cell.subscribeBtn.setTitle("Subscribed", for: .normal)
            cell.subscribeBtn.setTitleColor(.white, for: .normal)
        }else{
            cell.subscribeBtn.backgroundColor = UIColor(hexString: "#FFCD1A")
            cell.subscribeBtn.setTitle("Subscribe", for: .normal)
            cell.subscribeBtn.setTitleColor(.white, for: .normal)
        }
        
        cell.onClickSubscribe = {
            if self.videoUrl?[indexPath.row].isSubscribed == false {
                self.videoUrl?[indexPath.row].isSubscribed = true
                self.addSubscriptionStatus(channel_id:self.videoUrl?[indexPath.row].channelID ?? "") { [weak self](result) in
                    guard let self else {return}
                    DispatchQueue.main.async {
                        cell.subscribeBtn.backgroundColor = .gray
                        cell.subscribeBtn.setTitle("Subscrbied", for: .normal)
                        cell.subscribeBtn.setTitleColor(.white, for: .normal)
                    }
                }
            }
        }
        
        cell.shareBtn.addTarget(self, action:#selector(shareBtn(_ :)), for: .touchUpInside)
        cell.shareBtn.tag = indexPath.row
//        cell.subscribeBtn.addTarget(self, action:#selector(self.subscribeBtnAction(_ :)), for: .touchUpInside)
//        cell.subscribeBtn.tag = indexPath.row
        return cell
    }
    
    @objc func subscribeBtnAction(_ sender :UIButton){
        self.videoUrl?[sender.tag].isSubscribed = true
        addSubscriptionStatus(channel_id:self.videoUrl?[sender.tag].channelID ?? "") { [weak self](result) in
            guard let self else {return}
            DispatchQueue.main.async {
                debugPrint("Done")
            }
        }
        self.videoTableVw.reloadData()
    }
    
    @objc func shareBtn(_ sender :UIButton) {
            self.showLoadingIndicator(in: self.view)
            let videoId = (self.videoUrl?[sender.tag].id ?? "")
            let title = (self.videoUrl?[sender.tag].shortTitle ?? "") + "Shared Via Team Jagananna"
            self.shareDeepLink(controller: self, videoId: videoId, message: title) {
                self.hideLoadingIndicator()
            }
        }
        
        func shareDeepLink(controller:UIViewController, videoId:String, message:String, handler: @escaping () -> Void) {
            let shareLink = "https://pkconnect.page.link/?item_type=live_short&short_video_id=" + videoId
            
            let linkParameter = URL(string: shareLink)!
            guard let dynamiclink = DynamicLinkComponents.init(link: linkParameter, domainURIPrefix: "https://pkconnect.page.link") else {
                return
            }
            dynamiclink.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.pk.connect")
            dynamiclink.iOSParameters?.appStoreID = "6448717716"
            dynamiclink.androidParameters = DynamicLinkAndroidParameters(packageName: DynamicLinkParams.packageName)
            dynamiclink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
            dynamiclink.socialMetaTagParameters?.title = DynamicLinkParams.packageName
            
            dynamiclink.shorten {[weak controller] (url, warnings, error) in
                if let url = url {
                    var shareText = "\(message)" + "\n\n\(url.absoluteString)"
                    DispatchQueue.main.async {
                        let activityVC = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
                        activityVC.popoverPresentationController?.sourceView = controller?.view
                        controller?.present(activityVC, animated: true, completion: nil)
                        handler()
                    }
                } else {
                    handler()
                }
            }
        }
    
    @objc func likeBtnAction(_ sender :UIButton){
        if self.videoUrl?[sender.tag].isLiked == true{
            self.videoUrl?[sender.tag].isLiked = false
            let likeValue = (Int(self.videoUrl?[sender.tag].likeCount ?? "") ?? 0) - 1
            self.videoUrl?[sender.tag].likeCount = "\(likeValue)"
            deletelikeshorts(shortId: self.videoUrl?[sender.tag].id ?? "") { [weak self] (result) in
                guard let self else {return}
                DispatchQueue.main.async {
                    print(self)
                }
            }
        }else{
            self.videoUrl?[sender.tag].isLiked = true
            let likeValue = (Int(self.videoUrl?[sender.tag].likeCount ?? "") ?? 0) + 1
            self.videoUrl?[sender.tag].likeCount = "\(likeValue)"
            likeshorts(shortId: self.videoUrl?[sender.tag].id ?? "") { [weak self](result) in
                guard let self else {return}
                DispatchQueue.main.async {
                    print(self)
                }
            }
        }
        //self.videoTableVw.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return videoTableVw.frame.height
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let videoCell = cell as? HomeVideoTableViewCell {
            DispatchQueue.main.async {
                videoCell.player?.play()
                self.visibleIndexPaths.insert(indexPath)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let videoCell = cell as? HomeVideoTableViewCell {
            DispatchQueue.main.async {
                videoCell.player?.pause()
                self.visibleIndexPaths.removeAll()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = videoTableVw.cellForRow(at: indexPath) as? HomeVideoTableViewCell
        if isVideoPlay {
            isVideoPlay = false
            cell?.img_Play.isHidden = false
            cell?.player?.pause()
        } else {
            DispatchQueue.main.async {
//                cell?.loaderView.stopAnimating()
                cell?.userNameBtn.isUserInteractionEnabled = true
            }
            isVideoPlay = true
            cell?.img_Play.isHidden = true
            cell?.player?.play()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleIndexPaths = videoTableVw.indexPathsForVisibleRows ?? []

        for indexPath in visibleIndexPaths {
            if let cell = videoTableVw.cellForRow(at: indexPath) as? HomeVideoTableViewCell {
                let videoRect = videoTableVw.convert(cell.videoLayerView.bounds, from: cell.videoLayerView)

                // Calculate the visible portion of the video
                let intersection = videoTableVw.bounds.intersection(videoRect)
                let visiblePercentage = (intersection.width * intersection.height) / (cell.videoLayerView.bounds.width * cell.videoLayerView.bounds.height)

                // If more than 80% of the video is visible, play it; otherwise, pause it
                if visiblePercentage > 0.9 {
                    DispatchQueue.main.async {
                        cell.player?.play()
                        self.isVideoPlay = true
                        self.objPlayer = cell.player!
//                        cell.loaderView.stopAnimating()
                    }
                } else {
                    DispatchQueue.main.async {
                        cell.player?.pause()
                        self.objPlayer = cell.player!
//                        cell.loaderView.stopAnimating()
                    }
                }
            }
        }
    }

    
    func getStoriLists(next:Int, completion: @escaping () -> Void) {
        print(next)
        ServiceManager.shared.getRequestWithArr(endpoint:"\(ServiceManager.shared.baseURL)/api/shortsListing?count=\(next)", type:(VideoModel).self) { [weak self] (result, error) in
            guard let self else {return}
            if let obj = result?.result {
                self.videoResponse?.next = result?.next ?? 0
                self.videoUrl?.append(contentsOf: obj)
            }
            completion()
        }
    }
}
