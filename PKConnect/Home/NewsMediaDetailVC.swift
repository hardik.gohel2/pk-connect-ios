//
//  NewsMediaDetailVC.swift
//  PKConnect
//
//  Created by admin on 02/11/23.
//

import UIKit

class NewsMediaDetailVC: BaseViewController {

    @IBOutlet weak var tblDetail: UITableView!{
        didSet {
            tblDetail.delegate = self
            tblDetail.dataSource = self
            tblDetail.register(UINib(nibName: "NewsDetailTVCell", bundle: nil), forCellReuseIdentifier: "NewsDetailTVCell")
        }
    }
    
    var arrayDetail : [NewsItem]?
   
    var scrollIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
    
        tblDetail.reloadData()
        tblDetail.scrollToRow(at: IndexPath(item: scrollIndex, section: 0), at: .none, animated: false)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension NewsMediaDetailVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsDetailTVCell") as! NewsDetailTVCell
        
       
            cell.imgView.isHidden = false
            cell.viewPlayer.isHidden = true
            cell.imgView.sd_setImage(with: URL(string: arrayDetail?[indexPath.row].image ?? ""))
        
        cell.lblTitle.text = arrayDetail?[indexPath.row].title
        let htmlString = arrayDetail?[indexPath.row].description

        
        if let data = htmlString?.data(using: .utf8),
           let attributedString = try? NSAttributedString(data: data,
                                                         options: [.documentType: NSAttributedString.DocumentType.html],
                                                         documentAttributes: nil) {
            
            
            let nonNewlineAttributedString = NSMutableAttributedString(attributedString: attributedString)
               
               let newlineCharacterSet = CharacterSet.newlines
               nonNewlineAttributedString.mutableString.replaceOccurrences(of: "\n", with: "", options: .literal, range: NSRange(location: 0, length: nonNewlineAttributedString.length))
               
            let viewMoreString = NSAttributedString(string: " View More", attributes: [
                    .foregroundColor: UIColor.black])
              
            nonNewlineAttributedString.append(viewMoreString)
            
            cell.lblNews.attributedText = attributedString
            
           
            cell.lblNews.numberOfLines = 0
            cell.lblNews.textAlignment = .left
            
            cell.lblViewMore.attributedText = nonNewlineAttributedString
            
            cell.selectionStyle = .none
            cell.lblViewMore.numberOfLines = 0
            cell.lblViewMore.textAlignment = .left
            cell.btnShare.tag = indexPath.row
            cell.btnLink.tag = indexPath.row
            cell.btnLink.addTarget(self, action:#selector(openLink(_ :)), for: .touchUpInside)
            cell.btnShare.addTarget(self, action:#selector(shareBtn(_ :)), for: .touchUpInside)
        }
        //cell.lblNews.text = arrayDetail?[indexPath.row].description
       // cell.lblViewMore.text = arrayDetail?[indexPath.row].description_trimmed
        cell.lblSeenBy.text = "Seen By \(arrayDetail?[indexPath.row].view_count ?? "")"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = dateFormatter.date(from: arrayDetail?[indexPath.row].created_date ?? "") {
            let timeAgo = timeAgoSinceDate(date)
            cell.lblTime.text = timeAgo
        } else {
            print("Invalid date format")
        }
            return cell
        
    }
    
    private func pauseVideoForCells() {
            guard let visibleIndexPaths = tblDetail.indexPathsForVisibleRows else { return }
            
            for indexPath in visibleIndexPaths {
                if let cell = tblDetail.cellForRow(at: indexPath) as? NewsDetailTVCell {
                    if cell.viewPlayer.isHidden == false {
                        cell.pauseVideo()
                    }
                }
            }
        }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pauseVideoForCells()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayDetail?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tblDetail.frame.height
    }
    
    func timeAgoSinceDate(_ date: Date) -> String {
        let calendar = Calendar.current
        let now = Date()
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: date, to: now)

        if let year = components.year, year > 0 {
            return "\(year) year\(year == 1 ? "" : "s") ago"
        }
        
        if let month = components.month, month > 0 {
            return "\(month) month\(month == 1 ? "" : "s") ago"
        }
        
        if let day = components.day, day > 0 {
            return "\(day) day\(day == 1 ? "" : "s") ago"
        }
        
        if let hour = components.hour, hour > 0 {
            return "\(hour) hour\(hour == 1 ? "" : "s") ago"
        }
        
        if let minute = components.minute, minute > 0 {
            return "\(minute) minute\(minute == 1 ? "" : "s") ago"
        }
        
        return "Just now"
    }
    
    @objc func openLink(_ sender : UIButton) {
        if let url = URL(string: arrayDetail?[sender.tag].url ?? "") {
            UIApplication.shared.open(url)
        }
    }
    
    
    @objc func shareBtn(_ sender: UIButton) {
        let url = URL(string: arrayDetail?[sender.tag].url ?? "")!

        let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        
        activityViewController.completionWithItemsHandler = { (activityType, completed, returnedItems, error) in
            if completed {
               
            let inputString = activityType?.rawValue ?? "Unknown App"
                let components = inputString.components(separatedBy: ".")
                if components.count > 1 {
                    let extractedValue = components[1]
                    print(extractedValue)
                    self.shareNewsMedia(newsId: self.arrayDetail?[sender.tag].id ?? "", platform: extractedValue, completion: {
                        print("shared success")
                    })
                } else {
                    print("The string doesn't have enough components.")
                }
            } else {
                print("Sharing was canceled or failed.")
            }
        }

        if let popoverController = activityViewController.popoverPresentationController {
            popoverController.sourceView = self.view
            
        }

        self.present(activityViewController, animated: true, completion: nil)
    }
}
