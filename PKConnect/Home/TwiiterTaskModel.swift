//
//  TwiiterTaskModel.swift
//  Team Jagananna
//
//  Created by MAC on 10/10/23.
//

import Foundation


struct TwitterModel: Codable {
    let CODE: Int?
    let MESSAGE: String?
    let RESULT: String?
}

struct TwitterCallack: Codable {
    let CODE: Int?
    let MESSAGE: String?
    let RESULT : ResultModel
}

struct ResultModel: Codable {
    let access_token : String?
}
struct TwitterInfo: Codable {
    let id: String?
    let api_key: String?
    let api_key_secret: String?
    let status: String?
}
