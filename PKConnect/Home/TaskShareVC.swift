//
//  TaskShareVC.swift
//  Team Jagananna
//
//  Created by MAC on 05/10/23.
//

import UIKit
import WebKit

class TaskShareVC: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var webView: WKWebView!
    var today: String?
    var shareTask:TaskResult?
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = shareTask?.task_title
        if  let link = URL(string:  "https://www.youtube.com/watch?v=\(shareTask?.video_id ?? "")"
        ) {
            let request = URLRequest(url: link)
            webView.load(request)
        }
        let date = Date()
        
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.string(from: date)
        today = dateFormatter.string(from: date)
    }
    
    
    @IBAction func onClickBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func onClickShare(_ sender: Any) {
        let vc = UIActivityViewController(activityItems: ["https://www.youtube.com/watch?v=\(shareTask?.video_id ?? "")"], applicationActivities: [])
        present(vc, animated: true, completion: nil)
        vc.completionWithItemsHandler = { activityType, completed, returnedItems, error in
            if completed {
                self.showLoadingIndicator(in: self.view)
                self.postfacebookTwitter(task_id: self.shareTask?.task_id ?? "") {
                    DispatchQueue.main.async {
                        self.hideLoadingIndicator()
                        vc.dismiss(animated: true) {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
            else
            {
                if let error = error {
                    
                } else
                {
                    
                }
            }
        }
    }
}

