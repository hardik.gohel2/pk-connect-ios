//
//  AllTasksModel.swift
//  PKConnect
//
//  Created by apple on 03/11/22.
//

import Foundation

struct AllTasksModel: Codable {
    let CODE: Int?
    let MESSAGE: String?
    let RESULT: [TaskResult]?
//    let NEXT: String?
    let TOTAL: Int?
    let TOTAL_COMPLETE_TASK: Int?
    let IS_PROFILE_EDITED: String?
    let IS_CAMPAIGN_FORM_FILLED: String?
    let CAMPAIGN_FORM_URL: String?
    let PAYMENT_DETAIL_ADDED: Int?
    let IS_ID_PROOF_ADDED: String?
}


// MARK: - Result
struct TaskResult: Codable {
    let task_id, task_code, task_title, task_description: String
    let notif_title, notif_description, greeting_message, is_send_notif: String?
    let post_url, post_content, post_id, points: String?
    let start_date, end_date, radius, latitude: String?
//    let longitude: String
////    let qrCodeURL: NSNull
    let state, district, gender, registeration_no: String?
    let video_id, channel_id, twitter_follow_id, twitter_follow_name: String?
    let facebook_follow_id, facebook_follow_name, instruction_description: String?
    let instruction_video_url: String?
    let task_image_url: String?
    let form_url, whatsapp_image_url1, whatsapp_image_url2, college: String?
    let form_id, form_steps, ac, pc: String?
    let task_type: TaskType?
    let action, task_status, show_popup, notification_update: String?
    let is_push_notif_sent, created_date, status, task_completed: String?
    let completed_time, total_survay_form, total_survay_form_complete, task_completed_user_count: String?
    let task_media_set: [TaskMediaSet]?
    let task_completed_user_list: [TaskCompletedUserList]?
}

// MARK: - TaskCompletedUserList
struct TaskCompletedUserList: Codable {
    let user_id, full_name, registeration_no: String
    let user_image: String?
    let phone_number: String
}

// MARK: - TaskMediaSet
struct TaskMediaSet: Codable {
    let media_url: String
    let media_type: String
    let media_thumb: String?
}

enum TaskType: String, Codable {
    
    case facebook = "facebook"
    case instagram = "instagram"
    case twitter = "twitter"
    case whatsapp = "whatsapp"
    case online = "online"
    case offline = "offline"
    case youtube = "youtube"
    
}
