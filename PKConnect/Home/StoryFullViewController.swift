

import UIKit
import AVKit

class StoryFullViewController: BaseViewController {
    
    @IBOutlet private var videoView: UIView!
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var progressView: UIProgressView!
    
    var timer = Timer(),
                duration = 15.0,
                progressBarIndex = 0.0,
                player: AVPlayer?,
                avpController = AVPlayerViewController()
    
    var storyItems = [FeedItem](),
        tappedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        loadData()
    }
   
    func loadData() {
        if let mediaSet = storyItems[tappedIndex].content_media_set {
            if mediaSet.first?.media_type == "1" {
                videoView.isHidden = true
                imageView.isHidden = false
                if let urlStr = mediaSet.first?.media_url {
                   // showLoadingIndicator(in: view)
                    downloadImageFrom(urlString: urlStr) { downloadedImage in
                        DispatchQueue.main.async {
                            //self.hideLoadingIndicator()
                            self.imageView.image = downloadedImage
                            self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.updateProgress), userInfo: nil, repeats: true)
                        }
                    }
                }
            } else if mediaSet.first?.media_type == "2", let urlStr = mediaSet.first?.media_url {
                videoView.isHidden = false
                imageView.isHidden = true
               
                let videoURL = URL(string: urlStr)
                player = AVPlayer(url: videoURL!)
                avpController.player = player
                avpController.view.backgroundColor = .white
                avpController.view.frame = videoView.bounds
                self.videoView.addSubview(avpController.view)
                
                NotificationCenter.default.addObserver(self, selector: #selector(videoLoaded(_:)), name: .AVPlayerItemNewAccessLogEntry, object: player?.currentItem)

                              //  showLoadingIndicator(in: videoView)

                
                player?.play()
                setupProgressTimer()
            }
        }
    }
    
    @objc func videoLoaded(_ notification: Notification) {
           DispatchQueue.main.async {
              // self.hideLoadingIndicator() // Hide loading indicator when video is loaded
           }
       }
   
    
    private func setupProgressTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 0.02, repeats: true, block: { [weak self] (completion) in
            guard let self = self else {
                print("Reached end")
                return
            }
            self.updateVideoProgress()
        })
    }
    
    @objc private func updateVideoProgress() {
        guard let videoDuration = player?.currentItem?.duration.seconds,
              let currentMoment = player?.currentItem?.currentTime().seconds else { return }
        let progressInFloat = Float(currentMoment/videoDuration)
        if progressInFloat == 1.0 {
            moveToNextStory()
        }
        progressView.progress = progressInFloat.isNaN ? 0 : progressInFloat
    }
    
    @objc func updateProgress() {
        if progressBarIndex == duration {
            timer.invalidate()
            moveToNextStory()
          
            progressBarIndex = 0
        }
       
        self.progressView.progress = Float(progressBarIndex) / Float(duration - 1)
        
      
        progressBarIndex += 1
    }
    
    func moveToNextStory()
    {
        timer.invalidate()
        player?.pause()
        if self.tappedIndex < self.storyItems.count - 1 {
            self.tappedIndex += 1
            self.loadData()
        } else {
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @objc func moveToPreviousStory() {
        player?.pause()
        timer.invalidate()
        if self.tappedIndex > 0 {
            self.tappedIndex -= 1
        }
        self.loadData()
    }
    
    @IBAction func storyTapped(_ sender: UITapGestureRecognizer) {
        let touchLocation = sender.location(ofTouch: 0, in: view)
        
        if touchLocation.x > view.frame.width/2 {
            moveToNextStory()
            progressBarIndex = 0
        } else {
            moveToPreviousStory()
            progressBarIndex = 0
        }
    }
    
    @IBAction func closeStoriesView(_ sender: Any) {
        timer.invalidate()
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func shareTapped(_ sender: Any) {
        player?.pause()
        if let mediaSet = storyItems[tappedIndex].content_media_set {
            if mediaSet.first?.media_type == "1" {
                if let urlStr = mediaSet.first?.media_url {
                    filedownLoadAndShare(view: self,mediaFile: urlStr,fileName: storyItems[tappedIndex].news_title) { success, fileURL in
                        //self.player?.play()
                    }
                    
                }
            } else if mediaSet.first?.media_type == "2", let urlStr = mediaSet.first?.media_url {
                filedownLoadAndShare(view: self,mediaFile: urlStr,fileName: storyItems[tappedIndex].news_title) { success, fileURL in
                    //self.player?.play()
                }
            }
        }
    }
    
    @IBAction func downloadTapped(_ sender: Any) {
        
        if let mediaSet = storyItems[tappedIndex].content_media_set {
            if mediaSet.first?.media_type == "1" {
                if let urlStr = mediaSet.first?.media_url {
                    filedownLoad(view: self,mediaFile: urlStr,fileName: storyItems[tappedIndex].news_title)
                }
            } else if mediaSet.first?.media_type == "2", let urlStr = mediaSet.first?.media_url {
                filedownLoad(view: self,mediaFile: urlStr,fileName: storyItems[tappedIndex].news_title)
            }
        }
    }
}
