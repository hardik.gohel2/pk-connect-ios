//
//  CommentVC.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit

class CommentVC: BaseViewController, UITableViewDataSource,UITableViewDelegate {

    @IBOutlet private var commentTextField: UITextField! {
        didSet{
            commentTextField.placeholder = "Add a comment".localize()
        }
    }
    @IBOutlet private var commentViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet private var userImage: UIImageView!
    
    @IBOutlet private var commentsTable: UITableView!
    @IBOutlet private var newsTitle: UILabel!
    
    var newsId: String?, headline = ""
    var isLoading : Bool = false
    var pageNo: Int = 1
    var getComments:GetComments?
    var isLikeOrRplay:Bool = false
    var isFromews = false
    
    private var commentList = [Comment](),
                updatedCommentList = [Comment](),
                replyView: ReplyCommentView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
        
        newsTitle.text = headline
        showLoadingIndicator(in: view)
        callGetCommentsAPI {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commentsTable.delegate = self
//        commentsTable.rowHeight = UITableView.automaticDimension
        userImage.layer.cornerRadius = 20
        if let urlStr = getRetrievedUser()?.user_image {
            userImage.setLoadedImage(urlStr)
        }
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func callGetCommentsAPI(_ completion: @escaping ()->()) {
        getAllComments(newsId: newsId ?? "0",pageNo: pageNo) { commentList in
            if (commentList?.RESULT?.count ?? 0) > 0 {
                DispatchQueue.main.async { [self] in
                    isLoading = false
                    print("comments list count123132132 =======>>>>>,",commentList?.RESULT?.count ?? 0)
                    hideLoadingIndicator()
                    self.getComments = commentList
                    if let commentList = commentList?.RESULT, commentList.count > 0 {
                        if pageNo == 1 {
                            self.commentList = commentList
                        } else {
                            for item in commentList{
                                self.commentList.append(item)
                            }
                        }
                        //self.commentList = commentList
                        setUpTableData()
                        commentsTable.reloadData()
                        print("comments list count =======>>>>>,",self.commentList.count)
                        if self.view.subviews.contains(noRecordsLabel) {
                            noRecordsLabel.removeFromSuperview()
                        }
                    } else {
                        updatedCommentList.removeAll()
                        commentsTable.reloadData()
                        addNoRecordsLabel("Regular", 16, to: self.view)
                    }
                    completion()
                }
            }
        }
    }
    
    private func setUpTableData() {
        updatedCommentList = [Comment]()
        for i in 0...commentList.count-1 {
            let comment = commentList[i]
            updatedCommentList.append(comment)
            if !isLikeOrRplay {
                if let replies = comment.comments, replies.count != 0 {
                    updatedCommentList.append(contentsOf: replies)
                }
            }
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction private func changeLanguage(_ sender: Any) {
        changeAppLanguage()
    }
    
    //Comments Table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        updatedCommentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentTableCell
        
        var comment = updatedCommentList[indexPath.row]
        
        let attrString = NSMutableAttributedString(string: comment.full_name,
                                                   attributes: [.font: UIFont(name: "NunitoSans-Regular", size: 14) as Any])
        attrString.append(NSAttributedString(string: "  ", attributes: nil))
        attrString.append(NSMutableAttributedString(string: comment.user_comment,
                                                    attributes: [.font: UIFont(name: "NunitoSans-Light", size: 12) as Any]))
       // tableView.estimatedRowHeight = 1000
        cell.name.attributedText = attrString
        if let urlStr = comment.user_image {
            cell.userImage.setLoadedImage(urlStr)
        }
        cell.timeStamp.text = getFormattedDate(from: comment.submitted_timestamp, inputFormat: "yyyy-MM-dd HH:mm:ss", outputFormat: 1)
        if !isFromews {
            cell.deleteButton.isHidden = (comment.user_id == getRetrievedUser()?.user_id) ? false : true
            cell.deleteCommentAction = { self.deleteComment(comment.comment_id, newsId: self.newsId!, indexPath: indexPath) }
            cell.replyCommentAction = { self.replyComment(comment, indexPath: indexPath) }
        }else{
            cell.deleteButton.isHidden = true
        }
        if isLikeOrRplay {
            cell.btn_Reply.isHidden = true
            cell.likeButton.isHidden = true
            cell.deleteButton.isHidden = true
        }
        
        let likeType = (comment.is_like == "like") ? true : false
        if likeType {
            cell.likeButton.setBackgroundImage(UIImage(named: "liked_small"), for: .normal)
        } else {
            cell.likeButton.setBackgroundImage(UIImage(named: "like_small"), for: .normal)
        }
        
        cell.likeCommentAction = {
            self.showLoadingIndicator(in: self.view)
            self.likeOrUnlikeComment(comment.comment_id, isLike: likeType) {
                comment.is_like = (likeType == false ? "dislike" : "like")
                DispatchQueue.main.async {
                    self.hideLoadingIndicator()
                    let likeType = (comment.is_like == "like") ? false : true
                    if likeType {
                        cell.likeButton.setBackgroundImage(UIImage(named: "liked_small"), for: .normal)
                    } else {
                        cell.likeButton.setBackgroundImage(UIImage(named: "like_small"), for: .normal)
                    }
                    comment = self.updatedCommentList[indexPath.row]
                    
                }
            }
        }
        cell.userImageLeadingConstraint.constant = (comment.parent_id == "0") ? 10 : 40
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
//        isLoading = true
        print("callning will display cell method------->>>>")
        let lastItem = self.commentList.count - 1
        if indexPath.row == lastItem &&  !isLoading && self.commentList.count >= 10{
            isLoading = true
            print("IndexRow\(indexPath.row)")

            self.pageNo = Int(self.getComments?.NEXT ?? "0") ?? 0
            callGetCommentsAPI {}

        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    
    @IBAction func addCommentButtonTapped(_ sender: UIButton) {

        pageNo = 1 //Int(self.getComments?.NEXT ?? "0") ?? 0
        if commentTextField.text != "" {
            postComment(comment: commentTextField.text!, parentId: "0", newsId: newsId!) { [self] in
                self.commentList.removeAll()
                callGetCommentsAPI { [self] in
                    commentTextField.text = ""
                    commentTextField.resignFirstResponder()
                }
            }
        }
    }
    private func deleteComment(_ commentId: String, newsId: String, indexPath: IndexPath) {
        deleteComment(commentId: commentId, newsId: newsId) {
//            self.pageNo = Int(self.getComments?.NEXT ?? "0") ?? 0
//            self.commentList.removeAll()
            self.updatedCommentList.remove(at: indexPath.row)
            self.updatedCommentList = self.updatedCommentList.filter { $0.parent_id != commentId }

            DispatchQueue.main.async {
                self.commentsTable.reloadData()
            }
        }
    }
    private func replyComment(_ comment: Comment, indexPath: IndexPath) {
        view.addSubview(backgroundView)
        backgroundView.frame = view.frame
        
        replyView = UINib(nibName: "ReplyCommentView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as? ReplyCommentView
        replyView.name.text = comment.full_name
        if let urlStr = comment.user_image {
            replyView.userImage.setLoadedImage(urlStr)
        }
        replyView.comment.text = comment.user_comment
        replyView.timeStamp.text = getFormattedDate(from: comment.submitted_timestamp, inputFormat: "yyyy-MM-dd HH:mm:ss", outputFormat: 1)
        replyView.closeButtonAction = { self.dismissReplyCommentView() }
        replyView.submitButtonAction = { self.addReply(self.replyView.textField.text, to: comment, indexPath: indexPath)}
        replyView.textField.delegate = self
        view.addSubview(replyView)
        replyView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            replyView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            replyView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -40),
            replyView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            replyView.heightAnchor.constraint(equalToConstant: 200)
        ])
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
           backgroundView.addGestureRecognizer(tapGesture)
           backgroundView.isUserInteractionEnabled = true
    }
    @objc private func handleTapGesture(_ sender: UITapGestureRecognizer) {
        dismissReplyCommentView()
    }

    
    private func addReply(_ text: String?, to comment: Comment, indexPath: IndexPath) {
        guard let replyText = text else { return }
        let parentId = (comment.parent_id == "0") ? comment.comment_id : comment.parent_id
        pageNo = 1 //Int(self.getComments?.NEXT ?? "0") ?? 0
        postComment(comment: replyText, parentId: parentId, newsId: newsId!) { [self] in
            DispatchQueue.main.async {
                let user = self.getRetrievedUser()
                let replyComment = Comment(full_name: (user?.user_name ?? ""),
                                           user_id: (user?.user_id ?? ""),
                                           user_comment: (text ?? ""),
                                           user_image: (user?.user_image ?? ""),
                                           submitted_timestamp: "\(Date())",
                                           is_like: "false",
                                           parent_id: parentId,
                                           comment_id: comment.comment_id)
                self.updatedCommentList.insert(replyComment, at: (indexPath.row + 1))
                self.commentTextField.text = ""
                self.commentTextField.resignFirstResponder()
                self.dismissReplyCommentView()
                self.commentsTable.reloadData()
            }
        }
    }
    
    private func dismissReplyCommentView() {
        replyView.removeFromSuperview()
        backgroundView.removeFromSuperview()
    }
    
}

extension CommentVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
