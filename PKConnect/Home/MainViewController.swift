//
//  MainViewController.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit

class MainViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var feedTable: UITableView!
    @IBOutlet private var segments: [UIButton]!
    @IBOutlet private var underlineLeadingConstraint: NSLayoutConstraint!
    private var haveNextItems: Bool = false
    
    var campaigns = [Campaign]()
    var gridArray = [ResultGrid]()
    var shortsArray = [AllShorts]()
    var newsMediaArray = [NewsItem]()
    var gridHeight : CGFloat?
    
    private var isHamburgerMenuShown: Bool = false,
                homeFeedItems = [FeedItem](),
                refreshControl = UIRefreshControl(),
                section = "",
                pageIndex = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gridHeight = 100.0
        getCampaigns { [self] campaigns in
            self.campaigns = campaigns
            
        }
        
        print("", campaigns)
        
        let cell = UINib(nibName: LoadMoreCell.identifier, bundle: nil)
        feedTable.register(cell, forCellReuseIdentifier: LoadMoreCell.identifier)
        
        feedTable.register(UINib(nibName: "FeedTableCell", bundle: nil), forCellReuseIdentifier: "feedTableCell")
        feedTable.register(UINib(nibName: "GridTVCell", bundle: nil), forCellReuseIdentifier: "GridTVCell")
        feedTable.register(UINib(nibName: "ShortsTVCell", bundle: nil), forCellReuseIdentifier: "ShortsTVCell")
        feedTable.register(UINib(nibName: "NewsMediaTVCell", bundle: nil), forCellReuseIdentifier: "NewsMediaTVCell")
        feedTable.register(UINib(nibName: "PollTableViewCell", bundle: nil), forCellReuseIdentifier: "PollTableViewCell")
        view.backgroundColor = .white
        view.layoutIfNeeded()
        
        callStoriesAPI()
        segmentTapped(segments[0])
        getProfile { profile in
            if var user = self.getRetrievedUser() {
                        self.sendAppVersion(user_id: user.user_id ?? "") { response in
                            DispatchQueue.main.async {
                                if let refCode = profile.referal_code { user.referal_code = refCode }
                                if let imageUrl = profile.user_image { user.user_image = imageUrl }
                                if let userData = try? JSONEncoder().encode(user)  {
                                    UserDefaults.standard.set(userData, forKey: "User")
                                }
                            }
                        }
                    }
        }
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        feedTable.addSubview(refreshControl)
        
        let footerView = UIView()
        footerView.frame.size.height = 1
        feedTable.tableFooterView = footerView
    }
    
    func callStoriesAPI() {
        showLoadingIndicator(in: self.view)
        getHomeData(section: "stories", count: "0") { feedItems in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                if let vc = self.children.first as? StoriesViewController, let feedItems = feedItems {
                    vc.storyItems = feedItems.RESULT ?? []
                    self.getHomeGridData()
                }
            }
        }
    }
    
    func getHomeGridData() {
        showLoadingIndicator(in: self.view)
        getHomeGrid { homegrid in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                self.gridArray = homegrid.RESULT ?? []
                self.feedTable.reloadData()
                self.getShortsData()
                
            }
        }
    }
    
    func getShortsData() {
        showLoadingIndicator(in: self.view)
        getHomeShorts { response in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                self.shortsArray = response.RESULT ?? []
                self.feedTable.reloadData()
                self.getNewsMediaData()
            }
            
        }
    }
    
    func getNewsMediaData() {
        showLoadingIndicator(in: self.view)
        getNewsMedia { response in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                self.hideLoadingIndicator()
                self.newsMediaArray = response.RESULT ?? []
                self.feedTable.reloadData()
            }
            
        }
    }
    
    func callHomeFeedAPI() {
        getHomeData(section: section, count: "\(pageIndex)") { feedItems in
            if let feedItems = feedItems {
                if(self.pageIndex == 1){
                    self.homeFeedItems.removeAll()
                }
                self.homeFeedItems = feedItems.RESULT ?? []
                self.haveNextItems = (feedItems.RESULT?.count ?? 0 > 0)
                DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                    self.feedTable.reloadData()
                }
            }
        }
    }
    
    @objc func refresh() {
        callHomeFeedAPI()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return gridHeight ?? 0
        }else if indexPath.section == 1{
            return 320.0
        }else if indexPath.section == 2 {
            return 390.0
        }
        else{
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in each section
        switch section {
        case 0:
            return 1 // For GridTVCell
        case 1:
            return 1 // For ShortsTVCell
        case 2:
            return 1 // For NewsMediaTVCell
        default:
            // For FeedTableCells
            return homeFeedItems.count
        }
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
            
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "GridTVCell") as! GridTVCell
            cell.gridArray = self.gridArray
            cell.parentVC = self
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShortsTVCell") as! ShortsTVCell
            cell.shortsArray = self.shortsArray
            cell.lblTitle.text = "Shorts"
            cell.parentVC = self
            cell.imgCat.image = UIImage(named: "ic_shorts")
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsMediaTVCell") as! NewsMediaTVCell
            cell.NewsMediaArray = self.newsMediaArray
            cell.parentVC = self
            return cell
            
        default:
            if(indexPath.row == homeFeedItems.count){
                let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreCell.identifier) as! LoadMoreCell
                cell.tapLoadMore = {
                    self.showLoadingIndicator(in: self.view)
                    self.pageIndex += 1
                    
                    self.getHomeData(section: self.section, count: "\(self.pageIndex * 10)") { feedItems in
                        if let feedItems = feedItems {
                            if(self.pageIndex == 1){
                                self.homeFeedItems.removeAll()
                            }
                            
                            self.haveNextItems = (feedItems.RESULT?.count ?? 0 > 0)
                            self.homeFeedItems.append(contentsOf: feedItems.RESULT ?? [])
                            DispatchQueue.main.async {
                                self.hideLoadingIndicator()
                                self.feedTable.reloadData()
                            }
                        }
                    }
                }
                return cell
            } else {
                if homeFeedItems[indexPath.row].home_section == "poll" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PollTableViewCell", for: indexPath) as! PollTableViewCell
                    cell.constant_Height_Option_Tbl.constant = 80
//                    cell.setupDataForPoll()
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "feedTableCell", for: indexPath) as! FeedTableCell
                    
                    var feedItem = homeFeedItems[indexPath.row]
                    feedItem.created_date = getFormattedDate(from: feedItem.created_date, inputFormat: "yyyy-MM-dd HH:mm:ss", outputFormat: 0)
                    
                    cell.tableDelegate = self
                    cell.setUpContent(feedItem)
                    
                    cell.likeAction = { self.likeTapped(cell: cell, newsId: feedItem.news_id) }
                    cell.commentAction = {
                        self.commentTapped(entry: feedItem, newsId: feedItem.news_id)
                    }
                    cell.bookmarkAction = {self.bookmarkTapped(cell: cell, newsId: feedItem.news_id) {}}
                    
                    cell.downloadAction = {
                        
                        if let mediaSet = feedItem.content_media_set {
                            if mediaSet.first?.media_type == "1" {
                                //
                            } else if mediaSet.first?.media_type == "2" {
                                filedownLoad(view: self,mediaFile: mediaSet.first!.media_url,fileName: feedItem.news_title)
                            }
                        }
                    }
                    
                    if let mediaSet = feedItem.content_media_set {
                        if mediaSet.first?.media_type == "1" {
                            //
                        } else if mediaSet.first?.media_type == "2" {
                            cell.shareAction = { self.shareVideoURL(url: mediaSet.first!.media_url,feedItem: feedItem) }
                        }
                    }
                    
                    cell.viewLikers = {
                        let vc = LikesListVC(nibName: LikesListVC.identifier, bundle: nil)
                        vc.news_id = feedItem.news_id
                        vc.clicks_user_list = feedItem.clicks_user_list
                        vc.modalPresentationStyle = .overCurrentContext
                        vc.modalTransitionStyle = .crossDissolve
                        self.present(vc, animated: true, completion: nil)
                    }
                    return cell
                    
                }
            }
        }
    }
    
    // Your table view delegate methods
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if campaigns.isEmpty {
            return nil
        } else if section == 0 && !campaigns.isEmpty {
            let bannerVC = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "PKPageView") as! PKPageViewController
            addChild(bannerVC)
            bannerVC.campaigns = self.campaigns
            bannerVC.didMove(toParent: self)
            return bannerVC.view
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 && !campaigns.isEmpty {
            return 190
        } else {
            return 0
        }
    }
    
    @IBAction func segmentTapped(_ sender: UIButton) {
        
        underlineLeadingConstraint.constant = sender.frame.origin.x
        
        pageIndex = 1
        switch sender.tag {
        case 20:
            underlineLeadingConstraint.constant = 5
            section = ""
        case 21:
            section = "news"
        case 22:
            section = "people_speaks"
        case 23:
            section = "gallery"
        default:
            return
        }
        
        callHomeFeedAPI()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) { }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 3 {
            guard let cell = cell as? FeedTableCell else { return }
            DispatchQueue.main.async {
                cell.player?.pause()
            }
        }
    }
}

extension MainViewController: FeedTableCellDelegate {
    
    func loadImg(urlStr: String, completion: @escaping (Data?, Error?) -> Void) {
        guard let imageUrl = URL(string: urlStr) else { return }
        self.loadImage(url: imageUrl) { data, error in
            completion(data, error)
        }
    }
    
    func updateTableView() {
        UIView.setAnimationsEnabled(false)
        feedTable.beginUpdates()
        feedTable.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func downloadMedia(_ img: UIImage) {
        self.downloadTapped(img)
    }
    
    func shareMedia(_ img: UIImage,feedItem:FeedItem) {
        self.shareTapped(img,feedItem:feedItem)
    }
    
    func showNewsDetailFor(_ id: String, _ img: UIImage?) {
        self.showNewsDetailView(id, img)
    }
}








