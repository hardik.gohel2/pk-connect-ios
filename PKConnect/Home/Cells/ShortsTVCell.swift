//
//  ShortsTVCell.swift
//  PKConnect
//
//  Created by admin on 30/10/23.
//

import UIKit

class ShortsTVCell: UITableViewCell {

    let cellWidth: CGFloat = 152
        let cellHeight: CGFloat = 246
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
            let nib = UINib(nibName: "ShortsCVCell", bundle: nil)
            collectionView.register(nib, forCellWithReuseIdentifier: "ShortsCVCell")

        }
    }
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgCat: UIImageView!
    var parentVC : MainViewController?
    var shortsArray : [AllShorts]? {
        didSet {
            collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func onClickViewAll(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ShortsViewAllVC") as! ShortsViewAllVC
        vc.shortsArray = self.shortsArray ?? []
        self.parentVC?.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension ShortsTVCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 152.0 , height: 246.0)
        }

        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        }

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 8.0
        }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShortsCVCell", for: indexPath) as! ShortsCVCell
        cell.imgNews.sd_setImage(with: URL(string: shortsArray?[indexPath.row].video_thumb ?? ""))
       
            return cell
        }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shortsArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "MyShotsVC") as! MyShotsVC
        vc.shortList = self.shortsArray ?? []
        vc.rowIndex = indexPath.row
        self.parentVC?.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
}
