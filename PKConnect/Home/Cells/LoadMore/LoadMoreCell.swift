//
//  LoadMoreCell.swift
//  PKConnect
//
//  Created by Yatindra on 19/01/23.
//

import UIKit

class LoadMoreCell: UITableViewCell {

    @IBOutlet weak var btnLoadMore: UIButton!

    var tapLoadMore: (()->())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnLoadMoreClickL(_ sender: UIButton) {
        tapLoadMore?()
    }
    
}


extension LoadMoreCell: Identifiable{
    static let identifier: String = String(describing: LoadMoreCell.self)
}
