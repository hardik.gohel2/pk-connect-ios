//
//  CommentTableCell.swift


import UIKit

class CommentTableCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var timeStamp: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var userImageLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var btn_Reply: RoundedButton!
    
    var replyCommentAction: (()->())?,
        deleteCommentAction: (()->())?,
        likeCommentAction: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImage.layer.cornerRadius = 22
    }
    
    override func prepareForReuse() {
        self.userImage.image = UIImage(named: "user_44")
    }
    
    @IBAction func replyCommentTapped(_ sender: Any) {
        replyCommentAction?()
    }
    
    @IBAction func deleteCommentTapped(_ sender: Any) {
        deleteCommentAction?()
    }
    
    @IBAction func likeCommentTapped(_ sender: Any) {
        likeCommentAction?()
    }
    
}
