//
//  GridTVCell.swift
//  PKConnect
//
//  Created by admin on 28/10/23.
//

import UIKit

class GridTVCell: UITableViewCell {
    
    var gridArray: [ResultGrid]? {
        didSet{
            let result = ResultGrid(title: "View More", image: "viewall", type: "", url: "", pta: "", district: "", ac: "", block: "", priority: "", status: "", createdDate: "")
            if gridArray?.count ?? 0 > 7   {
                if parentVC?.gridHeight == 140.0 {
                    gridArray?.insert(result, at: 7)
                }
            }
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
        }
    }
    var parentVC : MainViewController?
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            let layout = UICollectionViewFlowLayout()
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            collectionView.collectionViewLayout = layout
            let nib = UINib(nibName: "HomeGridCell", bundle: nil)
            collectionView.register(nib, forCellWithReuseIdentifier: "HomeGridCell")
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


extension GridTVCell : UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if gridArray?[indexPath.row].title == "View More" {
            let cell : HomeGridCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeGridCell", for: indexPath) as! HomeGridCell
            
                cell.imgCat.image = UIImage(named: "viewall")
            if parentVC?.gridHeight == 140.0 {
                cell.lblCatName.text = "View More"
                cell.imgCat.image = UIImage(named: "viewall")
            }else{
                cell.lblCatName.text = "View Less"
                cell.imgCat.image = UIImage(named: "showless")
            }
            return cell
        }else{
            let cell : HomeGridCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeGridCell", for: indexPath) as! HomeGridCell
            cell.imgCat.sd_setImage(with: URL(string:gridArray?[indexPath.row].image ?? ""))
            cell.lblCatName.text = gridArray?[indexPath.row].title
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return gridArray?.count ?? 0
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 4
        let height: CGFloat = 90.0
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if gridArray?[indexPath.row].title == "View More" {
            if parentVC?.gridHeight == 140.0 {
                let result = ResultGrid(title: "View More", image: "viewall", type: "", url: "", pta: "", district: "", ac: "", block: "", priority: "", status: "", createdDate: "")
                gridArray?.removeAll()
                self.parentVC?.gridArray.append(result)
               
                    
                    self.gridArray = self.parentVC?.gridArray
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                        let count = Int(self.gridArray?.count ?? 0) / 4
                        let height = CGFloat(self.gridArray?.count ?? 0) / 4
                        if height > CGFloat(count) {
                            self.parentVC?.gridHeight = CGFloat(Double(count) * 80.0)
                        } else {
                            self.parentVC?.gridHeight = CGFloat(Float(count) * 80.0)
                        }
                        self.parentVC?.feedTable.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                    }

                
            }else{
                self.parentVC?.gridHeight = 140.0
                parentVC?.gridArray.removeAll { (element) -> Bool in
                    
                    return element.title == "View More"
                }
                
                self.parentVC?.feedTable.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                
            }
        }else{
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "NewsListingVC") as! NewsListingVC
            vc.isfromMedia = false
            if gridArray?[indexPath.row].title == "Padyatra" {
                vc.section = "news"
                parentVC?.navigationController?.pushViewController(vc, animated: true)
            }else if gridArray?[indexPath.row].title == "Jansuraaj" {
                vc.section = "people_speaks"
                parentVC?.navigationController?.pushViewController(vc, animated: true)
            }else if gridArray?[indexPath.row].title == "Exclusive" {
                vc.section = "gallery"
                parentVC?.navigationController?.pushViewController(vc, animated: true)
            }else if gridArray?[indexPath.row].title == "Quiz" {
                let storyBoard = UIStoryboard(name: "Quiz", bundle: nil)
                let quizvc = storyBoard.instantiateViewController(withIdentifier: "QuizListingVC") as! QuizListingVC
                parentVC?.navigationController?.pushViewController(quizvc, animated: true)
            }else if gridArray?[indexPath.row].title == "Learning Module" {
                if UserDefaults.standard.value(forKey: "isPassedJourney") as? Bool == true {
                    let storyBoard = UIStoryboard(name: "Journey", bundle: nil)
                    let learnvc = storyBoard.instantiateViewController(withIdentifier: "LearningModuleVC") as! LearningModuleVC
                    parentVC?.navigationController?.pushViewController(learnvc, animated: true)
                }else {
                    let storyBoard = UIStoryboard(name: "Journey", bundle: nil)
                    let learnvc = storyBoard.instantiateViewController(withIdentifier: "MyJourneyVC") as! MyJourneyVC
                    parentVC?.navigationController?.pushViewController(learnvc, animated: true)
                }
            }
            else{
                
            }
            
            
        }
        
    }
    
}
