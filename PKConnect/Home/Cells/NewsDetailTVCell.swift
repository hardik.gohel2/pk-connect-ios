//
//  NewsDetailTVCell.swift
//  PKConnect
//
//  Created by admin on 02/11/23.
//

import UIKit
import AVFoundation
import AVKit

class NewsDetailTVCell: UITableViewCell {

    @IBOutlet weak var viewPlayer: UIView!
    @IBOutlet weak var btnLink: UIButton!
    @IBOutlet weak var lblSeenBy: UILabel!
    @IBOutlet weak var btnShare: RoundedButton!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblNews: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblViewMore: UILabel!
    
    var player: AVPlayer?,
        avpController = AVPlayerViewController()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(stopPlayer), name: Notification.Name("PlayerStop"), object: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func pauseVideo() {
            
            player?.pause()
        }
    
    @objc func stopPlayer() {
        player?.pause()
        avpController.removeFromParent()
    }
    
    func setVideoURL(urlStr: String) {
        let videoUrl = URL(string: urlStr)
        player = AVPlayer(url: videoUrl!)
        avpController.player = player
        avpController.view.frame = viewPlayer.bounds
        self.viewPlayer.addSubview(avpController.view)
        player?.play()
    }
    
}
