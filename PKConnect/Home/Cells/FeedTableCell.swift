//
//  FeedTableCell.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit
import AVKit
import SDWebImage

protocol FeedTableCellDelegate: AnyObject {
    func loadImg(urlStr: String, completion: @escaping (Data?, Error?) -> Void)
    func updateTableView()
    func downloadMedia(_ img: UIImage)
    func shareMedia(_ img: UIImage,feedItem:FeedItem)
    func showNewsDetailFor(_ id: String, _ img: UIImage?)
}

class FeedTableCell: UITableViewCell {
    
    @IBOutlet weak var newsDate: UILabel!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsDescription: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsVideo: UIView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var bookmarkButton: UIButton!
    
    @IBOutlet weak var likedUsersView: UIView!
    @IBOutlet weak var clickedUser1: UIImageView!
    @IBOutlet weak var clickedUser2: UIImageView!
    @IBOutlet weak var totalLikes: UILabel!
    @IBOutlet weak var stkPeople: UIStackView!
    @IBOutlet weak var likedUserViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    weak var tableDelegate: FeedTableCellDelegate?
    
    var player: AVPlayer?,
        avpController = AVPlayerViewController()
    
    var tapAction: (()->())?,
        likeAction: (()->())?,
        commentAction: (()->())?,
        shareAction: (()->())?,
        downloadAction: (()->())?,
        bookmarkAction: (()->())?,
        viewLikers: (()->())?
    
    var playAction: (()->())?
    private var reloadDataCompletionBlock: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(stopPlayer), name: Notification.Name("PlayerStop"), object: nil)
        let tapGesture1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(cellTapped))
        let tapGesture2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(cellTapped))
        tapGesture1.delegate = self
        tapGesture2.delegate = self
        
        newsImage.addGestureRecognizer(tapGesture1)
        newsImage.isUserInteractionEnabled = true
        
        newsDescription.addGestureRecognizer(tapGesture2)
        newsDescription.isUserInteractionEnabled = true
        
        likeButton.setImage(UIImage(named: "like"), for: .normal)
        likeButton.setImage(UIImage(named: "liked"), for: .selected)
        
        bookmarkButton.setImage(UIImage(named: "bookmark"), for: .normal)
        bookmarkButton.setImage(UIImage(named: "bookmarked"), for: .selected)
        
        clickedUser1.layer.cornerRadius = 25/2
        clickedUser2.layer.cornerRadius = 25/2
        
        let tapGestureLabel = UITapGestureRecognizer(target: self, action: #selector(self.tapAction(_:)))
        totalLikes.addGestureRecognizer(tapGestureLabel)
        
        let tapGestureStack = UITapGestureRecognizer(target: self, action: #selector(self.tapAction(_:)))
        stkPeople.addGestureRecognizer(tapGestureStack)
        
        self.layoutIfNeeded()
        self.layoutSubviews()
        
    }
    func reloadDataWithCompletion(completion: @escaping () -> Void) {
        reloadDataCompletionBlock = completion
        
    }
    @objc func tapAction(_ recognizer: UITapGestureRecognizer)  {
        viewLikers?()
    }
    @objc func stopPlayer() {
        player?.pause()
        avpController.removeFromParent()
    }
    
    func setVideoURL(urlStr: String) {
        newsVideo.isHidden = false
        newsImage.isHidden = true
        
        let videoUrl = URL(string: urlStr)
        player = AVPlayer(url: videoUrl!)
        avpController.player = player
        avpController.view.frame = newsVideo.bounds
        self.newsVideo.addSubview(avpController.view)
        player?.play()
        
        playButton.isHidden = true
    }
    
    @IBAction func likeTapped(_ sender: Any) {
        self.likeAction?()
    }
    
    @IBAction func commentTapped(_ sender: Any) {
        self.commentAction?()
    }
    
    @IBAction func bookmarkTapped(_ sender: Any) {
        self.bookmarkAction?()
    }
    
    @IBAction func shareTapped(_ sender: UIButton) {
        self.shareAction?()
    }
    
    @IBAction func downloadTapped(_ sender: Any) {
        self.downloadAction?()
    }
    
    @IBAction func playVideo(_ sender: Any) {
        self.playAction?()
    }
    
    @objc func cellTapped() {
        self.tapAction?()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        player?.pause()
        avpController.removeFromParent()
        let placeholderImage = resizeImage(image: UIImage(named: "PKConnectLogo")!, newWidth: self.frame.width)
        imageHeightConstraint.constant = placeholderImage.size.height
        newsImage.image = placeholderImage
    }
    

    func setDynamicImageFrom(url: String, completion: @escaping (UIImage?)->()) {
        if let delegate = tableDelegate {
            delegate.loadImg(urlStr: url) { data, error in
                DispatchQueue.main.async { [self] in
                    if let data = data, let customImage = UIImage(data: data) {
                        let resizedImage = self.resizeImage(image: customImage, newWidth: self.frame.size.width)
                        self.imageHeightConstraint.constant = resizedImage.size.height
                        self.newsImage.image = resizedImage
                        completion(resizedImage)
                    }
                }
            }
        }
    }


func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
    let scale = newWidth/image.size.width
    let newHeight = image.size.height * scale
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
    image.draw(in: CGRectMake(0, 0, newWidth, newHeight))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return newImage
}

//    func setUpContent(_ feedItem: FeedItem) {
//        self.playButton.isHidden = true
//        //Media
//        if let mediaSet = feedItem.content_media_set {
//            if mediaSet.first?.media_type == "1" {
//                if let urlStr = mediaSet.first?.media_url {
//
//                    self.setDynamicImageFrom(url: urlStr) { img in
//                        if let img = img {
//                            DispatchQueue.main.async {
//                                self.tapAction = { self.tableDelegate?.showNewsDetailFor(feedItem.news_id, img) }
//                                self.downloadAction = { self.tableDelegate?.downloadMedia(img) }
//                                self.shareAction = { self.tableDelegate?.shareMedia(img, feedItem: feedItem) }
//                                self.tableDelegate?.updateTableView()
//                            }
//                        }
//                    }
//
//                }
//            } else if mediaSet.first?.media_type == "2" {
//                self.playButton.isHidden = false
//                if let urlStr = mediaSet.first?.media_thumb {
//
//                    self.playAction = { self.setVideoURL(urlStr: mediaSet.first!.media_url) }
//
//                    self.setDynamicImageFrom(url: urlStr) { thumbImg in
//                        DispatchQueue.main.async {
//                            self.tapAction = {
//                                self.player?.pause()
//                                self.tableDelegate?.showNewsDetailFor(feedItem.news_id, thumbImg)
//                            }
//                            self.tableDelegate?.updateTableView()
//                        }
//                    }
//                }
//            } else {
//                DispatchQueue.main.async { [self] in
//                    let placeholderImage = resizeImage(image: UIImage(named: "PKConnectLogo")!, newWidth: self.frame.width)
//                    imageHeightConstraint.constant = placeholderImage.size.height
//                    newsImage.image = placeholderImage
//                }
//            }
//        }
//        self.newsVideo.isHidden = true
//        self.newsImage.isHidden = false
//
//
//        self.newsVideo.isHidden = true
//        self.newsImage.isHidden = false
//        self.playButton.isHidden = true
//
//
//        self.newsTitle.text = feedItem.news_title
//        self.newsDate.text = feedItem.created_date
//        self.newsDescription.text = feedItem.news_description.removeHTMLTag()
//
//        //Like, Comment, Share & Download
//        self.likeButton.isSelected = (feedItem.is_like == "dislike") ? false : true
//        self.bookmarkButton.isSelected = (feedItem.is_bookmarked == "yes") ? true : false
//
//        //Liked Users
//        let totalLikes = (Int(feedItem.total_likes) ?? 0)
//        if totalLikes == 0 {
//            self.likedUsersView.isHidden = true
//            self.likedUserViewHeight.constant = 10
//        } else {
//            self.likedUsersView.isHidden = false
//            self.likedUserViewHeight.constant = 45
//            self.clickedUser1.image = UIImage(named: "person_white")
//            self.clickedUser2.image = UIImage(named: "person_white")
//            if let img1 = feedItem.clicks_user_list[0].user_image {
//                clickedUser1.setLoadedImage(img1)
//            }
//            if totalLikes >= 2, let img2 = feedItem.clicks_user_list[1].user_image {
//                clickedUser2.setLoadedImage(img2)
//            }
//            self.clickedUser2.isHidden = totalLikes == 1 ? true : false
//            self.totalLikes.text = String(format: "%@ liked the post".localize(), (totalLikes > 2 ? "+\(totalLikes - 2)" : ""))
//        }
//
//        //Media
//        if let mediaSet = feedItem.content_media_set {
//            if mediaSet.first?.media_type == "1" {
//                if let urlStr = mediaSet.first?.media_url {
//                    self.setDynamicImageFrom(url: urlStr) { img in
//                        if let img = img {
//                            DispatchQueue.main.async {
//                                self.tapAction = { self.tableDelegate?.showNewsDetailFor(feedItem.news_id, img) }
//                                self.downloadAction = { self.tableDelegate?.downloadMedia(img) }
//                                self.shareAction = { self.tableDelegate?.shareMedia(img,feedItem: feedItem) }
//                                self.tableDelegate?.updateTableView()
//                            }
//                        }
//                    }
//                }
//            } else if mediaSet.first?.media_type == "2" {
//                self.playButton.isHidden = false
//                if let urlStr = mediaSet.first?.media_thumb {
//                    self.setDynamicImageFrom(url: urlStr) { thumbImg in
//                        DispatchQueue.main.async {
//                            self.tapAction = {
//                                self.player?.pause()
//                                self.tableDelegate?.showNewsDetailFor(feedItem.news_id, thumbImg)
//                            }
//                            self.playAction = { self.setVideoURL(urlStr: mediaSet.first!.media_url) }
//                            self.tableDelegate?.updateTableView()
//                        }
//                    }
//                }
//            } else {
//                DispatchQueue.main.async { [self] in
//                    let placeholderImage = resizeImage(image: UIImage(named: "PKConnectLogo")!, newWidth: self.frame.width)
//                    imageHeightConstraint.constant = placeholderImage.size.height
//                    newsImage.image = placeholderImage
//                }
//            }
//        }
//    }
    
    func setUpContent(_ feedItem: FeedItem) {
        
        
        
        self.playButton.isHidden = true
        //Media
        if let mediaSet = feedItem.content_media_set {
            if mediaSet.first?.media_type == "1" {
                if let urlStr = mediaSet.first?.media_url {
                    
                    self.setDynamicImageFrom(url: urlStr) { img in
                        if let img = img {
                            DispatchQueue.main.async {
                                self.tapAction = { self.tableDelegate?.showNewsDetailFor(feedItem.news_id, img) }
                                self.downloadAction = { self.tableDelegate?.downloadMedia(img) }
                                self.shareAction = { self.tableDelegate?.shareMedia(img, feedItem: feedItem) }
                                self.tableDelegate?.updateTableView()
                            }
                        }
                    }
                    
                }
            } else if mediaSet.first?.media_type == "2" {
                self.playButton.isHidden = false
                if let urlStr = mediaSet.first?.media_thumb {
                    
                    self.playAction = { self.setVideoURL(urlStr: mediaSet.first!.media_url) }

                    self.setDynamicImageFrom(url: urlStr) { thumbImg in
                        DispatchQueue.main.async {
                            self.tapAction = {
                                self.player?.pause()
                                self.tableDelegate?.showNewsDetailFor(feedItem.news_id, thumbImg)
                            }
                            self.tableDelegate?.updateTableView()
                        }
                    }
                }
            } else {
                DispatchQueue.main.async { [self] in
                    let placeholderImage = resizeImage(image: UIImage(named: "PKConnectLogo")!, newWidth: self.frame.width)
                    imageHeightConstraint.constant = placeholderImage.size.height
                    newsImage.image = placeholderImage
                }
            }
        }
        
        
        
        
        
        self.newsVideo.isHidden = true
        self.newsImage.isHidden = false
        self.playButton.isHidden = true

        self.newsTitle.text = feedItem.news_title
      //  self.newsDate.text = feedItem.created_date
        self.newsDescription.text = feedItem.news_description.removeHTMLTag()
        
        //Like, Comment, Share & Download
        self.likeButton.isSelected = (feedItem.is_like == "dislike") ? false : true
        self.bookmarkButton.isSelected = (feedItem.is_bookmarked == "yes") ? true : false
        
        //Liked Users
        let totalLikes = (Int(feedItem.total_likes) ?? 0)
        if totalLikes == 0 {
            self.likedUsersView.isHidden = true
            self.likedUserViewHeight.constant = 10
        } else {
            self.likedUsersView.isHidden = false
            self.likedUserViewHeight.constant = 45
            self.clickedUser1.image = UIImage(named: "person_white")
            self.clickedUser2.image = UIImage(named: "person_white")
//            if let img1 = feedItem.clicks_user_list[0].user_image {
//                clickedUser1.setLoadedImage(img1)
//            }
//            if totalLikes >= 2, let img2 = feedItem.clicks_user_list[1].user_image {
//                clickedUser2.setLoadedImage(img2)
//            }
//            self.clickedUser2.isHidden = totalLikes == 1 ? true : false
//            self.totalLikes.text = String(format: "%@ liked the post".localize(), (totalLikes > 2 ? "+\(totalLikes - 2)" : ""))
            
            //hardik addwd
            if feedItem.clicks_user_list.count > 0{
                         if let img1 = feedItem.clicks_user_list[0].user_image {
                             clickedUser1.setLoadedImage(img1)
                         }
                if feedItem.clicks_user_list.count < 1 {
                    if totalLikes >= 2, let img2 = feedItem.clicks_user_list[1].user_image {
                        clickedUser2.setLoadedImage(img2)
                    }
                }
                         self.clickedUser2.isHidden = totalLikes == 1 ? true : false
                         self.totalLikes.text = String(format: "%@ liked the post".localize(), (totalLikes > 2 ? "+\(totalLikes - 2)" : ""))
                     }
                    
        }

        //Media
        if let mediaSet = feedItem.content_media_set {
            if mediaSet.first?.media_type == "1" {
                if let urlStr = mediaSet.first?.media_url {
                    self.setDynamicImageFrom(url: urlStr) { img in
                        if let img = img {
                            DispatchQueue.main.async {
                                self.tapAction = { self.tableDelegate?.showNewsDetailFor(feedItem.news_id, img) }
                                self.downloadAction = { self.tableDelegate?.downloadMedia(img) }
                                self.shareAction = { self.tableDelegate?.shareMedia(img, feedItem: feedItem) }
                                self.tableDelegate?.updateTableView()
                            }
                        }
                    }
                }
            } else if mediaSet.first?.media_type == "2" {
                self.playButton.isHidden = false
                if let urlStr = mediaSet.first?.media_thumb {
                    self.setDynamicImageFrom(url: urlStr) { thumbImg in
                        DispatchQueue.main.async {
                            self.tapAction = {
                                self.player?.pause()
                                self.tableDelegate?.showNewsDetailFor(feedItem.news_id, thumbImg)
                            }
                            self.playAction = { self.setVideoURL(urlStr: mediaSet.first!.media_url) }
                            self.tableDelegate?.updateTableView()
                        }
                    }
                }
            } else {
                DispatchQueue.main.async { [self] in
                    let placeholderImage = resizeImage(image: UIImage(named: "PKConnectLogo")!, newWidth: self.frame.width)
                    imageHeightConstraint.constant = placeholderImage.size.height
                    newsImage.image = placeholderImage
                }
            }
        }
    }
}

