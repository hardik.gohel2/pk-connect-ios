//
//  NewsMediaListCell.swift
//  PKConnect
//
//  Created by admin on 11/12/23.
//

import UIKit

class NewsMediaListCell: UITableViewCell {

    @IBOutlet weak var btnShare: RoundedButton!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgNews: UIImageView!
    
    var readMoreTapped: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(readMoreTapped(_:)))
        lblDesc.addGestureRecognizer(tapGesture)
        lblDesc.isUserInteractionEnabled = true
    }
    
    @objc func readMoreTapped(_ sender: UITapGestureRecognizer) {
            readMoreTapped?()
        }
    func setupReadMore() {
         let fullText = lblDesc.text ?? ""
            let numberOfLines = 2
            let readMoreText = "... Read More"

            let attributedString = NSMutableAttributedString(string: fullText)

            let range = NSRange(location: min(numberOfLines, attributedString.length), length: (attributedString.length - min(numberOfLines, attributedString.length)))
            attributedString.replaceCharacters(in: range, with: "")

            let readMoreAttributedString = NSMutableAttributedString(string: readMoreText)
            readMoreAttributedString.addAttribute(.foregroundColor, value: UIColor.blue, range: NSRange(location: 0, length: readMoreAttributedString.length))
            readMoreAttributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 14, weight: .bold), range: NSRange(location: 0, length: readMoreAttributedString.length))
            readMoreAttributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: readMoreAttributedString.length))

            attributedString.append(readMoreAttributedString)
            lblDesc.attributedText = attributedString
            lblDesc.numberOfLines = numberOfLines
        }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
