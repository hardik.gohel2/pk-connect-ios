//
//  NewsMediaCVCell.swift
//  PKConnect
//
//  Created by admin on 01/11/23.
//

import UIKit

class NewsMediaCVCell: UICollectionViewCell {

   
   
    @IBOutlet weak var btnShare: RoundedButton!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblews: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgNews: UIImageView!{
        didSet {
            self.contentView.roundCorners(corners: [.topLeft, .topRight], radius: 6.0)
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
        setupCell()
        }
    
    private func setupCell() {
            
                layer.cornerRadius = 6.0
                layer.masksToBounds = false
                layer.shadowColor = UIColor.black.cgColor
                layer.shadowOffset = CGSize(width: 0, height: 2)
                layer.shadowRadius = 2
                layer.shadowOpacity = 0.4
                layer.backgroundColor = UIColor.white.cgColor
        
        
        }
    
    override func layoutSubviews() {
           super.layoutSubviews()
           
           layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
       }

}
extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
