//
//  PollTableViewCell.swift
//  PKConnect
//
//  Created by Koshal Saini on 09/01/24.
//

import UIKit

class PollTableViewCell: UITableViewCell {

    @IBOutlet weak var img_Icon: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_Poll_Title: UILabel!
    @IBOutlet weak var img_Poll: UIImageView!
    @IBOutlet weak var lbl_Votes: UILabel!
    @IBOutlet weak var lbl_DaysLeft: UILabel!
    @IBOutlet weak var tbl_Options: UITableView!
    @IBOutlet weak var constant_Height_Option_Tbl: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setupDataForPoll(data:FeedItem) {
        self.lbl_Title.text = "Polls"
        self.lbl_Date.text = data.created_date
        self.lbl_Poll_Title.text = data.news_title
        self.lbl_Votes.text = data.total_votes + " votes"
        self.lbl_DaysLeft.text = data.total_votes + " votes"
        
        self.tbl_Options.dataSource = self
        self.tbl_Options.delegate = self
        tbl_Options.register(UINib(nibName: "PollOptionsTableCell", bundle: nil), forCellReuseIdentifier: "PollOptionsTableCell")
    }
}

extension PollTableViewCell: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PollOptionsTableCell", for: indexPath) as! PollOptionsTableCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}
