//
//  LikeItemCell.swift
//  PKConnect
//
//  Created by Yatindra on 19/01/23.
//

import UIKit
import SDWebImage


class LikeItemCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!

    var clicks_user_list: ClickUser? {
        didSet{
            self.setData()
        }
    }

    private func setData() {
        guard let data = clicks_user_list else {return}
        self.lblUserName.text = data.full_name
        
        if let image = data.user_image, image != ""  {
            self.imgUser.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "default_user"))
        } else {
            self.imgUser.image = self.imageWith(name: data.full_name.initialsOne)
        }
        
    }
   
    
    func imageWith(name: String?) -> UIImage? {
         let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
         let nameLabel = UILabel(frame: frame)
         nameLabel.textAlignment = .center
         nameLabel.backgroundColor = appYelloColor
         nameLabel.textColor = .label
         nameLabel.font = UIFont(name: "NunitoSans-SemiBold", size: 35)!
         nameLabel.text = name
         UIGraphicsBeginImageContext(frame.size)
          if let currentContext = UIGraphicsGetCurrentContext() {
             nameLabel.layer.render(in: currentContext)
             let nameImage = UIGraphicsGetImageFromCurrentImageContext()
             return nameImage
          }
          return nil
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension LikeItemCell: Identifiable{
    static let identifier: String = String(describing: LikeItemCell.self)
}

