//
//  ReferralTableCell.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/20/22.
//

import UIKit

class ReferralTableCell: UITableViewCell {

    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var signUpDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
