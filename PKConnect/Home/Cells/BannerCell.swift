//
//  BannerCell.swift
//  Ruchika
//
//  Created by Ruchika on 21/05/20.
//  Copyright © 2020 Dev Mac Mini 2. All rights reserved.
//

import UIKit

class BannerCell: UICollectionViewCell {
    
    @IBOutlet weak var imgBanner: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgBanner.layer.cornerRadius = 10
        imgBanner.layer.masksToBounds = true

    }
}
