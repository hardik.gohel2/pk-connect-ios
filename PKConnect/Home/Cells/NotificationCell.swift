//
//  NotificationCell.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/15/22.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var notificationDate: UILabel!
    @IBOutlet weak var timeDifference: UILabel!
    @IBOutlet weak var message: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
