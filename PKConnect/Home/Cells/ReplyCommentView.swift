//
//  ReplyCommentView.swift
//  DidirDoot
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit

class ReplyCommentView: UIView {
    
    @IBOutlet weak var userImage: UIImageView! {
        didSet {
            userImage.layer.cornerRadius = 25
        }
    }
    
   
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var timeStamp: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var textField: UITextField! {
        didSet {
            textField.placeholder = "Add a comment".localize()
        }
    }
    
    var closeButtonAction: (()->())?, submitButtonAction: (()->())?

    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func closeReplyView(_ sender: Any) {
        closeButtonAction?()
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        if textField.text == "" {
//            showMyAlert()
            self.showToast(message: "Please add a Comments".localize())
              
          } else {
              submitButtonAction?()
          }
        
        
    }
    func showToast(message : String) {
        let alertDisapperTimeInSeconds = 2.0
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
//        self.present(alert, animated: true)
        if let viewController = self.parentViewController {
            viewController.present(alert, animated: true, completion: nil)
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + alertDisapperTimeInSeconds) {
            alert.dismiss(animated: true, completion: nil)
        }
    }
    func showMyAlert() {
            let alert = UIAlertController(title: "", message: "Enter a comments", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)
            if let viewController = self.parentViewController {
                viewController.present(alert, animated: true, completion: nil)
            }
        }
    
    
//    func showMyAlert() {
//        let alert = UIAlertController(title: "", message: NSLocalizedString("Enter a comments", comment: ""), preferredStyle: .alert)
//        let action = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil)
//        alert.addAction(action)
//        if let viewController = self.parentViewController {
//            viewController.present(alert, animated: true, completion: nil)
//        }
//    }
    
    // Get the parent view controller to present the alert
       private var parentViewController: UIViewController? {
           var parentResponder: UIResponder? = self
           while parentResponder != nil {
               parentResponder = parentResponder!.next
               if let viewController = parentResponder as? UIViewController {
                   return viewController
               }
           }
           return nil
       }
}
