//
//  NewsMediaTVCell.swift
//  PKConnect
//
//  Created by admin on 01/11/23.
//

import UIKit

class NewsMediaTVCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
            let nib = UINib(nibName: "NewsMediaCVCell", bundle: nil)
            collectionView.register(nib, forCellWithReuseIdentifier: "NewsMediaCVCell")
            
        }
    }
    var NewsMediaArray : [NewsItem]? {
        didSet {
            collectionView.reloadData()
        }
    }
    var parentVC : MainViewController?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func onClickViewAll(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "NewsListingVC") as! NewsListingVC
        vc.isfromMedia = true
        parentVC?.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension NewsMediaTVCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 302.0 , height: 286.0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 14.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsMediaCVCell", for: indexPath) as! NewsMediaCVCell
       
            cell.imgNews.sd_setImage(with: URL(string: NewsMediaArray?[indexPath.row].image ?? ""))
        
        cell.lblTitle.text = NewsMediaArray?[indexPath.row].title
        cell.lblews.text = NewsMediaArray?[indexPath.row].description?.removeHTMLTag()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = dateFormatter.date(from: NewsMediaArray?[indexPath.row].created_date ?? "") {
            let timeAgo = timeAgoSinceDate(date)
            cell.lblTime.text = timeAgo
        } else {
            print("Invalid date format")
        }
        cell.btnShare.tag = indexPath.row
        cell.btnShare.addTarget(self, action:#selector(shareBtn(_ :)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return NewsMediaArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Other", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "NewsMediaDetailVC") as! NewsMediaDetailVC
        vc.arrayDetail = NewsMediaArray
        vc.scrollIndex = indexPath.row
        parentVC?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func timeAgoSinceDate(_ date: Date) -> String {
        let calendar = Calendar.current
        let now = Date()
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: date, to: now)
        
        if let year = components.year, year > 0 {
            return "\(year) year\(year == 1 ? "" : "s") ago"
        }
        
        if let month = components.month, month > 0 {
            return "\(month) month\(month == 1 ? "" : "s") ago"
        }
        
        if let day = components.day, day > 0 {
            return "\(day) day\(day == 1 ? "" : "s") ago"
        }
        
        if let hour = components.hour, hour > 0 {
            return "\(hour) hour\(hour == 1 ? "" : "s") ago"
        }
        
        if let minute = components.minute, minute > 0 {
            return "\(minute) minute\(minute == 1 ? "" : "s") ago"
        }
        
        return "Just now"
    }
    
    @objc func shareBtn(_ sender: UIButton) {
        let url = URL(string: NewsMediaArray?[sender.tag].url ?? "")!
        
        let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        
        activityViewController.completionWithItemsHandler = { (activityType, completed, returnedItems, error) in
            if completed {
                
                let inputString = activityType?.rawValue ?? "Unknown App"
                let components = inputString.components(separatedBy: ".")
                if components.count > 1 {
                    let extractedValue = components[1]
                    print(extractedValue)
                    self.parentVC?.shareNewsMedia(newsId: self.NewsMediaArray?[sender.tag].id ?? "", platform: extractedValue, completion: {
                        print("shared success")
                    })
                } else {
                    print("The string doesn't have enough components.")
                }
            } else {
                print("Sharing was canceled or failed.")
            }
        }
        
        if let popoverController = activityViewController.popoverPresentationController {
            popoverController.sourceView = parentVC?.view
            
        }
        
        parentVC?.present(activityViewController, animated: true, completion: nil)
    }
    
    
    
}
