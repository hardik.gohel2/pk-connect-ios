//
//  StoryCollectionCell.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit

class StoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnailView: UIImageView!
    @IBOutlet weak var storyLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func layoutSubviews() {
        thumbnailView.layer.cornerRadius = 58/2
//        thumbnailView.layer.masksToBounds = true
        thumbnailView.backgroundColor = .white
        
        thumbnailView.layer.borderWidth = 2
    }
    
    func configure(with image: UIImage) {
//        imageView.image = image
    }
    
}
