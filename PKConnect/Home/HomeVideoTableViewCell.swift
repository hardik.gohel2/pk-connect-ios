//
//  HomeVideoTableViewCell.swift
//  YoutubeShotsDemo

import UIKit

import AVKit

class HomeVideoTableViewCell: UITableViewCell {
    @IBOutlet weak var videoLayerView: UIView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var userNameBtn: UIButton!
    @IBOutlet weak var noOfShareLbl: UILabel!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var numberOfCommentsLbl: UILabel!
    @IBOutlet weak var btnComments: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var numberOfLikes: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var viewCountLbl: UILabel!
    @IBOutlet weak var videoTitleLbl: UILabel!
    @IBOutlet weak var loaderView: UIActivityIndicatorView!
    @IBOutlet weak var img_Play: UIImageView!
    var player: AVPlayer?
    
    var avPlayer: AVPlayer?
    var avPlayerLayer: AVPlayerLayer?
    var paused: Bool = false
    var spinnerView: UIView!
    var isVideoPlaying: Bool = false
    
    @IBOutlet weak var subscribeBtn:UIButton!
    
    var videoPlayerItem: AVPlayerItem? = nil {
        didSet {
            avPlayer?.replaceCurrentItem(with: self.videoPlayerItem)
        }
    }
    var onclickBtn: ((String)->Void)!
    var onClickLike: (()->Void)?
    var onClickComment: (()->Void)?
    var onClickShare : ((UIButton)->Void)!
    var onClickUserTitle: (()->Void)?
    var onClickSubscribe: (()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        profilePic.layer.cornerRadius = profilePic.frame.height/2
    }
    
    func displayData(data: VideosData? = nil) {
        self.numberOfLikes.text = data?.likeCount
        self.numberOfCommentsLbl.text = data?.commentCount
        self.viewCountLbl.text = data?.viewCount
        self.userNameBtn.setTitle(data?.createdBy?.title, for: .normal)
        self.videoTitleLbl.text = data?.shortDesc
        if let imageStr = data?.createdBy?.image{
            self.profilePic.setLoadedImage(imageStr)
        }
    }
    
//    override func layoutSubviews() {
//           super.layoutSubviews()
//           avPlayerLayer?.frame = bounds
//       }
    
    
    func setupMoviePlayer() {
        self.avPlayer = AVPlayer.init(playerItem: self.videoPlayerItem)
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
        avPlayerLayer?.videoGravity = AVLayerVideoGravity.resizeAspect
        avPlayer?.volume = 4
        avPlayer?.actionAtItemEnd = .none
        
        avPlayerLayer?.frame = videoLayerView.bounds // Set the layer's frame to match the videoLayerView's bounds

        
        
        avPlayerLayer?.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.backgroundColor = .clear
        self.videoLayerView.layer.insertSublayer(avPlayerLayer!, at: 0)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.playerItemDidReachEnd(notification:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: avPlayer?.currentItem)
    }
    
    func stopPlayback(){
        self.avPlayer?.pause()
    }
   
    func startPlayback(){
        self.avPlayer?.play()
    }
    
    // A notification is fired and seeker is sent to the beginning to loop the video again
    @objc func playerItemDidReachEnd(notification: Notification) {
        let p: AVPlayerItem = notification.object as! AVPlayerItem
        p.seek(to: CMTime.zero)
    }
    
    @IBAction func onBtnClickName(_ sender: UIButton) {
        onclickBtn(userNameBtn.title(for: .normal)!)

    }
    
    
    @IBAction func onBtnClickLike(_ sender: UIButton) {
        self.onClickLike?()
    }
    
    @IBAction func onBtnClickComments(_ sender: UIButton) {
        self.onClickComment?()
    }
    
    @IBAction func tapSubscribeButton(_ sender: Any) {
        self.onClickSubscribe?()
    }
    
    @IBAction func onClickShareBtn(_ sender: UIButton) {
        //self.onClickShare(sender)
    }
    @IBAction func onClickUserBtn(_ sender: UIButton) {
        self.onClickUserTitle?()
    }
}
