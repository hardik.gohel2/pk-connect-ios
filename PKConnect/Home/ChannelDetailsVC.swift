//
//  ChannelDetailsVC.swift


import UIKit
import SDWebImage

class ChannelDetailsVC: BaseViewController {
    var channel_id:String?
    var shortList = [AllShorts]()
    var channelDetails:ChannelDetails?
    @IBOutlet private weak var shortsCollection:UICollectionView!
    @IBOutlet private weak var channelName:UILabel!
    @IBOutlet private weak var totalsubscribers:UILabel!
    @IBOutlet private weak var totalVideos:UILabel!
    @IBOutlet private weak var channelImg:UIImageView!
    @IBOutlet private weak var subscribeBtn:UIButton!
    @IBOutlet private weak var shortsCollectionheight:NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getchannelDetails(channel_id: channel_id  ?? "") { [weak self]  (result) in
            guard let self else {return}
            DispatchQueue.main.async {
                self.shortList = result.RESULT?.shorts ??  []
                self.channelDetails = result
                self.channelName.text = result.RESULT?.title ?? ""
                self.totalsubscribers.text = "\(result.RESULT?.subscribers ?? "") Subscribers"
                self.totalVideos.text = "\(result.RESULT?.shorts?.count ?? 0) Videos"
                if let imageStr = result.RESULT?.image{
                    self.channelImg.setLoadedImage(imageStr)
                }
                self.shortsCollectionheight.constant = CGFloat.greatestFiniteMagnitude
                self.shortsCollection.reloadData()
                self.shortsCollection.layoutIfNeeded()
                self.shortsCollectionheight.constant = self.shortsCollection.contentSize.height
                let subscription = result.RESULT?.is_subscribed == true
                ? "Subscribed" : "Subscribe"
                self.subscribeBtn.setTitle(subscription, for: .normal)
                if result.RESULT?.is_subscribed == true{
                    self.subscribeBtn.backgroundColor = .lightGray
                }else{
                    self.subscribeBtn.backgroundColor = UIColor(hexString: "#0000FF")
                }
            }
        }
    }
    
    @IBAction private func subscribeBtnAction(_ sender :UIButton){
        if  self.channelDetails?.RESULT?.is_subscribed == false{
            addSubscriptionStatus(channel_id:self.channel_id ?? "") { (result) in
               
                DispatchQueue.main.async {
                    self.subscribeBtn.setTitle("Subscribed", for: .normal)
                    self.subscribeBtn.backgroundColor = .lightGray
                }
            }
        }
    }
    
    @IBAction private func back_Btn(_ sender :UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}

extension ChannelDetailsVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.shortList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyShortsCell", for: indexPath as IndexPath) as? MyShortsCell else {return MyShortsCell()}
        if let imageStr = self.shortList[indexPath.row].video_thumb {
            cell.shorImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.shorImg.sd_setImage(with:URL(string: imageStr) , placeholderImage: nil)
        }
        cell.viewsCount.text = "\(self.shortList[indexPath.row].view_count ?? "") Views"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let numberofItem: CGFloat = 3
        
        let collectionViewWidth = collectionView.frame.width
        
        let extraSpace = (numberofItem - 1) * flowLayout.minimumInteritemSpacing
        
        let inset = flowLayout.sectionInset.right + flowLayout.sectionInset.left
        
        let width = Int((collectionViewWidth - inset - extraSpace) / numberofItem)
        
        return CGSize(width: width, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                let commentVC = storyBoard.instantiateViewController(withIdentifier: "MyShotsVC") as! MyShotsVC
        commentVC.shortList = self.shortList
        commentVC.rowIndex  = indexPath.row
                self.navigationController?.pushViewController(commentVC, animated: true)
    }
    
}


class MyShortsCell:UICollectionViewCell{
    @IBOutlet weak var shorImg:UIImageView!
    @IBOutlet weak var viewsCount:UILabel!
}
