//
//  MoreCell.swift
//  DidirDoot
//
//  Created by Navdip on 17/03/23.
//

import UIKit

class MoreCell: UICollectionViewCell {

    @IBOutlet weak var viewMore: UIView!
    @IBOutlet weak var lblCount: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        viewMore.layer.cornerRadius = (viewMore.mask?.bounds.height ?? 55) / 2
        viewMore.clipsToBounds = true
        // Initialization code
    }

}
extension MoreCell: Identifiable{
    static let identifier: String = String(describing: MoreCell.self)
}
