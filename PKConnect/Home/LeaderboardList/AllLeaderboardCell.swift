//
//  AllLeaderboardCell.swift
//  DidirDoot
//
//  Created by Navdip on 17/03/23.
//

import UIKit

class AllLeaderboardCell: UICollectionViewCell {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var rank: UILabel!
    @IBOutlet weak var name: UILabel!
   
    override func prepareForReuse() {
        name.text = ""
        rank.text = ""
        userImage.image = UIImage(named: "user_44")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        userImage.layer.cornerRadius = 45/2
        userImage.layer.masksToBounds = true
        // Initialization code
    }

}
extension AllLeaderboardCell: Identifiable{
    static let identifier: String = String(describing: AllLeaderboardCell.self)
}
