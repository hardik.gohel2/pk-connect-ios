//
//  LikesListVC.swift
//  DidirDoot
//
//  Created by Yatindra on 19/01/23.
//

import UIKit

class LeaderboardList: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblScreenName: CustomLabel!
    var clicks_user_list: [ClickUser]?
    var news_id = ""
    private var pageIndex = 0
    var leaderboardState: LeaderboardResult?
    var leaderboardDistrict: LeaderboardResult?
    var popUpType = ""

    @IBOutlet weak var collectionLeader: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionLeader.delegate = self
        collectionLeader.dataSource = self
        collectionLeader.reloadData()
        let cell = UINib(nibName: AllLeaderboardCell.identifier, bundle: nil)
        self.collectionLeader.register(cell, forCellWithReuseIdentifier: AllLeaderboardCell.identifier)
        if popUpType == "State" {
            lblScreenName.text = "State Leaderboard".localize()
        }else{
            lblScreenName.text = "District Leaderboard".localize()
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onCloseClick(_ sender: UIButton){
        self.dismiss(animated: true)
    }
}


extension LeaderboardList: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if popUpType == "State" {
            return leaderboardState?.topleaders?.count ?? 0
        } else {
            return leaderboardDistrict?.districtTopleaders?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if popUpType == "State" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllLeaderboardCell", for: indexPath) as! AllLeaderboardCell

            guard let entry = leaderboardState?.topleaders?[indexPath.row] else { return cell }
            cell.rank.text = "\(entry.ranking ?? 0)"
            cell.name.text = entry.full_name
            if let img = entry.user_image {
                cell.userImage.setLoadedImage(img)
            }
            return cell

        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllLeaderboardCell", for: indexPath) as! AllLeaderboardCell

            guard let entry = leaderboardDistrict?.districtTopleaders?[indexPath.row] else { return cell }
            cell.rank.text = "\(entry.ranking ?? 0)"
            cell.name.text = entry.full_name
            if let img = entry.user_image {
                cell.userImage.setLoadedImage(img)
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 4 , height: 100)
    }
}


extension LeaderboardList: Identifiable{
    static let identifier: String = String(describing: LeaderboardList.self)
}
