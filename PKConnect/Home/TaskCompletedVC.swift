//
//  TaskCompletedVC.swift
//  PKConnect
//
//  Created by apple on 14/11/22.
//

import UIKit

class TaskCompletedVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
  

    @IBOutlet weak var tableView: UITableView!
    var task: TaskResult?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
       

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return task?.task_completed_user_list?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCompletedCell", for: indexPath) as! TaskCompletedCell

        cell.nameText.text = self.task?.task_completed_user_list?[indexPath.row].full_name ?? ""
        downloadImageFrom(urlString: self.task?.task_completed_user_list?[indexPath.row].user_image ?? "") { downloadedImage in
            DispatchQueue.main.async {
                cell.photo.layer.cornerRadius = 25
                cell.photo.clipsToBounds = true
                cell.photo.image = downloadedImage
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
