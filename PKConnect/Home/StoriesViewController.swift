//
//  StoriesViewController.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit
import AVFoundation

class StoriesViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    
   
    var storyItems = [FeedItem]() {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: 70, height: 100)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 30
        collectionView.collectionViewLayout = layout
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        storyItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "storyCollectionCell", for: indexPath) as! StoryCollectionCell
        
        let feedItem = storyItems[indexPath.row]
        cell.storyLabel.text = feedItem.news_title
        if let urlStr = feedItem.content_media_set?.first?.media_thumb {
            downloadImageFrom(urlString: urlStr) { downloadedImage in
                DispatchQueue.main.async {
                    cell.thumbnailView.image = downloadedImage
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Other", bundle: nil)
        let storyFullView = storyboard.instantiateViewController(withIdentifier: "storyFullView") as! StoryFullViewController
        storyFullView.storyItems = self.storyItems
        storyFullView.tappedIndex = indexPath.row
        self.navigationController?.pushViewController(storyFullView, animated: false)
    }
    
}

