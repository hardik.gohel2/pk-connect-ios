//
//  ShortsCommetsVC.swift
//  Team Jagananna
//
//  Created by admin on 14/11/23.
//

import UIKit

class ShortsCommetsVC: BaseViewController {

    @IBOutlet weak var txtComment: UITextField!{
        didSet{
            txtComment.placeholder = "Add a comment".localize()
            txtComment.delegate = self
        }
    }
    @IBOutlet weak var commentViewButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var tblCommets: UITableView!{
        didSet {
            tblCommets.delegate = self
            tblCommets.dataSource = self
        }
    }
    @IBOutlet weak var lblTitle: UILabel!
    
    var commentList = [ShortComment]()
    var short_id = ""
    var short_title = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgUser.layer.cornerRadius = 20
        if let urlStr = getRetrievedUser()?.user_image {
            imgUser.setLoadedImage(urlStr)
        }
        lblTitle.text = short_title
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.navigationController?.navigationBar.isHidden = true
        callGetCommentsAPI {}
    }
    
    
    
    @IBAction func onClickSend(_ sender: Any) {
        if txtComment.text != "" {
            postShortComment(comment: txtComment.text!, short_id: short_id) { [self] in
                DispatchQueue.main.async {
                    
                    self.commentList.removeAll()
                    self.callGetCommentsAPI { [self] in
                        
                        txtComment.text = ""
                        txtComment.resignFirstResponder()
                        
                    }
                }
            }
        }
    }
    
    
    
    @IBAction func onClickChangeLan(_ sender: Any) {
        changeAppLanguage()
    }
    @IBAction func onClickBaack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            commentViewButtonConstraint.constant = view.safeAreaInsets.bottom - keyboardSize.height
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        commentViewButtonConstraint.constant = 0
    }
    
    private func callGetCommentsAPI(_ completion: @escaping ()->()) {
        getAllShortsComments(short_id: short_id) { commentList in
            DispatchQueue.main.async { [self] in
               hideLoadingIndicator()
                self.commentList = commentList.RESULT ?? []
                    tblCommets.reloadData()
                    if self.view.subviews.contains(noRecordsLabel) {
                        noRecordsLabel.removeFromSuperview()
                    }
                
                completion()
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ShortsCommetsVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


extension ShortsCommetsVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        commentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentTableCell
        
        var comment = commentList[indexPath.row]
        
        let attrString = NSMutableAttributedString(string: comment.createdBy.full_name,
                                                   attributes: [.font: UIFont(name: "NunitoSans-Regular", size: 14) as Any])
        attrString.append(NSAttributedString(string: "  ", attributes: nil))
        attrString.append(NSMutableAttributedString(string: comment.comment,
                                                    attributes: [.font: UIFont(name: "NunitoSans-Light", size: 12) as Any]))
       // tableView.estimatedRowHeight = 1000
        cell.name.attributedText = attrString
        if let urlStr = comment.createdBy.user_image {
            cell.userImage.setLoadedImage(urlStr)
        }
        cell.timeStamp.text = getFormattedDate(from: comment.created_on, inputFormat: "yyyy-MM-dd HH:mm:ss", outputFormat: 1)
       // cell.userImageLeadingConstraint.constant = (comment.parent_id == "0") ? 10 : 40
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
