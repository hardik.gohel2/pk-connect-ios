//
//  NewVerifyProfileVC.swift
//  PKConnect
//
//  Created by Navdip on 17/04/23.
//

import UIKit

class NewVerifyProfileVC: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var btnSendOtp: RoundedButton!
    private lazy var dropdownTable = UITableView()
    private let rowTitles = ["State", "District", "Assembly Constituency"],
                placeholderArray = ["Please Select Your State", "Please Select Your District", "Please Select Your AC"],
                errorTitles = ["Please Select Your State", "Please Select Your District", "Please Select Your Assembly Constituency"],
                toolBar = UIToolbar()
    var color = [UIColor.black, UIColor.black, UIColor.black]
    var img = ["downward-arrow", "downward-arrow", "downward-arrow"]

    var mobileNo = ""
    private var stateList: [State]?,
                districtList: [District]?,
                acList: [AC]?,
                indexOfDropdown = 88,
                updatedProfile: UserProfile!,
                headerView: UITableViewHeaderFooterView?,
                errorValues = [true, true, true],
                inputDetails = InputUserDetails(),
                state_id = "0",
                district_id = "0",
                ac_id = "0"
    @IBOutlet weak var lblComplete: UILabel!
    let containerView = {
        let shadowView = UIView()
        shadowView.layer.shadowColor = UIColor.lightGray.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        shadowView.layer.shadowOpacity = 1.0
        shadowView.layer.shadowRadius = 2
        return shadowView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        lblComplete.text = "Complete And Verify Profile".localize()
        dropdownTable.register(UITableViewCell.self, forCellReuseIdentifier: "dropdownCell")
        dropdownTable.dataSource = self
        dropdownTable.delegate = self
        
        showLoadingIndicator(in: view)
        getProfile { profile in
            DispatchQueue.main.async { [self] in
                hideLoadingIndicator()
                mobileNo = profile.phone_number!
            }
        }
        
        getStateList { stateList in
            DispatchQueue.main.async {
                self.stateList = stateList
            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == dropdownTable {
            if indexOfDropdown == 0 {
                return stateList?.count ?? 0
            } else if indexOfDropdown == 1 {
                return districtList?.count ?? 0
            } else if indexOfDropdown == 2 {
                return acList?.count ?? 0
            }
        }
        return rowTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "registrationCell", for: indexPath) as! RegistrationCell

            let attributedString = NSMutableAttributedString(attributedString: NSAttributedString(string: "", attributes: [.foregroundColor: UIColor.systemRed]))
            let combinedText = NSMutableAttributedString(attributedString: NSAttributedString(string: rowTitles[indexPath.row].localize()))
//            if indexPath.row != 1 {
//                combinedText.append(attributedString)
//            }

            cell.label.attributedText = combinedText
            cell.textField.tag = indexPath.row
            cell.textField.placeholder = placeholderArray[indexPath.row].localize()
            cell.textField.delegate = self
            cell.errorLabel.text = errorTitles[indexPath.row] .localize()
            cell.textField.layer.borderColor = color[indexPath.row].cgColor
            cell.arrow.tintColor = color[indexPath.row]
            cell.arrow.image = UIImage(named: img[indexPath.row])
            cell.errorLabel.isHidden = errorValues[indexPath.row]
            cell.textField.inputAccessoryView = nil
            cell.textField.inputView = nil
            
            switch indexPath.row {
            case 0:
                cell.textField.text = inputDetails.state_name
            case 1:
                cell.textField.text = inputDetails.district_name
            case 2:
                cell.textField.text = inputDetails.ac_name
            default:
                cell.textField.text = ""
            }
            
            return cell

        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell", for: indexPath)
            var config = cell.defaultContentConfiguration()
            config.textProperties.font = UIFont(name: "NunitoSans-Regular", size: 14)!
           if indexOfDropdown == 0 {
                config.text = stateList?[indexPath.row].state_name_en
            } else if indexOfDropdown == 1 {
                config.text = districtList?[indexPath.row].district_name
            } else if indexOfDropdown == 2 {
                config.text = acList?[indexPath.row].ac
            }
            
            cell.contentConfiguration = config

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var newVal = ""
        if indexOfDropdown == 0 { //State
            resetCellFor(index1: 1, index2: 2)
            district_id = "0"
            inputDetails.district_name = nil
            ac_id = "0"
            inputDetails.ac_name = nil
            state_id = stateList![indexPath.row].state_id
            inputDetails.state_name = stateList![indexPath.row].state_name_en
            newVal = inputDetails.state_name!
            img[0] = "downward-arrow"
            img[1] = "downward-arrow"
            img[2] = "downward-arrow"
        } else if indexOfDropdown == 1 { //District
            resetCellFor(index1: nil, index2: 2)
            ac_id = "0"
            inputDetails.ac_name = nil
            district_id = districtList![indexPath.row].district_id
            inputDetails.district_name = districtList![indexPath.row].district_name
            newVal = inputDetails.district_name!
            img[0] = "downward-arrow"
            img[1] = "downward-arrow"
            img[2] = "downward-arrow"
        } else if indexOfDropdown == 2 { //AC
            ac_id = acList![indexPath.row].id
            inputDetails.ac_name = acList![indexPath.row].ac
            newVal = inputDetails.ac_name!
            img[0] = "downward-arrow"
            img[1] = "downward-arrow"
            img[2] = "downward-arrow"
            tableView.reloadData()
        }
        containerView.removeFromSuperview()
        if state_id != "0" && district_id != "0" && ac_id != "0"{
            btnSendOtp.backgroundColor = UIColor.pkConnectYellow
          //  color = [UIColor.black, UIColor.black, UIColor.black]
            img[0] = "downward-arrow"
            img[1] = "downward-arrow"
            img[2] = "downward-arrow"
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            btnSendOtp.layer.borderWidth = 0
        }else{
            btnSendOtp.backgroundColor = UIColor.white
            btnSendOtp.layer.borderWidth = 1
        }
        let cell = self.tableView.cellForRow(at: IndexPath(row: indexOfDropdown, section: 0)) as! RegistrationCell

        cell.textField.text = newVal.localize()
        errorValues[indexOfDropdown] = true
        cell.errorLabel.isHidden = errorValues[indexOfDropdown]
        resetTableCell()
    }
    private func resetCellFor(index1: Int?, index2: Int?) {
        if index1 != nil {
            if (tableView.indexPathsForVisibleRows?.contains(IndexPath(row: index1!, section: 0)) == true) {
                let cell = self.tableView.cellForRow(at: IndexPath(row: index1!, section: 0)) as! RegistrationCell
                cell.textField.text = ""
            }
        }
        if index2 != nil {
            if (tableView.indexPathsForVisibleRows?.contains(IndexPath(row: index2!, section: 0)) == true) {
                let cell = self.tableView.cellForRow(at: IndexPath(row: index2!, section: 0)) as! RegistrationCell
                cell.textField.text = ""
                tableView.reloadData()
            }
        }
    }
    
    @objc func tappedOutsideDropdown() {
        backgroundView.removeFromSuperview()
        containerView.removeFromSuperview()
    }

    @objc func didSwipe(_ sender: UISwipeGestureRecognizer) {
        backgroundView.removeFromSuperview()
        containerView.removeFromSuperview()
    }
  
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            print("Main Table Scrolled")
            if self.view.subviews.contains(containerView) {
                containerView.removeFromSuperview()
            }
        } else if scrollView == self.dropdownTable {
            print("Dropdown Table")
        }
    }
    
    private func setUpDropDownTable(indexPath: IndexPath) {
//        self.view.addSubview(self.backgroundView)
//        self.backgroundView.frame = self.view.frame
//        self.backgroundView.backgroundColor = .clear
//        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedOutsideDropdown)))
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(didSwipe(_:)))
        swipeUp.direction = .up
        backgroundView.addGestureRecognizer(swipeUp)
        var tableHeight: CGFloat = 0, yPos: CGFloat = 0
        let cell = tableView.cellForRow(at: indexPath) as! RegistrationCell
        let currentTextFieldFrame = cell.textField.frame
        
        view.addSubview(containerView)
        containerView.addSubview(dropdownTable)
        if indexPath.row == 0 {
            tableHeight = CGFloat((stateList?.count ?? 0) * 44)
        } else if indexPath.row == 1 {
            tableHeight = CGFloat((districtList?.count ?? 0) * 44)
        } else if indexPath.row == 2 {
            tableHeight = CGFloat((acList?.count ?? 0) * 44)
        }else{
        }
        
        let h1 = abs(tableView.frame.origin.y + cell.frame.origin.y + 5 + currentTextFieldFrame.origin.y - self.tableView.contentOffset.y)
        let h2 = abs(view.frame.size.height - h1 - currentTextFieldFrame.height - view.safeAreaInsets.bottom)
        if h1 > h2 {
            tableHeight = min(CGFloat(Int(h1) - 50 - 10), tableHeight)
            yPos = (h1 > tableHeight) ? (h1 - tableHeight) : (view.safeAreaInsets.top)
        } else {
            tableHeight = min(CGFloat(Int(h2)), tableHeight)
            yPos = h1 + 50
        }
        containerView.frame = CGRect(x: 15, y: yPos, width: self.view.frame.size.width - 30, height: CGFloat(tableHeight))
        dropdownTable.frame = containerView.bounds
        if #available(iOS 15.0, *) {
            dropdownTable.sectionHeaderTopPadding = 0
        } else {
            // Fallback on earlier versions
        }
        dropdownTable.reloadData()

        view.bringSubviewToFront(dropdownTable)
    }
    private func validateFields() -> Bool {
        //State
        if inputDetails.state_name == nil {
            errorValues[0] = false
            color[0] = UIColor.red

        }
        //DIST
        if inputDetails.district_name == nil {
            errorValues[1] = false
            color[1] = UIColor.red
        }
        //AC
        if inputDetails.ac_name == nil {
            errorValues[2] = false
            color[2] = UIColor.red
        }
        
        if errorValues.compactMap({ $0 }).contains(false) {
            tableView.reloadData()
            return false
        } else {
            return true
        }
    }
    @IBAction func btnSendOtpClick(_ sender: UIButton) {
        if validateFields(){
            sendOTP(for: mobileNo) { responseObj in
                if responseObj!.status == "200" {
                    DispatchQueue.main.async {
                        self.hideLoadingIndicator()
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let verifyOTPView = storyboard.instantiateViewController(withIdentifier: "verifyOTPVC") as! VerifyOTPVC
                        verifyOTPView.mobileNo = self.mobileNo
                        verifyOTPView.state = self.state_id
                        verifyOTPView.District = self.district_id
                        verifyOTPView.Aseembly = self.ac_id
                        verifyOTPView.UpdateAll = true
                        verifyOTPView.currentOTP = responseObj!.data.otp!
                        self.navigationController?.pushViewController(verifyOTPView, animated: false)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.hideLoadingIndicator()
                       // self.errorLabel.isHidden = false
                    }
                }
            }
        }
    }
    
}
extension NewVerifyProfileVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
     
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 0 {
            if !string.isEmpty && string.rangeOfCharacter(from: .letters.union(.whitespaces)) == nil {
                return false
            }
        }
        if errorValues[textField.tag] == false {
            let cell = self.tableView.cellForRow(at: IndexPath(row: textField.tag, section: 0)) as! RegistrationCell
            cell.errorLabel.isHidden = true
            errorValues[textField.tag] = true
            resetTableCell()
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if ![0, 1, 3].contains(textField.tag) {
            view.endEditing(true)
        }
        if textField.tag == 0 {
            color[0] = UIColor.pkConnectYellow
            color[1] = UIColor.black
            color[2] = UIColor.black
            img[0] = "upward-arrow"
            img[1] = "downward-arrow"
            img[2] = "downward-arrow"
        }else if textField.tag == 1 {
            color[0] = UIColor.black
            color[1] = UIColor.pkConnectYellow
            color[2] = UIColor.black
            img[0] = "downward-arrow"
            img[1] = "upward-arrow"
            img[2] = "downward-arrow"
        }else if textField.tag == 2 {
            color[0] = UIColor.black
            color[1] = UIColor.black
            color[2] = UIColor.pkConnectYellow
            img[0] = "downward-arrow"
            img[1] = "downward-arrow"
            img[2] = "upward-arrow"
        }else{

        }
        tableView.reloadData()
//        if textField.tag == indexOfDropdown, view.subviews.contains(containerView) {
//       //     containerView.removeFromSuperview()
//            indexOfDropdown = 88
//            return false
//        }
        let row = textField.tag
       // containerView.removeFromSuperview()
//        indexOfDropdown = 88
        switch row {
        case 0: //State
            indexOfDropdown = textField.tag
            setUpDropDownTable(indexPath: IndexPath(row: row, section: 0))

            return false
        case 1: //District
            indexOfDropdown = textField.tag
            if state_id != "0" {
                getDistrictList(for: state_id) { districtList in
                    DispatchQueue.main.async {
                        self.districtList = districtList
                        self.setUpDropDownTable(indexPath: IndexPath(row: row, section: 0))
                    }
                }
            }
            return false
        case 2: //AC
            indexOfDropdown = textField.tag

            if district_id != "0" {
                getACList(for: district_id, completion: { acList in
                    DispatchQueue.main.async {
                        self.acList = acList
                        self.setUpDropDownTable(indexPath: IndexPath(row: row, section: 0))
                    }
                })
            }
            return false
        default:
            return true
        }
    }
    
    func setupHideKeyboardOnTap() {
        self.view.addGestureRecognizer(self.endEditingRecognizer())
        self.navigationController?.navigationBar.addGestureRecognizer(self.endEditingRecognizer())
    }
    
    //Dismiss keyboard if shown
    private func endEditingRecognizer() -> UIGestureRecognizer {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        return tap
    }
    
    private func resetTableCell() {
        UIView.setAnimationsEnabled(false)
        tableView.beginUpdates()
        tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func getTableHeaderWith(title: String) -> UITableViewHeaderFooterView {
        headerView = UITableViewHeaderFooterView()
        
        var config = UIListContentConfiguration.groupedHeader()
        config.attributedText = NSAttributedString(string: title, attributes: [.font: UIFont(name: "NunitoSans-SemiBold", size: 22)!, .foregroundColor: UIColor.label])
        config.textProperties.alignment = .center
        headerView?.contentConfiguration = config
        
        return headerView!
    }
}
