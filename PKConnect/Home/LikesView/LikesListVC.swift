//
//  LikesListVC.swift
//  PKConnect
//
//  Created by Yatindra on 19/01/23.
//

import UIKit

class LikesListVC: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var clicks_user_list: [ClickUser]?
    var news_id = ""
    private var pageIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cell = UINib(nibName: LikeItemCell.identifier, bundle: nil)
        self.tableView.register(cell, forCellReuseIdentifier: LikeItemCell.identifier)
        
        self.getNewsLikesList(newsId: news_id, count: "0") {
            likeList in
            DispatchQueue.main.async {
                
                if let list = likeList {
                    self.clicks_user_list = list
                }
                self.tableView.reloadData()
                
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onCloseClick(_ sender: UIButton){
        self.dismiss(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}



extension LikesListVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clicks_user_list?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LikeItemCell.identifier) as! LikeItemCell
        cell.clicks_user_list = clicks_user_list?[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == (clicks_user_list?.count ?? 0) - 1 {
            pageIndex += 1
            //API Call
            
            self.getNewsLikesList(newsId: news_id, count: "\(pageIndex * 10)") {
                likeList in
                DispatchQueue.main.async {
                    
                    if let list = likeList {
                        self.clicks_user_list?.append(contentsOf: list)
                    }
                    self.tableView.reloadData()
                    
                }
            }
            
        }
    }
}


extension LikesListVC: Identifiable{
    static let identifier: String = String(describing: LikesListVC.self)
}
