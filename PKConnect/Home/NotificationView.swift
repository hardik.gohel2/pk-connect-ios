//
//  NotificationView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/15/22.
//

import UIKit

class NotificationView: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet private var tableView: UITableView!

    private var notificationList = [BellNotification](),
                sectionHeaders = [String](),
                rowData = [[BellNotification]](),
                headerView: UITableViewHeaderFooterView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.sectionFooterHeight = 0
        showLoadingIndicator(in: view)
        getNotificationList { [self] notificationList in
            DispatchQueue.main.async { [self] in
                hideLoadingIndicator()
                if let notificationList = notificationList, notificationList.count > 0 {
                    sortNotifications(notificationList)
                    tableView.reloadData()
                } else {
                    self.addNoRecordsLabel("Regular", 16, to: self.view)
                }
            }
        }
    }
    
    @IBAction private func changeLanguage(_ sender: Any) {
        changeAppLanguage()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        sectionHeaders.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        rowData[section].count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationCell
        
        let notification = rowData[indexPath.section][indexPath.row]
        cell.message.text = notification.notification_text
        cell.timeDifference.text = getTimeDifferenceFromNow(notification.inserted_on)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        35
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        getTableHeaderWith(title: sectionHeaders[section])
    }
    
    @IBAction private func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    private func sortNotifications(_ notificationList: [BellNotification]) {
        var group: [BellNotification]?, oldDate = ""
        notificationList.forEach { notification in
            let newDate = getFormattedDate(from: notification.inserted_on, inputFormat: "yyyy-MM-dd HH:mm:ss", outputFormat: 5)
            if oldDate == newDate {
                group?.append(notification)
            } else {
                if group != nil {
                    sectionHeaders.append(oldDate)
                    rowData.append(group!)
                    group?.removeAll()
                }
                group = [notification]
                oldDate = newDate
            }
        }
        if group != nil {
            sectionHeaders.append(oldDate)
            rowData.append(group!)
            group?.removeAll()
        }
    }
    
    private func getTimeDifferenceFromNow(_ notificationDate: String) -> String {
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = inputDateFormatter.date(from: notificationDate)!
        let notificationDateComponents = Calendar.current.dateComponents([.month, .day, .hour, .minute], from: date)
        let currentDateComponents = Calendar.current.dateComponents([.month, .day, .hour, .minute], from: Date())
        let difference = Calendar.current.dateComponents([.month, .day, .hour, .minute], from: notificationDateComponents, to: currentDateComponents)
        if let month = difference.month, month != 0 {
            return "\(month) \(month == 1 ? "month" : "months") ago"
        } else if let day = difference.day, day != 0 {
            return "\(day) \(day == 1 ? "day" : "days") ago"
        } else if let hour = difference.hour, hour != 0 {
            return "\(hour) \(hour == 1 ? "hour" : "hours") ago"
        } else if let minute = difference.minute, minute != 0 {
            return "\(minute) \(minute == 1 ? "minute" : "minutes") ago"
        }
        return "just now".localize()
    }
    
    private func getTableHeaderWith(title: String) -> UITableViewHeaderFooterView {
        headerView = UITableViewHeaderFooterView()

        var config = UIListContentConfiguration.groupedHeader()
        config.attributedText = NSAttributedString(string: title, attributes: [.font: UIFont(name: "NunitoSans-Regular", size: 12)!, .foregroundColor: UIColor.label])
        config.textProperties.alignment = .center
        headerView?.contentConfiguration = config

        return headerView!
    }
}
