//
//  AddContactVC.swift
//  PKConnect
//
//  Created by apple on 09/01/24.
//

import UIKit
import Contacts
import ContactsUI

class AddContactVC: BaseViewController, UITableViewDelegate, UITableViewDataSource,CNContactPickerDelegate, BannerSuccessViewDelegate {
    func startJourneyTapped() {
        print("Tapppp")
        backgroundView.removeFromSuperview()
        alertView?.removeFromSuperview()
        alertView = nil
       // backgroundView = nil
    }
    

    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet weak var btn_add: RoundedButton!
    @IBOutlet weak var btn_submit: RoundedButton!
    @IBOutlet weak var contact_tableview: UITableView!
    
    let minimumContacts = 4
    let maximumContacts = 10
    var contacts: [CNContact] = []
    var selectedContact: [CNContact]? = []
    var alertView: BannerSuccessView?
    
      var referalModel = [BannerOptionModel]()
     //  var contacts: [CNContact] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        contact_tableview.delegate = self
        contact_tableview.dataSource = self
       // fetchContacts()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btn_ClickSubmit(_ sender: RoundedButton) {
        DispatchQueue.main.async {
            self.view.addSubview(self.backgroundView)
            self.backgroundView.frame = self.view.frame
            
            self.alertView = UINib(nibName: "BannerSuccessView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BannerSuccessView
            self.alertView?.layer.cornerRadius = 32
            self.alertView?.translatesAutoresizingMaskIntoConstraints = false
            self.alertView?.delegate = self
            
            self.view.addSubview(self.alertView!)
            NSLayoutConstraint.activate([
                self.alertView!.heightAnchor.constraint(equalToConstant: 311),
                self.alertView!.widthAnchor.constraint(equalToConstant: 328),
                self.alertView!.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 1),
                self.alertView!.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 1)
            ])
        }
    }
    
    @IBAction func btn_ClickAdd(_ sender: RoundedButton) {
      //  addContactsToPhone()
        let contactPicker = CNContactPickerViewController()
         contactPicker.delegate = self
         present(contactPicker, animated: true, completion: nil)
        
//        if selectedContact?.count ?? 0 < maximumContacts{
//            
//            btn_add.isHidden = false
//            if selectedContact?.count ?? 0 < minimumContacts{
//                btn_submit.isHidden = false
//            }else{
//                btn_submit.isHidden = true
//            }
//            let contactPicker = CNContactPickerViewController()
//             contactPicker.delegate = self
//             present(contactPicker, animated: true, completion: nil)
//        }else{
//            btn_add.isHidden = true
//           print( "Value :- ",selectedContact?.count ?? 0)
//        }
              
    }
    
    // MARK: - CNContactPickerDelegate

//      func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
//          // Display the selected contact's name and number in text fields
//          lbl_count.text = "\(contact.givenName) \(contact.familyName)"
//          print("Name : \(contact.givenName) \(contact.familyName)")
//          
//          if let phoneNumber = contact.phoneNumbers.first?.value.stringValue {
//             // contactNumberTextField.text = phoneNumber
//              print("Phone NUmber = ",phoneNumber)
//          }
//          
//          dismiss(animated: true, completion: nil)
//      }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
          //selectedContact = contact
        
        var fullname = String()
        var mobile = String()
        fullname = "\(contact.givenName) \(contact.familyName)"
        mobile = "\(contact.phoneNumbers.first?.value.stringValue ?? "")"
        
        // Example usage:
        let phoneNumberWithSpecialCharacters = mobile
        //"+1 (123) 456-7890"
        let phoneNumberWithoutSpecialCharacters = removeSpecialCharactersAndSpaces(from: phoneNumberWithSpecialCharacters)

        print("Original Phone Number: \(phoneNumberWithSpecialCharacters)")
        print("Sanitized Phone Number: \(phoneNumberWithoutSpecialCharacters)")
        
        
        print(" --->>> ",fullname , mobile)
        referalContacts(name: fullname, mobile: phoneNumberWithoutSpecialCharacters){ response in
            DispatchQueue.main.async {
                if response.CODE == 200{
                    self.selectedContact?.append(contact)
                }else{
                    let alertController = UIAlertController(title: "Error".localize(), message: response.MESSAGE.localize(), preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: "OK".localize(), style: .default) { (action) in
                        self.dismiss(animated: true)
                        print("OK button tapped")
                    }
                    
                    
                    alertController.addAction(okAction)
                    
                    
                    self.present(alertController, animated: true, completion: nil)
                }
               
                print("Gaurav = ",response.MESSAGE)
                // You can do something with the selected contact, e.g., display details or save it to another array
                print("Selected contact: \(contact.givenName) \(contact.familyName), Phone: \(contact.phoneNumbers.first?.value.stringValue ?? "")")
                self.contact_tableview.reloadData()
            }
        }

      }
  

    

    

      func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
          dismiss(animated: true, completion: nil)
      }
    
//
//    func addContactsToPhone() {
//        let contactStore = CNContactStore()
//        
//        // Request permission to access contacts
//        contactStore.requestAccess(for: .contacts) { [weak self] (granted, error) in
//            guard granted else {
//                print("Access to contacts denied.")
//                return
//            }
//            
//            let minimumContacts = 5
//            let maximumContacts = 10
//            
//            for index in 1...maximumContacts {
//                if let newContact = self?.createContact(index: index) {
//                    self?.contacts.append(newContact)
//                    
//                    // Add contact to the contact store
//                    let saveRequest = CNSaveRequest()
//                    
//                    for index in 1...maximumContacts {
//                        if let newContact = self?.createContact(index: index) {
//                            self?.contacts.append(newContact)
//
//                            // Add contact to the save request, not directly to the contact store
//                            saveRequest.add(newContact)
//                        }
//
//                        if index == maximumContacts {
//                            do {
//                                // Execute the save request after adding all contacts
//                                try contactStore.execute(saveRequest)
//                                DispatchQueue.main.async {
//                                    self?.contact_tableview.reloadData()
//                                }
//                            } catch {
//                                print("Error saving contacts: \(error)")
//                            }
//                        }
//                    }
//
//                }
//            }
//            }
//        }
    
    // MARK: - Fetch Contacts

     func fetchContacts() {
         let store = CNContactStore()

         // Request access to the user's contacts
         store.requestAccess(for: .contacts) { [weak self] granted, error in
             guard let self = self else { return }

             if granted {
                 let keysToFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey] as [CNKeyDescriptor]

                 let request = CNContactFetchRequest(keysToFetch: keysToFetch)

                 do {
                     try store.enumerateContacts(with: request, usingBlock: { contact, _ in
                         // Append the contact to the array
                         self.contacts.append(contact)
                     })

                     // Reload the TableView on the main thread
                     DispatchQueue.main.async {
                         self.contact_tableview.reloadData()
                     }
                 } catch {
                     print("Error fetching contacts: \(error)")
                 }
             } else {
                 print("Access to contacts not granted")
             }
         }
     }
    
    
        
        func createContact(index: Int) -> CNContact {
            let contact = CNMutableContact()
            
            contact.givenName = "Contact"
            contact.familyName = "\(index)"
            
            let phoneNumber = CNLabeledValue(label: CNLabelPhoneNumberMobile, value: CNPhoneNumber(stringValue: "+123456789"))
            contact.phoneNumbers = [phoneNumber]
            
            return contact.copy() as! CNContact
        }
    
    func removeSpecialCharactersAndSpaces(from phoneNumber: String) -> String {
        // Define a character set with the characters to be removed
        let charactersToRemove = CharacterSet(charactersIn: "+0123456789").inverted

        // Use regular expression to remove special characters and spaces
        let sanitizedPhoneNumber = phoneNumber.components(separatedBy: charactersToRemove).joined()

        return sanitizedPhoneNumber
    }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return selectedContact?.count ?? 0
        }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ContactDetailCell
        
        let contact = selectedContact?[indexPath.row]
        
        if selectedContact?.count == maximumContacts {
            btn_add.isHidden  = true
        }else{
            btn_add.isHidden  = false
            if selectedContact?.count ?? 0 > minimumContacts{
                btn_submit.isHidden = false
            }else{
                btn_submit.isHidden = false
            }
            
            
        }
        cell.txt_name.text = "\(contact?.givenName ?? "") \(contact?.familyName ?? "")"
           cell.txt_number.text = contact?.phoneNumbers.first?.value.stringValue
        cell.btn_delete.tag = indexPath.row
        cell.btn_delete.addTarget(self, action: #selector(deleteButtonTapped(_:)), for: .touchUpInside)
           return cell
       }
    // Action method for the delete button
       @objc func deleteButtonTapped(_ sender: UIButton) {
           // Get the indexPath of the cell containing the button
           print(sender.tag)
           
           let phoneNumberWithSpecialCharacters = selectedContact?[sender.tag].phoneNumbers.first?.value.stringValue
           //"+1 (123) 456-7890"
           let phoneNumberWithoutSpecialCharacters = removeSpecialCharactersAndSpaces(from: phoneNumberWithSpecialCharacters!)

           print("Original Phone Number: \(phoneNumberWithSpecialCharacters)")
           print("Sanitized Phone Number: \(phoneNumberWithoutSpecialCharacters)")
           
           
           removereferalContacts(mobile: phoneNumberWithoutSpecialCharacters){ response in
               DispatchQueue.main.async {
                   if response.CODE == 200{
                       self.selectedContact?.remove(at: sender.tag)
                      // self.selectedContact?.append(contact)
                       print("Delete Successfully")
                   }else{
                       let alertController = UIAlertController(title: "Error".localize(), message: response.MESSAGE.localize(), preferredStyle: .alert)
                       
                       let okAction = UIAlertAction(title: "OK".localize(), style: .default) { (action) in
                           self.dismiss(animated: true)
                           print("OK button tapped")
                       }
                       alertController.addAction(okAction)
                       self.present(alertController, animated: true, completion: nil)
                   }
                  
                   print("Gaurav = ",response.MESSAGE)
                   // You can do something with the selected contact, e.g., display details or save it to another array
                   self.contact_tableview.reloadData()
               }
           }
       }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 185
    }
    
    }
    
    

