//
//  FacebookViewController.swift
//  KCR Sainyam
//
//  Created by Haritej on 22/08/22.
//

import UIKit
import WebKit

class FacebookViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var myWebview: WKWebView!
    var progressView: UIProgressView!

    override func viewDidLoad() {
        super.viewDidLoad()

        myWebview.navigationDelegate = self
        myWebview.translatesAutoresizingMaskIntoConstraints = false
        
        guard let url = URL(string: "https://www.facebook.com/jansuraajofficial/") else {return
                       }
                       let request = URLRequest(url: url)
                self.myWebview.load(request)
        // Do any additional setup after loading the view.
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        //loader
        //self.showLoading()
        
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //  print("two")
        //hide
        //self.hideLoading()
        
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        //  print("three")
        //hide
        // self.hideLoading()
    }
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        //  print("four")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
