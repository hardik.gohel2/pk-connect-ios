
import UIKit
import AVKit

class MessageCell: UITableViewCell {
    
    @IBOutlet weak var newsDate: UILabel!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsDescription: UILabel!
        @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsVideo: UIView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    @IBOutlet weak var playButton: UIButton!
   
    var player: AVPlayer!
    var avpController = AVPlayerViewController()
    var playerLayer: AVPlayerLayer?
    var likeCommentAction: (()->())?
    var playAction: (()->())?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        stopVideo()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setVideoURL(urlStr: String) {
        
        self.newsVideo.isHidden = false
        self.newsImage.isHidden = true
        playButton.isHidden = true
        
        let videoUrl = URL(string: urlStr)
        player = AVPlayer(url: videoUrl!)
        avpController.player = player
        avpController.view.frame = newsVideo.bounds
        self.newsVideo.addSubview(avpController.view)
        player?.play()
        playButton.isHidden = true
        
      
    }
    func setUpContent(_ messageItem: ResultMsg?) {
        
        guard let mediaURL = messageItem?.media_url else {
            return
        }

        self.playAction = { self.setVideoURL(urlStr: mediaURL)}
//        self.likeButton.isSelected = (messageItem?.is_liked == "dislike") ? false : true

        self.newsVideo.isHidden = true
        self.newsImage.isHidden = false
        self.playButton.isHidden = false
        self.avpController.removeFromParent()

        if((self.player?.currentItem) != nil){
            self.player.replaceCurrentItem(with: nil)
        }
    }
    
    func playVideo() {
           player?.play()
       }
       
       func pauseVideo() {
           player?.pause()
       }
       
       func stopVideo() {
           player?.pause()
           player?.seek(to: CMTime.zero)
       }
    
    func setVideoURL1(urlStr: String) {
        let videoUrl = URL(string: urlStr)
        let playerItem1 = AVPlayerItem.init(url: videoUrl!)
        //        player?.addObserver(self, forKeyPath: "rate", options: .new, context: nil)
        player = AVQueuePlayer(items: [playerItem1])
        avpController.player = player
        avpController.view.frame = newsVideo.bounds
        
        self.newsVideo.addSubview(avpController.view)
        
        //        avpController.player?.pause()
        
        playButton.isHidden = false
    }
    
    @IBAction func btnLikeClicked(_ sender: UIButton) {
        self.likeCommentAction?()
    }
    @IBAction func playVideo(_ sender: Any) {
        self.playAction?()
        
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}
