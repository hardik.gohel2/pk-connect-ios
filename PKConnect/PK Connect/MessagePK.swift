
import UIKit
import AVKit


class MessagePK: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBAction func btnNotificationPress(_ sender: Any) {
        let vc = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "notificationView") as! NotificationView
        NotificationCenter.default.removeObserver(self, name: Notification.Name("PlayerStop"), object: nil)
        NotificationCenter.default.post(name: Notification.Name("PlayerStop"), object: nil,userInfo:nil)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @IBAction func btnChangeLanguage(_ sender: Any) {
        changeAppLanguage()
    }
    @IBOutlet weak var videosTitile: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var allmessages: MessageModel?
    var visibleCellsIndexPaths = Set<IndexPath>()
        var currentlyPlayingIndexPath: IndexPath?

    override func viewDidLoad() {
        super.viewDidLoad()
        showLoadingIndicator(in: view)

        videosTitile.text = "Videos".localize()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        getAllMessagePK(completion: { fullresponse in
        
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                print("tets: ", fullresponse)
                self.allmessages = fullresponse
                self.tableView.reloadData()
                print("djshd: ", self.allmessages?.result?[0].media_url ?? "")
            }})
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
          NotificationCenter.default.removeObserver(self, name: Notification.Name("PlayerStop"), object: nil)
          NotificationCenter.default.post(name: Notification.Name("PlayerStop"), object: nil,userInfo:nil)
      }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
        return self.allmessages?.result?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
        cell.newsTitle.text = self.allmessages?.result?[indexPath.row].video_title
        cell.newsDate.text = getFormattedDate(from: (self.allmessages?.result?[indexPath.row].created_time)!, inputFormat: "dd-MM-yyyy HH:mm:ss", outputFormat: 0)
        
        cell.newsDate.text = self.allmessages?.result?[indexPath.row].created_time ?? ""

        
        if let description = self.allmessages?.result?[indexPath.row].video_desc?.htmlToAttributedString {
            description.addAttributes([.font: UIFont(name: "NunitoSans-Regular", size: 13)!], range: NSRange(location: 0, length: description.length))
            cell.newsDescription.attributedText = description
        }
        cell.setUpContent(self.allmessages?.result?[indexPath.row])
        
        cell.newsImage.sd_setImage(with: URL(string: self.allmessages?.result?[indexPath.row].media_thumb ?? ""), placeholderImage: UIImage())
        
        
        if ((cell.avpController.player?.isPlaying) != nil){
            self.allmessages?.result?[indexPath.row].isPlaying = false
        }else{
            self.allmessages?.result?[indexPath.row].isPlaying = true
        }
        
        if let isPLaying = self.allmessages?.result?[indexPath.row].isPlaying{
            if isPLaying == true{
                cell.avpController.player?.play()
            }else{
                cell.avpController.player?.pause()
            }
        }
    
        
        if let urlStr = self.allmessages?.result?[indexPath.row].media_url {
            cell.setVideoURL1(urlStr: urlStr)
            
        }
        
        cell.shareButton.tag = indexPath.row
        cell.shareButton.isUserInteractionEnabled = true
        cell.shareButton.addTarget(self, action: #selector(shareClicked), for: .touchUpInside)
        
        cell.likeButton.isSelected = (self.allmessages?.result?[indexPath.row].is_liked == "1") ? true : false
        cell.likeCommentAction = {
            let likeType = (self.allmessages?.result?[indexPath.row].is_liked == "1") ? false : true
            self.likeOrUnlikeVideo((self.allmessages?.result?[indexPath.row].video_id)!, action: likeType) {
                DispatchQueue.main.async {
                    cell.likeButton.isSelected.toggle()
                   
                }
            }
        }
        
        return cell
        
    }
    
    // Helper method to pause video playback for a given index path
       func pauseVideo(for indexPath: IndexPath) {
           if let cell = tableView.cellForRow(at: indexPath) as? MessageCell {
               cell.pauseVideo()
           }
       }
    
    // Implement the UIScrollViewDelegate method to track scrolling events
     func scrollViewDidScroll(_ scrollView: UIScrollView) {
         for indexPath in tableView.indexPathsForVisibleRows ?? [] {
             if indexPath != currentlyPlayingIndexPath {
                 pauseVideo(for: indexPath)
             }
         }
     }
     
     // Implement the UITableViewDelegate method to play videos for visible cells
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
         if let videoCell = cell as? MessageCell {
             if currentlyPlayingIndexPath == indexPath {
                 print("Current index")
                 //videoCell.playVideo()
             } else {
                 videoCell.pauseVideo()
             }
         }
     }
     
     // Implement the UITableViewDelegate method to stop video when cell is about to be hidden
     func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
         if let videoCell = cell as? MessageCell {
             videoCell.stopVideo()
             if currentlyPlayingIndexPath == indexPath {
                 currentlyPlayingIndexPath = nil
             }
         }
     }
     
    
    @objc func shareClicked(sender: UIButton){
        
//        filedownLoadAndShare(view: self,mediaFile: self.allmessages?.result?[sender.tag].media_url ?? "",fileName: self.allmessages?.result?[sender.tag].video_title ?? "")
        let string = "\(self.allmessages?.result?[sender.tag].video_title ?? "")\n\nClick the link below to download the application\n\(appStoreURL)"
        
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [string], applicationActivities: nil)

        self.present(activityViewController, animated: true, completion: nil)

    }

  
    @IBAction func backTapped(_ sender: Any) {
        

          // Pop the view controller from the navigation stack
          
//        DispatchQueue.main.async {
//           // self.dismiss(animated: true)
//            self.navigationController?.popViewController(animated: false)
//        }
//
        DispatchQueue.main.async {
           // self.dismiss(animated: true)
            
            // Stop video when clicked back button
            for indexPath in self.tableView.indexPathsForVisibleRows ?? [] {
                if indexPath != self.currentlyPlayingIndexPath {
                    self.pauseVideo(for: indexPath)
                }
            }
            self.navigationController?.popViewController(animated: false)
        }
    }
}
