//
//  LiveCell.swift
//  KCR Sainyam
//
//  Created by apple on 25/09/22.
//

import UIKit

class LiveCell: UITableViewCell {

    @IBOutlet weak var userComment: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userPic: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
