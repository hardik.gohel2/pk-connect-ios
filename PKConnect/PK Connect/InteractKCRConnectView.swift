//
//  InteractKCRConnectView.swift
//  KCR Sainyam
//
//  Created by Poshitha Gajjala on 7/16/22.


import UIKit

class InteractKCRConnectView: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var interactTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @objc func cardButtonTapped() {
    
//        print("one")
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let newsDetailView = storyBoard.instantiateViewController(withIdentifier: "LiveViewController") as! LiveViewController
        newsDetailView.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.navigationController?.present(newsDetailView, animated: true)
    }
    
    @objc func chatTapped() {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let newsDetailView = storyBoard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        newsDetailView.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.navigationController?.present(newsDetailView, animated: true)

    }
    
    
    @objc func messagetapped() {
//        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
//        let newsDetailView = storyBoard.instantiateViewController(withIdentifier: "MessagePK") as! MessagePK
//        newsDetailView.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
//        self.navigationController?.present(newsDetailView, animated: true)
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
               let ProfilePhotoView = storyBoard.instantiateViewController(withIdentifier: "MessagePK") as! MessagePK
        self.navigationController?.pushViewController(ProfilePhotoView, animated: false)
    }
    
    @objc func getFeaturedTapped() {
        print("four")
        let storyboard = UIStoryboard(name: "Other", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "GetFeaturedVC")
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)

    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "interactTableCell", for: indexPath) as! InteractTableCell
        
        var cardButtonText = ""
        
        switch indexPath.section {
        case 0:
            cell.cardIcon.image = UIImage(named: "liveicon")
            cell.cardTitle.text = "PK Live".localize()
            cell.cardDescription.text = "Watch live sessions in one click.".localize()
            cardButtonText = "Watch Live".localize()
            cell.cardButton?.addTarget(self, action: #selector(cardButtonTapped), for: .touchUpInside)

        case 1:
            cell.cardIcon.image = UIImage(named: "chat_with_pk")
            cell.cardTitle.text = "Chat with PK".localize()
            cell.cardDescription.text = "Start your Conversation with Team PK".localize()
            cardButtonText = "Send Message".localize()
            cell.cardButton?.addTarget(self, action: #selector(chatTapped), for: .touchUpInside)

        case 2:
            cell.cardIcon.image = UIImage(named: "message_from_pk")
            cell.cardTitle.text = "Message from PK".localize()
            cell.cardDescription.text = "Check out PK's special message for you.".localize()
            cardButtonText = "Watch Now".localize()
            cell.cardButton?.addTarget(self, action: #selector(messagetapped), for: .touchUpInside)

        default:
            cell.cardIcon.image = UIImage(named: "get_featured")
            cell.cardTitle.text = "Get Featured".localize()
            cell.cardDescription.text = "Submit photos/videos to get it featured.".localize()
            cardButtonText = "Send Photos / Videos".localize()
            cell.cardButton?.addTarget(self, action: #selector(getFeaturedTapped), for: .touchUpInside)

        }
        let attributedText = NSAttributedString(string: cardButtonText, attributes: [.font: UIFont(name: "NunitoSans-Regular", size: 12)!])
        cell.cardButton.setAttributedTitle(attributedText, for: .normal)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        switch indexPath.section {
//        case 0:
//            //
//        default:
//            //
//        }
    }
}
