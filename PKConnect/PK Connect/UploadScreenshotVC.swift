//
//  UploadScreenshotVC.swift
//  PKConnect
//
//  Created by apple 09/11/22.
//

import UIKit

class UploadScreenshotVC: BaseViewController, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate & UINavigationControllerDelegate {

    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var ttilefive: UILabel!
    @IBOutlet weak var titlefour: UILabel!
    @IBOutlet weak var titlethree: UILabel!
    @IBOutlet weak var titletwo: UILabel!
    @IBOutlet weak var titleone: UILabel!
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var remarksText: UITextField!
    @IBOutlet weak var dottedView: DashedBorderView!
    @IBOutlet weak var plusBtn: UIButton!
    var taskmediaURl: String?
    var taskid: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ttilefive.text = "Remarks".localize()
        titletwo.text = "(You have to add screenshot)".localize()
        titleone.text = "Upload Photos/Videos".localize()
        titlefour.text = "To earn reward points upload a screenshot of the performed task for verification".localize()
        remarksText.placeholder = "Enter Remarks".localize()
        titlethree.text = "Upload Screenshot".localize()
        submitBtn.setTitle("Submit".localize(), for: .normal)
        
//        print(taskid)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))

        myView.addGestureRecognizer(tap)

        myView.isUserInteractionEnabled = true

     

        // function which is triggered when handleTap is called
       

        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true)
    }
    
    
    @IBAction func plusBtnTapped(_ sender: Any) {
        print("yes")
        
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "Option to select", preferredStyle: .actionSheet)

        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)

        let saveActionButton = UIAlertAction(title: "Choose from Gallery", style: .default)
            { _ in
               print("Gallery")
                let imagePickerController = UIImagePickerController()
                imagePickerController.allowsEditing = false
                imagePickerController.sourceType = .photoLibrary
                imagePickerController.delegate = self
                DispatchQueue.main.async {
                    self.present(imagePickerController, animated: true, completion: nil)
                }
               
                
        }
        actionSheetControllerIOS8.addAction(saveActionButton)

        let deleteActionButton = UIAlertAction(title: "Choose from Camera", style: .default)
            { _ in
                print("camera")
                let imagePickerController = UIImagePickerController()
                imagePickerController.allowsEditing = false
                imagePickerController.sourceType = .camera
                imagePickerController.delegate = self
                DispatchQueue.main.async {
                    self.present(imagePickerController, animated: true, completion: nil)
                }
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }

    @IBAction func submitClicked(_ sender: Any) {
        print("done")
        if self.taskmediaURl == "" || self.taskmediaURl == nil {
            print("Please select atleaest one image")
            self.showToast(message: "Please select image".localize())
            
        }
//        else if remarksText.text == ""{
//           // showToast(message: "Please add remarks".localize())
//        }
        else {
            self.postTask(task_id: self.taskid ?? "", media: self.taskmediaURl ?? "") {
                DispatchQueue.main.async {
                    self.hideLoadingIndicator()
                    self.showToast(message: "Success".localize())
                    NotificationCenter.default.removeObserver(self, name: Notification.Name("APICALL"), object: nil)
                    NotificationCenter.default.post(name: Notification.Name("APICALL"), object: nil,userInfo:nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.dismiss(animated: true)
                    }
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("\(info)")
        if let image = info[.originalImage] as? UIImage {
            //                imageView?.image = image
            plusBtn.setImage(image, for: .normal)
            self.uploadImageToS3(image: image)
            dismiss(animated: true, completion: nil)
        }
    }
    
    private func uploadImageToS3(image: UIImage){
        
        // guard let cell = self.registerTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ProfilePictureCell else { return }
//        self.activityIndicator.isHidden = false
        
//        activityIndicator.startAnimating()
        
            showLoadingIndicator(in: view)
        
        self.view.isUserInteractionEnabled = false
        
        image.uploadImageToS3( success: { (uploaded : Bool, imageUrl : String) in
            
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            
            print("MYURL: ", imageUrl)
            self.taskmediaURl = imageUrl
            
           
            
            if uploaded {
                
              
//
//                self.activityIndicator.stopAnimating()
//                self.activityIndicator.isHidden = true
                
                self.view.isUserInteractionEnabled = true
                
               // self.imageorphotoUrl = imageUrl
               // self.mediaType = "1"
                
                //   self.uploadImage(imageUrl: imageUrl, media_type: "1", remarks: self.remarksBtn.text ?? "")
                //    self.updateImage(imageUrl: imageUrl)   //Release
            }
        }, progress: { (uploadPercentage : CGFloat) in
//            print_debug(uploadPercentage)
            
        }, size: {
            (_) in
        }, failure: { (err : Error) in
//            CommonFunction.showToast(msg: err.localizedDescription)
//            self.activityIndicator.stopAnimating()
//            self.activityIndicator.isHidden = true
//
            self.view.isUserInteractionEnabled = true
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

