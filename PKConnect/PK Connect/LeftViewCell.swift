//
//  LeftViewCell.swift
//  ChatSample
//
//  Created by Nagababu on 20/09/2019.
//  Copyright © 2019 Nibs. All rights reserved.
//

import UIKit

class LeftViewCell: UITableViewCell {

    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var logoHeight: NSLayoutConstraint!
    @IBOutlet weak var recieveView: UIView!
    @IBOutlet weak var recieveText: UILabel!
    @IBOutlet weak var lblTime: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        recieveView.rounded(radius: 12)
        recieveView.backgroundColor = .white
        
        contentView.backgroundColor = .clear
        backgroundColor = .clear
    }
    
//    func configureCell(message: Message) {
//        textMessageLabel.text = message.text
//    }
    
}
