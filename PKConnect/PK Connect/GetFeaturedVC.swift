//
//  GetFeaturedVC.swift
//  KCR Sainyam
//
//  Created by Nagababu on 29/08/22.
//

import UIKit
import AVFoundation

class GetFeaturedVC: BaseViewController, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate & UINavigationControllerDelegate  {
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var remarks: UILabel!
    @IBOutlet weak var uploadScreenshot: UILabel!
    @IBOutlet weak var uploadVideosText: UILabel!
    @IBOutlet weak var titleone: UILabel!
    @IBOutlet weak var remarksText: UITextField!
    @IBOutlet weak var dottedView: UIView!
    @IBOutlet weak var featuredText: UILabel!
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var plusBtn: UIButton!
    var taskmediaURl: String?
    var taskid: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        submitBtn.setTitle("Submit".localize(), for: .normal)
        titleone.text = "You can share the following things to get featured on the app:".localize()
        featuredText.text = "Your art piece on Prashant Kishor or PK Connect(Sketch, Poem, Song, Dance, Sculpture, etc)".localize()
        uploadVideosText.text = "Upload Photos/Videos".localize()
        remarks.text = "Remarks".localize()
        remarksText.placeholder = "Enter Remarks".localize()
        uploadScreenshot.text = "Upload Screenshot".localize()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))

        myView.addGestureRecognizer(tap)

        myView.isUserInteractionEnabled = true
        
        
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true)
    }
    
    @IBAction func submitClicked(_ sender: Any) {


        print("done")
        if self.taskmediaURl == "" || self.taskmediaURl == nil {
            print("Please select at least one image")
            self.showToast(message: "Please select image".localize())
        }else if remarksText.text == ""{
            self.showToast(message: "Please add remarks".localize())
        }else {
            print("media: ", self.taskmediaURl)
            self.postgetfeatured(remarks: remarksText.text ?? "", media: self.taskmediaURl ?? "") {
                DispatchQueue.main.async {
                    self.hideLoadingIndicator()
                    self.showToast(message: "File has been successfully uploaded".localize())
                    NotificationCenter.default.removeObserver(self, name: Notification.Name("APICALL"), object: nil)
                    NotificationCenter.default.post(name: Notification.Name("APICALL"), object: nil,userInfo:nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.dismiss(animated: true)
                    }
                }
                
            }
        }

    }
    
    
    @IBAction func plusBtnTapped(_ sender: Any) {
        
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
             
             switch cameraAuthorizationStatus {
             case .authorized:
                 openImagePicker()
                 
             case .notDetermined:
                 AVCaptureDevice.requestAccess(for: .video) { granted in
                     if granted {
                         DispatchQueue.main.async {
                             self.openImagePicker()
                         }
                     } else {
                         // Camera access denied, show an alert or handle the issue
                     }
                 }
                 
             case .denied, .restricted:
                 // Camera access denied or restricted, show an alert or handle the issue
                 break
             }
      
    }
    
    
    private func openImagePicker() {
           let imagePickerController = UIImagePickerController()
           imagePickerController.allowsEditing = false
           imagePickerController.delegate = self
           
           let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: "Option to select", preferredStyle: .actionSheet)
           
           let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
               print("Cancel")
           }
           actionSheetController.addAction(cancelActionButton)
           
           let galleryActionButton = UIAlertAction(title: "Choose from Gallery", style: .default) { _ in
               imagePickerController.sourceType = .photoLibrary
               DispatchQueue.main.async {
                   self.present(imagePickerController, animated: true, completion: nil)
               }
           }
           actionSheetController.addAction(galleryActionButton)
           
           let cameraActionButton = UIAlertAction(title: "Choose from Camera", style: .default) { _ in
               imagePickerController.sourceType = .camera
               DispatchQueue.main.async {
                   self.present(imagePickerController, animated: true, completion: nil)
               }
           }
           actionSheetController.addAction(cameraActionButton)
           
           present(actionSheetController, animated: true, completion: nil)
       }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("\(info)")
        if let image = info[.originalImage] as? UIImage {
            //                imageView?.image = image
            plusBtn.setImage(image, for: .normal)
            self.uploadImageToS3(image: image)
            dismiss(animated: true, completion: nil)
        }
    }
    
    private func uploadImageToS3(image: UIImage){
        
        
        showLoadingIndicator(in: view)
        
        self.view.isUserInteractionEnabled = false
        
        image.uploadImageToS3( success: { (uploaded : Bool, imageUrl : String) in
            
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            
            print("MYURL: ", imageUrl)
            self.taskmediaURl = imageUrl
            
            if uploaded {
                self.view.isUserInteractionEnabled = true
              
            }
        }, progress: { (uploadPercentage : CGFloat) in
            //            print_debug(uploadPercentage)
            
        }, size: {
            (_) in
        }, failure: { (err : Error) in
        
            self.view.isUserInteractionEnabled = true
        })
    }
}
