//
//  RightViewImgCell.swift
//  DidirDoot
//
//  Created by Navdip on 09/03/23.


import UIKit

class RightViewImgCell: UITableViewCell {
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var sendView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        sendView.rounded(radius: 12)
        sendView.backgroundColor = UIColor(hexString: "ECEBEB")
        
        
        contentView.backgroundColor = .clear
        backgroundColor = .clear

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
