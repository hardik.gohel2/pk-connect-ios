
import UIKit
import YoutubePlayer_in_WKWebView
import FirebaseFirestore


class LiveViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

  
    @IBOutlet weak var BtnHeartPress: UIButton!
    @IBOutlet weak var backText: UILabel!
    
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var commentTextfiled: UITextField!
//    @IBOutlet weak var animateddView: UIView!
    @IBOutlet weak var liveCount: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var playerView: WKYTPlayerView!
    var commentArray = [FirestoreComments]()

    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var novideoView: UIView!
    var videoID = ""
    var session_ID = ""
    var timer = Timer()
    var timer1: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timer1 = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(showHearts), userInfo: nil, repeats: true)
            
        backText.text = "PK Live".localize()
        self.novideoView.isHidden = true
        self.videoView.isHidden = true
        
       // videoView.isHidden = true
        
        let db = Firestore.firestore()
        db.collection("live_session").document("data").getDocument { (querySnapshot, err) in
            DispatchQueue.main.async {
                
                if querySnapshot?.data()?["video_id"] as! String == "" {
                    
                    self.novideoView.isHidden = false
                    self.videoView.isHidden = true
                } else {
                    self.videoView.isHidden = false
                    self.novideoView.isHidden = true
                    
                    self.commentView.layer.borderWidth = 1.0
                    self.commentView.layer.borderColor = UIColor(hexString: "FFCE1A").cgColor
                    self.commentView.layer.cornerRadius = 20.0
                    self.commentView.clipsToBounds = true
                    
                    
                    self.getVideoID()
                    
                    
                    self.tableView.dataSource = self
                    self.tableView.delegate = self
                    
                    
                    self.updateCount()
                    self.getLiveCount()
                    
                }
            }
        }
        

    }
    
    override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            
            // Invalidate the timer when the view is about to disappear to stop the animation
            timer1?.invalidate()
            timer1 = nil
        }
    
    @objc func showHearts() {
            let heartImageView = UIImageView(image: UIImage(named: "liked_small.png"))
            let imageSize = CGSize(width: 30, height: 30)
            
            // Set the initial position at the bottom right corner of the view with a 20-point margin
            let initialXPosition = view.bounds.width - imageSize.width - 20
            let initialYPosition = view.bounds.height - imageSize.height - 60
            heartImageView.frame = CGRect(x: initialXPosition, y: initialYPosition, width: imageSize.width, height: imageSize.height)
            
            view.addSubview(heartImageView)
            
            // Calculate the end position at the half right corner of the view
            let endXPosition = view.bounds.width - imageSize.width - 20 // Adjusted for the margin
            let endYPosition = (view.bounds.height - imageSize.height) / 2
            
            UIView.animate(withDuration: 2.0, animations: {
                // Move the heartImageView to the half right corner of the view
                heartImageView.frame.origin.x = endXPosition
                heartImageView.frame.origin.y = endYPosition
            }, completion: { _ in
                heartImageView.removeFromSuperview()
            })
        }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    @IBAction func BtnHeartPress(_ sender: Any) {
        
        showHearts()
        
        
    }
    func GetliveComments() {
        let db = Firestore.firestore()
        let docRef = db.collection("live").document(self.session_ID).collection("comments").order(by: "time", descending: false).limit(to: 30)

        docRef.getDocuments { (querySnapshot, error) in

            if let error = error {
                print("fulleoor\(error.localizedDescription)")
            }else{
                self.commentArray = []
                for i in querySnapshot!.documents {
                    print(i.data())
//                    if let data = i.data() as? [String: Any],
//                          let profilePic = data["profilePic"] as? UIImage,
//                          data.count != 0 {
//                        self.commentArray.append(FirestoreComments(dictionary: data)!)
//                       }
                                    if i.data().count == 0 {
                                            
                                    } else {
                                        if let item = FirestoreComments(dictionary: i.data()){
                                            self.commentArray.append(item)
                                        }
                                    }
                }
                print(self.commentArray.count)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    if self.commentArray.count == 0 {
                    }
                    else {
                        //self.scrollToBottom()
                    }
                }
            }

        }
    }
    
    func writeaddData(CommentTest: String){
        let db = Firestore.firestore()
//        let substring1 = String(CommentTest.dropLast(2))
        let newSweet = FirestoreComments(name: getRetrievedUser()?.user_name ?? "", message: CommentTest , profilepic: "", userID: getRetrievedUser()?.user_id ?? "43721", time: Date().millisecondsSince1970)
        var ref:DocumentReference? = nil
        ref = db.collection("live").document(self.session_ID).collection("comments").addDocument(data: newSweet.dictionary) {
            error in
            if let error = error {
                print("Error adding document: \(error.localizedDescription)")
            }else{
                print("Document added with ID: \(ref!.documentID)")
            }
        }
    }
    
    @IBAction func sendCommentClicked(_ sender: Any) {
      
        if commentTextfiled.text == "" {
           // CommonFunction.showToast(msg: "Please enter comment!")
        } else {
            self.commentArray.append(FirestoreComments(name: "Nagababu", message: commentTextfiled.text ?? "", profilepic:  "", userID: getRetrievedUser()?.user_id ?? "43721", time: Date().millisecondsSince1970))

            writeaddData(CommentTest: commentTextfiled.text ?? "")

            commentTextfiled.text = ""
            commentTextfiled.resignFirstResponder()
            self.tableView.reloadData()
      
        }
        
        
    }
    @IBAction func closeTapped(_ sender: Any) {
        updateCountMinus1()
        self.playerView.stopVideo()
        self.dismiss(animated: true)

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveCell", for: indexPath) as! LiveCell
        cell.selectionStyle = .none
        if commentArray.count == 0 {
            
        } else {
          
            cell.userComment.text = commentArray[indexPath.row].message
            
            cell.userName.text = commentArray[indexPath.row].name

            downloadImageFrom(urlString: commentArray[indexPath.row].profilepic ?? "") { downloadedImage in
                DispatchQueue.main.async {
                    cell.userPic.layer.cornerRadius = 25
                    cell.userPic.clipsToBounds = true
                    cell.userPic.image = downloadedImage
                }
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension

    }

    override func viewDidDisappear(_ animated: Bool) {
        playerView.stopVideo()
    }
    func getLiveCount(){
        
        let db = Firestore.firestore()
        db.collection("live_session").document("data").getDocument { (querySnapshot, err) in
            let mai = querySnapshot?.data()?["live_count"] as! Int
           
            let someadd = mai
            if someadd == 1 {
             
                self.liveCount.text = "1"

            } else {
                self.liveCount.text = someadd.description

            }
        }
    }
    func getVideoID(){
      
        let db = Firestore.firestore()
        db.collection("live_session").document("data").getDocument { (querySnapshot, err) in

            DispatchQueue.main.async {
                
                self.session_ID = querySnapshot?.data()?["session_id"] as! String
                self.videoID = querySnapshot?.data()?["video_id"] as! String
                self.playerView.load(withVideoId: self.videoID )
                self.playerView.delegate = self
                
                self.GetliveComments()
            }
        }
    }
    func updateCount() {
        let db = Firestore.firestore()
        db.collection("live_session").document("data").setData(["live_count": FieldValue.increment(Int64(1))], merge: true)
    }
    func updateCountMinus1() {
        let db = Firestore.firestore()
        db.collection("live_session").document("data").setData(["live_count": FieldValue.increment(Int64(-1))], merge: true)
    }
}
extension LiveViewController: WKYTPlayerViewDelegate {
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        playerView.playVideo()
    }
}
extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}
extension Int {
    func dateFromMilliseconds() -> Date {
        return Date(timeIntervalSince1970: TimeInterval(self)/1000)
    }
}
