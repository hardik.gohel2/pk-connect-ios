//
//  FirestoreComments.swift
//  Stalinani
//
//  Created by Haritej on 06/10/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import FirebaseFirestore

protocol DocumentSerializable  {
    init?(dictionary:[String:Any])
}


struct FirestoreComments {
    var name:String?
    var message:String?
    var profilepic:String?
    var userID:String?
    var time: Int64?
  //  var code: String?

    
    var dictionary:[String:Any] {
        return [
            "name":name ?? "",
            "message" : message ?? "",
            "profilePic" : profilepic ?? " ",
            "userId" : userID ?? "",
            "time" : time ?? 00
         //   "code" : code ?? ""
            
        ]
    }
    
}

extension FirestoreComments : DocumentSerializable {
    init?(dictionary: [String : Any]) {
        guard let name = dictionary["name"] as? String,
            let message = dictionary["message"] as? String,
            let profilepic = dictionary["profilePic"] as? String,
            let userID = dictionary["userId"] as? String,
            let time = dictionary["time"] as? Int64 else {return nil}
          //  let code = dictionary["code"] as? String else
            self.init(name: name, message: message, profilepic : profilepic, userID: userID, time: time)

        
        //self.init(name: name, message: message, profilepic : profilepic, userID: userID, time: time, code: code)
    }
}


