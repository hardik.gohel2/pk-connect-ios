//
//  InteractTableCell.swift
//  KCR Sainyam
//
//  Created by Poshitha Gajjala on 7/16/22.
//

import UIKit

class InteractTableCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardIcon: UIImageView!
    @IBOutlet weak var cardTitle: UILabel!
    @IBOutlet weak var cardDescription: UILabel!
    @IBOutlet weak var cardButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cardView.layer.cornerRadius = 8
        cardButton.layer.cornerRadius = 15
    }

}
