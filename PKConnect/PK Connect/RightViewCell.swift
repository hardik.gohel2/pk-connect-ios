//
//  RightViewCell.swift
//  ChatSample
//
//  Created by Nagababu on 20/09/2019.
//  Copyright © 2019 Nibs. All rights reserved.
//

import UIKit

class RightViewCell: UITableViewCell {

    @IBOutlet weak var logoHeight: NSLayoutConstraint!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var sendText: UILabel!
    @IBOutlet weak var lblTime: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        sendView.rounded(radius: 12)
        sendView.backgroundColor = UIColor(hexString: "ECEBEB")
        
        
        contentView.backgroundColor = .clear
        backgroundColor = .clear
    }
    
   
}
