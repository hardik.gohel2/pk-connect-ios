//
//  KCRSocialViewController.swift
//  KCR Sainyam
//
//  Created by Haritej on 22/08/22.
//

import UIKit

class KCRSocialViewController: UIViewController {

    @IBOutlet weak var twitterContainer: UIView!
    @IBOutlet weak var facebookContainer: UIView!
    @IBOutlet weak var instagramContainer: UIView!
    @IBOutlet weak var youtubeContainer: UIView!
    
    @IBOutlet weak var twitterView: UIView!
    @IBOutlet weak var fbView: UIView!
    @IBOutlet weak var instagramView: UIView!
    @IBOutlet weak var youtubeView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async { [self] in
            self.twitterContainer.isHidden = false
            self.facebookContainer.isHidden = true
            self.instagramContainer.isHidden = true
            self.youtubeContainer.isHidden = true
            
            self.twitterView.backgroundColor = UIColor(hexString: "#FFCE1A")
            self.fbView.backgroundColor = .white
            self.instagramView.backgroundColor = .white
            self.youtubeView.backgroundColor = .white
        }


        // Do any additional setup after loading the view.
    }
    
    @IBAction func twitterTapped(_ sender: Any) {
        
        DispatchQueue.main.async {
            
            self.twitterContainer.isHidden = false
            self.facebookContainer.isHidden = true
            self.instagramContainer.isHidden = true
            self.youtubeContainer.isHidden = true
            
            self.twitterView.backgroundColor = UIColor(hexString: "#FFCE1A")
            self.fbView.backgroundColor = .white
            self.instagramView.backgroundColor = .white
            self.youtubeView.backgroundColor = .white
        }
    }
    @IBAction func facebookTapped(_ sender: Any) {
        DispatchQueue.main.async {
            
            self.twitterContainer.isHidden = true
            self.facebookContainer.isHidden = false
            self.instagramContainer.isHidden = true
            self.youtubeContainer.isHidden = true
            
            self.twitterView.backgroundColor = .white
            self.fbView.backgroundColor = UIColor(hexString: "#FFCE1A")
            self.instagramView.backgroundColor = .white
            self.youtubeView.backgroundColor = .white
        }
        
    }
    @IBAction func instagramTapped(_ sender: Any) {
        DispatchQueue.main.async {
            
            self.twitterContainer.isHidden = true
            self.facebookContainer.isHidden = true
            self.instagramContainer.isHidden = false
            self.youtubeContainer.isHidden = true

            self.twitterView.backgroundColor = .white
            self.fbView.backgroundColor = .white
            self.instagramView.backgroundColor = UIColor(hexString: "#FFCE1A")
            self.youtubeView.backgroundColor = .white
            
         
            
        }
        
    }
    @IBAction func youTubeTapped(_ sender: Any) {
        DispatchQueue.main.async {
            
            self.twitterContainer.isHidden = true
            self.facebookContainer.isHidden = true
            self.instagramContainer.isHidden = true
            self.youtubeContainer.isHidden = false

            self.twitterView.backgroundColor = .white
            self.fbView.backgroundColor = .white
            self.instagramView.backgroundColor = .white
            self.youtubeView.backgroundColor = UIColor(hexString: "#FFCE1A")
            
     
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
