//
//  PKConnectViewController.swift
//  KCR Sainyam
//
//  Created by Poshitha Gajjala on 7/15/22.
//

import UIKit

class PKConnectViewController: UIViewController {
    
    @IBOutlet weak var interactSegment: UIButton!
    
    @IBOutlet weak var interactView: UIView!
    @IBOutlet weak var kcrSocialView: UIView!
    @IBOutlet weak var segmentSelectorLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var segmentSelectorWidthConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async { [self] in
            self.interactView.isHidden = true
            self.kcrSocialView.isHidden = false
        }
      
        
        view.layoutIfNeeded()
        self.interactSegmentSelected(interactSegment)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @IBAction func interactSegmentSelected(_ sender: UIButton) {
        
        DispatchQueue.main.async { [self] in
            self.interactView.isHidden = false
            self.kcrSocialView.isHidden = true
        }
        segmentSelectorWidthConstraint.constant = sender.titleLabel?.intrinsicContentSize.width ?? 0
        segmentSelectorLeadingConstraint.constant = sender.frame.origin.x + (sender.titleLabel?.frame.origin.x ?? 0)
    }
    
    @IBAction func kcrSocialSegmentSelected(_ sender: UIButton) {
        DispatchQueue.main.async { [self] in
            self.interactView.isHidden = true
            self.kcrSocialView.isHidden = false
        }
        segmentSelectorWidthConstraint.constant = sender.titleLabel?.intrinsicContentSize.width ?? 0
        segmentSelectorLeadingConstraint.constant =  sender.frame.origin.x + (sender.titleLabel?.frame.origin.x ?? 0)
    }
    
}
