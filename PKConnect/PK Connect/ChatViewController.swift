//
//  ChatViewController.swift
//  KCR Sainyam
//
//  Created by Nagababu on 25/10/22.
//

import UIKit
import ISEmojiView
import IQKeyboardManagerSwift

class ChatViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var BtnSubmit: UIButton!
    @IBOutlet weak var titleTextt: UILabel!
    @IBOutlet weak var bottomView: NSLayoutConstraint!
    @IBOutlet weak var fullView: UIView!
    @IBOutlet weak var messageText: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var chatLists: [chatDetails]?
    var emojiClick = false
    private let imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //BtnSubmit.setTitle("Submit".localize(), for: .normal)
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(ChatViewController.self)
        IQKeyboardManager.shared.disabledToolbarClasses = [ChatViewController.self]
        imagePicker.allowsEditing = true
        imagePicker.delegate = self

        titleTextt.text = "Chat with PK".localize()

        //messageText.text = "Please enter your message".localize()
        messageText.attributedPlaceholder = NSAttributedString(
            string: "Please enter your message".localize(),
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.pkConnectYellow]
        )
        messageText.delegate = self
        
        fullView.layer.borderWidth = 1.0
        fullView.layer.borderColor = UIColor(hexString: "FFCE1A").cgColor
        fullView.layer.cornerRadius = 20.0
        fullView.clipsToBounds = true
        
        showLoadingIndicator(in: view)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.backgroundColor = UIColor(hexString: "F6F6F6")
        
        tableView.register(UINib(nibName: "RightViewCell", bundle: nil), forCellReuseIdentifier: "RightViewCell")
        tableView.register(UINib(nibName: "LeftViewCell", bundle: nil), forCellReuseIdentifier: "LeftViewCell")
        tableView.register(UINib(nibName: "RightViewImgCell", bundle: nil), forCellReuseIdentifier: "RightViewImgCell")
        tableView.register(UINib(nibName: "LeftViewImgCell", bundle: nil), forCellReuseIdentifier: "LeftViewImgCell")
        

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.getChatAPI()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tableView.addGestureRecognizer(tap)
        
//        #ECEBEB
        // Do any additional setup after loading the view.
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            bottomView.constant =  keyboardSize.height - view.safeAreaInsets.bottom
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        bottomView.constant = 0
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if emojiClick{
            emojiClick = false
        }else{
            messageText.inputView = nil
            messageText.reloadInputViews()

            messageText.textColor = .black
        }
    }
    
    func getChatAPI() {
        getChats { chatList in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            self.chatLists = []
            print("chatLists: ", chatList)
            self.chatLists = chatList
            DispatchQueue.main.async { [self] in
                self.tableView.reloadData()
                self.scrollToBottom(isAnimated: false)

            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatLists?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if chatLists?[indexPath.row].sender_type == "0" {
            if chatLists?[indexPath.row].media_type == "image"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "RightViewImgCell", for: indexPath) as! RightViewImgCell
//                cell.sendText?.text = chatLists?[indexPath.row].message
                cell.lblTime.text = calculateFormat(date : self.chatLists?[indexPath.row].send_timestamp , formatterReq : "hh:mm a")

                if chatLists?[indexPath.row].media_type == "image" {
                    downloadImageFrom(urlString: chatLists?[indexPath.row].media_thumb ?? "") { downloadedImage in
                        DispatchQueue.main.async {
//                            cell.logoImage.isHidden = false
//                            cell.logoHeight.constant = 114
                            cell.imgMain.image = downloadedImage
                        }
                    }
                } else {
//                    cell.logoImage.isHidden = true
//                    cell.logoHeight.constant = 0
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "RightViewCell", for: indexPath) as! RightViewCell
                cell.sendText?.text = chatLists?[indexPath.row].message
                
                cell.lblTime.text = calculateFormat(date : self.chatLists?[indexPath.row].send_timestamp , formatterReq : "hh:mm a")

                if chatLists?[indexPath.row].media_type == "image" {
                    downloadImageFrom(urlString: chatLists?[indexPath.row].media_thumb ?? "") { downloadedImage in
                        DispatchQueue.main.async {
                            cell.logoImage.isHidden = false
                            cell.logoHeight.constant = 114
                            cell.logoImage.image = downloadedImage
                        }
                    }
                } else {
                    cell.logoImage.isHidden = true
                    cell.logoHeight.constant = 0
                }
                return cell
            }
        }
        else {
            if chatLists?[indexPath.row].media_type == "image"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeftViewImgCell", for: indexPath) as! LeftViewImgCell
//                cell.sendText?.text = chatLists?[indexPath.row].message
                cell.lblTime.text = calculateFormat(date : self.chatLists?[indexPath.row].send_timestamp , formatterReq : "hh:mm a")

                if chatLists?[indexPath.row].media_type == "image" {
                    downloadImageFrom(urlString: chatLists?[indexPath.row].media_thumb ?? "") { downloadedImage in
                        DispatchQueue.main.async {
                            cell.imgMain.image = downloadedImage
                        }
                    }
                } else {
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeftViewCell", for: indexPath) as! LeftViewCell
                cell.logoImage.isHidden = true
                cell.logoHeight.constant = 0
                cell.recieveText?.text = chatLists?[indexPath.row].message
                cell.lblTime.text = calculateFormat(date : self.chatLists?[indexPath.row].send_timestamp , formatterReq : "hh:mm a")

                return cell
            }
        }
    }
    func calculateFormat(date : String? , formatterReq : String) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let newDate = formatter.date(from: date!) ?? Date()
        formatter.dateFormat = formatterReq
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter.string(from: newDate)
    }
    @IBAction func backClicked(_ sender: Any) {
        DispatchQueue.main.async {
            self.dismiss(animated: true)
        }
    }
    @IBAction func sendTapped(_ sender: Any) {
        showLoadingIndicator(in: view)
        if messageText.text == "" || messageText.text == nil || messageText.text == "Please enter your message" {
            
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            
            self.showToast(message: "Please enter your message!".localize())
//            let alert = UIAlertController(title: "Chat With PK", message: "Please enter your message!", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            
            postChat(message: messageText.text!) {
                DispatchQueue.main.async {
                    self.hideLoadingIndicator()
                }
                print("success meaaage")
                //
                DispatchQueue.main.async { [self] in
                    self.messageText.text = ""
                }
                
                self.getChatAPI()
                
            }
        }
        
        
    }
    
    @IBAction func btnAttachment(_ sender: UIButton) {
        showAlert()
    }
    @IBAction func btnEmojiClick(_ sender: UIButton) {
        emojiClick = true
        let keyboardSettings = KeyboardSettings(bottomType: .pageControl)
        let emojiView = EmojiView(keyboardSettings: keyboardSettings)
        emojiView.translatesAutoresizingMaskIntoConstraints = false
        emojiView.delegate = self
        messageText.inputView = emojiView
        messageText.reloadInputViews()
        messageText.becomeFirstResponder()
    }
    
}
extension ChatViewController {

    func scrollToBottom(isAnimated:Bool = true){

        DispatchQueue.main.async {
            if self.chatLists!.count >= 2{
                self.tableView.scrollToRow(at: IndexPath(row: self.chatLists!.count - 1, section: 0), at: .bottom, animated: false)
            }
        }
    }
}
extension ChatViewController: EmojiViewDelegate{
    func emojiViewDidSelectEmoji(_ emoji: String, emojiView: ISEmojiView.EmojiView) {
        messageText.insertText(emoji)
    }
    
    // callback when tap delete button on keyboard
    func emojiViewDidPressDeleteBackwardButton(_ emojiView: EmojiView) {
        messageText.deleteBackward()
    }
    
    func emojiViewDidPressDismissKeyboardButton(_ emojiView: EmojiView) {
        messageText.resignFirstResponder()
    }

}
extension UIView {
    func rounded(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showAlert() {
        let alert = UIAlertController(title: "Add Photo!".localize(), message: nil, preferredStyle: .actionSheet)

        
        alert.addAction(UIAlertAction(title: "Choose from Library".localize(), style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel".localize(), style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var img = UIImage()
        var img_url = String()
        
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
//            imageView.contentMode = .scaleToFill
//            imageView.image = pickedImage
     //
            img = pickedImage
         }
        
        if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL{
                let imgName = imgUrl.lastPathComponent
                let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
                let localPath = documentDirectory?.appending(imgName)

                let image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
                let data = image.pngData()! as NSData
                data.write(toFile: localPath!, atomically: true)
                //let imageData = NSData(contentsOfFile: localPath!)!
                let photoURL = URL.init(fileURLWithPath: localPath!)//NSURL(fileURLWithPath: localPath!)
                print(photoURL)
            img_url = "\(photoURL.absoluteString)"
                
            }
            let img_data = img.jpegData(compressionQuality: 1)
            uploadImage(img_url, data: img_data!)
          //  uploadImage(fileName: img_url, image: img)
            dismiss(animated: true, completion: nil)
    }
    
    private func uploadImage(_ img: String, data: Data) {
        var dict = dataResponse()
        dict["attached_media"] =  img
        showLoadingIndicator(in: view)
        let parm = ["sender_type":"0","media_type":"image"] as [String : Any]
        self.requestForMultiPartURL(strURL: img, imgName: data, Parameter: parm, dictImage: dict, success: { (response) in
             if let respon = response, let res = respon as? dataResponse{
                 print(res)
                 if let status = res["status"]{
                     if status as! String == "200"{
                         self.getChatAPI()
                     }else{
                         self.showToast(message: "Something went wrong. Please try again")
                     }
                 }
             }
        }) {(error) in
            print(error as Any)
        }

    }
    
    func openCamera() {
        if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }

    func openGallery() {
        imagePicker.sourceType = .photoLibrary
        imagePicker.modalPresentationStyle = .fullScreen
        present(imagePicker, animated: true, completion: nil)
    }
}
