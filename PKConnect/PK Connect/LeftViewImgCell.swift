//
//  LeftViewImgCell.swift
//  DidirDoot
//
//  Created by Navdip on 09/03/23.
//

import UIKit

class LeftViewImgCell: UITableViewCell {

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var recieveView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        recieveView.rounded(radius: 12)
        recieveView.backgroundColor = .white
        
        contentView.backgroundColor = .clear
        backgroundColor = .clear
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
