//
//  ChatTableViewCell.swift
//  KCR Sainyam
//
//  Created by Nagababu on 25/10/22.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    
    @IBOutlet weak var recieveView: UIView!
    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var recieveText: UILabel!
    @IBOutlet weak var sendText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
