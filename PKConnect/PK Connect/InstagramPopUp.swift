//
//  InstagramPopUp.swift
//  PKConnect
//
//  Created by apple on 09/11/22.
//



import UIKit

protocol instaPopupDismiss : AnyObject{
    func dismisss(type: String,TaskData: TaskResult, videoPath: String)
}


class InstagramPopUP: BaseViewController {
    
    var tasktype: String?
    var taskData:TaskResult?
    @IBOutlet weak var lblStepOne: UILabel!
    @IBOutlet weak var lblStepTwo: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var lblHeading: UILabel!

    weak var delegate : instaPopupDismiss?
    //       var task : TaskTabModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHeading.text = "Task will be completed in following two steps:".localize()
        lblStepOne.text = "Step 1. Complete the task".localize()
        lblStepTwo.text = "Step 2. Upload 2 screenshots".localize()
        btnOk.setTitle("Ok".localize(), for: .normal)

        // Do any additional setup after loading the view.
    }
    @IBAction func btnActionOk(_ sender: UIButton) {
        if tasktype == "whatsapp"{
            if taskData?.action == "link"{
                self.dismiss(animated: true)
                self.delegate?.dismisss(type: self.tasktype ?? "",TaskData: self.taskData!, videoPath: "")
            }else{
                self.showToast(message: "Downloading")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.delegate?.dismisss(type: self.tasktype ?? "",TaskData: self.taskData!, videoPath: "")
                    self.dismiss(animated: true)
                }
            }
        }else if tasktype == "instagram" {
            if taskData?.action == "link" || taskData?.action == "like"{
                self.dismiss(animated: true)
                self.delegate?.dismisss(type: self.tasktype ?? "",TaskData: self.taskData!, videoPath: "")
            }else{
                self.showToast(message: "Downloading")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.dismiss(animated: true)
                    self.dismiss(animated: true)
                    self.downloadAndSaveVideo(url: self.taskData?.task_media_set?[0].media_url ?? "")
                   
                    
                    
                }
            }
        }
        else{
            self.delegate?.dismisss(type: self.tasktype ?? "",TaskData: self.taskData!, videoPath: "")
            self.dismiss(animated: true)
            
        }
        
    }
    
    func downloadAndSaveVideo(url:String) {
       
        let videoURLString = url

        showLoadingIndicator(in: self.view)
        if let videoURL = URL(string: videoURLString) {
            // Define the destination file path where the video will be saved.
            let destinationPath = FileManager.documentsDirectoryURL.appendingPathComponent("downloaded_video.mp4")
            
            // Check if the file already exists at the destination path.
            if FileManager.default.fileExists(atPath: destinationPath.path) {
                print("File already exists at path: \(destinationPath.path)")
                hideLoadingIndicator()
                self.delegate?.dismisss(type: self.tasktype ?? "",TaskData: self.taskData!, videoPath: destinationPath.path)
               
            } else {
                let downloadTask = URLSession.shared.downloadTask(with: videoURL) { (tempURL, response, error) in
                    if let tempURL = tempURL, error == nil {
                        do {
                            try FileManager.default.moveItem(at: tempURL, to: destinationPath)
                            print("Downloaded video and saved at path: \(destinationPath.path)")
                            self.hideLoadingIndicator()
                            
                            self.delegate?.dismisss(type: self.tasktype ?? "",TaskData: self.taskData!, videoPath: destinationPath.path)
                        } catch {
                            print("Error moving the downloaded file: \(error.localizedDescription)")
                            // Handle the error.
                        }
                    } else {
                        print("Error downloading the video: \(error?.localizedDescription ?? "Unknown error")")
                        // Handle the error.
                    }
                }
                
                downloadTask.resume()
            }
        }
    }
    @IBAction func btnActionDismiss(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension FileManager {
    static var documentsDirectoryURL: URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
}
