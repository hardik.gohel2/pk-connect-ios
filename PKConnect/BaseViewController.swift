//
//  BaseViewController.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 10/30/22.
//

import UIKit
import SVProgressHUD
import Alamofire


class BaseViewController: UIViewController {
    //
   // live url  app   https://pkconnect.com/dashboard"
//   private let baseURL = "https://stage.pkconnect.com/",
//                username = "admin",
//                password = "12345"
    
        private let baseURL = "https://stage.pkconnect.com/",
                    username = "admin",
                    password = "12345"
    
    private var httpHeaders: [String: String]?,
                spinnerView: UIView!
    var appLang = "en"
    lazy var noRecordsLabel = UILabel()
    
    lazy var backgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let lang = UserDefaults.standard.object(forKey: "selectedLang") as? String {
            self.appLang = lang
        }
        guard let data = "\(username):\(password)".data(using: .utf8) else { return  }
        let base64 = data.base64EncodedString()
        httpHeaders = ["Authorization" : "Basic \(base64)", "lang" : (appLang == "hi" ? "hi" : "en"),"Accept-Language" : (appLang == "hi" ? "hi" : "en"), "Accesstoken": getRetrievedUser()?.accessToken ?? ""]
        
        LinkManager.shared.delegate = self
    }
    
    func getRetrievedUser() -> VerifiedUser? {
        if let userData = UserDefaults.standard.object(forKey: "User") as? Data,
           let retrievedUser = try? JSONDecoder().decode(VerifiedUser.self, from: userData) {
            return retrievedUser
        }
        return nil
    }
    
    func addDashedPageControl(with currentPage: Int) {
        let stackView   = UIStackView()
        stackView.axis  = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        stackView.spacing   = 34.0
        
        for i in 0...2 {
            let view1 = UIView()
            view1.layer.cornerRadius = 3
            view1.heightAnchor.constraint(equalToConstant: 4).isActive = true
            view1.backgroundColor = (currentPage == i) ? .pkConnectYellow : UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1)
            stackView.addArrangedSubview(view1)
        }
        self.view.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
            stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 60),
            stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -60),
            stackView.heightAnchor.constraint(equalToConstant: 72)
        ])
    }
    
    func showLoadingIndicator(in container: UIView) {
        
        var spinnerFrame = container.bounds
        spinnerFrame.size.height += 20
        spinnerView = UIView.init(frame: spinnerFrame)
        spinnerView.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.style = .large
        spinner.color = .pkConnectYellow
        spinner.startAnimating()
        spinner.center = spinnerView.center
        
        spinnerView.addSubview(spinner)
        container.addSubview(spinnerView)
      
        
    }
    
    func hideLoadingIndicator() {
        
        spinnerView.removeFromSuperview()
    }
    
    func goToWelcomeView() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let welcomeVC = storyBoard.instantiateViewController(withIdentifier: "LoginOPTVC")
        self.navigationController?.setViewControllers([welcomeVC], animated: false)
    }
    
    func getFormattedDate(from dateStr: String, inputFormat: String, outputFormat: Int) -> String {
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = inputFormat
        guard let date = inputDateFormatter.date(from: dateStr) else { return "" }
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "hh:mm a"
        let time = timeFormatter.string(from: date)
        
        let components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .timeZone], from: date)
        let monthName = DateFormatter().monthSymbols[components.month! - 1].localize()
        
        switch outputFormat {
        case 0:
            return "\(components.day!) \(monthName), \(components.year!)"
        case 1:
            return "\(monthName) \(components.day!), \(time)"
        case 2:
            return "\(components.day!).\(components.month!).\(components.year!)"
        case 3:
            return "\(time)"
        case 4:
            return "\(components.day!)-\(components.month!)-\(components.year!)"
        default:
            return "\(components.day!) \(monthName) \(components.year!)"
        }
    }
    
    func shareTapped(_ image: UIImage, feedItem : FeedItem) {
        
        shareMediaforDeeplink(view: self, feedItem: feedItem)
     
    }
    
    func shareVideoURL(url: String,feedItem : FeedItem) {
        
        shareMediaforDeeplink(view: self, feedItem: feedItem)
    }
    
    func showNewsDetailView(_ newsId: String, _ img: UIImage?) {
        let storyBoard = UIStoryboard(name: "Other", bundle: nil)
        let newsDetailView = storyBoard.instantiateViewController(withIdentifier: "newsDetailView") as! NewsDetailView
        newsDetailView.newsId = newsId
        newsDetailView.img = img
        self.navigationController?.pushViewController(newsDetailView, animated: false)
    }
    
    func likeTapped(cell: FeedTableCell, newsId: String) {
        if !cell.likeButton.isSelected {
            postLike(newsId: newsId) {
                DispatchQueue.main.async {
                    cell.likeButton.isSelected.toggle()
                }
            }
        } else {
            unlikeNews(newsId: newsId) {
                DispatchQueue.main.async {
                    cell.likeButton.isSelected.toggle()
                }
            }
        }
    }
    
    func commentTapped(entry: FeedItem, newsId: String) {
        let storyBoard = UIStoryboard(name: "Other", bundle: nil)
        let commentVC = storyBoard.instantiateViewController(withIdentifier: "commentVC") as! CommentVC
        commentVC.newsId = newsId
        commentVC.headline = entry.news_title
        commentVC.isFromews = true
        self.navigationController?.pushViewController(commentVC, animated: false)
    }
    
    func bookmarkTapped(cell: FeedTableCell, newsId: String, completion: @escaping ()->()) {
        postBookmark(newsId: newsId) {
            DispatchQueue.main.async {
                cell.bookmarkButton.isSelected.toggle()
                completion()
            }
        }
    }
    
    func downloadTapped(_ downloadedImage: UIImage) {
        UIImageWriteToSavedPhotosAlbum(downloadedImage, nil, nil, nil)
        self.showToast(message: "Downloading completed.".localize())
    }
    
    func getChats(completion: @escaping(([chatDetails])->())) {
        let url = URL(string: "\(baseURL)api/getKcrChats")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, error == nil {
                do {
                    let responseObj = try JSONDecoder().decode(chatModel.self, from: data)
                    completion(responseObj.data)
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    func postChat(message: String, completion: @escaping ()->()) {
        
        let url = URL(string: "\(baseURL)api/createKcrChat")!
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = httpHeaders
        request.httpBody = "message=\(message)&sender_type=\(0)&media_type=\("text")".data(using: .utf8)
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data {
                if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                {
                    completion()
                }
            }
        }.resume()
    }
    
    func postTask(task_id: String, media: String, completion: @escaping ()->()) {
        
        let url = URL(string: "\(baseURL)api/task")!
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = httpHeaders
        request.httpBody = "task_id=\(task_id)&media=\(media)".data(using: .utf8)
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data {
                if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                {
                    print("json:  ", json)
                    
                    completion()
                }
            }
        }.resume()
    }
    
    func requestForMultiPartURL(strURL : String, imgName: Data, Parameter : [String :Any]?,dictImage : dataResponse?, success : @escaping (Any?) -> Void,failure: @escaping (NSError?) -> Void){
   
        var httpHeader1 : HTTPHeaders = HTTPHeaders()
        if let heads = httpHeaders {
            for (field, value) in heads {
                httpHeader1.add(name: field, value: value)
            }
        }
//         httpHeader1.add(name: "Content-Type", value: "multipart/form-data; boundary=<calculated when request is sent>")

        let url = URL(string: "\(baseURL)api/createKcrChat")!

        AF.upload(multipartFormData: { multiPart in
            if let parm = Parameter{
                for p in parm {
                    multiPart.append("\(p.value)".data(using: String.Encoding.utf8)!, withName: p.key)
                }
            }
            if let parm = dictImage{
                    print(parm)
                    multiPart.append(imgName, withName: "attached_media", fileName: strURL,mimeType: "image/jpeg")
            }
        }, to: url, method: .post, headers: httpHeader1) .uploadProgress(queue: .main, closure: { progress in
            print("Upload Progress: \(progress.fractionCompleted)")
        }).responseJSON(completionHandler: { data in
            switch data.result {
            case .success(_):
                print("upload success result: \(data.value ?? "")")
                success(data.value ?? nil)
            case .failure(let err):
                print("upload err: \(err)")
                failure(err as NSError)
            }
         })
       }

    func postgetfeatured(remarks: String, media: String, completion: @escaping ()->()) {
        
        let url = URL(string: "\(baseURL)api/user_uploads")!
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = httpHeaders
        request.httpBody = "media_type=0&media=\(media)&&remarks=\(remarks)".data(using: .utf8)
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data {
                if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                {
                    print("json:  ", json)
                    completion()
                }
            }
        }.resume()
    }
    
    func likeOrUnlikeVideo(_ video_id: String, action: Bool, completion: @escaping ()->()) {
        let subPath = (action == true ? "like" : "dislike")
        let httpStr = "video_id=\(video_id)&action=\(subPath)"
        makeAPICall(path: "special_video_likes", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: Normal.self) { responseObj in
            if responseObj.status == "200" {
                completion()
            }
        } failure: {}
    }
    func postfacebookTwitter(task_id: String, completion: @escaping ()->()) {
        
        let url = URL(string: "\(baseURL)api/task")!
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = httpHeaders
        request.httpBody = "task_id=\(task_id)".data(using: .utf8)
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data {
                if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                {
                    print("json:  ", json)
                    
                    completion()
                }
            }
        }.resume()
    }
    
    func postTask(task_id: String, completion: @escaping ()->()) {
        
        let url = URL(string: "\(baseURL)api/task")!
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = httpHeaders
        request.httpBody = "task_id=\(task_id)".data(using: .utf8)
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data {
                if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                {
                    print("json:  ", json)
                    
                    completion()
                }
            }
        }.resume()
    }
    
    func changeAppLanguage() {
        let vc = LanguageSelectionVC()
        vc.isLangChange = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(vc, animated: true)
    }
    
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{1,4}$"
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    private func unauthorizedError() {
        DispatchQueue.main.async {
            self.goToWelcomeView()
        }
    }
    
    func addNoRecordsLabel(_ style: String, _ size: CGFloat, to mainView: UIView) {
        noRecordsLabel.attributedText = NSAttributedString(string: "No Records Found!".localize(), attributes: [.font: UIFont(name: "NunitoSans-\(style)", size: size)!])
        mainView.addSubview(noRecordsLabel)
        noRecordsLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            noRecordsLabel.centerXAnchor.constraint(equalTo: mainView.centerXAnchor),
            noRecordsLabel.centerYAnchor.constraint(equalTo: mainView.centerYAnchor)
        ])
    }
}

//MARK: API Calls
extension BaseViewController {
    
    private func makeAPICall<T: Decodable>(path: String, httpMethod: HttpMethodType, queryItems: [URLQueryItem]?, httpBody: String?, decodableClassType: T.Type ,success: @escaping (_ responseObj: T)->(), failure: ()->()) {
        
        var urlComponents = URLComponents(string: "\(baseURL)api/\(path)")!
        if let queryItems = queryItems {
            urlComponents.queryItems = queryItems
        }
        
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = httpMethod.rawValue
        
        request.httpBody = httpBody?.data(using: .utf8)
        request.allHTTPHeaderFields = httpHeaders
//                request.timeoutInterval = 60
        print(request.url!)
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, error == nil {
                do {
                    let responseObj = try JSONDecoder().decode(T.self, from: data)
                    success(responseObj)
                } catch {
                    print(error)
                    
                }
            }
        }.resume()
        
    }
    
    func sendOTP(for mobileNo: String, completionHandler: @escaping (SendOTPResponse?)->()){
        let httpStr = "mobile_no=\(mobileNo)"
        makeAPICall(path: "sendOtp", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: SendOTPResponse.self) { responseObj in
            if responseObj.status == "200" {
                print(responseObj.data)
                completionHandler(responseObj)
            } else {
                completionHandler(responseObj)
            }
        } failure: {
            
        }
    }
    func mobileVerify(for mobileNo: String, completionHandler: @escaping (_ MobileVerifiedUser: MobileVerifiedUser?)->()) {
        let httpStr = "mobile_no=\(mobileNo)&device_token=\(appDelegate.strFCMToken ?? "abc123")"
        makeAPICall(path: "mobileVerify", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: MobileVerifiedResponse.self) { responseObj in
            if responseObj.status == "200" {
                print(responseObj.data)
                completionHandler(responseObj.data)
            } else {
                completionHandler(nil)
            }
        } failure: {
            
        }
    }
    
    func verifyOTP(_ enteredOTP: String, _ mobileNo: String, completionHandler: @escaping (_ verifiedUser: VerifiedUser?) -> ()) {
        let httpStr = "mobile_no=\(mobileNo)&otp=\(enteredOTP)&device_token="
        makeAPICall(path: "verifyOtp", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: VerifyOTPResponse.self) { responseObj in
            if responseObj.status == "200" {
                completionHandler(responseObj.data)
            } else {
                completionHandler(nil)
            }
        } failure: {
            
        }
    }
    
    func verifyOTPProfile(_ enteredOTP: String, _ mobileNo: String, _ stateID: String, _ DistrictID: String, _ acID: String, completionHandler: @escaping (_ verifiedUser: VerifiedUser?) -> ()) {
        let httpStr = "mobile_no=\(mobileNo)&otp=\(enteredOTP)&device_token=\(appDelegate.strFCMToken ?? "abc123")&state=\(stateID)&district=\(DistrictID)&ac=\(acID)"
        makeAPICall(path: "profileUpdate", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: VerifyOTPResponseProfile.self) { responseObj in
            if responseObj.CODE == 200 {
                completionHandler(responseObj.RESULT)
            } else {
                completionHandler(nil)
            }
        } failure: {
            
        }
    }
    
    func registerUser(_ user: VerifiedUser, _ inputDetails: InputUserDetails, completionHandler: @escaping (_ errorMsg: String?) -> ()) {
        let url = URL(string: "\(baseURL)api/register")!
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        httpHeaders?["Accesstoken"] = user.accessToken ?? ""
        request.allHTTPHeaderFields = httpHeaders
        let referralCode =  UserDefaults.standard.value(forKey: "referral_code") as? String
        var referral = ""
        if let code = referralCode {
            referral = String(format: "%@", code as! CVarArg)
        }
        var httpStr = "lang=\(appLang)&refered_by=\(referral)"
        if let dict = inputDetails.dictionary {
            for key in dict.keys {
                httpStr.append("&\(key)=\(dict[key] ?? "")")
            }
        }
        
        request.httpBody = httpStr.data(using: .utf8)
        request.timeoutInterval = 1
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, error == nil {
                do {
                    let responseObj = try JSONDecoder().decode(RegisterUserResponse.self, from: data)
                    if responseObj.status == "200" {
//                        var registeredUser = user
//                        var registeredUser = VerifiedUser?
                        let registeredUser = VerifiedUser(user_id: user.accessToken, is_registered:  "1", user_name: inputDetails.name, accessToken: user.accessToken ?? "")

//                        registeredUser.user_name = inputDetails.name
//                        registeredUser.is_registered = "1"
                        UserDefaults.standard.removeObject(forKey: "referral_code")
                        if let userData = try? JSONEncoder().encode(registeredUser)  {
                            UserDefaults.standard.set(userData, forKey: "User")
                            
                        }
                        completionHandler(nil)
                    } else {
                        completionHandler(responseObj.message)
                    }
                } catch {
                    completionHandler(error.localizedDescription)
                }
            }
        }.resume()
    }
    
    func NewRegisterUser(_ user: MobileVerifiedUser, _ inputDetails: InputUserDetails, completionHandler: @escaping (_ errorMsg: String?) -> ()) {

        httpHeaders?["Accesstoken"] = user.accessToken ?? ""

        let httpStr = "name=\(inputDetails.name ?? "")&refered_by=\(inputDetails.refered_by ?? "")"
        makeAPICall(path: "newSignup", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: NewRegisterUserResponse.self) { responseObj in
            if responseObj.status == "200" {
//                        var registeredUser = user
//                        var registeredUser = VerifiedUser?
                let registeredUser = VerifiedUser(user_id: user.user_id, is_registered:  "1", user_name: inputDetails.name, accessToken: user.accessToken ?? "")

//                        registeredUser.user_name = inputDetails.name
//                        registeredUser.is_registered = "1"
                if let userData = try? JSONEncoder().encode(registeredUser)  {
                    UserDefaults.standard.set(userData, forKey: "User")
                }
                completionHandler(nil)
            } else {
                completionHandler(responseObj.message)
            }

        } failure: {
        }
    }
    
//    func getHomeData(section: String?, count: String, completion: @escaping((GetHomeData)->())) {
//
//        let url = URL(string: "\(baseURL)api/pk-quiz?page=\(page)&user_attempted=\(user_attempted)")!
//        var request = URLRequest(url: url)
//        request.httpMethod = "GET"
//        request.allHTTPHeaderFields = httpHeaders
//        request.timeoutInterval = 20
//        let session = URLSession.shared
//        session.dataTask(with: request) { (data, response, error) in
//            //        print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
//
//            if let fulldata = data, error == nil {
//                do {
//                    let fullresponse = try JSONDecoder().decode(QuizData.self, from: fulldata)
//                    print("",fullresponse)
//                    completion(fullresponse)
//                } catch{
//                    print("model class issue: ")
//                }
//            } else {
//                print("error: ", error!)
//            }
//        } .resume()
//    }
    
    func getHomeData(section: String?, count: String, completion: @escaping((GetHomeData?)->())) {
        var queryItems: [URLQueryItem]? = nil
        
        if let section = section {
            queryItems = [URLQueryItem(name: "section", value: section), URLQueryItem(name: "count", value: count)]
        }
        
        makeAPICall(path: "News", httpMethod: .GET, queryItems: queryItems, httpBody: nil, decodableClassType: GetHomeData.self, success: { responseObj in
            print(responseObj)
            if responseObj.CODE == 200 {
                completion(responseObj)
            }
        }, failure: {
           
        })
    }


    
    func getCampaigns(completion: @escaping ([Campaign])->()) {
        makeAPICall(path: "Campaigns", httpMethod: .GET, queryItems: nil, httpBody: nil, decodableClassType: GetCampaignsResponse.self) { responseObj in
            print(responseObj)
            completion(responseObj.RESULT!)
        } failure: { }
    }
    
    func getNotificationList(completion: @escaping ([BellNotification]?)->()) {
        makeAPICall(path: "notification", httpMethod: .GET, queryItems: nil, httpBody: nil, decodableClassType: BellNotificationResponse.self) { responseObj in
            if responseObj.CODE == 200 || responseObj.CODE == 202 {
                completion(responseObj.RESULT)
            } else if responseObj.CODE == 401 {
                self.unauthorizedError()
            }
        } failure: { }
    }
    
    func getNewsDetailFor(_ newsId: String, completionHandler: @escaping ([FeedItem]) -> ()) {
        let queryItems = [URLQueryItem(name: "news_id", value: "\(newsId)")]
        makeAPICall(path: "news", httpMethod: .GET, queryItems: queryItems, httpBody: nil, decodableClassType: GetHomeData.self) { responseObj in
            if responseObj.CODE == 200 {
                completionHandler(responseObj.RESULT!)
            }
        } failure: {
        }
    }
    
    func postLike(newsId: String, completion: @escaping ()->()) {
        let httpStr = "news_id=\(newsId)&like_type=\("heart")"
        makeAPICall(path: "news/likenews", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: DefaultAPIResponse.self) { responseObj in
            if responseObj.CODE == 200 {
                completion()
            }
        } failure: {
        }
    }
    
    func shareNewsMedia(newsId: String, platform:String, completion: @escaping ()->()) {
        let httpStr = "news_id=\(newsId)&platform=\(platform)"
        makeAPICall(path: "news-media/share", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: DefaultAPIResponse.self) { responseObj in
            if responseObj.CODE == 200 {
                completion()
            }
        } failure: {
        }
    }
    
    func unlikeNews(newsId: String, completion: @escaping ()->()) {
        let httpStr = "news_id=\(newsId)&like_type=\("heart")"
        makeAPICall(path: "news/dislikenews", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: DefaultAPIResponse.self) { responseObj in
            if responseObj.CODE == 200 {
                completion()
            }
        } failure: { }
    }
    
//    func getAllComments(newsId: String, completionHandler: @escaping ([Comment]?) -> ()) {
//        let httpStr = "news_id=\(newsId)&count=0"
//        makeAPICall(path: "news/getallcomments", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: GetComments.self) { responseObj in
//            if responseObj.CODE == 200 {
//                completionHandler(responseObj.RESULT)
//            }
//        } failure: { }
//    }
//
    func getAllComments(newsId: String, pageNo:Int, completionHandler: @escaping (GetComments?) -> ()) {
        let httpStr = "news_id=\(newsId)&count=\(pageNo)"
        makeAPICall(path: "news/getallcomments", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: GetComments.self) { responseObj in
            if responseObj.CODE == 200 {
                completionHandler(responseObj)
            }
        } failure: { }
    }
    
    
    
    func postShortComment(comment: String, short_id: String, completion: @escaping ()->()) {
        let httpStr = "comment=\(comment)&short_id=\(short_id)"
        makeAPICall(path: "createShortComment", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: DefaultAPIResponse.self) { responseObj in
          
            if responseObj.CODE == 200 {
                completion()
            }
        } failure: {}
    }
    
    func getShortsById(short_id:String,completion: @escaping((VideoModel1)->())) {
        let url = URL(string: "\(baseURL)api/News/shortbyid?short_id=\(short_id)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            //        print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(VideoModel1.self, from: fulldata)
//                    if let result = fullresponse {
                        completion(fullresponse)
//                    }
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    func getAllShortsComments(short_id: String, completion: @escaping ((GetCommentsShorts) -> ())) {
        let url = URL(string: "\(baseURL)api/getShortComments?short_id=\(short_id)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = .infinity
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            //    print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)

            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(GetCommentsShorts.self, from: fulldata)
                    completion(fullresponse)
                } catch {
                    print("Error decoding JSON:", error)
                }
            } else {
                print("Error in dataTask:", error!)
            }
        }.resume()
    }
    
    func getAllQuiz(page: String, user_attempted: Bool, completion: @escaping((QuizData)->())) {
        let url = URL(string: "\(baseURL)api/pk-quiz?page=\(page)&user_attempted=\(user_attempted)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            //        print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(QuizData.self, from: fulldata)
                    print("",fullresponse)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    func getQuizLeaderboard(quiz_id: Int, completion: @escaping((QuizLeaderboardResponse)->())) {
        let url = URL(string: "\(baseURL)api/pk-quiz/leaderboard?quiz_id=\(quiz_id)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            //        print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(QuizLeaderboardResponse.self, from: fulldata)
                    print("",fullresponse)
                    completion(fullresponse)
                } catch{
             
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    func submitAnswer(question_id : Int, quiz_id : Int, attempt_id: Int, option_id: Int, completionHandler: @escaping (QuizResponseSubmit) -> ()) {
        let httpStr = "quiz_id=\(quiz_id)&attempt_id=\(attempt_id)&question_id=\(question_id)&option_id=\(option_id)"
        makeAPICall(path: "pk-quiz/submit-answer", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: QuizResponseSubmit.self) { responseObj in
            if responseObj.CODE == 200 {
                completionHandler(responseObj)
            }
        } failure: { }
    }
    
    func EndQuiz(quiz_id : Int, attempt_id: Int, completionHandler: @escaping (QuizEndResponse) -> ()) {
        let httpStr = "quiz_id=\(quiz_id)&attempt_id=\(attempt_id)"
        makeAPICall(path: "pk-quiz/end", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: QuizEndResponse.self) { responseObj in
            if responseObj.CODE == 200 {
                completionHandler(responseObj)
            }
        } failure: { }
    }
    
    
    func startQuiz(quiz_id: String, completion: @escaping((QuizResponse) ->())) {
        let url = URL(string: "\(baseURL)api/pk-quiz/start")!
        let httpStr = "quiz_id=\(quiz_id)"
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = httpStr.data(using: .utf8)
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
        if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(QuizResponse.self, from: fulldata)
                    print("",fullresponse)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    func likeOrUnlikeComment(_ commentId: String, isLike: Bool, completion: @escaping ()->()) {
        let subPath = (isLike == true ?  "dislikecomment" : "likecomment")
        let httpStr = "comment_id=\(commentId)&like_type=heart"
        makeAPICall(path: "news/\(subPath)", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: DefaultAPIResponse.self) { responseObj in
            if responseObj.CODE == 200 {
                completion()
            }
        } failure: {}
    }
    
    func postComment(comment: String, parentId: String, newsId: String, completion: @escaping ()->()) {
        let httpStr = "user_comment=\(comment)&parent_id=\(parentId)&news_id=\(newsId)"
        makeAPICall(path: "news/addcomment", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: DefaultAPIResponse.self) { responseObj in
            if responseObj.CODE == 200 {
                completion()
            }
        } failure: {}
    }
    
    func deleteComment(commentId: String, newsId: String, completion: @escaping ()->()) {
        let httpStr = "id=\(commentId)"
        makeAPICall(path: "news/deletecomment", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: DefaultAPIResponse.self) { responseObj in
            if responseObj.CODE == 200 {
                completion()
            }
        } failure: {}
    }
    
    func postBookmark(newsId: String, completion: @escaping ()->()) {
        let httpStr = "news_id=\(newsId)"
        makeAPICall(path: "news/bookmarknews", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: DefaultAPIResponse.self) { responseObj in
            completion()
        } failure: {
        }
    }
    
    func getLeaderboard(completionHandler: @escaping (LeaderboardResult) -> ()) {
        makeAPICall(path: "leaderboard", httpMethod: .GET, queryItems: nil, httpBody: nil, decodableClassType: LeaderboardResponse.self) { responseObj in
            completionHandler(responseObj.RESULT)
        } failure: {
            DispatchQueue.main.async {
             //   self.hideLoadingIndicator()
            }
        }
    }
    func getLeaderboardRanking(completionHandler: @escaping (LeaderboardResult) -> ()) {
         makeAPICall(path: "leaderboard/ranking", httpMethod: .GET, queryItems: nil, httpBody: nil, decodableClassType: LeaderboardResponse.self) { responseObj in
             print("",responseObj)
            completionHandler(responseObj.RESULT)
        } failure: {
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
        }
    }
    func getLeaderboardStateRanking(completionHandler: @escaping (LeaderboardResult) -> ()) {
        makeAPICall(path: "leaderboard/stateRanking", httpMethod: .GET, queryItems: nil, httpBody: nil, decodableClassType: LeaderboardResponse.self) { responseObj in
            completionHandler(responseObj.RESULT)
        } failure: {
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
        }
    }
    
    func getLeaderboardDistrictRanking(completionHandler: @escaping (LeaderboardResult) -> ()) {
        makeAPICall(path: "leaderboard/districtRanking", httpMethod: .GET, queryItems: nil, httpBody: nil, decodableClassType: LeaderboardResponse.self) { responseObj in
            completionHandler(responseObj.RESULT)
        } failure: {
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
        }
    }

    func sendAppVersion(user_id: String,completion: @escaping((AppVersion)->())){
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let url = URL(string: "\(baseURL)api/App_Version?user_id=\(user_id)&version_code=\(appVersion ?? "")&platform=1")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(AppVersion.self, from: fulldata)

                        completion(fullresponse)

                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    
    func getProfile(completion: @escaping (UserProfile)->()) {
        makeAPICall(path: "Profile", httpMethod: .GET, queryItems: nil, httpBody: nil, decodableClassType: GetProfileResponse.self) { responseObj in
            if let result = responseObj.RESULT {
                print("",result)
                completion(result)
            } else {
                DispatchQueue.main.async { self.goToWelcomeView() }
            }
        } failure: {
        }
    }
    
    func updateProfile(_ editedProfile: UpdatedProfile, completion: @escaping (UserProfile?)->()) {
        var httpString: String?
        if let dict = editedProfile.dictionary {
            dict.forEach { (key, value) in
                if httpString != nil {
                    httpString!.append("&\(key)=\(value)")
                } else {
                    httpString = ("\(key)=\(value)")
                }
            }
        }
        makeAPICall(path: "Profile", httpMethod: .POST, queryItems: nil, httpBody: httpString, decodableClassType: GetProfileResponse.self) { responseObj in
            completion(responseObj.RESULT)
        } failure: {
        }
    }
    
    func getLevelData(completionHandler: @escaping ([Level], _ points: String) -> ()) {
        makeAPICall(path: "level", httpMethod: .GET, queryItems: nil, httpBody: nil, decodableClassType: LevelResponse.self) { responseObj in
            //EDIT SHUBHENDU
            // completionHandler(responseObj.levels, responseObj.total_wallet_amount)
            completionHandler(responseObj.levels, responseObj.total_wallet_amount ?? "")
        } failure: {
        }
    }
    
    func getRewardTransactions(completionHandler: @escaping ([RewardTransaction]) -> ()) {
        let queryItems = [URLQueryItem(name: "lang", value: appLang)]
        makeAPICall(path: "wallet_logs", httpMethod: .GET, queryItems: queryItems, httpBody: nil, decodableClassType: RewardTransactionResponse.self) { responseObj in
            completionHandler(responseObj.RESULT)
        } failure: {
        }
    }
    
    func getBookmarkedItems(completionHandler: @escaping ([FeedItem]?,String?) -> ()) {
        makeAPICall(path: "news/bookmarklisting", httpMethod: .GET, queryItems: nil, httpBody: nil, decodableClassType: GetHomeData.self) { responseObj in
            if responseObj.CODE == 200 {
                completionHandler(responseObj.RESULT!,nil)
            } else if responseObj.CODE == 202 {
                completionHandler(nil, responseObj.MESSAGE)
            } else if responseObj.CODE == 401 {
                self.unauthorizedError()
            }
        } failure: {
        }
    }
    
    func getReferralList(completion: @escaping(([Referral])->())) {
        makeAPICall(path: "referal_user_list", httpMethod: .GET, queryItems: nil, httpBody: nil, decodableClassType: GetReferralListResponse.self) { responseObj in
            if let result = responseObj.RESULT {
                completion(result)
            } else {
                DispatchQueue.main.async { self.goToWelcomeView() }
            }
        } failure: {
        }
    }
    
    func getContactUsCategories(completion: @escaping([ContactUsCategory])->()){
        makeAPICall(path: "contactus", httpMethod: .GET, queryItems: nil, httpBody: nil, decodableClassType: GetContactUsResponse.self) { responseObj in
            completion(responseObj.RESULT!)
        } failure: {
        }
    }
    
    func postContactUsRequest(_ categoryId: String, description: String, completion: @escaping ()->()) {
        let httpStr = "category_id=\(categoryId)&contact_content=\(description)&lang=\(appLang)"
        makeAPICall(path: "contactus", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: DefaultAPIResponse.self) { responseObj in
            if responseObj.CODE == 200 { completion() }
        } failure: {
        }
    }
    
    func addEditProfilePicture(_ imageUrl: String, completion: @escaping(()->())) {
        let urlComponents = URLComponents(string: "\(baseURL)api/update_profile")!
        var request = URLRequest(url: urlComponents.url!)
        
        request.httpMethod = "PUT"
        request.allHTTPHeaderFields = httpHeaders
        request.httpBody = "profileImage=\(imageUrl)".data(using: .utf8)
        request.timeoutInterval = 20
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, error == nil  {
                do {
                    let responseObj = try JSONDecoder().decode(GetProfileResponse.self, from: data)
                    if responseObj.CODE == 200 { completion() }
                    
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    func getStateList(completion: @escaping(([State])->())){
        var urlComponents = URLComponents(string: "\(baseURL)api/getState")!
        urlComponents.queryItems = [URLQueryItem(name: "lang", value: appLang)]
        
        var request = URLRequest(url: urlComponents.url!)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, error == nil {
                do {
                    let responseObj = try JSONDecoder().decode(GetStateResponse.self, from: data)
                    completion(responseObj.data)
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    func getDistrictList(for stateId: String, completion: @escaping(([District])->())) {
        var urlComponents = URLComponents(string: "\(baseURL)api/getDistricts")!
        urlComponents.queryItems = [URLQueryItem(name: "state_id", value: stateId), URLQueryItem(name: "lang", value: appLang)]
        
        var request = URLRequest(url: urlComponents.url!)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, error == nil {
                do {
                    let responseObj = try JSONDecoder().decode(GetDistrictsResponse.self, from: data)
                    completion(responseObj.data)
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    func getACList(for districtID: String, completion: @escaping(([AC])->())) {
        var urlComponents = URLComponents(string: "\(baseURL)api/getac")!
        urlComponents.queryItems = [URLQueryItem(name: "district_id", value: districtID), URLQueryItem(name: "lang", value: appLang)]
        
        var request = URLRequest(url: urlComponents.url!)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, error == nil {
                do {
                    let responseObj = try JSONDecoder().decode(GetACResponse.self, from: data)
                    completion(responseObj.data)
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    
    func getBlockList(for districtID: String, completion: @escaping(([BlockList])->())) {
        var urlComponents = URLComponents(string: "\(baseURL)api/getblock")!
        urlComponents.queryItems = [URLQueryItem(name: "district_id", value: districtID), URLQueryItem(name: "lang", value: appLang)]
        
        var request = URLRequest(url: urlComponents.url!)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, error == nil {
                do {
                    let responseObj = try JSONDecoder().decode(GetBlockResponse.self, from: data)
                    completion(responseObj.RESULT)
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    func getNewsLikesList(newsId: String, count:String, completionHandler: @escaping ([ClickUser]?) -> ()) {
        let httpStr = "news_id=\(newsId)&count=\(count)"
        makeAPICall(path: "news/getPeerGroup", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: GetAPeerGroupResponse.self) { responseObj in
            if responseObj.CODE == 200 {
                completionHandler(responseObj.RESULT)
            }
        } failure: { }
    }
    
    
    
    func getHomeGrid(completion: @escaping((HomeGridModel)->())) {
        let url = URL(string: "\(baseURL)api/home-grid")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(HomeGridModel.self, from: fulldata)
                    print("",fullresponse)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    
    func getHomeShorts(completion: @escaping((ShortsModel)->())) {
        let url = URL(string: "\(baseURL)api/shortsListing")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(ShortsModel.self, from: fulldata)
                    print("",fullresponse)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    func getNewsMedia(completion: @escaping((NewsResponse)->())) {
        let url = URL(string: "\(baseURL)api/news-media")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(NewsResponse.self, from: fulldata)
                    print("",fullresponse)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    
    
//    func updateFacebook(facebookId: String, fbUsername: String, completion: @escaping ()->()) {
//
//        let url = URL(string: "\(baseURL)api/update_profile")!
//        var request = URLRequest(url: url)
//
//        request.httpMethod = "PUT"
//        request.allHTTPHeaderFields = httpHeaders
//        request.httpBody = "facebookId=\(facebookId)&fbUsername=\(fbUsername)".data(using: .utf8)
//
//        let session = URLSession.shared
//        session.dataTask(with: request) { (data, response, error) in
//            if let data = data {
//                if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
//                {
//                    completion()
//                }
//            }
//        }.resume()
//    }
    func updateFacebook(facebookId: String, fbUsername: String, completion: @escaping (([String: Any]))->()) {
        
        let url = URL(string: "\(baseURL)api/update_profile")!
        var request = URLRequest(url: url)
        
        request.httpMethod = "PUT"
        request.allHTTPHeaderFields = httpHeaders
        request.httpBody = "facebookId=\(facebookId)&fbUsername=\(fbUsername)".data(using: .utf8)
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data {
                if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                {
                    completion(json)
                }
            }
        }.resume()
    }

    func updateTwitter(id: String, Username: String, completion: @escaping (([String: Any]))->()) {
        
        let url = URL(string: "\(baseURL)api/update_profile")!
        var request = URLRequest(url: url)
        
        request.httpMethod = "PUT"
        request.allHTTPHeaderFields = httpHeaders
        request.httpBody = "twitterId=\(id)&twitterUsername=\(Username)".data(using: .utf8)
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let data = data {
                if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                {
                    completion(json)
                }
            }
        }.resume()
    }
    func getAllMessagePK(completion: @escaping((MessageModel)->())) {
        let url = URL(string: "\(baseURL)api/getSpecialVideoListing?lang=en")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            //        print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(MessageModel.self, from: fulldata)
                    print("",fullresponse)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    func getAllCompletedTasks(date: String, count: String, completion: @escaping((AllTasksModel)->())) {
        let url = URL(string: "\(baseURL)api/task/completedTasks?start_date=\(date)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 60
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            //        print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(AllTasksModel.self, from: fulldata)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    func getAllPendingTasks(date: String, count: String, completion: @escaping((AllTasksModel)->())) {
        let url = URL(string: "\(baseURL)api/task/pendingTasks?start_date=\(date)&count=\(count)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 60
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            //        print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(AllTasksModel.self, from: fulldata)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            }else{
                print("error: ", error!)
            }
        }.resume()
    }
    func getAllTasks(date: String, count: String, completion: @escaping((AllTasksModel)->())) {
        let url = URL(string: "\(baseURL)api/task?start_date=\(date)&count=\(count)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            //        print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(AllTasksModel.self, from: fulldata)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    //-- Gaurav --//
    
    func getBannerSurvey(completion: @escaping((CampaignSurveyModel)->())) {
        let url = URL(string: "\(baseURL)api/campaignreferal_surver")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(CampaignSurveyModel.self, from: fulldata)
                    print("",fullresponse)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    func updateSurvey(qn_id:String , op_id: String,completion: @escaping((BannerOptionModel)->())) {
        guard let url = URL(string: "\(baseURL)api/cruseroptions") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = "qn_id=\(qn_id)&op_id=\(op_id)".data(using: .utf8)
       // "qn_id=\(shortId)".data(using: .utf8)
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(BannerOptionModel.self, from: fulldata)
                    DispatchQueue.main.async {
                        self.hideLoadingIndicator()
                    }
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    
    func referalContacts(name:String , mobile: String,completion: @escaping((BannerOptionModel)->())) {
        guard let url = URL(string: "\(baseURL)api/referalcontacts") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = "name=\(name)&mobile=\(mobile)".data(using: .utf8)
       // "qn_id=\(shortId)".data(using: .utf8)
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(BannerOptionModel.self, from: fulldata)
                    DispatchQueue.main.async {
                        self.hideLoadingIndicator()
                    }
                    completion(fullresponse)
                } catch{
                    print("model class issue: ", error.localizedDescription)
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    func removereferalContacts( mobile: String,completion: @escaping((BannerOptionModel)->())) {
        guard let url = URL(string: "\(baseURL)api/removeReferalContact") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = "mobile=\(mobile)".data(using: .utf8)
       // "qn_id=\(shortId)".data(using: .utf8)
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(BannerOptionModel.self, from: fulldata)
                    DispatchQueue.main.async {
                        self.hideLoadingIndicator()
                    }
                    completion(fullresponse)
                } catch{
                    print("model class issue: ", error.localizedDescription)
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    
    // Amit
    
    func Ranking1(filterBy: String, dateRange: String, completion: @escaping((LeaderBoardModel)->())) {
        let url = URL(string: "\(baseURL)api/leaderboard/ranking_v2?filter_by=\(filterBy)&date_range=\(dateRange)")!
        print("Request URL: \(url)")

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            //        print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(LeaderBoardModel.self, from: fulldata)
                    print("",fullresponse)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    func downloadImageFrom(urlString: String, completion: @escaping ((UIImage)->())) {
        if let url = URL(string: urlString) {
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                if let image = UIImage(data: data) {
                    completion(image)
                }
            }
            task.resume()
        }
    }
    
}

//MARK: Feed Media Set Up
extension BaseViewController {
    
    func loadImage(url: URL, completion: @escaping (Data?, Error?) -> Void) {
        // Compute a path to the URL in the cache
        let fileCachePath = FileManager.default.temporaryDirectory
            .appendingPathComponent(url.lastPathComponent, isDirectory: false)
        
        // If the image exists in the cache, load the image from the cache and exit
        do {
            let data = try Data(contentsOf: fileCachePath)
            completion(data, nil)
            return
        } catch {}
        
        // If the image does not exist in the cache, download the image to the cache
        downloadImageFrom(url: url, toFile: fileCachePath) { (error) in
            do {
                let data = try Data(contentsOf: fileCachePath)
                completion(data, error)
            } catch {}
        }
    }
    
    func downloadImageFrom(url: URL, toFile file: URL, completion: @escaping (Error?) -> Void) {
        // Download the remote URL to a file
        let task = URLSession.shared.downloadTask(with: url) {
            (tempURL, response, error) in
            guard let tempURL = tempURL else {
                completion(error)
                return
            }
            
            do {
                // Remove any existing document at file
                if FileManager.default.fileExists(atPath: file.path) {
                    try FileManager.default.removeItem(at: file)
                }
                
                // Copy the tempURL to file
                try FileManager.default.copyItem(at: tempURL, to: file)
                completion(nil)
            } catch {}
        }
        task.resume()
    }
    
    func getchannelDetails(channel_id:String,completion: @escaping((ChannelDetails)->())) {
        let url = URL(string: "\(baseURL)api/getShortProfileDetail?channel_id=\(channel_id)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            //        print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(ChannelDetails.self, from: fulldata)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    func likeshorts(shortId:String = "",completion: @escaping((AllTasksModel)->())) {
        guard let url = URL(string: "\(baseURL)api/createShortLike") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = "short_id=\(shortId)".data(using: .utf8)
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(AllTasksModel.self, from: fulldata)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    
    func deletelikeshorts(shortId:String = "",completion: @escaping((AllTasksModel)->())) {
        guard let url = URL(string: "\(baseURL)api/deleteShortLike") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = "short_id=\(shortId)".data(using: .utf8)
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(AllTasksModel.self, from: fulldata)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    func addSubscriptionStatus(channel_id:String = "",completion: @escaping((GetStateResponse)->())) {
        guard let url = URL(string: "\(baseURL)api/subsribeProfile") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = "channel_id=\(channel_id)".data(using: .utf8)
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(GetStateResponse.self, from: fulldata)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    
    
    
    // Learning Module APIS
    func getModules(completion: @escaping ([Module])->()) {
        showLoadingIndicator(in: view)
        makeAPICall(path: "getmodules", httpMethod: .POST, queryItems: nil, httpBody: nil, decodableClassType: ModuleData.self) { responseObj in
            print(responseObj)
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            completion(responseObj.data)
        } failure: { }
    }
    
    func getModulesVideos(module_id: String,completion: @escaping (VideoData)->()) {
        showLoadingIndicator(in: view)
        let body = "module_id=\(module_id)"
        makeAPICall(path: "getVideos", httpMethod: .POST, queryItems: nil, httpBody: body, decodableClassType: VideoData.self) { responseObj in
            print(responseObj)
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            completion(responseObj)
        } failure: { }
    }
    
    func updateWatchVideo(module_id: String, video_id: String,completion: @escaping (GetStateResponse)->()) {
        showLoadingIndicator(in: view)
        let body = "module_id=\(module_id)&video_id=\(video_id)"
        makeAPICall(path: "updateVideoWatch", httpMethod: .POST, queryItems: nil, httpBody: body, decodableClassType: GetStateResponse.self) { responseObj in
            print(responseObj)
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            completion(responseObj)
        } failure: { }
    }
    
    func getModuleQuestions(module_id:String = "",completion: @escaping((QuizDataLearn)->())) {
       
        guard let url = URL(string: "\(baseURL)api/getquiz") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = "module_id=\(module_id)".data(using: .utf8)
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(QuizDataLearn.self, from: fulldata)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    
    func submitLearnQ(module_id: String, responses: [[String: String]],completion: @escaping (GetStateResponse)->()) {
        showLoadingIndicator(in: view)
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: responses, options: [])
            
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                print("JSON String: \(jsonString)")
                let body = "module_id=\(module_id)&responses=\(jsonString)"
                makeAPICall(path: "submitQuizResponse", httpMethod: .POST, queryItems: nil, httpBody: body, decodableClassType: GetStateResponse.self) { responseObj in
                    print(responseObj)
                    DispatchQueue.main.async {
                        self.hideLoadingIndicator()
                    }
                    completion(responseObj)
                } failure: { }
            }
        } catch {
            print("Error: \(error)")
        }
       
    }
    
    func completeModule(module_id: String,completion: @escaping (GetStateResponse)->()) {
        showLoadingIndicator(in: view)
        let body = "module_id=\(module_id)"
        makeAPICall(path: "updateModCompletion", httpMethod: .POST, queryItems: nil, httpBody: body, decodableClassType: GetStateResponse.self) { responseObj in
            print(responseObj)
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            completion(responseObj)
        } failure: { }
    }
    
    func verifyOTPWhatsapp( _ mobileNo: String, completionHandler: @escaping (_ verifiedUser: VerifiedUser?) -> ()) {
        let httpStr = "mobile_no=\(mobileNo)&otp=&device_token=&login_type=WL"
        makeAPICall(path: "verifyOtp", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: VerifyOTPResponse.self) { responseObj in
            if responseObj.status == "200" {
                completionHandler(responseObj.data)
            } else {
                completionHandler(nil)
            }
        } failure: {
            
        }
    }
    
    func whatsappLogin(token: String,completion: @escaping (UserWhatsapp)->()) {
        showLoadingIndicator(in: view)
        guard let url = URL(string: "https://auth.otpless.app/auth/userInfo") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let bodyString = "token=\(token)&client_id=idsep1h1&client_secret=s4fvussq34d1jo2b"
        request.httpBody = bodyString.data(using: .utf8)
        print(bodyString)
        let headers = ["Content-Type" : "application/x-www-form-urlencoded"]
        request.allHTTPHeaderFields = headers
        request.timeoutInterval = 20
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(UserWhatsapp.self, from: fulldata)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
}


extension BaseViewController {
    
    func getTwittwerAuth(completion: @escaping((TwitterModel)->())) {
        let url = URL(string: "\(baseURL)api/twitter_authorize")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            //        print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(TwitterModel.self, from: fulldata)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    func getTwittwerCallack(code:String, completion: @escaping((TwitterCallack)->())) {
        let url = URL(string: "\(baseURL)api/twitter_callback?code=\(code)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 20
        let session = URLSession.shared
        showLoadingIndicator(in: view)
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
            //        print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
            
            if let fulldata = data, error == nil {
                do {
                    let fullresponse = try JSONDecoder().decode(TwitterCallack.self, from: fulldata)
                    completion(fullresponse)
                } catch{
                    print("model class issue: ")
                }
            } else {
                print("error: ", error!)
            }
        } .resume()
    }
    
    func CreateTwitter(access_token:String, task_id:String, completion: @escaping ()->()) {
        let httpStr = "twitter_bearer_token=\(access_token)&task_id=\(task_id)"
        makeAPICall(path: "create_tweet", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: DefaultAPIResponse.self) { responseObj in
            if responseObj.CODE == 200 {
                print(responseObj)
                completion()
            }
        } failure: {
        }
    }
    
    func TaskSumitTwitter(task_id:String, completion: @escaping ()->()) {
        let httpStr = "task_id=\(task_id)"
        makeAPICall(path: "task", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: DefaultAPIResponse.self) { responseObj in
            if responseObj.CODE == 200 {
                print(responseObj)
                completion()
            }
        } failure: {
        }
    }
    
    func LikeTwitter(access_token:String, task_id:String, completion: @escaping ()->()) {
        let httpStr = "twitter_bearer_token=\(access_token)&task_id=\(task_id)"
        makeAPICall(path: "tweet_like", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: DefaultAPIResponse.self) { responseObj in
            if responseObj.CODE == 200 {
                print(responseObj)
                completion()
            }
        } failure: {
        }
    }
    
    func CreateTwitterRetweet(access_token:String, task_id:String, completion: @escaping ()->()) {
        let httpStr = "twitter_bearer_token=\(access_token)&task_id=\(task_id)"
        makeAPICall(path: "create_retweet", httpMethod: .POST, queryItems: nil, httpBody: httpStr, decodableClassType: DefaultAPIResponse.self) { responseObj in
            if responseObj.CODE == 200 {
                print(responseObj)
                completion()
            }
        } failure: {
        }
    }
}
