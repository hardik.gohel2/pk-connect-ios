//
//  ApiCall.swift
//  blipearth
//
//  Created by Govind Kumawat on 09/07/21.
//

import Foundation

final class ApiCall {
    
    static let shared: ApiCall = ApiCall()
    
    private init() {}
    
    public func downloadImageData(with url: URL, completionHandler: @escaping (Data?) -> ()) {
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard error == nil
            else {
                print("\(#function) ===> \(url) ", error ?? "")
                completionHandler(nil)
                return
            }
            
            guard let data = data
            else {
                print("\(#function) ===> No data found.")
                completionHandler(nil)
                return
            }
            
            completionHandler(data)
            
        }.resume()
        
    }
}
