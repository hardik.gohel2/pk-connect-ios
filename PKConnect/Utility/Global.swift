

import UIKit
import Foundation
import MapKit
import MobileCoreServices
import FirebaseDynamicLinks


// Make updates for compact font sizes

public let appStoreURL :String = "https://apps.apple.com/us/app/pk-connect/id6448717716"


private var spinnerView: UIView!



//#if DEBUG
//    //Do Nothing
//#else
//    func print(_ items: Any...){
//        //nothing will print in production mode
//    }
//#endif

public typealias KeyValues<Type> = [String:Type]

// define app name
public let AppName = "PKConnect"

// define application version
public let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""

// define device type
public let deviceType = "2" //1- Android,2-iPhone

// define device os version
public let deviceOS = UIDevice.current.systemVersion



// define device unique id
public let deviceId = UIDevice.current.identifierForVendor!.uuidString


// define device Locale
public let deviceLocale = Locale.current

// define device countryCode
public let deviceRegionCode = deviceLocale.regionCode ?? ""

// define device country name
public let deviceRegionName = (deviceLocale as NSLocale).displayName(forKey: NSLocale.Key.countryCode, value: deviceRegionCode) ?? ""

// define device current language
public let deviceLang = Locale.preferredLanguages.first ?? ""

// define device current timezone
public let deviceTimeZone = TimeZone.current.localizedName(for: .standard, locale: .current) ?? ""

// define device current timezone
public let deviceGMTTime = TimeZone.current.localizedName(for: .shortStandard, locale: .current) ?? ""


// define static colors
public let appYelloColor : UIColor = UIColor(red: CGFloat(255.0 / 255.0), green: CGFloat(206.0 / 255.0), blue: CGFloat(26.0 / 255.0), alpha: CGFloat(1))


let screenBounds = UIScreen.main.bounds
let screenWidth = screenBounds.width
let screenHeight = screenBounds.height


public protocol JSONValues {
    static var defaultValue : Self { get }
}

extension Int: JSONValues{
    public static var defaultValue: Int{
        return 0
    }
}

extension String: JSONValues{
    public static var defaultValue: String{
        return ""
    }
}

extension String {
    func removeSpace() -> String {
        return self.replacingOccurrences(of: " ", with: "-")
    }
}
extension Date {
    static var currentTimeStamp: String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy_MM_dd_hh_mm_ss"
        return (formatter.string(from: Date()) as NSString) as String
    }
}

extension Dictionary{
    
    public func string(_ key: Key,_ defaultValue: String = "") -> String {
        if self[key] == nil || self[key] is NSNull {
            return ""
        }
        return String(describing: self[key]!)
    }
    
    public func bool(_ key: Key,_ defaultValue: Bool = false) -> Bool {
        if self[key] == nil || self[key] is NSNull {
            return false
        }
        return (self[key]! as! Bool)
    }
    
    public func get<T:JSONValues>(_ key: Key) -> T {
        return self[key] as? T ?? T.defaultValue
    }
    
    public func getValue(_ key: Key) -> String {
        return self[key] as? String ?? ""
    }
    
    func nullKeyRemoval() -> [AnyHashable: Any] {
        var dict: [AnyHashable: Any] = self
        
        let keysToRemove = dict.keys.filter { dict[$0] is NSNull }
        let keysToCheck = dict.keys.filter({ dict[$0] is Dictionary })
        for key in keysToRemove {
            dict.removeValue(forKey: key)
        }
        for key in keysToCheck {
            if let valueDict = dict[key] as? [AnyHashable: Any] {
                dict.updateValue(valueDict.nullKeyRemoval(), forKey: key)
            }
        }
        return dict
    }
}


//public var reachability:Reachability?

func debugPrint(items: Any..., separator: String = " ", terminator: String = "\n") {}

func print(items: Any..., separator: String = " ", terminator: String = "\n") {}


public func openMopFromSelectedCoordinate (latitude:Double,longiture:Double)
{
    let lat = latitude
    let long = longiture
    
    let googleMapsInstalled = UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)
    if googleMapsInstalled
    {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string:"comgooglemapsurl://maps.google.com/maps?f=d&daddr=\(lat ),\(long )&sll=35.6586,139.7454&sspn=0.2,0.1&nav=1&x-source=DriverApp&x-success=driverapp://?resume=true")!)
        } else {
            // Fallback on earlier versions
            UIApplication.shared.openURL(URL(string: "comgooglemapsurl://maps.google.com/maps?f=d&daddr=\(lat ),\(long )&sll=35.6586,139.7454&sspn=0.2,0.1&nav=1&x-source=DriverApp&x-success=driverapp://?resume=true")!)
        }
        
    }else{
        let coordinates = CLLocationCoordinate2DMake(lat, long)
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.openInMaps(launchOptions: nil)
    }
    
}


public func openMopFromSelectedCoordinateWithSource (Source_latitude:Double,Source_longiture:Double,Destination_latitude:Double,Destination_longiture:Double)
{
    
    let s_lat = Source_latitude
    let s_long = Source_longiture
    
    let d_lat = Destination_latitude
    let d_long = Destination_longiture
    
    let googleMapsInstalled = UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)
    if googleMapsInstalled
    {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string:"comgooglemapsurl://maps.google.com/maps?f=d&saddr=\(s_lat ),\(s_long )&daddr=\(d_lat ),\(d_long )&sll=35.6586,139.7454&sspn=0.2,0.1&nav=1&x-source=DriverApp&x-success=driverapp://?resume=true")!)
        } else {
            // Fallback on earlier versions
            UIApplication.shared.openURL(URL(string: "comgooglemapsurl://maps.google.com/maps?f=d&saddr=\(s_lat ),\(s_long )&daddr=\(d_lat ),\(d_long )&sll=35.6586,139.7454&sspn=0.2,0.1&nav=1&x-source=DriverApp&x-success=driverapp://?resume=true")!)
        }
        
    }else{
        let coordinates = CLLocationCoordinate2DMake(d_lat, d_long)
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.openInMaps(launchOptions: nil)
    }
    
}

func showLoadingIndicator(in container: UIView) {
    var spinnerFrame = container.bounds
    spinnerFrame.size.height += 20
    spinnerView = UIView.init(frame: spinnerFrame)
    spinnerView.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
    
    let spinner = UIActivityIndicatorView(style: .large)
    spinner.style = .large
    spinner.color = .pkConnectYellow
    spinner.startAnimating()
    spinner.center = spinnerView.center
    
    spinnerView.addSubview(spinner)
    container.addSubview(spinnerView)
}

func hideLoadingIndicator() {
    spinnerView.removeFromSuperview()
}

func filedownLoad(view:UIViewController, mediaFile:String,fileName:String) {

    let fileURL = URL(string: mediaFile)!
    let fileType : URLFileType = fileURL.fileType
    
    switch fileType {
    case .video:
        
        let fileExt = ".mp4"
        let videoFileName = fileName.removeSpace() + (fileExt)
        let tmpPathOriginal = NSTemporaryDirectory() as String
        let filePathOriginal = tmpPathOriginal + "/" + videoFileName
        
        showLoadingIndicator(in: view.view)
        
        DispatchQueue.global(qos: .background).async {
            var request = URLRequest(url: fileURL)
            request.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                
                DispatchQueue.main.async {
                    hideLoadingIndicator()
                }
                
                if(error != nil){
                    print("\n\nsome error occured\n\n")
                    return
                }
                if let response = response as? HTTPURLResponse{
                    if response.statusCode == 200{
                        DispatchQueue.main.async {
                            if let data = data{
                                if let _ = try? data.write(to: URL(fileURLWithPath: filePathOriginal), options: Data.WritingOptions.atomic){
                                    print("\n\nurl data written\n\n")
                                    UISaveVideoAtPathToSavedPhotosAlbum(filePathOriginal,nil,nil,nil)
                                    view.showToast(message: "Downloading completed.".localize())
                                }
                                else{
                                    print("\n\nerror again\n\n")
                                }
                            }//end if let data
                        }//end dispatch main
                    }//end if let response.status
                }
            })
            task.resume()
        }
        
        break
    case .image:
        
        showLoadingIndicator(in: view.view)
        
        ApiCall.shared.downloadImageData(with: fileURL) { imageData in
            DispatchQueue.main.async {
                hideLoadingIndicator()
            }
            
            guard let imageData = imageData,
                  let downloadedImage = UIImage(data: imageData)
            else {
                return
            }
            
            
            DispatchQueue.main.async {
                UIImageWriteToSavedPhotosAlbum(downloadedImage, nil, nil, nil)
                view.showToast(message: "Downloading completed.".localize())
            }
            
        }
        
        break
    default:
        break
    }
}

func filedownLoadAndShare(view: UIViewController, mediaFile: String, fileName: String, completion: @escaping (Bool, URL?) -> Void) {
    
    let fileURL = URL(string: mediaFile)!
    let fileType: URLFileType = fileURL.fileType
    
    switch fileType {
    case .video:
        
        let fileExt = ".mp4"
        let videoFileName = fileName.removeSpace() + fileExt
        let tmpPathOriginal = NSTemporaryDirectory() as String
        let filePathOriginal = tmpPathOriginal + "/" + videoFileName
        
        showLoadingIndicator(in: view.view)
        
        DispatchQueue.global(qos: .background).async {
            var request = URLRequest(url: fileURL)
            request.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                
                DispatchQueue.main.async {
                    hideLoadingIndicator()
                }
                
                if let error = error {
                    print("\n\nSome error occurred: \(error)\n\n")
                    completion(false, nil)
                    return
                }
                
                if let response = response as? HTTPURLResponse, response.statusCode == 200, let data = data {
                    if let _ = try? data.write(to: URL(fileURLWithPath: filePathOriginal), options: .atomic) {
                        
                        DispatchQueue.main.async {
                            print("Processed URL ==> \(filePathOriginal)")
                            
                            let localVideoPath = filePathOriginal
                            let videoURL = URL(fileURLWithPath: localVideoPath)
                            let string = "\(fileName)\n\nClick the link below to download the application\n\(appStoreURL)"
                            
                            let activityItems = [videoURL, OptionalTextActivityItemSource(text: string)]
                            
                            let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: [])
                            activityVC.completionWithItemsHandler = { [weak view] _, _, _, _ in
                                       
                                if let vc = view as? StoryFullViewController {
                                    vc.player?.play()
                                }
                                       
                                   }
                            view.present(activityVC, animated: true, completion: nil)
                            completion(true, videoURL)
                        }
                        
                    } else {
                        print("\n\nError while writing data\n\n")
                        completion(false, nil)
                    }
                }
            })
            task.resume()
        }
        
    case .image:
        
        showLoadingIndicator(in: view.view)
        
        ApiCall.shared.downloadImageData(with: fileURL) { imageData in
            DispatchQueue.main.async {
                hideLoadingIndicator()
            }
            
            guard let imageData = imageData else {
                completion(false, nil)
                return
            }
            
            let fileExt = ".jpg"
            let imageFileName = fileName.removeSpace() + fileExt
            let tmpPathOriginal = NSTemporaryDirectory() as String
            let filePathOriginal = tmpPathOriginal + "/" + imageFileName
            
            if let _ = try? imageData.write(to: URL(fileURLWithPath: filePathOriginal), options: .atomic) {
                
                DispatchQueue.main.async {
                    print("Processed URL ==> \(filePathOriginal)")
                    
                    let localImagePath = filePathOriginal
                    let imageURL = URL(fileURLWithPath: localImagePath)
                    let string = "\(fileName)\n\nClick the link below to download the application\n\(appStoreURL)"
                    
                    let activityItems = [imageURL, OptionalTextActivityItemSource(text: string)]
                    
                    let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: [])
                    
                    view.present(activityVC, animated: true, completion: nil)
                    completion(true, imageURL)
                }
                
            } else {
                print("\n\nError while writing image data\n\n")
                completion(false, nil)
            }
        }
        
    default:
        break
    }
}



func shareDeepLink(controller:UIViewController, quiz_id:Int, message:String, handler: @escaping () -> Void) {
            let shareLink = "https://pkconnect.page.link/?item_type=quiz&quiz_id=\(quiz_id)"
            let linkParameter = URL(string: shareLink)!
            guard let dynamiclink = DynamicLinkComponents.init(link: linkParameter, domainURIPrefix: "https://pkconnect.page.link") else {
                return
            }
            
            dynamiclink.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.pk.connect")
    dynamiclink.iOSParameters?.appStoreID = DynamicLinkParams.appStoreID
            dynamiclink.androidParameters = DynamicLinkAndroidParameters(packageName: DynamicLinkParams.packageName)
            dynamiclink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
            dynamiclink.socialMetaTagParameters?.title = DynamicLinkParams.packageName
            
            dynamiclink.shorten {[weak controller] (url, warnings, error) in
                if let url = url {
                    let shareText = "\(message)" + "\n\n\(url.absoluteString)"
                    DispatchQueue.main.async {
                        let activityVC = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
                        activityVC.popoverPresentationController?.sourceView = controller?.view
                        controller?.present(activityVC, animated: true, completion: nil)
                        handler()
                    }
                } else {
                    handler()
                }
            }
        }
func shareMediaforDeeplink(view:UIViewController, feedItem:FeedItem) {
    
    let fileURL = URL(string: feedItem.content_media_set?.first?.media_url ?? "")!
    let fileType : URLFileType = fileURL.fileType
    
    let createDeeplinkURL = "\(DynamicLinkParams.dynamicLinkDomain)?apn=\(DynamicLinkParams.packageName)&ibi=\(DynamicLinkParams.packageName)&isi=1530520823&link=https%3A%2F%2Fpkconnect.page.link%2F%3Fitem_type%3Dnews%26news_id%3D\(feedItem.news_id)%26referral_code%3D\("")&ofl=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3D\(DynamicLinkParams.packageName)"
    
    
    
    var shareText = "\(feedItem.news_title)\n\nRead more\n\n\(createDeeplinkURL)\n\nShared Via PK Connect"
    
    
    DynamicLinkComponents.shortenURL(URL(string: createDeeplinkURL)!, options: nil) { url, warnings, error in
        print("The short URL is: \(url?.absoluteString ?? "")")
        shareText = "\(feedItem.news_title)\n\nRead more\n\n\(url?.absoluteString ?? createDeeplinkURL)\n\nShared Via PK Connect"
        
        switch fileType {
        case .video:
            
            let fileExt = ".mp4"
            let videoFileName = feedItem.news_title.removeSpace() + (fileExt)
            let tmpPathOriginal = NSTemporaryDirectory() as String
            let filePathOriginal = tmpPathOriginal + "/" + videoFileName
            
            showLoadingIndicator(in: view.view)
            
            DispatchQueue.global(qos: .background).async {
                var request = URLRequest(url: fileURL)
                request.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                    
                    DispatchQueue.main.async {
                        hideLoadingIndicator()
                    }
                    
                    if(error != nil){
                        print("\n\nsome error occured\n\n")
                        return
                    }
                    if let response = response as? HTTPURLResponse{
                        if response.statusCode == 200{
                            DispatchQueue.main.async {
                                if let data = data{
                                    if let _ = try? data.write(to: URL(fileURLWithPath: filePathOriginal), options: Data.WritingOptions.atomic){
                                        
                                        DispatchQueue.main.async {
                                            //                    let shareText: String = "Share post"
                                            print("processedUrl ==> \(filePathOriginal)")
                                            
                                            let localVideoPath = filePathOriginal
                                            let videoURL = URL(fileURLWithPath: localVideoPath)
                                            let string = shareText
                                            
                                            let activityItems = [videoURL, string]
                                            
                                            let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: [])
                                            //                    activityVC.excludedActivityTypes = [.print, .postToWeibo, .copyToPasteboard, .addToReadingList, .postToVimeo]
                                            view.present(activityVC, animated: true, completion: nil)
                                        }
                                        
                                    }
                                    else{
                                        print("\n\nerror again\n\n")
                                    }
                                }//end if let data
                            }//end dispatch main
                        }//end if let response.status
                    }
                })
                task.resume()
            }
            
            break
        case .image:
            
            showLoadingIndicator(in: view.view)
            
            ApiCall.shared.downloadImageData(with: fileURL) { imageData in
                DispatchQueue.main.async {
                    hideLoadingIndicator()
                }
                
                guard let imageData = imageData
                else {
                    return
                }
                
                let fileExt = ".jpg"
                let imageFileName = feedItem.news_title.removeSpace() + (fileExt)
                let tmpPathOriginal = NSTemporaryDirectory() as String
                let filePathOriginal = tmpPathOriginal + "/" + imageFileName
                
                if let _ = try? imageData.write(to: URL(fileURLWithPath: filePathOriginal), options: Data.WritingOptions.atomic){
                    
                    DispatchQueue.main.async {
                        print("processedUrl ==> \(filePathOriginal)")
                        
                        let localImagePath = filePathOriginal
                        let imageURL = URL(fileURLWithPath: localImagePath)
                        let string = shareText
                        
                        let activityItems = [imageURL, string]
                        
                        let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: [])
                        //                    activityVC.excludedActivityTypes = [.print, .postToWeibo, .copyToPasteboard, .addToReadingList, .postToVimeo]
                        view.present(activityVC, animated: true, completion: nil)
                    }
                    
                }
                else{
                    print("\n\nerror again\n\n")
                }
                
            }
            
            break
        default:
            break
        }
        
    }
    
    
}

func shareMediaforDeeplinkTask(view:UIViewController, TaskResult:TaskResult) {
    if TaskResult.action?.lowercased() == "video" || TaskResult.action?.lowercased() == "image" {
        let fileURL = URL(string: TaskResult.task_media_set?.first?.media_url ?? "")!
        let fileType : URLFileType = fileURL.fileType
        
        
        
        let createDeeplinkURL = "\(DynamicLinkParams.dynamicLinkDomain)?apn=\(DynamicLinkParams.packageName)&ibi=\(DynamicLinkParams.packageName)&isi=1530520823&link=https%3A%2F%2Fpkconnect.page.link%2F%3Fitem_type%3Dnews%26news_id%3D\(TaskResult.task_id ?? "")%26referral_code%3D\("")&ofl=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3D\(DynamicLinkParams.packageName)"
    

        
        
        var shareText = "\(TaskResult.task_title)\n\nRead more\n\n\(createDeeplinkURL)\n\nShared Via PK Connect"
        
        var newUrl = URL(string: createDeeplinkURL)!
        DynamicLinkComponents.shortenURL(newUrl, options: nil){ url,warning,Error  in
            
            if let url = url{
//                shareText = "Perform tasks to earn reward points\n\n\(TaskResult.task_title)\n\n\(url.absoluteString)\n\nShared Via Didir Doot"
                shareText = "Perform tasks to earn reward points".localize() + "\n\n\(TaskResult.task_title)\n\n\(url.absoluteString)\n\n" + "Shared Via PK Connect".localize()

                switch fileType {
                case .video:
                    showLoadingIndicator(in: view.view)

                    let fileExt = ".mp4"
                    let videoFileName = TaskResult.task_title.removeSpace() + (fileExt)
                    let tmpPathOriginal = NSTemporaryDirectory() as String
                    let filePathOriginal = tmpPathOriginal + "/" + videoFileName
                    
                    
                    DispatchQueue.global(qos: .background).async {
                        var request = URLRequest(url: fileURL)
                        request.httpMethod = "GET"
                        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                            
                            DispatchQueue.main.async {
                            }
                            
                            if(error != nil){
                                print("\n\nsome error occured\n\n")
                                return
                            }
                            if let response = response as? HTTPURLResponse{
                                if response.statusCode == 200{
                                    DispatchQueue.main.async {
                                        if let data = data{
                                            if let _ = try? data.write(to: URL(fileURLWithPath: filePathOriginal), options: Data.WritingOptions.atomic){
                                                
                                                DispatchQueue.main.async {
                                                    //                    let shareText: String = "Share post"
                                                    print("processedUrl ==> \(filePathOriginal)")
                                                    hideLoadingIndicator()

                                                    let localVideoPath = filePathOriginal
                                                    let videoURL = URL(fileURLWithPath: localVideoPath)
                                                    let string = shareText
                                                    
                                                    let activityItems = [videoURL, string]
                                                    
                                                    let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: [])
                                                    //                    activityVC.excludedActivityTypes = [.print, .postToWeibo, .copyToPasteboard, .addToReadingList, .postToVimeo]
                                                    view.present(activityVC, animated: true, completion: nil)
                                                }
                                                
                                            }
                                            else{
                                                hideLoadingIndicator()
                                                print("\n\nerror again\n\n")
                                            }
                                        }//end if let data
                                    }//end dispatch main
                                }//end if let response.status
                            }
                        })
                        task.resume()
                    }
                    
                    break
                case .image:
                    
                    showLoadingIndicator(in: view.view)
                    
                    ApiCall.shared.downloadImageData(with: fileURL) { imageData in
                        DispatchQueue.main.async {
                        }
                        
                        guard let imageData = imageData
                        else {
                            return
                        }
                        
                        let fileExt = ".jpg"
                        let imageFileName = TaskResult.task_title.removeSpace() + (fileExt)
                        let tmpPathOriginal = NSTemporaryDirectory() as String
                        let filePathOriginal = tmpPathOriginal + "/" + imageFileName
                        
                        if let _ = try? imageData.write(to: URL(fileURLWithPath: filePathOriginal), options: Data.WritingOptions.atomic){
                            
                            DispatchQueue.main.async {
                                print("processedUrl ==> \(filePathOriginal)")
                                hideLoadingIndicator()

                                let localImagePath = filePathOriginal
                                let imageURL = URL(fileURLWithPath: localImagePath)
                                let string = shareText
                                
                                let activityItems = [imageURL, string]
                                
                                let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: [])
                                //                    activityVC.excludedActivityTypes = [.print, .postToWeibo, .copyToPasteboard, .addToReadingList, .postToVimeo]
                                view.present(activityVC, animated: true, completion: nil)
                            }
                            
                        }
                        else{
                            DispatchQueue.main.async {
                                hideLoadingIndicator()
                            }
                            print("\n\nerror again\n\n")
                        }
                        
                    }
                    
                    break
                default:
                    break
                }
            }else if Error != nil{
                print(Error)
                DispatchQueue.main.async {
                    hideLoadingIndicator()
                }
            }else{
                return
            }
        }
    }else{
        showLoadingIndicator(in: view.view)
            
        
        let createDeeplinkURL = "\(DynamicLinkParams.dynamicLinkDomain)?apn=\(DynamicLinkParams.packageName)&ibi=\(DynamicLinkParams.packageName)&isi=1530520823&link=https%3A%2F%2Fpkconnect.page.link%2F%3Fitem_type%3Dnews%26news_id%3D\(TaskResult.task_id ?? "")%26referral_code%3D\("")&ofl=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3D\(DynamicLinkParams.packageName)"
    
        var shareText = "\(TaskResult.task_title)\n\nRead more\n\n\(createDeeplinkURL)\n\nShared Via PK Connect"
        
        var newUrl = URL(string: createDeeplinkURL)!
        DynamicLinkComponents.shortenURL(newUrl, options: nil){ url,warning,Error  in
            
            if let url = url{
                shareText = "Perform tasks to earn reward points".localize() + "\n\n\(TaskResult.task_title)\n\n\(url.absoluteString)\n\n" + "Shared Via PK Connect".localize()
                
                DispatchQueue.main.async {
                    let string = shareText
                    hideLoadingIndicator()

                    let activityItems = [string]
                    
                    let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: [])
                    view.present(activityVC, animated: true, completion: nil)
                }
                
                
            }else if Error != nil{
                print(Error)
                DispatchQueue.main.async {
                    hideLoadingIndicator()
                }
            }else{
                return
            }
        }
    }
    
    
}

public enum URLFileType: Int {
    case image = 0
    case video = 1
    case none = 2
    
    public var fileExtenstion: String {
        switch self {
        case .image:
            return "jpeg"
        case .video:
            return "mp4"
        case .none:
            return ""
        }
    }
}


extension URL {
    func mimeType() -> String {
        let pathExtension = self.pathExtension
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
    var fileType: URLFileType{
        let mimeType = self.mimeType()
        guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mimeType as CFString, nil)?.takeRetainedValue() else {
            return .none
        }
        if UTTypeConformsTo(uti, kUTTypeImage){
            return .image
        }
        else if UTTypeConformsTo(uti, kUTTypeMovie){
            return .video
        }
        else {
            return .none
        }
    }
}


extension UIViewController {
    
    func showToast(message : String) {
        let alertDisapperTimeInSeconds = 1.5
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        self.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + alertDisapperTimeInSeconds) {
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
}


class OptionalTextActivityItemSource: NSObject, UIActivityItemSource {
    let text: String
    
    init(text: String) {
        self.text = text
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return text
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        if activityType?.rawValue == "net.whatsapp.WhatsApp.ShareExtension" {
            return nil
        } else {
            return text
        }
    }
}
