//
//  DashboardViewController.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/2/22.
//
//


import UIKit

class DashboardViewController: BaseViewController {
    
    @IBOutlet weak var Tableview: UITableView!
    @IBOutlet weak var image_rank2: UIImageView!
    @IBOutlet weak var img_rank1: UIImageView!
    @IBOutlet weak var image_rank3: UIImageView!
    @IBOutlet weak var lbl_rank3: UILabel!
    @IBOutlet weak var lbl_rank2: UILabel!
    @IBOutlet weak var lbl_Rank1: UILabel!
    @IBOutlet weak var btn_Districk: UIButton!
    @IBOutlet weak var btn_State: UIButton!
    @IBOutlet weak var view_Filter: UIView!
    @IBOutlet weak var tbl_Filter: UITableView!
    
    var selectedButton: UIButton?
    let data = ["Today", "This week", "This Month", "All Time"]
    var rankingData : [LeaderBoardDataModel]?
    var tap: String?
    var datarange = "all"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view_Filter.isHidden = true
        view_Filter.layer.cornerRadius = 15
        view_Filter.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view_Filter.layer.borderWidth = 1
        view_Filter.layer.borderColor = UIColor.gray.cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getRanking(filter_by: "state" , date_range: "all" )
    }
    
    //MARK: - UIButton Action
    @IBAction func tapStateButton(_ sender: UIButton) {
        self.btn_State.backgroundColor = .black
        self.btn_Districk.backgroundColor = .white
        self.btn_State.setTitleColor(UIColor.white, for: .normal)
        self.btn_Districk.setTitleColor(UIColor.black, for: .normal)
        self.tap = "state"
        self.getRanking(filter_by: tap ?? "", date_range: datarange )
        
    }
    
    @IBAction func tapDistrickButton(_ sender: UIButton) {
        self.btn_State.backgroundColor = .white
        self.btn_Districk.backgroundColor = .black
        self.btn_State.setTitleColor(UIColor.black, for: .normal)
        self.btn_Districk.setTitleColor(UIColor.white, for: .normal)
        self.tap = "district"
        self.getRanking(filter_by: tap ?? "", date_range: datarange )
    }
    
    @IBAction func tapFilterButton(_ sender: UIButton) {
        self.view_Filter.isHidden = false
    }
    
    @IBAction func tapShareButton(_ sender: UIButton) {}
    
    @IBAction func tapCancelButton(_ sender: UIButton) {
        self.view_Filter.isHidden = true
    }
    
    
    //
    //MARK: - Api
    func getRanking(filter_by: String, date_range: String) {
        self.showLoadingIndicator(in: self.view)
        Ranking1(filterBy: filter_by, dateRange: date_range) { response in
            self.rankingData = response.RESULT.leaderBoard
            print("Received Data: \(response)")
            
            let leaderBoard = response.RESULT.leaderBoard
            let firstThreeRanks = Array(leaderBoard.prefix(3))
            
            for (index, rank) in firstThreeRanks.enumerated() {
                let rankValue = String(rank.rank)
                let imageURL = URL(string: rank.user_image)
                DispatchQueue.main.async {
                    switch index {
                    case 0:
                        self.lbl_Rank1.text = "\(rankValue)\n\(rank.full_name)"
                        self.img_rank1.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "person_large"))
                        self.img_rank1.layer.borderColor = (UIColor(hexString: "#FFD700")).cgColor
                    case 1:
                        self.lbl_rank2.text = "\(rankValue)\n\(rank.full_name)"
                        self.image_rank2.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "person_large"))
                        self.image_rank2.layer.borderColor = (UIColor(hexString: "#D1D1D1")).cgColor
                    case 2:
                        self.lbl_rank3.text = "\(rankValue)\n\(rank.full_name)"
                        self.image_rank3.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "person_large"))
                        self.image_rank3.layer.borderColor = (UIColor(hexString: "#BD8383")).cgColor
                    default:
                        break
                    }
                    self.Tableview.reloadData()
                }
            }
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
            }
        }
    }
}

    
//
//MARK: - TableView
extension DashboardViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.Tableview {
            return rankingData?.count ?? 0
        }
        else if tableView == self.tbl_Filter {
            return data.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.Tableview {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderboardCell", for: indexPath) as! LeaderboardCell
            cell.lbl_Username.text = rankingData?[indexPath.row].full_name
            cell.lbl_point.text = rankingData?[indexPath.row].total_points
            cell.lbl_Rank.text = "\(rankingData?[indexPath.row].rank ?? 0)"
            if let imageURLString = rankingData?[indexPath.row].user_image, let imageURL = URL(string: imageURLString) {
                cell.img_User.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "person_large"))
            } else {
                cell.img_User.image = UIImage(named: "person_large")
            }
            return cell
        }
        else if tableView == self.tbl_Filter {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableCell", for: indexPath) as! FilterTableCell
            cell.selectionStyle = .none
            cell.lbl_Name.text = data[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.Tableview {
            return 70
        }
        else if tableView == tbl_Filter {
            return 33
        }
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tbl_Filter {
            let selectedFilter = data[indexPath.row]
            var dateRange: String = ""
            switch selectedFilter {
            case "all":
                dateRange = "all"
            case "this_month":
                dateRange = "this_month"
            case "this_week":
                dateRange = "this_week"
            case "today":
                dateRange = "today"
            default:
                break
            }
            self.getRanking(filter_by: tap ?? "", date_range: dateRange)
            self.view_Filter.isHidden = true
        }
    }
}




class FilterTableCell: UITableViewCell {
    @IBOutlet weak var lbl_Name: UILabel!
}





//import UIKit
//
//class DashboardViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
//    @IBOutlet weak var NoDataState: UILabel!
//    @IBOutlet weak var NoDataDistrict: UILabel!
//    @IBOutlet weak var NoDataRewards: UILabel!
//
//    @IBOutlet private var stateLeaderboardCollection: UICollectionView!
//    @IBOutlet private var districtLeaderboardCollection: UICollectionView!
//    @IBOutlet private var rewardsTable: UITableView!
//    @IBOutlet private var rewardsTableHeightConstraint: NSLayoutConstraint!
//    
//    @IBOutlet private var stateRank: UILabel!
//    @IBOutlet private var stateTotal: UILabel!
//    @IBOutlet private var districtRank: UILabel!
//    @IBOutlet private var districtTotal: UILabel!
//    
//    var leaderboard: LeaderboardResult?
//    var leaderboardState: LeaderboardResult?
//    var leaderboardDistrict: LeaderboardResult?
//    @IBOutlet weak var btnStateViewAll: UIButton!
//    @IBOutlet weak var btnDistViewAll: UIButton!
//    var countState  = 0
//    var numberOfStateUser: Int = 5
//    var numberOfDistUser: Int = 5
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        NoDataState.text = "No Records Found!".localize()
//        NoDataDistrict.text = "No Records Found!".localize()
//        NoDataRewards.text = "No Records Found!".localize()
//
//        let cell = UINib(nibName: MoreCell.identifier, bundle: nil)
//        stateLeaderboardCollection.register(cell, forCellWithReuseIdentifier: MoreCell.identifier)
//        districtLeaderboardCollection.register(cell, forCellWithReuseIdentifier: MoreCell.identifier)
//
//        self.navigationItem.leftBarButtonItem?.title = "Dashboard"
//    //    setUpView()
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        showLoadingIndicator(in: view)
//        btnStateViewAll.setTitle("View all".localize(), for: .normal)
//        btnDistViewAll.setTitle("View all".localize(), for: .normal)
//
//
//        getLeaderboardRanking { leaderboard in
//            self.leaderboard = leaderboard
//            DispatchQueue.main.async { [self] in
//
//                if let vc = self.children.first as? LeaderboardProfileView
//                {
//                    vc.leaderBoardProfileDelegate = self
//                    vc.levelID = leaderboard.level_id ?? "1"
//                    vc.userInfo = leaderboard.userInfo
//                }
//            }
//        }
//        getLeaderboardStateRanking { leaderboard in
//            self.leaderboardState = leaderboard
//         
//            DispatchQueue.main.async { [self] in
//                setUpView()
//                if self.leaderboardState?.topleaders?.count == 0{
//                    self.btnStateViewAll.isHidden = true
//                }else{
//                    self.btnStateViewAll.isHidden = false
//                }
//                stateLeaderboardCollection.reloadData()
//            }
//        }
//        getLeaderboardDistrictRanking { leaderboard in
//            self.leaderboardDistrict = leaderboard
//            DispatchQueue.main.async { [self] in
//                districtLeaderboardCollection.reloadData()
//                DispatchQueue.main.async { [self] in
//                    if self.leaderboardDistrict?.districtTopleaders?.count == 0{
//                        self.btnDistViewAll.isHidden = true
//                    }else{
//                        self.btnDistViewAll.isHidden = false
//                    }
//                    hideLoadingIndicator()
//                    
//                    rewardsTable.reloadData()
//                    
//                    if leaderboardDistrict?.task?.count == 0{
//                        rewardsTableHeightConstraint.constant = CGFloat(100)
//                    }else{
//                        rewardsTableHeightConstraint.constant = CGFloat((leaderboardDistrict?.task?.count ?? 0) * 235)
//                    }
//                }
//            }
//        }
//
//    }
//    
//    private func setUpView() {
//        stateRank.text = "\(leaderboard?.state_rank ?? "")"
//        stateTotal.text = String(format: "Out of %@".localize(), leaderboard?.state_total ?? "")
//        districtRank.text = "\(leaderboard?.district_rank ?? "")"
//        districtTotal.text = String(format: "Out of %@".localize(), leaderboard?.district_total ?? "")
//        
//        //Collections
//        let layout = UICollectionViewFlowLayout()
//        layout.scrollDirection = .horizontal
//        layout.itemSize = CGSize(width: 50, height: 80)
//        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
//        layout.minimumInteritemSpacing = 10
//        stateLeaderboardCollection.collectionViewLayout = layout
//        
//        let layout1 = UICollectionViewFlowLayout()
//        layout1.scrollDirection = .horizontal
//        layout1.itemSize = CGSize(width: 50, height: 80)
//        layout1.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
//        layout1.minimumInteritemSpacing = 10
//        districtLeaderboardCollection.collectionViewLayout = layout1
//        
//        stateLeaderboardCollection.showsHorizontalScrollIndicator = false
//        stateLeaderboardCollection.dataSource = self
//        stateLeaderboardCollection.delegate = self
//        
//        districtLeaderboardCollection.showsHorizontalScrollIndicator = false
//        districtLeaderboardCollection.dataSource = self
//        districtLeaderboardCollection.delegate = self
//    }
//    
//    @IBAction func btnViewAllStateClick(_ sender: UIButton) {
//        
//        let vc = LeaderboardList(nibName: LeaderboardList.identifier, bundle: nil)
//        vc.popUpType = "State"
//        vc.leaderboardState = leaderboardState
//        vc.modalPresentationStyle = .overCurrentContext
//        vc.modalTransitionStyle = .crossDissolve
//        self.present(vc, animated: true, completion: nil)
//
//    }
//    @IBAction func btnViewAllDistClick(_ sender: UIButton) {
//        let vc = LeaderboardList(nibName: LeaderboardList.identifier, bundle: nil)
//        vc.popUpType = "District"
//        vc.leaderboardDistrict = leaderboardDistrict
//        vc.modalPresentationStyle = .overCurrentContext
//        vc.modalTransitionStyle = .crossDissolve
//        self.present(vc, animated: true, completion: nil)
//
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if collectionView == stateLeaderboardCollection {
//            if leaderboardState?.topleaders?.count == 0 || leaderboardState?.topleaders?.count == nil{
//                NoDataState.isHidden = false
//                return leaderboardState?.topleaders?.count ?? 0
//            }else{
//                NoDataState.isHidden = true
//                if numberOfStateUser < (leaderboardState?.topleaders?.count ?? 0){
//                    return numberOfStateUser + 1
//                }else{
//                    return leaderboardState?.topleaders?.count ?? 0
//                }
//            }
//        } else {
//            if leaderboardDistrict?.districtTopleaders?.count == 0 || leaderboardDistrict?.districtTopleaders?.count == nil{
//                NoDataDistrict.isHidden = false
//                return leaderboardDistrict?.districtTopleaders?.count ?? 0
//            }else{
//                NoDataDistrict.isHidden = true
//                if numberOfDistUser < (leaderboardDistrict?.districtTopleaders?.count ?? 0){
//                    return numberOfDistUser + 1
//                }else{
//                    return leaderboardDistrict?.districtTopleaders?.count ?? 0
//                }
//            }
//        }
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 55 , height: 80)
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        
//        if collectionView == stateLeaderboardCollection {
//            if(indexPath.row < numberOfStateUser){
//                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "stateLeaderboardCell", for: indexPath) as! LeaderboardCell
//                
//                guard let entry = leaderboardState?.topleaders?[indexPath.row] else { return cell }
//                cell.rank.text = "\(entry.ranking ?? 0)"
//                cell.name.text = entry.full_name
//                if let img = entry.user_image {
//                    cell.userImage.setLoadedImage(img)
//                }
//                return cell
//            }else{
//                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoreCell", for: indexPath) as! MoreCell
//                
//                return cell
//            }
//            
//        } else {
//            if(indexPath.row < numberOfDistUser){
//                
//                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "districtLeaderboardCell", for: indexPath) as! DistrictLeaderboardCell
//                
//                guard let entry = leaderboardDistrict?.districtTopleaders?[indexPath.row] else { return cell }
//                cell.rank.text = "\(entry.ranking ?? 0)"
//                cell.name.text = entry.full_name
//                if let img = entry.user_image {
//                    cell.image.setLoadedImage(img)
//                }
//                return cell
//            }else{
//                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoreCell", for: indexPath) as! MoreCell
//                
//                return cell
//            }
//        }
//    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if collectionView == stateLeaderboardCollection {
//            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoreCell", for: indexPath) as? MoreCell{
//                if numberOfStateUser <= (leaderboardState?.topleaders!.count)! {
//                    numberOfStateUser = numberOfStateUser + 5
//                }
//                stateLeaderboardCollection.reloadData()
//            }
//        }else{
//            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoreCell", for: indexPath) as? MoreCell{
//                if numberOfDistUser <= (leaderboardDistrict?.districtTopleaders!.count)! {
//                    numberOfDistUser = numberOfDistUser + 5
//                }
//                districtLeaderboardCollection.reloadData()
//            }
//        }
//    }
//}
//
//extension DashboardViewController: UITableViewDataSource, UITableViewDelegate {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if leaderboardDistrict?.task?.count == 0{
//            NoDataRewards.isHidden = false
//            return leaderboardDistrict?.task?.count ?? 0
//        }else{
//            NoDataRewards.isHidden = true
//            return leaderboardDistrict?.task?.count ?? 0
//        }
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "rewardsTableCell", for: indexPath)  as! RewardsTableViewCell
//        
//        guard let task = leaderboardDistrict?.task?[indexPath.row] else { return cell }
//        cell.addedDate.text = getFormattedDate(from: task.added_on, inputFormat: "yyyy-MM-dd", outputFormat: 0)
//        cell.completedTask.text = "\(task.completed_task)/\(task.total_task)"
//        cell.pointsEarned.text = task.reward_points
//        cell.waCompletedTask.text = getSocialMediaProgress(for: 0, task: task).0
//        cell.whatsappProgressView.progress = getSocialMediaProgress(for: 0, task: task).1
//        cell.twitterCompletedTask.text = getSocialMediaProgress(for: 1, task: task).0
//        cell.twitterProgressView.progress = getSocialMediaProgress(for: 1, task: task).1
//        cell.fbCompletedTask.text = getSocialMediaProgress(for: 2, task: task).0
//        cell.fbProgressView.progress = getSocialMediaProgress(for: 2, task: task).1
//        cell.youtubeCompletedTask.text = getSocialMediaProgress(for: 3, task: task).0
//        cell.youtubeProgressView.progress = getSocialMediaProgress(for: 3, task: task).1
//        
//        return cell
//    }
//     
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        235
//    }
//    
//    private func getSocialMediaProgress(for index: Int, task: Task) -> (String, Float) {
//        var progressInFloat: Float = 0, progressInStr = ""
//        var completed = "", total = ""
//        switch index {
//        case 0: //whatsapp
//            completed = task.completed_whatsapp_task
//            total = task.total_whatsapp_task
//        case 1: //twitter
//            completed = task.completed_twitter_task
//            total = task.total_twitter_task
//        case 2: //facebook
//            completed = task.completed_fb_task
//            total = task.total_fb_task
//        default: //youtube
//            completed = task.completed_youtube_task
//            total = task.total_youtube_task
//        }
//        progressInFloat = (Float(completed) ?? 0)/(Float(total) ?? 0)
//        progressInStr = "\(completed)/\(total)"
//        return (progressInStr, progressInFloat)
//    }
//}
//
//extension DashboardViewController: LeaderBoardProfileDelegate {
//
//    func showRewardTransactions() {
//        let storyBoard = UIStoryboard(name: "Other", bundle: nil)
//        let rewardTransactionVC = storyBoard.instantiateViewController(withIdentifier: "rewardTransactionsView")
//        rewardTransactionVC.modalPresentationStyle = .fullScreen
//        self.navigationController?.pushViewController(rewardTransactionVC, animated: false)
//    }
//}
