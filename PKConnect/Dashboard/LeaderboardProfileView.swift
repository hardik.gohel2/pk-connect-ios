//
//  LeaderboardProfileView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/2/22.
//

import UIKit

protocol LeaderBoardProfileDelegate {
    func showRewardTransactions()
}

class LeaderboardProfileView: BaseViewController {

    @IBOutlet private var userImage: UIImageView!
    @IBOutlet private var name: UILabel!
    @IBOutlet private var joiningDate: UILabel!
    @IBOutlet private var pointsEarned: UILabel!
    @IBOutlet private var levelId: UILabel!
    
    @IBOutlet weak var levelsPinnedView: UIView!
    
    var levelID = "1"
    var leaderBoardProfileDelegate: LeaderBoardProfileDelegate?
    
    var userInfo: UserInfo? {
        didSet {
            name.text = userInfo?.full_name
            if let imageStr = userInfo?.user_image {
                userImage.setLoadedImage(imageStr)
            }
            if let registeredDate = userInfo?.registered_on {
                joiningDate.text = "Joined On : ".localize() + getFormattedDate(from: registeredDate, inputFormat: "yyyy-MM-dd HH:mm:ss", outputFormat: 2)
            }
            pointsEarned.text = userInfo?.points_earned
            levelId.text = levelID
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userImage.layer.cornerRadius = 125/2
    }

    @IBAction func rewardsTransactionTapped(_ sender: UIButton) {
        if userInfo == nil{
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0){
                let storyBoard = UIStoryboard(name: "Other", bundle: nil)
                let rewardTransactionVC = storyBoard.instantiateViewController(withIdentifier: "rewardTransactionsView")
                rewardTransactionVC.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(rewardTransactionVC, animated: false)
            }
        }else{
            leaderBoardProfileDelegate?.showRewardTransactions()
        }
    }
    
}
