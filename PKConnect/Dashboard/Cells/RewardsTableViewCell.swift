//
//  RewardsTableViewCell.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/2/22.
//

import UIKit

class RewardsTableViewCell: UITableViewCell {

    @IBOutlet weak var addedDate: UILabel!
    @IBOutlet weak var completedTask: UILabel!
    @IBOutlet weak var pointsEarned: UILabel!
    
    @IBOutlet weak var waCompletedTask: UILabel!
    @IBOutlet weak var whatsappProgressView: UIProgressView!
    
    @IBOutlet weak var fbCompletedTask: UILabel!
    @IBOutlet weak var fbProgressView: UIProgressView!
    
    @IBOutlet weak var twitterCompletedTask: UILabel!
    @IBOutlet weak var twitterProgressView: UIProgressView!
    
    @IBOutlet weak var youtubeCompletedTask: UILabel!
    @IBOutlet weak var youtubeProgressView: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
