//
//  LeaderboardCell.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/2/22.
//

//import UIKit
//
//class LeaderboardCell: UICollectionViewCell {
//    
//    @IBOutlet weak var userImage: UIImageView!
//    @IBOutlet weak var rank: UILabel!
//    @IBOutlet weak var name: UILabel!
//    
//    
//    override func layoutSubviews() {
//        userImage.layer.cornerRadius = 45/2
//        userImage.layer.masksToBounds = true
//    }
//    
//    override func prepareForReuse() {
//        name.text = ""
//        rank.text = ""
//        userImage.image = UIImage(named: "user_44")
//    }
//}

import UIKit

class LeaderboardCell:UITableViewCell {
    
    @IBOutlet weak var lbl_point: UILabel!
    @IBOutlet weak var lbl_Username: UILabel!
    @IBOutlet weak var img_User: UIImageView!
    @IBOutlet weak var lbl_Rank: UILabel!
    //    @IBOutlet weak var userImage: UIImageView!
//    @IBOutlet weak var rank: UILabel!
//    @IBOutlet weak var name: UILabel!
//    @IBOutlet weak var baseView:UIView!
//    @IBOutlet weak var pointsLbl:UILabel!
//
//    @IBOutlet weak var starImage: UIImageView!
//    override func layoutSubviews() {
//        userImage.layer.cornerRadius = 45/2
//        userImage.layer.masksToBounds = true
//    }
    
    override func prepareForReuse() {
//        name.text = ""
//        rank.text = ""
//        userImage.image = UIImage(named: "user_44")
    }
}
