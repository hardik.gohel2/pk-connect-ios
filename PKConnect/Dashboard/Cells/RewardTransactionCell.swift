//
//  RewardTransactionCell.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/2/22.
//

import UIKit

class RewardTransactionCell: UITableViewCell {

    @IBOutlet weak var taskTypeView: UIImageView!
    @IBOutlet weak var taskTitle: UILabel!
    @IBOutlet weak var taskDescription: UILabel!
    @IBOutlet weak var updatedTime: UILabel!
    @IBOutlet weak var pointsEarned: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }


}
