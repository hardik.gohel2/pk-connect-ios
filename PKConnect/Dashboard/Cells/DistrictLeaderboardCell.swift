//
//  DistrictLeaderboardCell.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/2/22.
//

import UIKit

class DistrictLeaderboardCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var rank: UILabel!
    @IBOutlet weak var name: UILabel!
    
    override func layoutSubviews() {
        image.layer.cornerRadius = 45/2
        image.layer.masksToBounds = true
    }
    
    override func prepareForReuse() {
        name.text = ""
        rank.text = ""
        self.image.image = UIImage(named: "user_44")
    }
}
