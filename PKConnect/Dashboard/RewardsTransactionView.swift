//
//  RewardsTransactionView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/2/22.
//

import UIKit

class RewardsTransactionView: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet private var tableView: UITableView!
    
    private var sectionHeaders = [String](),
                rowData = [[RewardTransaction]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.sectionFooterHeight = 0
        showLoadingIndicator(in: view)
        getRewardTransactions { transactions in
            self.sortTransactions(transactions)
            DispatchQueue.main.async { [self] in
                hideLoadingIndicator()
                transactions.count == 0 ? addNoRecordsLabel("Regular", 16, to: self.view) : tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @IBAction private func changeLanguage(_ sender: Any) {
        changeAppLanguage()
    }
    
    private func sortTransactions(_ transactions: [RewardTransaction]) {
        var group: [RewardTransaction]?, oldDate = ""
        transactions.forEach { tx in
            let newDate = getFormattedDate(from: tx.created_date, inputFormat: "yyyy-MM-dd HH:mm:ss", outputFormat: 0)
            if oldDate == newDate {
                group?.append(tx)
            } else {
                if group != nil {
                    sectionHeaders.append(oldDate)
                    rowData.append(group!)
                    group?.removeAll()
                }
                group = [tx]
                oldDate = newDate
            }
        }
        if group != nil {
            sectionHeaders.append(oldDate)
            rowData.append(group!)
            group?.removeAll()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        sectionHeaders.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        rowData[section].count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "rewardTransactionCell", for: indexPath) as! RewardTransactionCell
        
        let rewardTransaction = rowData[indexPath.section][indexPath.row]
        let img = UIImage(named: rewardTransaction.type)
        cell.taskTypeView.image = (img == nil ? UIImage(systemName: "info.circle") : img)
        cell.taskTitle.text = rewardTransaction.title
        cell.taskDescription.text = rewardTransaction.description
        cell.updatedTime.text = getFormattedDate(from: rewardTransaction.created_date, inputFormat: "yyyy-MM-dd HH:mm:ss", outputFormat: 3)
        cell.pointsEarned.text = "+\(Int(Double(rewardTransaction.point) ?? 0))"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        getTableHeaderWith(title: sectionHeaders[section])
    }
    
    private func getTableHeaderWith(title: String) -> UITableViewHeaderFooterView {
        let headerView = UITableViewHeaderFooterView()

        var config = UIListContentConfiguration.groupedHeader()
        config.attributedText = NSAttributedString(string: title, attributes: [.font: UIFont(name: "NunitoSans-SemiBold", size: 14)!, .foregroundColor: UIColor.label])
        headerView.contentConfiguration = config

        return headerView
    }
    
    @IBAction private func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}
