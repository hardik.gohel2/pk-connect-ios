//
//  BannerSuccessView.swift
//  PKConnect
//
//  Created by apple on 10/01/24.
//

import UIKit
import SwiftGifOrigin

class BannerSuccessView: UIView {
    
    @IBOutlet weak var img_success: UIImageView!
    
    @IBOutlet weak var lbl_msg: UILabel!
    @IBOutlet weak var lbl_success: UILabel!
    @IBOutlet weak var btn_submit: UIButton!
    
    weak var delegate: BannerSuccessViewDelegate?
     
    override func awakeFromNib() {
        super.awakeFromNib()
        img_success.image = UIImage.gif(name: "green_gif_success")

        btn_submit.layer.cornerRadius = 25
        btn_submit.layer.borderWidth = 1
        btn_submit.layer.borderColor = UIColor.pkConnectYellow.cgColor
        btn_submit.backgroundColor = .pkConnectYellow
    }
    
    @IBAction func btn_submit(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.startJourneyTapped()
        }
    }
    
}
protocol BannerSuccessViewDelegate: AnyObject {
    func startJourneyTapped()
}
