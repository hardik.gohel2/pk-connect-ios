//
//  ManifestoVC.swift
//  PKConnect
//
//  Created by apple on 08/01/24.
//

import UIKit

class ManifestoVC: BaseViewController {

     @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txt_number: UITextField!
    var campaignDetail: Campaign?
    
//    var retrievedUser: VerifiedUser? {
//        didSet {
//            txt_name.text = retrievedUser?.user_name
//            txt_number.text = retrievedUser?.contactNumber
//           
//        }
//    }  
    override func viewDidLoad() {
        super.viewDidLoad()
        txt_name.text = getRetrievedUser()?.user_name ?? ""
        //"\(UserDefaults.standard.value(forKey: "name"))"
        txt_number.text = getRetrievedUser()?.contactNumber ?? ""
        // Do any additional setup after loading the view.
    }
    
    override func getRetrievedUser() -> VerifiedUser? {
        if let userData = UserDefaults.standard.object(forKey: "User") as? Data,
           let retrievedUser = try? JSONDecoder().decode(VerifiedUser.self, from: userData) {
            return retrievedUser
        }
        return nil
    }
     
    @IBAction func btn_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_sapat(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Home", bundle: nil)
        let taskDetailVC = vc.instantiateViewController(withIdentifier: "BannerSurvayVC") as! BannerSurvayVC
      //  taskDetailVC.campaignDetail = campaigns[indexPath.row]
        self.navigationController?.pushViewController(taskDetailVC, animated: true)
    }
    
}
