//
//  CampaignSurveyModel.swift
//  PKConnect
//
//  Created by apple on 08/01/24.
//

import Foundation

struct CampaignSurveyModel: Codable {

    let CODE: Int
    let MESSAGE: String
    let RESULT: [CampaignSurveyDataModel]
    let NEXT: String
    let TOTAL: Int

}
struct CampaignSurveyDataModel: Codable {

    let qn_id: String
    let campaign_question: String
    let status: String
    let type: String
    let optionArr: [OptionArrayModel]

}
struct OptionArrayModel: Codable {

    let option_id: String
    let option_name: String

}
