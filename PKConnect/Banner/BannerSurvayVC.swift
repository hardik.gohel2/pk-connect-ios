//
//  BannerSurvayVC.swift
//  PKConnect
//
//  Created by apple on 08/01/24.
//

import UIKit

class BannerSurvayVC: BaseViewController {
    
    @IBOutlet weak var imgOPT2: UIImageView!
    @IBOutlet weak var imgOPT1: UIImageView!
    @IBOutlet weak var btnOpt2: UIButton!
    @IBOutlet weak var btnOPT1: UIButton!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblQCount: UILabel!
    @IBOutlet weak var progrssView: UIProgressView!
    
    var indexQ = 0
    var isselected = false
    var campainsurvayArray = [CampaignSurveyDataModel]()
    var banneroptionModel = [BannerOptionModel]()
    
    var arrQuestions: [QuestionLearn] = [
        QuestionLearn(
            question: "Do you want to participate in active politics?",
            options: [Options(name: "Yes", isselected: false),
                     Options(name: "No", isselected: false)]
        ),
        QuestionLearn(
            question: "Are you associated with any political party?",
            options: [Options(name: "Yes", isselected: false),
                     Options(name: "No", isselected: false)]
        ),
        QuestionLearn(
            question: "Do you use social media?",
            options: [Options(name: "Yes", isselected: false),
                     Options(name: "No", isselected: false)]
        ),
        QuestionLearn(
            question: "Are you willing to volunteer for public welfare in the future?",
            options: [Options(name: "Yes", isselected: false),
                     Options(name: "No", isselected: false)]
        )]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getBannerSurveyData()
        
        
    }
    
    func getBannerSurveyData() {
        print("---sdcs")
        showLoadingIndicator(in: self.view)
        getBannerSurvey { bannersurvey in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                self.campainsurvayArray = bannersurvey.RESULT 
                print("Gaurav",self.campainsurvayArray[1].campaign_question)
                self.updateQuestions()
               
                
            }
        }
    }
    @IBAction func onClickOPT(_ sender: UIButton) {
//        updateOptions()
      //  isselected = true
                    self.btnOPT1.setTitleColor(.white, for: .normal)
                   // self.btnOPT1.setImage(UIImage(named: "radio_button_checked"), for: .normal)
                    self.btnOPT1.ViewBorderWidth = 1.0
                    self.btnOPT1.backgroundColor = .black
                    self.btnOPT1.tintColor = .white
                  
                   
                   
       // arrQuestions[indexQ].options[sender.tag-1].isselected = true
//        if sender.tag == 1 {
//            self.btnOPT1.setTitleColor(.white, for: .normal)
//            self.btnOPT1.setImage(UIImage(named: "radio_button_checked"), for: .normal)
//            self.imgOPT1.tintColor = .white
//            self.btnOPT1.ViewBorderWidth = 1.0
//            self.btnOPT1.backgroundColor = .black
//            self.btnOPT1.tintColor = .white
//        }else{
//            self.btnOpt2.setTitleColor(.white, for: .normal)
//            self.btnOpt2.setImage(UIImage(named: "radio_button_checked"), for: .normal)
//            self.btnOpt2.tintColor = .white
//            self.btnOpt2.ViewBorderWidth = 1.0
//            self.btnOpt2.backgroundColor = .black
//            self.imgOPT2.tintColor = .white
//        }
        if indexQ < 4 {
         
            callstartSurvey()

        }else {
            print("--- > > Gaurav Else")
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let learnvc = storyBoard.instantiateViewController(withIdentifier: "AddContactVC") as! AddContactVC
            self.navigationController?.pushViewController(learnvc, animated: true)
        }
    }
    
    
    @IBAction func btn_clickNo(_ sender: UIButton) {
//updateOptions()
       // isselected = true
        
        self.btnOpt2.setTitleColor(.white, for: .normal)
       // self.btnOpt2.setImage(UIImage(named: "radio_button_checked"), for: .normal)
        self.btnOpt2.ViewBorderWidth = 1.0
        self.btnOpt2.backgroundColor = .black
        self.btnOpt2.tintColor = .white
        if indexQ < 4 {
            indexQ = indexQ + 1
            callstartNoSurvey()
        }else {
            print("--- > > Gaurav Else")
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let learnvc = storyBoard.instantiateViewController(withIdentifier: "AddContactVC") as! AddContactVC
            self.navigationController?.pushViewController(learnvc, animated: true)
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func callstartSurvey() {
      //showLoadingIndicator(in: self.view)
        updateSurvey(qn_id: campainsurvayArray[indexQ].qn_id,op_id: campainsurvayArray[indexQ].optionArr[0].option_id){ response in
                        DispatchQueue.main.async {
                            self.hideLoadingIndicator()
                            
                            self.indexQ = self.indexQ + 1
//                            self.btnOpt2.setTitleColor(.white, for: .normal)
//                            self.btnOpt2.setImage(UIImage(named: "radio_button_checked"), for: .normal)
//                            self.btnOpt2.ViewBorderWidth = 1.0
//                            self.btnOpt2.backgroundColor = .black
//                            self.btnOpt2.tintColor = .white
                            self.updateOptions()
                            self.updateQuestions()
                            print("Index == ",self.indexQ)
                                       
                        }
        }
    }
    func callstartNoSurvey() {
     // showLoadingIndicator(in: self.view)
        updateSurvey(qn_id: campainsurvayArray[indexQ].qn_id,op_id: campainsurvayArray[indexQ].optionArr[1].option_id){ response in
                        DispatchQueue.main.async {
                            self.hideLoadingIndicator()
                            self.indexQ = self.indexQ + 1
                            self.updateQuestions()
                            self.updateOptions()
//                            self.btnOPT1.setTitleColor(.black, for: .normal)
//                            self.btnOPT1.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
//                            self.btnOPT1.tintColor = .black
//                            self.btnOPT1.ViewBorderWidth = 1.0
//                            self.btnOPT1.backgroundColor = .white
                                       
                        }
        }
    }
    
    @IBAction func onClickNext(_ sender: Any) {
        if indexQ < 3 {
            if isselected {
                indexQ = indexQ + 1
                updateQuestions()
            }else{
                let alertController = UIAlertController(title: "Error".localize(), message: "please select any one option".localize(), preferredStyle: .alert)
                
                
                let okAction = UIAlertAction(title: "OK".localize(), style: .default) { (action) in
                    self.dismiss(animated: true)
                    print("OK button tapped")
                }
                
                
                alertController.addAction(okAction)
                
                
                self.present(alertController, animated: true, completion: nil)
                
            }
        }else {
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let learnvc = storyBoard.instantiateViewController(withIdentifier: "AddContactVC") as! AddContactVC
            self.navigationController?.pushViewController(learnvc, animated: true)
        }
        
    }
    
    func updateQuestions() {
       
        let dict = campainsurvayArray[indexQ]
        arrQuestions[indexQ]
        lblQuestion.text = "\(dict.campaign_question)"
        // Q\(indexQ + 1).
        //lblQCount.text = "\(indexQ + 1)/4"
        isselected = false
      //  self.progrssView.progress = Float(indexQ/10)
        if indexQ == 0{
            
            let progressValue = 0.2
            self.progrssView.progress = Float(min(max(progressValue, 0.0), 1.0))
        }else{
            let progressValue = Float(indexQ * 2) / Float(10)
            self.progrssView.progress = min(max(progressValue, 0.0), 1.0)
        }
       
       // updateOptions()
    }
    
    func updateOptions() {
        self.btnOPT1.backgroundColor = .white
        self.btnOPT1.setTitleColor(.black, for: .normal)
       // self.btnOPT1.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
        self.btnOPT1.tintColor = .black
       // self.imgOPT1.tintColor = .black
        self.btnOPT1.ViewBorderWidth = 1.0
        self.btnOpt2.tintColor = .black
        self.btnOpt2.backgroundColor = .white
        self.btnOpt2.setTitleColor(.black, for: .normal)
       // self.btnOpt2.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
       // self.imgOPT2.tintColor = .black
        self.btnOpt2.ViewBorderWidth = 1.0
        
    }
    
}
