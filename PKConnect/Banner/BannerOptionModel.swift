//
//  BannerOptionModel.swift
//  PKConnect
//
//  Created by apple on 08/01/24.
//

import Foundation

struct BannerOptionModel: Codable {

    let CODE: Int
    let MESSAGE: String

}
