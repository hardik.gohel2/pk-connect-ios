//
//  ContactDetailCell.swift
//  PKConnect
//
//  Created by apple on 09/01/24.
//

import UIKit

class ContactDetailCell: UITableViewCell {

    @IBOutlet weak var txt_name: UITextField!
    
    @IBOutlet weak var btn_delete: UIButton!
    @IBOutlet weak var txt_number: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
