//
//  WebViewController.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.


import UIKit
import WebKit

class WebViewController: BaseViewController, WKNavigationDelegate {

    private var webView: WKWebView!,
                isInjected: Bool = false,
                path = "",
                hideNavBar = false

    init(with path: String, _ hideNavBar: Bool) {
        super.init(nibName: nil, bundle: nil)
        self.path = path
        self.hideNavBar = hideNavBar
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
  

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = hideNavBar

        guard var urlComponents = URLComponents(string: "https://pkconnect.com/dashboard/\(path)") else { return }
        if appLang == "hi" {
            urlComponents.queryItems = [URLQueryItem(name: "lang", value: "hi")]
        }
        let request = URLRequest(url: urlComponents.url!)
        showLoadingIndicator(in: view)
        webView.load(request)
    }

    override func loadView() {
        super.loadView()

        webView = WKWebView()
        webView.navigationDelegate = self
        view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10),
            webView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -10)
        ])
        
        
    }
 
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            if Float(webView.estimatedProgress) == 1.0 {
                hideLoadingIndicator()
            }
        }
    }
    // MARK: WKNavigationDelegate
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

        if isInjected == true {
            return
        }
        self.isInjected = true
        // get HTML text
        let js = "document.body.outerHTML"
        webView.evaluateJavaScript(js) { (html, error) in
            let headerString = "<head><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></head>"
            webView.loadHTMLString(headerString + (html as! String), baseURL: nil)
          //  self.showAlert()
        }
        
    }
    

    
}

