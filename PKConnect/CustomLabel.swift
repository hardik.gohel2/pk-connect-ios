//
//  CustomLabel.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 10/30/22.
//

import UIKit

@IBDesignable
class CustomLabel: UILabel {

    @IBInspectable var localizableText: String = "" {
        didSet {
            let attributedTitle = NSAttributedString(string: localizableText.localize(), attributes: [.font: self.font ?? UIFont(name: "OpenSans-Semibold", size: 16)!])
            self.attributedText = attributedTitle
        }
    }
    

}

class CustomButton: UIButton {

    @IBInspectable var localizableText: String = "" {
        didSet {
            
//            let attributedTitle = localizableText.localize()
            self.setTitle(localizableText.localize(), for: .normal)
//            self.attributedText = attributedTitle
        }
    }
    

}
