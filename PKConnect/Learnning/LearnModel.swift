//
//  LearnModel.swift
//  PKConnect
//
//  Created by admin on 13/12/23.
//

import Foundation


struct QuestionLearn {
    var question: String
    var options: [Options]
    

    init(question: String, options: [Options]) {
        self.question = question
        self.options = options
    }
}


struct Options {
    var name: String
    var isselected : Bool
}


// Learning Module List
struct ModuleData: Codable {
    let status: String
    let message: String
    let data: [Module]
}

struct Module: Codable {
    let module_id: String?
    let module_title: String?
    let module_desc: String?
    let video_compl_percent: Int?
    let module_compl_status: String?
    let quiz_submit_time: String?

}


// Learning Module Videos

struct VideoData: Codable {
    let status: String
    let message: String
    let data: [Video]
    let totalWatchedVideos: Int
    let numberOfQuestions: Int
    let isQuizCompleted: Int
    let moduleTitle: String

    enum CodingKeys: String, CodingKey {
        case status, message, data
        case totalWatchedVideos = "total_watched_video"
        case numberOfQuestions = "no_questions"
        case isQuizCompleted = "is_quiz_completed"
        case moduleTitle = "module_title"
    }
}

struct Video: Codable {
    let moduleID: String
    let videoID: String
    let videoTitle: String
    let videoDescription: String
    let videoURL: String
    let videoThumbnail: String
    let videoWatchFlag: String

    enum CodingKeys: String, CodingKey {
        case moduleID = "module_id"
        case videoID = "video_id"
        case videoTitle = "video_title"
        case videoDescription = "video_desc"
        case videoURL = "video_url"
        case videoThumbnail = "video_thumb"
        case videoWatchFlag = "video_watch_flag"
    }
}


// get quiz questions
struct QuizDataLearn: Codable {
    let status: String
    let message: String
    let data: [QuizDataLearnData]
}

struct QuizDataLearnData: Codable {
    let question_id: String
    let question_text: String
    let options: [QuizDataLearnOptions]
}


struct QuizDataLearnOptions: Codable {
    let option_id: String
    let ques_id: String
    let option_text: String
    let correct_ans: String
}

//struct QuizDataLearn: Codable {
//    let status: String
//    let message: String
//    let data: [QuizQuestion]
//}
//
//struct QuizQuestion: Codable {
//    let questionID: String
//    let questionText: String
//    let options: [QuestionOption]
//    
//    enum CodingKeys: String, CodingKey {
//        case questionID = "question_id"
//        case questionText = "question_text"
//        case options
//    }
//}
//
//struct QuestionOption: Codable {
//    let optionID: String
//    let questionID: String
//    let optionText: String
//    let correctAnswer: String
//    
//    enum CodingKeys: String, CodingKey {
//        case optionID = "option_id"
//        case questionID = "ques_id"
//        case optionText = "option_text"
//        case correctAnswer = "correct_ans"
//    }
//}

struct QuizResponseLearn: Codable {
    let status: String
    let message: String
    let data: QuizResponseData
}

struct QuizResponseData: Codable {
    let moduleID: String
    let totalQuestionCount: String
    let correctQuestionCount: String

    enum CodingKeys: String, CodingKey {
        case moduleID = "module_id"
        case totalQuestionCount = "total_qn_count"
        case correctQuestionCount = "correct_qn_count"
    }
}
