//
//  PreLearninngQVC.swift
//  PKConnect
//
//  Created by admin on 12/12/23.
//

import UIKit

class PreLearninngQVC: BaseViewController {

    @IBOutlet weak var imgOPT2: UIImageView!
    @IBOutlet weak var imgOPT1: UIImageView!
    @IBOutlet weak var btnOpt2: UIButton!
    {
        didSet {
            btnOpt2.tag = 2
        }
    }
    @IBOutlet weak var btnOPT1: UIButton!{
        didSet {
            btnOPT1.tag = 1
        }
    }
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblQCount: UILabel!
    @IBOutlet weak var progrssView: UIProgressView!
    
    var indexQ = 0
    var isselected = false
    
    var arrQuestions: [QuestionLearn] = [
        QuestionLearn(
            question: "Do you want to participate in active politics?",
            options: [Options(name: "Yes", isselected: false),
                     Options(name: "No", isselected: false)]
        ),
        QuestionLearn(
            question: "Are you associated with any political party?",
            options: [Options(name: "Yes", isselected: false),
                     Options(name: "No", isselected: false)]
        ),
        QuestionLearn(
            question: "Do you use social media?",
            options: [Options(name: "Yes", isselected: false),
                     Options(name: "No", isselected: false)]
        ),
        QuestionLearn(
            question: "Are you willing to volunteer for public welfare in the future?",
            options: [Options(name: "Yes", isselected: false),
                     Options(name: "No", isselected: false)]
        )]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateQuestions()
    }
    

    @IBAction func onClickOPT(_ sender: UIButton) {
        updateOptions()
        isselected = true
        arrQuestions[indexQ].options[sender.tag-1].isselected = true
       
        sender.setImage(UIImage(named: "radio_button_checked"), for: .normal)
        sender.tintColor = .white
        imgOPT1.tintColor = .white
        sender.ViewBorderWidth = 1.0
        sender.backgroundColor = .black
        sender.setTitleColor(UIColor.white, for: .normal)
            
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickNext(_ sender: Any) {
        if indexQ < 3 {
            if isselected {
                indexQ = indexQ + 1
                updateQuestions()
            }else{
                let alertController = UIAlertController(title: "Error".localize(), message: "please select any one option".localize(), preferredStyle: .alert)
                
                
                let okAction = UIAlertAction(title: "OK".localize(), style: .default) { (action) in
                    self.dismiss(animated: true)
                    print("OK button tapped")
                }
                
                
                alertController.addAction(okAction)
                
                
                self.present(alertController, animated: true, completion: nil)
                
            }
        }else {
            let storyBoard = UIStoryboard(name: "Journey", bundle: nil)
            let learnvc = storyBoard.instantiateViewController(withIdentifier: "RecommendationVC") as! RecommendationVC
            self.navigationController?.pushViewController(learnvc, animated: true)
        }
        
    }
    
    func updateQuestions() {
        let dict = arrQuestions[indexQ]
        lblQuestion.text = "Q\(indexQ + 1). \(dict.question)"
        lblQCount.text = "\(indexQ + 1)/4"
        self.btnOPT1.setTitle(dict.options[0].name, for: .normal)
        self.btnOpt2.setTitle(dict.options[1].name, for: .normal)
        isselected = false
        self.progrssView.progress = Float(self.indexQ) / 10.0
        updateOptions()
    }
    
    func updateOptions() {
        self.btnOPT1.backgroundColor = .white
        self.btnOPT1.setTitleColor(.black, for: .normal)
        self.btnOPT1.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
        self.btnOPT1.tintColor = .black
        self.imgOPT1.tintColor = .black
        self.btnOPT1.ViewBorderWidth = 1.0
        self.btnOpt2.tintColor = .black
        self.btnOpt2.backgroundColor = .white
        self.btnOpt2.setTitleColor(.black, for: .normal)
        self.btnOpt2.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
        self.imgOPT2.tintColor = .black
        self.btnOpt2.ViewBorderWidth = 1.0
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
