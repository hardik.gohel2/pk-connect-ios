//
//  RecommendationVC.swift
//  PKConnect
//
//  Created by admin on 12/12/23.
//

import UIKit

class RecommendationVC: UIViewController {

    @IBOutlet weak var lblGoal: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickBack(_ sender: Any) {
    }
    @IBAction func onClickSubmit(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Journey", bundle: nil)
        let learnvc = storyBoard.instantiateViewController(withIdentifier: "LearningModuleVC") as! LearningModuleVC
        self.navigationController?.pushViewController(learnvc, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
