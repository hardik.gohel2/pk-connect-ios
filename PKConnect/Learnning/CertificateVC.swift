//
//  CertificateVC.swift
//  PKConnect
//
//  Created by admin on 16/12/23.
//

import UIKit

class CertificateVC: BaseViewController {

    @IBOutlet weak var viewCerti: UIView!
    @IBOutlet weak var lblTextEng: UILabel!
    @IBOutlet weak var lblTextHindi: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
//
//        if let fullName = getRetrievedUser()?.user_name,
//           let quizTitle = quizEndData?.quiz_title,
//           let score = quizEndData?.score {
//
//            var attributedString = NSMutableAttributedString(string: "यह प्रमाणपत्र है कि \(fullName) आपने PK Connect ऐप के माध्यम से \(quizTitle) क्विज कॉन्टेस्ट में भाग लिया है। आपने क्विज में \(score) अंक प्राप्त किए हैं। जन सुराज आपको आगे के जीवन में उज्ज्वल भविष्य की कामना करता है।")
//
//            let fullNameRange = (attributedString.string as NSString).range(of: fullName)
//            let quizTitleRange = (attributedString.string as NSString).range(of: quizTitle)
//            let scoreRange = (attributedString.string as NSString).range(of: "\(score)")
//
//            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: fullNameRange)
//            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: quizTitleRange)
//            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: scoreRange)
//
//            lblDescHindi.attributedText = attributedString
//            
//            let attributedString1 = NSMutableAttributedString(string: "This is certificate that has \(fullName) participated in the \(quizTitle) quiz contest via PK Connect App. You have scored \(score) marks in the quiz. Jan Suraaj wishes you a bright future ahead.")
//            
//            let fullNameRange1 = (attributedString1.string as NSString).range(of: fullName)
//            let quizTitleRange1 = (attributedString1.string as NSString).range(of: quizTitle)
//            let scoreRange1 = (attributedString1.string as NSString).range(of: "\(score)")
//
//            attributedString1.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: fullNameRange1)
//            attributedString1.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: quizTitleRange1)
//            attributedString1.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: scoreRange1)
//
//            lblDescEnglish.attributedText = attributedString1
//        }
    }
    

    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func onClickDownload(_ sender: Any) {
        if let viewToCapture = viewCerti {
            if let screenshot = viewToCapture.takeScreenshot() {
                UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil)
                let alert = UIAlertController(title: "Certificate Saved", message: "The Certificate has been saved to your photo album.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(alert, animated: true, completion: nil)
            } else {
                print("Failed to capture screenshot")
            }
        }
    }
    @IBAction func onClickShare(_ sender: Any) {
        if let viewToCapture = viewCerti {
            if let screenshot = viewToCapture.takeScreenshot() {
                UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil)
                let imageToShare = screenshot
                    let activityViewController = UIActivityViewController(activityItems: [imageToShare], applicationActivities: nil)
                    activityViewController.excludedActivityTypes = [
                        .addToReadingList,
                        .assignToContact,
                        
                    ]
                    
                    if let popoverController = activityViewController.popoverPresentationController {
                        popoverController.sourceView = self.view
                        popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                        popoverController.permittedArrowDirections = []
                    }
                    
                    present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
