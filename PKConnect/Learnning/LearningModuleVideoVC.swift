//
//  LearningModuleVideoVC.swift
//  PKConnect
//
//  Created by admin on 13/12/23.
//

import UIKit

class LearningModuleVideoVC: BaseViewController {

    
    @IBOutlet weak var btnStartModule: RoundedButton!
    @IBOutlet weak var tblVideos: UITableView!{
        didSet {
            tblVideos.delegate = self
            tblVideos.dataSource = self
        }
    }
    
    var module_id = ""
    var arrayVideos = [Video]()
    var videoOBJ : VideoData?
    var iscalled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getModulesVideos(module_id: module_id) { responnse in
            self.videoOBJ = responnse
            self.arrayVideos = responnse.data
            DispatchQueue.main.async {
                
                self.tblVideos.reloadData()
                if self.arrayVideos[0].videoWatchFlag == "1"
                {
                    self.btnStartModule.isHidden = false
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LearningModuleVideoVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayVideos.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTVCell", for: indexPath) as! VideoTVCell
        
        if indexPath.row < arrayVideos.count {
            cell.imgLock.isHidden = true
            let dict = arrayVideos[indexPath.row]
            cell.imgVideo.sd_setImage(with: URL(string: dict.videoThumbnail))
            cell.viewPlayer.isHidden = false
            cell.lblTitle.text = dict.videoTitle
            cell.lblDesc.isHidden = true
            
            if  indexPath.row == 0 {
                
                cell.viewLock.isHidden = true
                cell.imgCheck.isHidden = false
                if dict.videoWatchFlag == "0" {
                    cell.imgCheck.image = UIImage(named: "checklist")
                    cell.viewContainer.backgroundColor = .white
                }else{
                    cell.imgCheck.image = UIImage(named: "checklist_green")
                    cell.viewContainer.backgroundColor = UIColor(hexString: "FBEAF7")
                }
                cell.playAction = { self.gotToDetail(index: indexPath.row) }
            }else{
                if dict.videoWatchFlag == "1" {
                    cell.viewLock.isHidden = true
                    cell.imgCheck.isHidden = false
                    cell.imgCheck.image = UIImage(named: "checklist_green")
                    cell.viewContainer.backgroundColor = UIColor(hexString: "FBEAF7")
                    cell.playAction = { self.gotToDetail(index: indexPath.row) }
                }else{
                    if arrayVideos[indexPath.row - 1].videoWatchFlag == "1" {
                        cell.viewLock.isHidden = true
                        cell.imgCheck.isHidden = false
                        cell.imgCheck.image = UIImage(named: "checklist")
                        cell.viewContainer.backgroundColor = .white
                        cell.playAction = { self.gotToDetail(index: indexPath.row) }
                    }else{
                        cell.imgCheck.isHidden = true
                        cell.viewLock.isHidden = false
                    }
                }
            }
        }
        else{
            cell.lblTitle.text = videoOBJ?.moduleTitle
            cell.lblDesc.text = "\(videoOBJ?.numberOfQuestions ?? 0)"
            
            if videoOBJ?.isQuizCompleted == 0 {
                if videoOBJ?.totalWatchedVideos == arrayVideos.count {
                    cell.viewPlayer.isHidden = true
                    cell.imgLock.isHidden = false
                    cell.imgLock.image = UIImage(named: "checklist")
                    cell.viewContainer.backgroundColor = .white
                }else{
                    cell.viewPlayer.isHidden = true
                    cell.imgLock.isHidden = false
                    cell.imgLock.image = UIImage(named: "lock")
                    cell.viewContainer.backgroundColor = .white
                }
            }else{
                cell.viewPlayer.isHidden = true
                cell.imgLock.isHidden = false
                cell.imgLock.image = UIImage(named: "checklist_green")
                cell.viewContainer.backgroundColor = UIColor(hexString: "FBEAF7")
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func gotToDetail (index : Int) {
        let storyBoard = UIStoryboard(name: "Journey", bundle: nil)
        let learnvc = storyBoard.instantiateViewController(withIdentifier: "LearningModuleDetailVC") as! LearningModuleDetailVC
        learnvc.videoData = arrayVideos[index]
        self.navigationController?.pushViewController(learnvc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row >= arrayVideos.count {
            let storyBoard = UIStoryboard(name: "Quiz", bundle: nil)
            let quizvc = storyBoard.instantiateViewController(withIdentifier: "QuizVC") as! QuizVC
            quizvc.isFromLearning = true
            quizvc.module_id = module_id
            self.navigationController?.pushViewController(quizvc, animated: true)
        }
    }
    
    
   
}
