//
//  MyJourneyVC.swift
//  PKConnect
//
//  Created by admin on 12/12/23.
//

import UIKit
import AVKit

class MyJourneyVC: BaseViewController {
    
    
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var viewPlayer: UIView!
    @IBOutlet weak var onClickBackTop: UIView!
    
    var player: AVPlayer?,
        avpController = AVPlayerViewController()
    var timeObserver: Any?
    var isScrubbing: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(stopPlayer), name: Notification.Name("PlayerStop"), object: nil)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        player?.pause()
    }
    
    
    @IBAction func onClickBackTop(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
       
    }
    @IBAction func onClickNotification(_ sender: Any) {
        let vc = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "notificationView") as! NotificationView
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @IBAction func onClickLang(_ sender: Any) {
        changeAppLanguage()
    }
    @IBAction func onClickPlay(_ sender: Any) {
        setVideoURL(urlStr: "https://s3.ap-south-1.amazonaws.com/pkconnect/android1695191207408.mp4")
    }
   
    @IBAction func onClickStart(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "isPassedJourney")
        UserDefaults.standard.synchronize()
        let storyBoard = UIStoryboard(name: "Journey", bundle: nil)
        let learnvc = storyBoard.instantiateViewController(withIdentifier: "PreLearninngQVC") as! PreLearninngQVC
        self.navigationController?.pushViewController(learnvc, animated: true)
    }
    
    
    func setVideoURL(urlStr: String) {
        let videoUrl = URL(string: urlStr)
        player = AVPlayer(url: videoUrl!)
        avpController.player = player
        avpController.view.frame = viewPlayer.bounds
        self.viewPlayer.addSubview(avpController.view)
        player?.play()
        btnPlay.isHidden = true
    }
    
    
@objc func stopPlayer() {
    player?.pause()
    avpController.removeFromParent()
}
    
    
    
   
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
