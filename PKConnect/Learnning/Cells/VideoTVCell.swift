//
//  VideoTVCell.swift
//  PKConnect
//
//  Created by admin on 13/12/23.
//

import UIKit
import AVKit

class VideoTVCell: UITableViewCell {

    @IBOutlet weak var viewContainer: CustomView!{
        didSet {
            viewContainer.layer.cornerRadius = 6.0
            viewContainer.layer.masksToBounds = false
            viewContainer.layer.shadowColor = UIColor.black.cgColor
            viewContainer.layer.shadowOffset = CGSize(width: 0, height: 2)
            viewContainer.layer.shadowRadius = 2
            viewContainer.layer.shadowOpacity = 0.4
            viewContainer.layer.backgroundColor = UIColor.white.cgColor
            
            self.roundCorners(corners: [.allCorners], radius: 12.0)
        }
    }
    @IBOutlet weak var imgLock: UIImageView!
    @IBOutlet weak var viewLock: UIView!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var viewPlayer: UIView!
    
    
    var playAction: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    @IBAction func playVideo(_ sender: Any) {
        self.playAction?()
    }
    

}
