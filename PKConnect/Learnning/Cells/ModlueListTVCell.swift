//
//  ModlueListTVCell.swift
//  PKConnect
//
//  Created by admin on 13/12/23.
//

import UIKit

class ModlueListTVCell: UITableViewCell {

    
    @IBOutlet weak var viewContainer: CustomView!
    @IBOutlet weak var btnLock: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var viewProgress: UIProgressView!
    @IBOutlet weak var lblPer: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
