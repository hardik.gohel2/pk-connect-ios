//
//  LearningModuleDetailVC.swift
//  PKConnect
//
//  Created by admin on 13/12/23.
//

import UIKit
import AVKit

class LearningModuleDetailVC: BaseViewController {

    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var viewPlayer: UIView!
    
    var player: AVPlayer?,
        avpController = AVPlayerViewController()
    var videoData : Video?
    var activityIndicator = UIActivityIndicatorView(style: .medium)
    var iscompleted = false

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(stopPlayer), name: Notification.Name("PlayerStop"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        setupActivityIndicator()
        self.lblTitle.text = videoData?.videoTitle
        self.lblDesc.text = videoData?.videoDescription.removeHTMLTag()
    }
    
    func setupActivityIndicator() {
          activityIndicator.center = viewPlayer.center
          activityIndicator.hidesWhenStopped = true
          view.addSubview(activityIndicator)
      }
    
    @objc func stopPlayer() {
        player?.pause()
        avpController.removeFromParent()
    }
    
    @objc func playerDidFinishPlaying() {
           iscompleted = true
           print("Video completed")
       }
    
    func setVideoURL(urlStr: String) {
           guard let videoUrl = URL(string: urlStr) else { return }
           let asset = AVAsset(url: videoUrl)
           let playerItem = AVPlayerItem(asset: asset)
           player = AVPlayer(playerItem: playerItem)
           avpController.player = player
           avpController.view.frame = viewPlayer.bounds
           viewPlayer.addSubview(avpController.view)

           player?.currentItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.new, .initial], context: nil)

           player?.play()
           btnPlay.isHidden = true
           activityIndicator.startAnimating()
       }
    
    
    
    @IBAction func onClickNext(_ sender: Any) {
        if iscompleted {
            updateWatchVideo(module_id: videoData?.moduleID ?? "", video_id: videoData?.videoID ?? "") { response in
                DispatchQueue.main.async {
                if response.status == "200" {
                   let alertController = UIAlertController(title: "Sucess", message: response.message, preferredStyle: .alert)
                        
                        
                        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                            
                            
                            self.dismiss(animated: true)
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                        
                        
                        alertController.addAction(okAction)
                        
                        
                        self.present(alertController, animated: true, completion: nil)
                    }else{
                        let alertController = UIAlertController(title: "Error", message: "please watch the video to unlock the next video", preferredStyle: .alert)
                        
                        
                        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                            self.dismiss(animated: true)
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                        
                        alertController.addAction(okAction)
                        
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            }
        }
    }
   
    @IBAction func onClickPlay(_ sender: Any) {
        setVideoURL(urlStr: videoData?.videoURL ?? "")
    }
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
            guard let item = object as? AVPlayerItem else { return }

            if keyPath == #keyPath(AVPlayerItem.status) {
                if item.status == .readyToPlay {
                    // Video is ready to play
                    activityIndicator.stopAnimating()
                } else if item.status == .failed || item.status == .unknown {
                    // Handle failure cases
                    activityIndicator.stopAnimating()
                    // You may want to handle error cases here
                }
            }
        }
        
        deinit {
            // Remove observer when not needed anymore
            player?.currentItem?.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status))
        }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
