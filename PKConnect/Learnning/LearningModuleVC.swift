//
//  LearningModuleVC.swift
//  PKConnect
//
//  Created by admin on 12/12/23.
//

import UIKit

class LearningModuleVC: BaseViewController {

   
    @IBOutlet weak var lblCertiDescEng: UILabel!
    @IBOutlet weak var lblCertiDescHindi: UILabel!
    @IBOutlet weak var viewcerti: UIView!
    @IBOutlet weak var viewCertificatePopup: UIView!
    @IBOutlet weak var btnCertificate: RoundedButton!
    
    @IBOutlet weak var tblModules: UITableView!{
        didSet {
            tblModules.delegate = self
            tblModules.dataSource = self
        }
    }
    
    var arrayModules = [Module]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let fullName = getRetrievedUser()?.user_name {

         var attributedString = NSMutableAttributedString(string: "जन सुराज द्वारा यह प्रमाणित किया जाता है कि \(fullName) ने पीके कनेक्ट ऐप के माध्यम से पॉलिटिकल ई-लर्निंग कोर्स को सफलतापूर्वक दिनांक को पूर्ण कर लिया है। जन सुराज \(fullName) के उज्ज्वल भविष्य की कामना करता है।")

         let fullNameRange = (attributedString.string as NSString).range(of: fullName)

         attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: fullNameRange)
         

         lblCertiDescHindi.attributedText = attributedString
         
         let attributedString1 = NSMutableAttributedString(string: "Jan Suraaj certifies that \(fullName) has successfully completed the Political E-learning course through the PK Connect app on. Jan Suraaj wishes \(fullName) a bright future ahead.")
         
         let fullNameRange1 = (attributedString1.string as NSString).range(of: fullName)
         

         attributedString1.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: fullNameRange1)
         

         lblCertiDescEng.attributedText = attributedString1
     }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getModules { respose in
            self.arrayModules = respose
            DispatchQueue.main.async {
                self.tblModules.reloadData()
                if self.arrayModules.allSatisfy({ $0.video_compl_percent == 100 }) {
                    self.btnCertificate.isEnabled = true
                }
            }
        }
    }
    
    @IBAction func onClickCloseCerti(_ sender: Any) {
        self.viewCertificatePopup.isHidden = true
    }
    @IBAction func onCloickShareCerti(_ sender: Any) {
        if let viewToCapture = viewcerti {
            if let screenshot = viewToCapture.takeScreenshot() {
                UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil)
                let imageToShare = screenshot
                    let activityViewController = UIActivityViewController(activityItems: [imageToShare], applicationActivities: nil)
                    activityViewController.excludedActivityTypes = [
                        .addToReadingList,
                        .assignToContact,
                        
                    ]
                    
                    if let popoverController = activityViewController.popoverPresentationController {
                        popoverController.sourceView = self.view
                        popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                        popoverController.permittedArrowDirections = []
                    }
                    
                    present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    @IBAction func onClickDownloadCerti(_ sender: Any) {
        if let viewToCapture = viewcerti {
            if let screenshot = viewToCapture.takeScreenshot() {
                UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil)
                let alert = UIAlertController(title: "Certificate Saved", message: "The Certificate has been saved to your photo album.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(alert, animated: true, completion: nil)
            } else {
                print("Failed to capture screenshot")
            }
        }
    }
    @IBAction func onClickBack(_ sender: Any) {
        if let navigationController = self.navigationController {
            for viewController in navigationController.viewControllers {
                if viewController is SlidingViewController {
                    navigationController.popToViewController(viewController, animated: true)
                    return
                }
            }
        }
    }
    @IBAction func onClickGetYourCerti(_ sender: Any) {
        self.viewCertificatePopup.isHidden = false
    }
    @IBAction func onClickSubmit(_ sender: Any) {
    }
    
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LearningModuleVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayModules.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ModlueListTVCell", for: indexPath) as! ModlueListTVCell
        
        let dict = arrayModules[indexPath.row]
        
        cell.lblPer.text = "\(dict.video_compl_percent ?? 0)%"
        cell.lblTitle.text = dict.module_title
        cell.lblDesc.text = dict.module_desc?.removeHTMLTag()
        cell.viewProgress.progress = Float(dict.video_compl_percent ?? 0) / 100.0
        cell.btnContinue.tag = indexPath.row
        cell.btnContinue.setTitle("Continue".localize(), for: .normal)
        cell.btnContinue.addTarget(self, action:#selector(onClickContinue(_ :)), for: .touchUpInside)
        cell.lblCount.text = "\(indexPath.row + 1)"
        
        if indexPath.row == 0 {
            cell.btnLock.isHidden = true
            cell.btnContinue.isHidden = false
//            if dict.video_compl_percent == 100 {
//                cell.btnLock.isHidden = true
//                cell.btnContinue.isHidden = true
//            }else if dict.video_compl_percent ?? 0 > 0 && dict.video_compl_percent != 100 {
//                cell.btnContinue.isHidden = false
//            }else{
//                cell.btnLock.isHidden = true
//                cell.btnContinue.isHidden = true
//            }
        }else{
            if arrayModules[indexPath.row - 1].video_compl_percent == 100 {
                cell.btnContinue.isHidden = false
                cell.btnLock.isHidden = true
            }else{
                cell.btnLock.isHidden = false
                cell.btnContinue.isHidden = true
            }
           
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    @objc func onClickContinue(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Journey", bundle: nil)
        let learnvc = storyBoard.instantiateViewController(withIdentifier: "LearningModuleVideoVC") as! LearningModuleVideoVC
        learnvc.module_id = arrayModules[sender.tag].module_id ?? ""
        self.navigationController?.pushViewController(learnvc, animated: true)
    }
}
