//
//  QuizModel.swift
//  PKConnect
//
//  Created by admin on 06/11/23.
//

import Foundation


struct QuizData: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: [Quiz]?
    let NEXT: String
    @StringForcible var TOTAL: String?
}

struct Quiz: Codable {
    let id: Int
    let title: String
    let description: String
    let instructions: String
    let url: String
    let image: String
    let start_date: String
    let end_date: String
    let quiz_status: String
    let duration: Int
    let total_questions: Int
    let total_attempts: Int
    let user_attempted: Int
    let user_started_time: String?
    let submitted_time: String?
    let user_submitted: Int
    let user_attempt_id: Int
    let user_play_status: String
    let available_play_time_in_sec: Int
    let user_score: UserScore?
    let participants: [Participant]?
}

struct UserScore: Codable {
    let rank: Int
    let total_correct_ans: Int
    let score: Int
}

struct Participant: Codable {
    let user_id: Int
    let user_image: String?
    let full_name: String?
}



// Start Quiz Model

struct QuizResponse: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: QuizResult?
}

struct QuizResult: Codable {
    let attempt_id: Int?
    let quiz_questions: Int?
    let total_correct_ans: Int?
    let total_attempted_questions: Int?
    let questions: [Question]?
}

struct Question: Codable {
    let question_id: Int?
    let quiz_id: Int?
    let question: String?
    let is_attempted_by_user: Bool?
    let options: [Option]?
}

struct Option: Codable {
    let option_id: Int?
    let option: String?
    let is_answer: Int?
}


// submit answer model

struct QuizResponseSubmit: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: QuizResultSubmit?
}

struct QuizResultSubmit: Codable {
    let questions: QuestionDetails?
}

struct QuestionDetails: Codable {
    let status: Bool?
    let is_correct_answer: Bool?
    let correct_option_id: String?
}


// Quiz End

struct QuizEndResponse: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: QuizEndResult
}

struct QuizEndResult: Codable {
    let attempt_id: String
    let quiz_id: Int
    let quiz_title: String
    let quiz_image: String
    let rank: Int
    let score: Int
    let start_date: String
    let end_date: String
    let total_questions: Int
    let total_attempts: Int
}


// Quiz LeaderBoard

struct QuizLeaderboardResponse: Codable {
    let CODE: Int
    let MESSAGE: String
    let RESULT: LeaderboardArray?
}

struct LeaderboardArray: Codable {
    let leaderboard: [Leaderboard]?
}

struct Leaderboard: Codable {
    let rank: Int
    let attempt_id: Int
    let total_correct_ans: Int
    let score: Int
    let user_id: Int
    let user_image: String
    let full_name: String
    let user_play_duration: String
}
