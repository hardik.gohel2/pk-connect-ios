//
//  QuizListingTVCell.swift
//  PKConnect
//
//  Created by admin on 06/11/23.
//

import UIKit

class QuizListingTVCell: UITableViewCell {

    @IBOutlet weak var clickedUser3: UIImageView!
    @IBOutlet weak var cotainerView: UIView!
    @IBOutlet weak var btnActive: UIButton!
    @IBOutlet weak var btnShare: RoundedButton!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var clickedUser2: UIImageView!
    @IBOutlet weak var clickedUser1: UIImageView!
    @IBOutlet weak var stkPeople: UIStackView!
    @IBOutlet weak var lblQuizDuration: UILabel!
    @IBOutlet weak var lblCountQuestion: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgQuiz: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    private func setupCell() {
            
        self.cotainerView.layer.cornerRadius = 6.0
        self.cotainerView.layer.masksToBounds = false
        self.cotainerView.layer.shadowColor = UIColor.black.cgColor
        self.cotainerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.cotainerView.layer.shadowRadius = 2
        self.cotainerView.layer.shadowOpacity = 0.4
        self.cotainerView.layer.backgroundColor = UIColor.white.cgColor
        
        self.roundCorners(corners: [.topLeft, .topRight], radius: 6.0)
                
        }
    
    override func layoutSubviews() {
           super.layoutSubviews()
           
           layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
       }

   
    
}
