//
//  QuizLeaderboardVC.swift
//  PKConnect
//
//  Created by admin on 24/11/23.
//

import UIKit

class QuizLeaderboardVC: BaseViewController {
    
    
    @IBOutlet weak var tblUsers: UITableView!{
        didSet {
            tblUsers.delegate = self
            tblUsers.dataSource = self
        }
    }
    
    
    @IBOutlet weak var viewUser3: UIView!
    @IBOutlet weak var viewUser2: UIView!
    @IBOutlet weak var viewUser1: UIView!
    @IBOutlet weak var lblUser3Score: UILabel!
    @IBOutlet weak var imgUser3: UIImageView!
    @IBOutlet weak var lblUser3Nm: UILabel!
    @IBOutlet weak var lblUser2Score: UILabel!
    @IBOutlet weak var imgUser2: UIImageView!
    @IBOutlet weak var lblUser2Nm: UILabel!
    @IBOutlet weak var lblScoreUser1: UILabel!
    @IBOutlet weak var imgUser1: UIImageView!
    @IBOutlet weak var lblUser1Nm: UILabel!
    @IBOutlet weak var imgMask: UIImageView!
    @IBOutlet weak var viewMask: UIView!
    
    var quiz_id = 0
    var arrUsers = [Leaderboard]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        viewMask.layer.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        viewMask.layer.cornerRadius = 40
        viewMask.translatesAutoresizingMaskIntoConstraints = false
        
        showLoadingIndicator(in: self.view)
        getQuizLeaderboard(quiz_id: quiz_id) { response in
           self.arrUsers = response.RESULT?.leaderboard ?? []
                DispatchQueue.main.async {
                    self.hideLoadingIndicator()
                    if response.CODE != 200 {
                        let alertController = UIAlertController(title: "Error", message: response.MESSAGE, preferredStyle: .alert)
                        
                        
                        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                            self.dismiss(animated: true)
                            for viewController in (self.navigationController?.viewControllers ?? []).reversed() {
                                   if let dynamicVC = viewController as? QuizListingVC {
                                       self.navigationController?.popToViewController(dynamicVC, animated: true)
                                       break
                                   }
                               }
                        }
                        
                        
                        alertController.addAction(okAction)
                        
                        
                        self.present(alertController, animated: true, completion: nil)
                    }else{
                    
                    self.setUsers()
                }
            }
        }
        
    }
    
    @IBAction func onClickBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func setUsers() {
        if arrUsers.count > 3 {
            setUser1()
            setUser2()
            setUser3()
            
            DispatchQueue.main.async {
                self.arrUsers.removeFirst(3)
                self.tblUsers.reloadData()
            }
        }else if arrUsers.count >= 2 {
            let queue = DispatchQueue(label: "com.pk.connect.userQueue", attributes: .concurrent)
            
            
            self.setUser1()
            self.setUser2()
            
        }else{
            self.setUser1()
            
            
        }
    }
    
    
    func setUser1() {
        viewUser1.isHidden = false
        lblUser1Nm.text = arrUsers[0].full_name
        lblScoreUser1.text = "\(arrUsers[0].score)"
        imgUser1.sd_setImage(with: URL(string:arrUsers[0].user_image), placeholderImage: UIImage(named: "user_44"))
    }
    
    func setUser2() {
        viewUser2.isHidden = false
        lblUser2Nm.text = arrUsers[1].full_name
        lblUser2Score.text = "\(arrUsers[1].score)"
        imgUser2.sd_setImage(with: URL(string:arrUsers[1].user_image), placeholderImage: UIImage(named: "user_44"))
    }
    
    
    func setUser3() {
        viewUser3.isHidden = false
        lblUser3Nm.text = arrUsers[2].full_name
        lblUser3Score.text = "\(arrUsers[2].score)"
        imgUser3.sd_setImage(with: URL(string:arrUsers[2].user_image), placeholderImage: UIImage(named: "user_44"))
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension QuizLeaderboardVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUsers.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderboardTVCell", for: indexPath) as! LeaderboardTVCell
        
        let data = arrUsers[indexPath.row]
        let labelText = "\(data.rank)th"
        
        let superscriptAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 12),
            .baselineOffset: 5
        ]
        
        if arrUsers.count > 10 {
            if indexPath.row <= 10 {
                let attributedString = NSMutableAttributedString(string: labelText)
                let range = (labelText as NSString).range(of: "th")
                attributedString.addAttributes(superscriptAttributes, range: range)
                
                cell.lblNumber.attributedText = attributedString
                
                cell.imgUser.sd_setImage(with: URL(string:data.user_image), placeholderImage: UIImage(named: "user_44"))
                cell.lblUserScore.text = "\(data.score)"
                cell.imgUserNm.text = data.full_name
                let totalSeconds = Int(data.user_play_duration) ?? 0
                let formattedTime = formatTimeFromSeconds(totalSeconds)
                
                
                cell.lblDuration.text = "Completed in \(formattedTime)"
                
            }else{
                cell.lblNumber.text = "\(data.rank)"
            }
        }else{
            let attributedString = NSMutableAttributedString(string: labelText)
            let range = (labelText as NSString).range(of: "th")
            attributedString.addAttributes(superscriptAttributes, range: range)
            
            cell.lblNumber.attributedText = attributedString
            cell.imgUserNm.text = data.full_name
            cell.imgUser.sd_setImage(with: URL(string:data.user_image), placeholderImage: UIImage(named: "user_44"))
            cell.lblUserScore.text = data.user_image
            
            let totalSeconds = Int(data.user_play_duration) ?? 0
            let formattedTime = formatTimeFromSeconds(totalSeconds)
            
            
            cell.lblDuration.text = "Completed in \(formattedTime)"
            
        }
        cell.lblUserScore.text = "\(data.score)"
        
        return cell
    }
    
    func formatTimeFromSeconds(_ seconds: Int) -> String {
        let hours = seconds / 3600
        let minutes = (seconds % 3600) / 60
        let remainingSeconds = seconds % 60
        
        var formattedTime = ""
        
        if hours > 0 {
            formattedTime += "\(hours)h "
        }
        
        if minutes > 0 {
            formattedTime += "\(minutes)m "
        }
        
        if remainingSeconds > 0 {
            formattedTime += "\(remainingSeconds)s"
        }
        
        return formattedTime
    }
}
