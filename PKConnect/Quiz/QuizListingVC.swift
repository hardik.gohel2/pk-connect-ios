//
//  QuizListingVC.swift
//  PKConnect
//
//  Created by admin on 06/11/23.
//

import UIKit

class QuizListingVC: BaseViewController {
    
    @IBOutlet weak var tblQuiz: UITableView!{
        didSet{
            tblQuiz.delegate = self
            tblQuiz.dataSource = self
            tblQuiz.register(UINib(nibName: "QuizListingTVCell", bundle: nil), forCellReuseIdentifier: "QuizListingTVCell")
        }
    }
    
    @IBOutlet weak var lbNoRecord: UILabel!
    @IBOutlet weak var viewCompleted: UIView!
    @IBOutlet weak var btnCompleted: UIButton!
    @IBOutlet weak var viewPending: UIView!
    @IBOutlet weak var btnPending: UIButton!
    
    var isPendingTask = true
    var pageIndex = 1
    var user_attempted = false
    var arrayQuiz : [Quiz]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        onClickPending((Any).self)
    }
   
    @IBAction func onClickPending(_ sender: Any) {
        
        isPendingTask = true
        pageIndex = 1
        viewCompleted.backgroundColor = .white
        viewPending.backgroundColor = appYelloColor
        btnPending.titleLabel?.font = UIFont(name: "OpenSans-Semibold", size: 16.0)
        btnCompleted.titleLabel?.font = UIFont(name: "OpenSans-Regular", size: 15.0)
        user_attempted = false
        callQuiz(user_attempted: false)
        
        
    }
    
    @IBAction func onClickCompleted(_ sender: Any) {
        
        isPendingTask = false
        
        pageIndex = 1
        
        viewPending.backgroundColor = .white
        viewCompleted.backgroundColor = appYelloColor
        btnCompleted.titleLabel?.font = UIFont(name: "OpenSans-Semibold", size: 16.0)
        btnPending.titleLabel?.font = UIFont(name: "OpenSans-Regular", size: 15.0)
        user_attempted = false
        callQuiz(user_attempted: true)
        
        
    }
    
    func callQuiz(user_attempted : Bool) {
        getAllQuiz(page: String(pageIndex), user_attempted: user_attempted) { response in
            DispatchQueue.main.async {
                if response.CODE == 202 {
                    self.lbNoRecord.isHidden = false
                    self.arrayQuiz?.removeAll()
                    self.tblQuiz.reloadData()
                }else {
                    self.lbNoRecord.isHidden = true
                    self.arrayQuiz = response.RESULT ?? []
                    self.tblQuiz.reloadData()
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension QuizListingVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayQuiz?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuizListingTVCell", for: indexPath) as! QuizListingTVCell
        cell.imgQuiz.sd_setImage(with: URL(string: arrayQuiz?[indexPath.row].image ?? ""))
        cell.lblTitle.text = arrayQuiz?[indexPath.row].title
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        
        if let startDate = dateFormatter.date(from: arrayQuiz?[indexPath.row].start_date ?? ""),
           let endDate = dateFormatter.date(from: arrayQuiz?[indexPath.row].end_date ?? "") {
            
            let outputDateFormatter = DateFormatter()
            outputDateFormatter.dateFormat = "dd/MM/yyyy"
            
            let formattedStartDate = outputDateFormatter.string(from: startDate)
            let formattedEndDate = outputDateFormatter.string(from: endDate)
            
            let formattedDateRange = "\(formattedStartDate) - \(formattedEndDate)"
            cell.lblTime.text = formattedDateRange
        }
        cell.lblCountQuestion.text = "\(arrayQuiz?[indexPath.row].total_questions ?? 0) Questions"
        cell.lblQuizDuration.text = "\(arrayQuiz?[indexPath.row].duration ?? 0) Minutes"
        
        
        if self.arrayQuiz?[indexPath.row].participants?.count == 0 || self.arrayQuiz?[indexPath.row].participants?.count ?? 0 <= 2 {
            cell.lblLike.isHidden = true
            cell.clickedUser1.isHidden = true
            cell.clickedUser2.isHidden = true
            cell.clickedUser3.isHidden = true
            
        } else {
            cell.clickedUser1.isHidden = false
            cell.clickedUser2.isHidden = false
            cell.clickedUser3.isHidden = false
            cell.lblLike.isHidden = false
            let total = ((self.arrayQuiz?[indexPath.row].total_attempts ?? 0) - (self.arrayQuiz?[indexPath.row].participants?.count ?? 0))
            cell.lblLike.text = "+ \(total) Participated".localize()
            
            downloadImageFrom(urlString: self.arrayQuiz?[indexPath.row].participants?[0].user_image ?? "") { downloadedImage in
                DispatchQueue.main.async {
                    cell.clickedUser1.layer.cornerRadius = 15
                    cell.clickedUser1.clipsToBounds = true
                    cell.clickedUser1.image = downloadedImage
                }
            }
            downloadImageFrom(urlString: self.arrayQuiz?[indexPath.row].participants?[1].user_image ?? "") { downloadedImage in
                DispatchQueue.main.async {
                    cell.clickedUser2.layer.cornerRadius = 15
                    cell.clickedUser2.clipsToBounds = true
                    cell.clickedUser2.image = downloadedImage
                }
            }
            
            downloadImageFrom(urlString: self.arrayQuiz?[indexPath.row].participants?[2].user_image ?? "") { downloadedImage in
                DispatchQueue.main.async {
                    cell.clickedUser3.layer.cornerRadius = 15
                    cell.clickedUser3.clipsToBounds = true
                    cell.clickedUser3.image = downloadedImage
                }
            }
        }
        if arrayQuiz?[indexPath.row].quiz_status == "active" {
            cell.btnShare.isHidden = false
            cell.btnActive.setImage(UIImage(named: "ic_active") , for: .normal)
        }else{
            cell.btnActive.setImage(UIImage(named: "ic_inactive")  , for: .normal)
        }
        cell.btnShare.tag = indexPath.row
        cell.btnShare.addTarget(self, action: #selector(shareButtonTapped), for: .touchUpInside)
        
        return cell
    }
    
    
    @objc func shareButtonTapped(_ sender: RoundedButton){
//        shareDeepLink(controller: self, quiz_id: arrayQuiz?[sender.tag].id ?? 0, message: "") {
//            let alertController = UIAlertController(title: "Sucess", message: "Quiz shared Successfully", preferredStyle: .alert)
//
//
//            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
//                self.dismiss(animated: true)
//                self.navigationController?.popViewController(animated: true)
//            }
//
//
//            alertController.addAction(okAction)
//
//
//            self.present(alertController, animated: true, completion: nil)
//        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrayQuiz?[indexPath.row].quiz_status == "active"{
            let storyBoard = UIStoryboard(name: "Quiz", bundle: nil)
            let quizvc = storyBoard.instantiateViewController(withIdentifier: "QuizDetailVC") as! QuizDetailVC
            if let data = arrayQuiz?[indexPath.row] {
                quizvc.quizData = data
            }
            if isPendingTask {
                quizvc.isFromComplete = false
                self.navigationController?.pushViewController(quizvc, animated: true)
            }else{
                let storyBoard = UIStoryboard(name: "Quiz", bundle: nil)
                let quizvc = storyBoard.instantiateViewController(withIdentifier: "QuizLeaderboardVC") as! QuizLeaderboardVC
                quizvc.quiz_id = arrayQuiz?[indexPath.row].id ?? 0
                self.navigationController?.pushViewController(quizvc, animated: true)
            }
            
        }else{
            let storyBoard = UIStoryboard(name: "Quiz", bundle: nil)
            let quizvc = storyBoard.instantiateViewController(withIdentifier: "QuizDetailVC") as! QuizDetailVC
            if let data = arrayQuiz?[indexPath.row] {
                quizvc.quizData = data
            }
            quizvc.isFromComplete = true
            self.navigationController?.pushViewController(quizvc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}
