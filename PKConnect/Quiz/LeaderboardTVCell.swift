//
//  LeaderboardTVCell.swift
//  PKConnect
//
//  Created by admin on 24/11/23.
//

import UIKit

class LeaderboardTVCell: UITableViewCell {

    @IBOutlet weak var ViewContainer: UIView!{
        didSet {
            var shadows = UIView()
            shadows.frame = ViewContainer.frame
            shadows.clipsToBounds = false
            ViewContainer.addSubview(shadows)
            let shadowPath0 = UIBezierPath(roundedRect: shadows.bounds, cornerRadius: 16)
            let layer0 = CALayer()
            layer0.shadowPath = shadowPath0.cgPath
            layer0.shadowColor = UIColor(red: 0.534, green: 0.479, blue: 0.65, alpha: 0.08).cgColor
            layer0.shadowOpacity = 1
            layer0.shadowRadius = 40
            layer0.shadowOffset = CGSize(width: 0, height: 4)
        }
    }
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblUserScore: UILabel!
    @IBOutlet weak var imgUserNm: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
