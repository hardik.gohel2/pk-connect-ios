//
//  QuizVC.swift
//  PKConnect
//
//  Created by admin on 07/11/23.
//

import UIKit

class QuizVC: BaseViewController {
    
    
    @IBOutlet weak var viewTotaldetail: UIView!
    @IBOutlet weak var btnSubmit: RoundedButton!
    @IBOutlet weak var opt4: RoundedButton!{
        didSet {
            opt4.tag = 3
           
        }
    }
    @IBOutlet weak var opt3: RoundedButton!{
        didSet {
            opt3.tag = 2
        }
    }
    @IBOutlet weak var opt2: RoundedButton!{
        didSet {
            opt2.tag = 1
        }
    }
    @IBOutlet weak var opt1: RoundedButton!{
        didSet {
            opt1.tag = 0
        }
    }
    @IBOutlet weak var viewEndQuiz: UIView!
    @IBOutlet weak var viewEdQuizPopup: UIView!
    @IBOutlet weak var imgOPt4: UIImageView!
    @IBOutlet weak var imgOpt3: UIImageView!
    @IBOutlet weak var imgOpt2: UIImageView!
    @IBOutlet weak var imgOpt1: UIImageView!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblQNNumber: UILabel!
    @IBOutlet weak var lblTotalRemaining: UILabel!
    @IBOutlet weak var lblCorrect: UILabel!
    @IBOutlet weak var lblAttempt: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblHeadTime: UILabel!
    
    var timer: Timer?
    var quizData : Quiz?
    var seconds = 0
    var arrayQuiz : QuizResult?
    var arrQuestions = [Question]()
    var index = 0
    var tag = -1
    var response : QuizEndResponse?
    var isFromLearning = false
    var module_id = ""
    var arrayModuleQ = [QuizDataLearnData]()
    var arrAns = [[String : String]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !isFromLearning {
            callstartQuiz()
            seconds = quizData?.available_play_time_in_sec ?? 0
            updateTimerLabel()
            startTimer()
        }else{
            opt1.ViewCornerRadius = 18
            opt2.ViewCornerRadius = 18
            opt3.ViewCornerRadius = 18
            opt4.ViewCornerRadius = 18
            self.lblTotalRemaining.isHidden = true
            self.lblHeadTime.isHidden = true
            viewTotaldetail.isHidden = true
            viewEndQuiz.isHidden = true
            
           getModuleQuestions()
        }
        resetAll()
        
    }
    
    func getModuleQuestions() {
        self.getModuleQuestions(module_id: module_id) { response in
            self.arrayModuleQ = response.data
            DispatchQueue.main.async {
                self.updateData()
                if self.index == self.arrayModuleQ.count {
                    self.btnSubmit.backgroundColor = appYelloColor
                    self.btnSubmit.ViewBorderWidth = 0.0
                    self.btnSubmit.setTitle("Submit Answer".localize(), for: .normal)
                    self.btnSubmit.localizableText = "Submit Answer".localize()
                }
            }
        }
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @IBAction func onClickOPT(_ sender: RoundedButton) {
        resetAll()
        self.btnSubmit.isEnabled = true
        self.btnSubmit.backgroundColor = appYelloColor
        self.btnSubmit.ViewBorderWidth = 0.0
        self.btnSubmit.setTitle("Submit Answer".localize(), for: .normal)
        self.btnSubmit.localizableText = "Submit Answer".localize()
        if isFromLearning {
            sender.setTitleColor(.white, for: .normal)
            sender.setImage(UIImage(named: "radio_button_checked"), for: .normal)
            sender.tintColor = .white
            sender.ViewBorderWidth = 1.0
            sender.backgroundColor = .black
            sender.tintColor = .white
        }else{
            sender.backgroundColor = UIColor.darkGray
            sender.setTitleColor(.white, for: .normal)
        }
        tag = sender.tag
        
    }
    @IBAction func onClickEndQuiz(_ sender: Any) {
        if isFromLearning {
            self.navigationController?.popViewController(animated: true)
        } else {
            callEndQuiz()
        }
    }
    
    @IBAction func onClickViewScore(_ sender: Any) {
        
        if isFromLearning {
            if let navigationController = self.navigationController {
                for viewController in navigationController.viewControllers {
                    if viewController is LearningModuleVC {
                        navigationController.popToViewController(viewController, animated: true)
                        return
                    }
                }
            }
        }else {
            let storyBoard = UIStoryboard(name: "Quiz", bundle: nil)
            let quizvc = storyBoard.instantiateViewController(withIdentifier: "CompleteQuizVC") as! CompleteQuizVC
            quizvc.quizEndData = response?.RESULT
            self.navigationController?.pushViewController(quizvc, animated: true)
        }
    }
    
    @IBAction func onnClickClose(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
   
    
    @IBAction func onClickSubmit(_ sender: RoundedButton) {
        
        if sender.currentTitle == "Submit Answer" .localize() || sender.currentTitle == "Submit" .localize(){
            if tag < 0 {
                let alertController = UIAlertController(title: "Error", message: "please select at least one option", preferredStyle: .alert)
                
                
                let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    self.dismiss(animated: true)
                }
                
                
                alertController.addAction(okAction)
                
                
                self.present(alertController, animated: true, completion: nil)
            }else{
                if isFromLearning {
                if self.index !=  self.arrayModuleQ.count {
                   let dict = ["option_id": arrayModuleQ[index].options[tag].option_id,"ques_id": arrayModuleQ[index].question_id]
                    arrAns.append(dict)
                    self.resetAll()
                    self.progressView.progress = Float(self.index) / 10.0
                    
                        self.setButtons()
                        
                    }else {
                        callLearnSubmit(module_id: module_id, responses: arrAns)
                    }
                    self.index = self.index + 1
                    
                }else{
                    callSubmitAnswer(tag: tag, question_id: arrQuestions[index].question_id ?? 0, quiz_id: arrQuestions[index].quiz_id ?? 0, attempt_id: arrayQuiz?.attempt_id ?? 0, option_id: arrQuestions[index].options?[tag].option_id ?? 0)
                }
            }
        }else{
            if isFromLearning {
                if self.index <  self.arrayModuleQ.count {
                    self.updateData()
                    self.resetAll()
                }
            }else{
                if self.index !=  self.arrQuestions.count {
                    self.updateData()
                    self.resetAll()
                    
                }
            }
            
        }
        
    }
    
    
    
    func callEndQuiz()
    {
        showLoadingIndicator(in: self.view)
        EndQuiz(quiz_id: arrQuestions[index].quiz_id ?? 0, attempt_id: arrayQuiz?.attempt_id ?? 0) { response in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                self.response = response
                let alertController = UIAlertController(title: "Success", message: response.MESSAGE, preferredStyle: .alert)
                
                
                let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    self.dismiss(animated: true)
                    self.viewEdQuizPopup.isHidden = false
                }
                
                
                alertController.addAction(okAction)
                
                
                self.present(alertController, animated: true, completion: nil)
            }
            
        }
    }
    
    func resetAll() {
       
        if isFromLearning {
            imgOpt1.isHidden = true
            imgOpt2.isHidden = true
            imgOpt3.isHidden = true
            imgOPt4.isHidden = true
            self.opt1.backgroundColor = .white
            self.opt1.setTitleColor(.black, for: .normal)
            self.opt1.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
            self.opt1.tintColor = .black
            self.opt1.tintColor = .black
            self.opt1.ViewBorderWidth = 1.0
            self.opt1.ViewBorderColor = .black
            
            self.opt2.backgroundColor = .white
            self.opt2.setTitleColor(.black, for: .normal)
            self.opt2.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
            self.opt2.tintColor = .black
            self.opt2.tintColor = .black
            self.opt2.ViewBorderWidth = 1.0
            self.opt2.ViewBorderColor = .black
            
            self.opt3.backgroundColor = .white
            self.opt3.setTitleColor(.black, for: .normal)
            self.opt3.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
            self.opt3.tintColor = .black
            self.opt3.tintColor = .black
            self.opt3.ViewBorderWidth = 1.0
            self.opt3.ViewBorderColor = .black
            
            self.opt4.backgroundColor = .white
            self.opt4.setTitleColor(.black, for: .normal)
            self.opt4.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
            self.opt4.tintColor = .black
            self.opt4.tintColor = .black
            self.opt4.ViewBorderWidth = 1.0
            self.opt4.ViewBorderColor = .black
        }else{
            opt1.backgroundColor = UIColor.systemGroupedBackground
            opt1.setTitleColor(.black, for: .normal)
            opt2.backgroundColor = UIColor.systemGroupedBackground
            opt2.setTitleColor(.black, for: .normal)
            opt3.backgroundColor = UIColor.systemGroupedBackground
            opt3.setTitleColor(.black, for: .normal)
            opt4.backgroundColor = UIColor.systemGroupedBackground
            opt4.setTitleColor(.black, for: .normal)
            opt1.ViewBorderWidth = 0.0
            opt2.ViewBorderWidth = 0.0
            opt3.ViewBorderWidth = 0.0
            opt4.ViewBorderWidth = 0.0
            self.opt1.setImage(UIImage(), for: .normal)
            self.opt2.setImage(UIImage(), for: .normal)
            self.opt3.setImage(UIImage(), for: .normal)
            self.opt4.setImage(UIImage(), for: .normal)
            imgOpt1.isHidden = true
            imgOpt2.isHidden = true
            imgOpt3.isHidden = true
            imgOPt4.isHidden = true
        }
    }
    
    @objc func updateTimer() {
        if seconds > 0 {
            seconds -= 1
            updateTimerLabel()
        } else {
            timer?.invalidate()
            callEndQuiz()
        }
    }
    
    func updateTimerLabel() {
        let minutes = seconds / 60
        let seconds = seconds % 60
        lblTotalRemaining.text = String(format: "%02d:%02d", minutes, seconds)
    }
    
    
    func callstartQuiz() {
        showLoadingIndicator(in: self.view)
        startQuiz(quiz_id: String(quizData?.id ?? 0)) { response in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                self.arrayQuiz = response.RESULT
                self.arrQuestions = self.arrayQuiz?.questions ?? []
                DispatchQueue.main.async {
                    self.updateData()
                    if self.index == self.arrQuestions.count {
                        self.btnSubmit.backgroundColor = appYelloColor
                        self.btnSubmit.ViewBorderWidth = 0.0
                        self.btnSubmit.setTitle("Submit Answer".localize(), for: .normal)
                        self.btnSubmit.localizableText = "Submit Answer".localize()
                    }
                }
                
            }
        }
        
    }
    
    func updateData() {
        self.opt1.isUserInteractionEnabled = true
        self.opt2.isUserInteractionEnabled = true
        self.opt3.isUserInteractionEnabled = true
        self.opt4.isUserInteractionEnabled = true
        if isFromLearning {
            lblQuestion.text = "\(index+1). \(arrayModuleQ[index].question_text )"
            opt1.setTitle(arrayModuleQ[index].options[0].option_text, for: .normal)
            opt2.setTitle(arrayModuleQ[index].options[1].option_text, for: .normal)
            opt3.setTitle(arrayModuleQ[index].options[2].option_text, for: .normal)
            opt4.setTitle(arrayModuleQ[index].options[3].option_text, for: .normal)
            lblQNNumber.text = "\(index + 1)/\(arrayModuleQ.count)"
        }else {
            lblQuestion.text = "\(index+1). \(arrQuestions[index].question ?? "")"
            opt1.setTitle(arrQuestions[index].options?[0].option, for: .normal)
            opt2.setTitle(arrQuestions[index].options?[1].option, for: .normal)
            opt3.setTitle(arrQuestions[index].options?[2].option, for: .normal)
            opt4.setTitle(arrQuestions[index].options?[3].option, for: .normal)
            lblQNNumber.text = "\(index + 1)/\(arrQuestions.count)"
        }
    }
    
    func completeModule() {
        completeModule(module_id: module_id) { response in
           
        }
    }
    
    func callSubmitAnswer(tag: Int, question_id : Int, quiz_id : Int, attempt_id: Int, option_id : Int) {
        self.showLoadingIndicator(in: self.view)
        
        submitAnswer(question_id: question_id, quiz_id: quiz_id, attempt_id: attempt_id, option_id: option_id) { response in
            
            
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                
                if response.CODE != 200 {
                    
                    let alertController = UIAlertController(title: "Error", message: response.MESSAGE, preferredStyle: .alert)
                    
                    
                    let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        self.dismiss(animated: true)
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    
                    alertController.addAction(okAction)
                    
                    
                    self.present(alertController, animated: true, completion: nil)
                }else {
                   
                    self.resetAll()
                    self.progressView.progress = Float(self.index) / 10.0
                    if self.index  <  self.arrQuestions.count - 1 {
                        self.setButtons(response: response)
                        
                    }else{
                        self.callEndQuiz()
                    }
                    self.index = self.index + 1
                    
                    
                   
                }
                    
            }
            
        }
    }
    
    
    func callLearnSubmit(module_id : String, responses : [[String:String]]) {
       
        submitLearnQ(module_id: module_id, responses: responses) { response in
            
            DispatchQueue.main.async {
                if response.status != "200" {
                    let alertController = UIAlertController(title: "Error", message: response.message, preferredStyle: .alert)
                    
                    
                    let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        self.dismiss(animated: true)
                        if let navigationController = self.navigationController {
                            self.completeModule()
                            for viewController in navigationController.viewControllers {
                                if viewController is LearningModuleVC {
                                    navigationController.popToViewController(viewController, animated: true)
                                    return
                                }
                            }
                        }
                    }
                    
                    
                    alertController.addAction(okAction)
                    
                    
                    self.present(alertController, animated: true, completion: nil)
                }else {
                    
                        self.viewEdQuizPopup.isHidden = false
                    
                }
                    
            }
        }
    }
    
    
    func setButtons() {
        self.opt1.isUserInteractionEnabled = false
        self.opt2.isUserInteractionEnabled = false
        self.opt3.isUserInteractionEnabled = false
        self.opt4.isUserInteractionEnabled = false
        if index == arrayModuleQ.count - 1 {
            self.btnSubmit.setTitle("Submit".localize(), for: .normal)
            self.btnSubmit.localizableText = "Submit".localize()
            self.btnSubmit.backgroundColor = appYelloColor
            self.btnSubmit.ViewBorderWidth = 0.0
        }else{
            self.btnSubmit.setTitle("Next Question".localize(), for: .normal)
            self.btnSubmit.localizableText = "Next Question".localize()
            
            self.btnSubmit.backgroundColor = .white
            self.btnSubmit.ViewBorderColor = appYelloColor
            self.btnSubmit.ViewBorderWidth = 1.0
        }
        if arrayModuleQ[index].options[tag].correct_ans == "1" {
            switch tag {
            case 0 :
                self.opt1.backgroundColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                self.opt1.setTitleColor(.white, for: .normal)
                self.imgOpt1.image = UIImage(named: "ic_right_white")
                self.imgOpt1.isHidden = false
            case 1 :
                self.opt2.backgroundColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                self.opt2.setTitleColor(.white, for: .normal)
                self.imgOpt2.image = UIImage(named: "ic_right_white")
                self.imgOpt2.isHidden = false
                
            case 2 :
                self.opt3.backgroundColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                self.opt3.setTitleColor(.white, for: .normal)
                self.imgOpt3.image = UIImage(named: "ic_right_white")
                self.imgOpt3.isHidden = false
                
            case 3 :
                self.opt4.backgroundColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                self.opt4.setTitleColor(.white, for: .normal)
                self.imgOPt4.image = UIImage(named: "ic_right_white")
                self.imgOPt4.isHidden = false
                
            default:
                self.resetAll()
            }
        }else{
            resetAll()
            for i in 0..<(arrayModuleQ[index].options.count) {
                if arrayModuleQ[index].options[i].correct_ans == "1"  {
                    switch i {
                    case 0 :
                        self.opt1.ViewBorderColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                        self.opt1.ViewBorderWidth = 1.0
                        self.imgOpt1.image = UIImage(named: "ic_right")
                        self.imgOpt1.isHidden = false
                    case 1 :
                        self.opt2.ViewBorderColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                        self.opt2.ViewBorderWidth = 1.0
                        self.imgOpt2.image = UIImage(named: "ic_right")
                        self.imgOpt2.isHidden = false
                    case 2 :
                        self.opt3.ViewBorderColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                        self.opt3.ViewBorderWidth = 1.0
                        self.imgOpt3.image = UIImage(named: "ic_right")
                        self.imgOpt3.isHidden = false
                    case 3 :
                        self.opt4.ViewBorderColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                        self.opt4.ViewBorderWidth = 1.0
                        self.imgOPt4.image = UIImage(named: "ic_right")
                        self.imgOPt4.isHidden = false
                    default:
                        self.resetAll()
                    }
                }else {
                    switch tag {
                    case 0 :
                        self.opt1.backgroundColor = .red
                        self.opt1.setTitleColor(.white, for: .normal)
                        self.imgOpt1.image = UIImage(named: "ic_wrong")
                        self.imgOpt1.isHidden = false
                    case 1 :
                        self.opt2.backgroundColor = .red
                        self.opt2.setTitleColor(.white, for: .normal)
                        self.imgOpt2.image = UIImage(named: "ic_wrong")
                        self.imgOpt2.isHidden = false
                    case 2 :
                        self.opt3.backgroundColor = .red
                        self.opt3.setTitleColor(.white, for: .normal)
                        self.imgOpt3.image = UIImage(named: "ic_wrong")
                        self.imgOpt3.isHidden = false
                    case 3 :
                        self.opt4.backgroundColor = .red
                        self.opt4.setTitleColor(.white, for: .normal)
                        self.imgOPt4.image = UIImage(named: "ic_wrong")
                        self.imgOPt4.isHidden = false
                    default:
                        self.resetAll()
                    }
                }
            }
        }
    }
        
    
    
    
    func setButtons(response: QuizResponseSubmit?) {
        self.opt1.isUserInteractionEnabled = false
        self.opt2.isUserInteractionEnabled = false
        self.opt3.isUserInteractionEnabled = false
        self.opt4.isUserInteractionEnabled = false
        self.btnSubmit.setTitle("Next Question".localize(), for: .normal)
        self.btnSubmit.localizableText = "Next Question".localize()
        self.btnSubmit.backgroundColor = .white
        self.btnSubmit.ViewBorderColor = appYelloColor
        self.btnSubmit.ViewBorderWidth = 1.0
        if response?.RESULT?.questions?.is_correct_answer == true {
            if Int(response?.RESULT?.questions?.correct_option_id ?? "") == arrQuestions[index].options?[tag].option_id {
                switch tag {
                case 0 :
                    self.opt1.backgroundColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                    self.opt1.setTitleColor(.white, for: .normal)
                    self.imgOpt1.image = UIImage(named: "ic_right_white")
                    self.imgOpt1.isHidden = false
                   
                case 1 :
                    self.opt2.backgroundColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                    self.opt2.setTitleColor(.white, for: .normal)
                    self.imgOpt2.image = UIImage(named: "ic_right_white")
                    self.imgOpt2.isHidden = false
                    
                case 2 :
                    self.opt3.backgroundColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                    self.opt3.setTitleColor(.white, for: .normal)
                    self.imgOpt3.image = UIImage(named: "ic_right_white")
                    self.imgOpt3.isHidden = false
                    
                case 3 :
                    self.opt4.backgroundColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                    self.opt4.setTitleColor(.white, for: .normal)
                    self.imgOPt4.image = UIImage(named: "ic_right_white")
                    self.imgOPt4.isHidden = false
                    
                default:
                    self.resetAll()
                }
            }
        }else{
            resetAll()
            for i in 0..<(arrQuestions[index].options?.count ?? 0) {
                if arrQuestions[index].options?[i].option_id == Int(response?.RESULT?.questions?.correct_option_id ?? "") {
                    switch i {
                    case 0 :
                        self.opt1.ViewBorderColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                        self.opt1.ViewBorderWidth = 1.0
                        self.imgOpt1.image = UIImage(named: "ic_right")
                        self.imgOpt1.isHidden = false
                    case 1 :
                        self.opt2.ViewBorderColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                        self.opt2.ViewBorderWidth = 1.0
                        self.imgOpt2.image = UIImage(named: "ic_right")
                        self.imgOpt2.isHidden = false
                    case 2 :
                        self.opt3.ViewBorderColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                        self.opt3.ViewBorderWidth = 1.0
                        self.imgOpt3.image = UIImage(named: "ic_right")
                        self.imgOpt3.isHidden = false
                    case 3 :
                        self.opt4.ViewBorderColor = UIColor(red: 72/255.0, green: 170/255.0, blue: 56/255.0, alpha: 1.0)
                        self.opt4.ViewBorderWidth = 1.0
                        self.imgOPt4.image = UIImage(named: "ic_right")
                        self.imgOPt4.isHidden = false
                    default:
                        self.resetAll()
                    }
                }
            }
            switch tag {
            case 0 :
                self.opt1.backgroundColor = .red
                self.opt1.setTitleColor(.white, for: .normal)
                self.imgOpt1.image = UIImage(named: "ic_wrong")
                self.imgOpt1.isHidden = false
            case 1 :
                self.opt2.backgroundColor = .red
                self.opt2.setTitleColor(.white, for: .normal)
                self.imgOpt2.image = UIImage(named: "ic_wrong")
                self.imgOpt2.isHidden = false
            case 2 :
                self.opt3.backgroundColor = .red
                self.opt3.setTitleColor(.white, for: .normal)
                self.imgOpt3.image = UIImage(named: "ic_wrong")
                self.imgOpt3.isHidden = false
            case 3 :
                self.opt4.backgroundColor = .red
                self.opt4.setTitleColor(.white, for: .normal)
                self.imgOPt4.image = UIImage(named: "ic_wrong")
                self.imgOPt4.isHidden = false
            default:
                self.resetAll()
            }
        }
    }
    
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
