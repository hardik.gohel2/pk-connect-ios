//
//  QuizDetailVC.swift
//  PKConnect
//
//  Created by admin on 07/11/23.
//

import UIKit

class QuizDetailVC: UIViewController {
    
    @IBOutlet weak var btnTakeQuiz: RoundedButton!
    @IBOutlet weak var lblInsentiveDesc: UILabel!
    @IBOutlet weak var lblInstructionsDesc: UILabel!
    @IBOutlet weak var lblAboutQuizDesc: UILabel!
    @IBOutlet weak var lblAttempted: UILabel!
    @IBOutlet weak var lblSecDuration: UILabel!
    @IBOutlet weak var lblQuestions: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgQuiz: UIImageView!
    
    var quizData : Quiz?
    var isFromComplete = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDetail()
    }
    
    func setDetail() {
        
        if isFromComplete {
            btnTakeQuiz.localizableText = "View LeaderBoard".localize()
            btnTakeQuiz.setTitle("View LeaderBoard".localize(), for: .normal)
        }
        lblInstructionsDesc.text = quizData?.instructions
        
        let htmlString = quizData?.description
        
        
        if let data = htmlString?.data(using: .utf8),
           let attributedString = try? NSAttributedString(data: data,
                                                          options: [.documentType: NSAttributedString.DocumentType.html],
                                                          documentAttributes: nil) {
            
            
            let nonNewlineAttributedString = NSMutableAttributedString(attributedString: attributedString)
            
            let newlineCharacterSet = CharacterSet.newlines
            nonNewlineAttributedString.mutableString.replaceOccurrences(of: "\n", with: "", options: .literal, range: NSRange(location: 0, length: nonNewlineAttributedString.length))
            
            lblAboutQuizDesc.attributedText = attributedString
            
        }
        lblAttempted.text = "\(quizData?.total_attempts ?? 0)"
        lblSecDuration.text = "\(quizData?.duration ?? 0)"
        lblQuestions.text = "\(quizData?.total_questions ?? 0)"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        if let startDate = dateFormatter.date(from: quizData?.start_date ?? ""),
           let endDate = dateFormatter.date(from: quizData?.end_date ?? "") {
            
            let outputDateFormatter = DateFormatter()
            outputDateFormatter.dateFormat = "dd/MM/yyyy"
            
            let formattedStartDate = outputDateFormatter.string(from: startDate)
            let formattedEndDate = outputDateFormatter.string(from: endDate)
            
            let formattedDateRange = "\(formattedStartDate) - \(formattedEndDate)"
            lblDate.text = formattedDateRange
        }
        lblTitle.text = quizData?.title
        imgQuiz.sd_setImage(with: URL(string: quizData?.image ?? ""))
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickStart(_ sender: RoundedButton) {
        if isFromComplete {
            let storyBoard = UIStoryboard(name: "Quiz", bundle: nil)
            let quizvc = storyBoard.instantiateViewController(withIdentifier: "QuizLeaderboardVC") as! QuizLeaderboardVC
            quizvc.quiz_id = quizData?.id ?? 0
            self.navigationController?.pushViewController(quizvc, animated: true)
        }else{
            let storyBoard = UIStoryboard(name: "Quiz", bundle: nil)
            let quizvc = storyBoard.instantiateViewController(withIdentifier: "QuizVC") as! QuizVC
            quizvc.quizData = quizData
            quizvc.isFromLearning = false
            self.navigationController?.pushViewController(quizvc, animated: true)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
