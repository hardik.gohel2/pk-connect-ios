//
//  CompleteQuizVC.swift
//  PKConnect
//
//  Created by admin on 24/11/23.
//

import UIKit

class CompleteQuizVC: BaseViewController {

    @IBOutlet weak var viewCerti: UIView!
    @IBOutlet weak var lblDescEnglish: UILabel!
    @IBOutlet weak var lblDescHindi: UILabel!
    @IBOutlet weak var imgCerti: UIImageView!
    @IBOutlet weak var viewCertiPopUp: UIView!
    @IBOutlet weak var btnActive: UIButton!
    @IBOutlet weak var lblRank: UILabel!
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgQuiz: UIImageView!
    
    var quizEndData : QuizEndResult?
    var fullname = ""
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblScore.text = "\(quizEndData?.score ?? 0)/\(quizEndData?.total_questions ?? 0)"
        lblRank.text = "\(quizEndData?.rank ?? 0)/\(quizEndData?.total_attempts ?? 0)"
        imgQuiz.sd_setImage(with: URL(string: quizEndData?.quiz_image ?? ""))
        lblTitle.text = quizEndData?.quiz_title
        lblDate.text = "\(quizEndData?.start_date ?? "") - \(quizEndData?.end_date ?? "")"
        if let fullName = getRetrievedUser()?.user_name,
           let quizTitle = quizEndData?.quiz_title,
           let score = quizEndData?.score {

            var attributedString = NSMutableAttributedString(string: "यह प्रमाणपत्र है कि \(fullName) आपने PK Connect ऐप के माध्यम से \(quizTitle) क्विज कॉन्टेस्ट में भाग लिया है। आपने क्विज में \(score) अंक प्राप्त किए हैं। जन सुराज आपको आगे के जीवन में उज्ज्वल भविष्य की कामना करता है।")

            let fullNameRange = (attributedString.string as NSString).range(of: fullName)
            let quizTitleRange = (attributedString.string as NSString).range(of: quizTitle)
            let scoreRange = (attributedString.string as NSString).range(of: "\(score)")

            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: fullNameRange)
            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: quizTitleRange)
            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: scoreRange)

            lblDescHindi.attributedText = attributedString
            
            let attributedString1 = NSMutableAttributedString(string: "This is certificate that has \(fullName) participated in the \(quizTitle) quiz contest via PK Connect App. You have scored \(score) marks in the quiz. Jan Suraaj wishes you a bright future ahead.")
            
            let fullNameRange1 = (attributedString1.string as NSString).range(of: fullName)
            let quizTitleRange1 = (attributedString1.string as NSString).range(of: quizTitle)
            let scoreRange1 = (attributedString1.string as NSString).range(of: "\(score)")

            attributedString1.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: fullNameRange1)
            attributedString1.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: quizTitleRange1)
            attributedString1.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: scoreRange1)

            lblDescEnglish.attributedText = attributedString1
        }
    }
    
    @IBAction func onClickBack(_ sender: UIButton) {
        for viewController in (navigationController?.viewControllers ?? []).reversed() {
               if let dynamicVC = viewController as? QuizListingVC {
                   navigationController?.popToViewController(dynamicVC, animated: true)
                   break
               }
           }
    }
    
    @IBAction func onClickLeaderBoard(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Quiz", bundle: nil)
        let quizvc = storyBoard.instantiateViewController(withIdentifier: "QuizLeaderboardVC") as! QuizLeaderboardVC
        quizvc.quiz_id = quizEndData?.quiz_id ?? 0
        self.navigationController?.pushViewController(quizvc, animated: true)
    }
    
    @IBAction func onClickClose(_ sender: UIButton) {
        self.viewCertiPopUp.isHidden = true
    }
    
    @IBAction func onClickDownloadCerti(_ sender: UIButton) {
        if let viewToCapture = viewCerti {
            if let screenshot = viewToCapture.takeScreenshot() {
                UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil)
                let alert = UIAlertController(title: "Certificate Saved", message: "The Certificate has been saved to your photo album.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(alert, animated: true, completion: nil)
            } else {
                print("Failed to capture screenshot")
            }
        }
    }
    
    @IBAction func onClickShareCerti(_ sender: UIButton) {
        if let viewToCapture = viewCerti {
            if let screenshot = viewToCapture.takeScreenshot() {
                UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil)
                let imageToShare = screenshot
                    let activityViewController = UIActivityViewController(activityItems: [imageToShare], applicationActivities: nil)
                    activityViewController.excludedActivityTypes = [
                        .addToReadingList,
                        .assignToContact,
                        
                    ]
                    
                    if let popoverController = activityViewController.popoverPresentationController {
                        popoverController.sourceView = self.view
                        popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                        popoverController.permittedArrowDirections = []
                    }
                    
                    present(activityViewController, animated: true, completion: nil)
            }
        }
        
    }
    
    
    @IBAction func onClickCertifcate(_ sender: UIButton) {
        viewCertiPopUp.isHidden = false
    }
    
    @IBAction func onClickShare(_ sender: UIButton) {
        if let viewToCapture = viewCerti {
            if let screenshot = viewToCapture.takeScreenshot() {
                UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil)
                let imageToShare = screenshot
                    let activityViewController = UIActivityViewController(activityItems: [imageToShare], applicationActivities: nil)
                    activityViewController.excludedActivityTypes = [
                        .addToReadingList,
                        .assignToContact,
                        
                    ]
                    
                    if let popoverController = activityViewController.popoverPresentationController {
                        popoverController.sourceView = self.view
                        popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                        popoverController.permittedArrowDirections = []
                    }
                    
                    present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIView {
    func takeScreenshot() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        defer { UIGraphicsEndImageContext() }
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        layer.render(in: context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
