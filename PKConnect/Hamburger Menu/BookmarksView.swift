//
//  BookmarksView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit

class BookmarksView: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var bookmarksTable: UITableView!
    
    var bookmarkedItems = [FeedItem]()
    private var noDataMessae = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bookmarksTable.register(UINib(nibName: "FeedTableCell", bundle: nil), forCellReuseIdentifier: "feedTableCell")
        callBookmarksAPI()
    }
    
    func callBookmarksAPI() {
        showLoadingIndicator(in: view)
        getBookmarkedItems { feedItems, message  in
            DispatchQueue.main.async {
                self.hideLoadingIndicator()
                
                if let items = feedItems {
                    self.bookmarkedItems = items
                } else {
                    self.noDataMessae = message ?? ""
                    self.bookmarkedItems.removeAll()
                }
                self.bookmarksTable.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.bookmarkedItems.count == 0 {
            self.bookmarksTable.setEmptyMessage(self.noDataMessae.localize())
        } else {
            self.bookmarksTable.restore()
        }
        
        return self.bookmarkedItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "feedTableCell", for: indexPath) as! FeedTableCell
        
        var feedItem = bookmarkedItems[indexPath.row]
        feedItem.created_date = getFormattedDate(from: feedItem.created_date, inputFormat: "yyyy-MM-dd HH:mm:ss", outputFormat: 0)
        cell.tableDelegate = self
        cell.setUpContent(feedItem)
        cell.likeAction = { self.likeTapped(cell: cell, newsId: feedItem.news_id) }
        cell.commentAction = { self.commentTapped(entry: feedItem, newsId: feedItem.news_id) }
        cell.bookmarkAction = {
            self.bookmarkTapped(cell: cell, newsId: feedItem.news_id) {
                self.callBookmarksAPI()
            }
        }
        
        cell.downloadAction = {
            
            if let mediaSet = feedItem.content_media_set {
                if mediaSet.first?.media_type == "1" {
                    //
                } else if mediaSet.first?.media_type == "2" {
                    filedownLoad(view: self,mediaFile: mediaSet.first!.media_url,fileName: feedItem.news_title)
                }
            }
            
        }
        
        if let mediaSet = feedItem.content_media_set {
            if mediaSet.first?.media_type == "1" {
                //
            } else if mediaSet.first?.media_type == "2" {
                cell.shareAction = { self.shareVideoURL(url: mediaSet.first!.media_url,feedItem: feedItem) }
            }
        }
        return cell
    }
    
}

extension BookmarksView: FeedTableCellDelegate {
    func showNewsDetailFor(_ id: String, _ img: UIImage?) {
        self.showNewsDetailView(id, img)
    }
    
    func loadImg(urlStr: String, completion: @escaping (Data?, Error?) -> Void) {
        guard let imageUrl = URL(string: urlStr) else { return }
        self.loadImage(url: imageUrl) { data, error in
            completion(data, error)
        }
    }
    
    func updateTableView() {
        UIView.setAnimationsEnabled(false)
        bookmarksTable.beginUpdates()
        bookmarksTable.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func downloadMedia(_ img: UIImage) {
        self.downloadTapped(img)
    }
    
    func shareMedia(_ img: UIImage,feedItem :FeedItem) {
        self.shareTapped(img,feedItem:feedItem)
    }
    
}
