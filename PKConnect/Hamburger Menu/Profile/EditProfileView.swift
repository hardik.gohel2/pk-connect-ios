//
//  EditProfileView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit

class EditProfileView: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var verifiedUser: VerifiedUser?
    var userProfile: UserProfile?,
        refreshProfileAction: (()->())?
    private lazy var dropdownTable = UITableView()
    private let rowTitles = [["Name", "Contact Number"], ["State", "District", "Assembly Constituency"]],
                placeholderArray = [["Enter Your Name", ""], ["", "", ""]],
                errorTitles = [["Please enter your correct full name", ""], ["", "", ""]]
    private var dropdownItems = [String](),
                acList: [AC]?,
                indexOfDropdown = 88,
                updatedProfile: UserProfile!,
                headerView: UITableViewHeaderFooterView?,
                errorValues = [[true, true, true, true], [true, true, true], [true, true]]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updatedProfile = UserProfile()
        setupHideKeyboardOnTap()
        if let userProfile = userProfile {
            updatedProfile = userProfile
            self.tableView.reloadData()
        }
        
        dropdownTable.register(UITableViewCell.self, forCellReuseIdentifier: "dropdownCell")
        dropdownTable.dataSource = self
        dropdownTable.delegate = self
        
        if let districtId = userProfile?.district {
            getACList(for: districtId) { acList in
                self.acList = acList
            }
        }
        
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
//    @objc private func keyboardWillShow(_ notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            let insets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
//            tableView.contentInset = insets
//            tableView.scrollIndicatorInsets = insets
//        }
//    }
//
//    @objc private func keyboardWillHide(_ notification: NSNotification) {
//        tableView.contentInset = .zero
//        tableView.scrollIndicatorInsets = .zero
//    }
    
    
    @IBAction func updateProfile(_ sender: Any) {
        //Name
        if updatedProfile.full_name?.trimmingCharacters(in: .whitespaces) == "" {
            errorValues[0][0] = false
            
            
        }
        
//        //Whatsapp number
//        if let waNumber = updatedProfile.whatsup_number, waNumber.count == 10 {
//
//        } else {
//            errorValues[0][2] = false
//        }
//
//        //Email
//        if !isValidEmail(email: updatedProfile.email_id!) {
//            errorValues[0][3] = false
//        }
//
//        //Education
//        if updatedProfile.education == nil {
//            errorValues[2][0] = false
//        }
//
//        //Occupation
//        if updatedProfile.occupation == nil {
//            errorValues[2][1] = false
//        }
//
        if errorValues.flatMap({ $0 }).contains(false) {
            tableView.reloadData()
        } else {
            let newProfile = UpdatedProfile(full_name: updatedProfile.full_name, whatsup_number: updatedProfile.whatsup_number, email_id: updatedProfile.email_id, ac: updatedProfile.ac, education: updatedProfile.education, occupation: updatedProfile.occupation)
            showLoadingIndicator(in: view)
            updateProfile(newProfile) { updatedProfile in
                if updatedProfile != nil {
                    DispatchQueue.main.async {
                        self.hideLoadingIndicator()
                        self.refreshProfileAction?()
                        self.navigationController?.popViewController(animated: false)
                    }
                }
            }
        }
    }
    
    @IBAction private func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction private func changeLangTapped(_ sender: Any) {
        changeAppLanguage()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (tableView == self.tableView ? 2 : 1)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == dropdownTable {
            if let acList = acList, dropdownItems.count == 0 {
                return acList.count
            }
            return dropdownItems.count
        }
        return rowTitles[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "editProfileCell", for: indexPath) as! EditProfileCell
            
            let attributedString = NSMutableAttributedString(attributedString: NSAttributedString(string: " *", attributes: [.foregroundColor: UIColor.systemRed]))
            let combinedText = NSMutableAttributedString(attributedString: NSAttributedString(string: rowTitles[indexPath.section][indexPath.row].localize()))
            combinedText.append(attributedString)
            cell.label.tag = Int("\(indexPath.section)" + "\(indexPath.row)") ?? 99
            if cell.label.tag == 01{
                cell.label.text = rowTitles[indexPath.section][indexPath.row].localize()
            }else{
                cell.label.attributedText = combinedText
            }
            cell.textField.tag = Int("\(indexPath.section)" + "\(indexPath.row)") ?? 99
            switch cell.textField.tag {
            case 00:
                cell.textField.text = updatedProfile.full_name
            case 01:
                cell.textField.text = updatedProfile.phone_number
            case 02:
                cell.textField.text = updatedProfile.whatsup_number
                cell.textField.keyboardType = .numberPad
            case 03:
                cell.textField.text = updatedProfile.email_id
                cell.textField.keyboardType = .emailAddress
            case 10:
                cell.textField.text = updatedProfile.state_name
            case 11:
                cell.textField.text = updatedProfile.district_name
            case 12:
                cell.textField.text = updatedProfile.ac_name
            case 20:
                cell.textField.text = updatedProfile.education
            case 21:
                cell.textField.text = updatedProfile.occupation
            default:
                cell.textField.text = ""
            }
            cell.textField.placeholder = placeholderArray[indexPath.section][indexPath.row].localize()
            cell.textField.delegate = self
            cell.errorLabel.text = errorTitles[indexPath.section][indexPath.row] .localize()
            cell.errorLabel.isHidden = errorValues[indexPath.section][indexPath.row]
            
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell", for: indexPath)
            var config = cell.defaultContentConfiguration()
            config.textProperties.font = UIFont(name: "NunitoSans-Regular", size: 14)!
            if let acList = acList, dropdownItems.count == 0 {
                config.text = acList[indexPath.row].ac
            } else {
                config.text = dropdownItems[indexPath.row].localize()
            }
            cell.contentConfiguration = config
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == self.tableView {
            return section == 2 ? 200 : 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (tableView == self.tableView ? 60 : 0)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == self.tableView {
            var headerTitle = "Personal Information"
            if section == 1 {
                headerTitle = "Demographic Information"
            }
            return getTableHeaderWith(title: headerTitle.localize())
        }
        return nil
    }
    
    private func setUpDropDownTable(indexPath: IndexPath) {
        var tableHeight = 0
        let cell = tableView.cellForRow(at: indexPath) as! EditProfileCell
        let currentTextFieldFrame = cell.textField.frame
        tableView.addSubview(dropdownTable)
        if let acList = acList, dropdownItems.count == 0 {
            tableHeight = acList.count * 44
        } else {
            tableHeight = dropdownItems.count * 44
        }
        dropdownTable.frame = CGRect(x: 15, y: cell.frame.origin.y + currentTextFieldFrame.origin.y + 50 + 10, width: self.view.frame.size.width - 30, height: CGFloat(tableHeight))
        if #available(iOS 15.0, *) {
            dropdownTable.sectionHeaderTopPadding = 0
        } else {
            // Fallback on earlier versions
        }
        dropdownTable.reloadData()
        tableView.bringSubviewToFront(dropdownTable)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let header = view as? UITableViewHeaderFooterView {
            header.contentView.backgroundColor = UIColor(red: 255/255, green: 241/255, blue: 220/255, alpha: 1)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var newVal = ""
        if let acList = acList, dropdownItems.count == 0 {
            updatedProfile.ac = acList[indexPath.row].id
            updatedProfile.ac_name = acList[indexPath.row].ac
            newVal = updatedProfile.ac_name!
        } else {
            if indexOfDropdown == 20 {
                updatedProfile.education = dropdownItems[indexPath.row]
                newVal = updatedProfile.education!
                errorValues[2][0] = true
            } else {
                updatedProfile.occupation = dropdownItems[indexPath.row]
                newVal = updatedProfile.occupation!
                errorValues[2][1] = true
            }
        }
        dropdownTable.removeFromSuperview()
        
        let cell = self.tableView.cellForRow(at: IndexPath(row: indexOfDropdown % 10, section: indexOfDropdown/10)) as! EditProfileCell
        cell.textField.text = newVal.localize()
        cell.errorLabel.isHidden = errorValues[indexOfDropdown/10][indexOfDropdown % 10]
    }
    
    
    func getTableHeaderWith(title: String) -> UITableViewHeaderFooterView {
        headerView = UITableViewHeaderFooterView()
        
        var config = UIListContentConfiguration.groupedHeader()
        config.attributedText = NSAttributedString(string: title, attributes: [.font: UIFont(name: "NunitoSans-SemiBold", size: 18)!, .foregroundColor: UIColor.label])
        headerView?.contentConfiguration = config
        
        return headerView!
    }
}

extension EditProfileView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
      
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 00 {
            updatedProfile.full_name = textField.text
        } else if textField.tag == 02 {
            updatedProfile.whatsup_number = textField.text
        } else if textField.tag == 03 {
            updatedProfile.email_id = textField.text
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let enteredText = textField.text else { return false }
        if textField.tag == 00 {
            return string.isEmpty || string.rangeOfCharacter(from: .letters.union(.whitespaces)) != nil
        } else if textField.tag == 02 {
            if string.isEmpty {
                return true
            }
            return enteredText.count > 9 ? false : true
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if ![00, 02, 03].contains(textField.tag) {
            view.endEditing(true)
        }
        if textField.tag == indexOfDropdown {
            dropdownTable.removeFromSuperview()
            indexOfDropdown = 88
            dropdownItems.removeAll()
            return false
        }
        
        let section = textField.tag / 10,
            row = textField.tag % 10
        dropdownTable.removeFromSuperview()
        indexOfDropdown = 88
        dropdownItems.removeAll()
        switch textField.tag {
        case 00, 02, 03:
            if errorValues[section][row] == false {
                let cell = self.tableView.cellForRow(at: IndexPath(row: row, section: section)) as! EditProfileCell
                cell.errorLabel.isHidden = true
                errorValues[section][row] = true
            }
            return true
        case 01, 10, 11, 12:
            return false
        case 20:
            indexOfDropdown = textField.tag
            dropdownItems = ["10th", "12th", "Bachelor's Degree", "Master's Degree", "Doctorate or Higher", "Other"]
            setUpDropDownTable(indexPath: IndexPath(row: row, section: section))
            return false
        case 21:
            indexOfDropdown = textField.tag
            dropdownItems = ["Public Sector", "Private Sector", "Business", "Self Employed", "Other"]
            setUpDropDownTable(indexPath: IndexPath(row: row, section: section))
            return false
        default:
            return true
        }
    }
    
    func setupHideKeyboardOnTap() {
        self.view.addGestureRecognizer(self.endEditingRecognizer())
        self.navigationController?.navigationBar.addGestureRecognizer(self.endEditingRecognizer())
    }
    
    //Dismiss keyboard if shown
    private func endEditingRecognizer() -> UIGestureRecognizer {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        return tap
    }
}


