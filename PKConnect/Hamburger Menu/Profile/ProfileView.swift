//
//  ProfileView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit
import Swifter
import AuthenticationServices

class ProfileView: BaseViewController, UITableViewDelegate, UITableViewDataSource, ASWebAuthenticationPresentationContextProviding {
    func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        return self.view.window ?? ASPresentationAnchor()
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet private var name: UILabel!
    @IBOutlet weak var contactNo: UILabel!
    var mobNo = ""
    
    var callbackObserver: Any? {
      willSet {
        // we will add and remove this observer on an as-needed basis
        guard let token = callbackObserver else { return }
        NotificationCenter.default.removeObserver(token)
      }
    }
    
    var userProfile: UserProfile? {
        didSet {
            name.text = userProfile?.full_name
        }
    }
    private let imagePicker = UIImagePickerController()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      //callProfileAPI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.layer.cornerRadius = 54/2
        imagePicker.delegate = self
        imageView.layer.borderWidth = 0
        imagePicker.allowsEditing = true
        
        
        callProfileAPI()
    }
    
    private func callProfileAPI() {
        showLoadingIndicator(in: view)
        getProfile { profile in
            DispatchQueue.main.async { [self] in
                hideLoadingIndicator()
                userProfile = profile
                if let imgUrl = userProfile?.user_image {
                    imageView.setLoadedImage(imgUrl)
                }
                //                name.text = profile.full_name
                updateUserDefaults(name: profile.full_name, imageStr: nil)
                contactNo.text = profile.phone_number
                mobNo = profile.phone_number!
                UserDefaults.standard.set(profile.fb_username, forKey: "fbname")
                UserDefaults.standard.set(profile.twitter_username, forKey: "twittername")
                tableView.reloadData()
            }
        }
    }
    
    func updateUserDefaults(name: String?, imageStr: String?) {
        if name != nil {
            if var user = getRetrievedUser(), user.user_name != name {
                user.user_name = name
                if let userData = try? JSONEncoder().encode(user)  {
                    UserDefaults.standard.set(userData, forKey: "User")
                }
            }
        }
    }
    
    func showSuccessMessage() {
        let label = UILabel()
        label.textAlignment = .center
        label.attributedText = NSAttributedString(string: "Your profile has been updated".localize(), attributes: [.font: UIFont(name: "NunitoSans-SemiBold", size: 16)!, .foregroundColor: UIColor.label])
        label.backgroundColor = .white
        label.layer.cornerRadius = 20
        label.layer.masksToBounds = true
        view.addSubview(label)
        label.frame = CGRect(x: 20, y: view.frame.size.height - 150, width: view.frame.size.width - 40, height: 40)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            label.removeFromSuperview()
        }
    }
    
    @IBAction func didTapImageView(_ sender: UITapGestureRecognizer) {
       // showAlert()
        let storyBoard = UIStoryboard(name: "Other", bundle: nil)
        let ProfilePhotoView = storyBoard.instantiateViewController(withIdentifier: "ProfilePhotoVC") as! ProfilePhotoVC
        self.navigationController?.pushViewController(ProfilePhotoView, animated: false)

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profileTableCell", for: indexPath) as! ProfileTableCell
        
        switch indexPath.row {
        case 0:
            cell.icon.image = UIImage(named: "account_circle")
            cell.label.text = "Basic Details".localize()
        case 1:
            cell.icon.image = UIImage(named: "facebook")
//            cell.label.text = "Login to Facebook".localize()
            
            if userProfile?.fb_username == "" || userProfile?.fb_username == nil {
                cell.label.text = "Login to Facebook".localize()
            } else {
                cell.label.text = UserDefaults.standard.value(forKey: "fbname") as? String
            }
//            cell.icon.image = UIImage(named: "twitter")
//            if userProfile?.twitter_username == "" || userProfile?.twitter_username == nil {
//                cell.label.text = "Login to Twitter".localize()
//            }else{
//                if UserDefaults.standard.value(forKey: "twittername") != nil{
//                    cell.label.text = UserDefaults.standard.value(forKey: "twittername") as? String
//                }else{
//                    cell.label.text = userProfile?.twitter_username
//                }
//            }
        default:
            break
//            cell.icon.image = UIImage(named: "facebook")
////            cell.label.text = "Login to Facebook".localize()
//
//            if userProfile?.fb_username == "" || userProfile?.fb_username == nil {
//                cell.label.text = "Login to Facebook".localize()
//            } else {
//                cell.label.text = UserDefaults.standard.value(forKey: "fbname") as? String
//            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if UserDefaults.standard.value(forKey: "UserVerification") != nil, UserDefaults.standard.value(forKey: "UserVerification") as! String == "0" ||  UserDefaults.standard.value(forKey: "UserVerification") as! String == "2"{
                let NewVerifyProfileVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "NewVerifyProfileVC") as! NewVerifyProfileVC
                self.navigationController?.pushViewController(NewVerifyProfileVC, animated: false)
            }else if UserDefaults.standard.value(forKey: "UserVerification") != nil, UserDefaults.standard.value(forKey: "UserVerification") as! String == "3"{
                sendOTP(for: mobNo) { responseObj in
                    if responseObj!.status == "200" {
                        DispatchQueue.main.async {
                            let VerifyOTPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "verifyOTPVC") as! VerifyOTPVC
                            VerifyOTPVC.mobileNo = self.mobNo
                            VerifyOTPVC.UpdateAll = false
                            VerifyOTPVC.currentOTP = responseObj!.data.otp!
                            self.navigationController?.pushViewController(VerifyOTPVC, animated: false)
                        }
                    }
                }

            }else{
                let storyBoard = UIStoryboard(name: "Other", bundle: nil)
                let editProfileVC = storyBoard.instantiateViewController(withIdentifier: "editProfileView") as! EditProfileView
                editProfileVC.userProfile = userProfile
                editProfileVC.refreshProfileAction = {
                    self.showSuccessMessage()
                    self.callProfileAPI()
                }
                self.navigationController?.pushViewController(editProfileVC, animated: true)
            }
        } else   if indexPath.row == 1 {
            if userProfile?.fb_username == "" || userProfile?.fb_username == nil {
                FacebookController.shared.getFacebookUserInfo(fromViewController: self, success: { (result, string) in
                    print("\(result) \(string)")
                    print("fdssdsdasas", result.name)
                    print("fdssdsdasasidd", result.id)
                    
                    self.updateFacebook(facebookId: result.id, fbUsername: result.name) {res in
                        print(res)
                        DispatchQueue.main.async {
                            self.hideLoadingIndicator()
                            if res["CODE"] as! Int == 513{
                                DispatchQueue.main.async {
                                    self.showToast(message: "The Facebook ID you have entered is already registered with another user. Please choose a different account and try again.".localize())
                                    return
                                }
                            }
                            self.getProfile { profile in
                                DispatchQueue.main.async { [self] in
                                    hideLoadingIndicator()
                                    UserDefaults.standard.set(result.name, forKey: "fbname")
                                    userProfile = profile
                                    if let refCode = userProfile?.referal_code {
                                        if var user = getRetrievedUser(), user.referal_code != "" {
                                            user.referal_code = refCode
                                            if let userData = try? JSONEncoder().encode(user)  {
                                                UserDefaults.standard.set(userData, forKey: "User")
                                            }
                                        }
                                    }
                                    if let urlStr = userProfile?.user_image {
                                        downloadImageFrom(urlString: urlStr) { downloadedImage in
                                            DispatchQueue.main.async {
                                                self.imageView.image = downloadedImage
                                            }
                                        }
                                    }
                                    //                name.text = profile.full_name
                                    updateUserDefaults(name: profile.full_name, imageStr: nil)
                                    contactNo.text = profile.phone_number
                                    tableView.reloadData()
                                }
                            }
                        }
                    }
                    
                }) { (error) in
                    print(error?.localizedDescription ?? "")
                }
            }else{
                
            }
            
        }
        
//        else if indexPath.row == 1 {
//            if userProfile?.twitter_username == "" || userProfile?.twitter_username == nil {
//
//                var swifter: Swifter!
//                swifter = Swifter(consumerKey: "7dfm8RoglWqOMUbgz3ebbz38g", consumerSecret: "SuFQzZoF8DTX4YUi1HShqxZhyLDKKMoWPapeEcmathY6QkqxCj")
//                if let url = URL(string: "twitterkit-7dfm8RoglWqOMUbgz3ebbz38g://") {
//                    swifter.authorize(withProvider: self, callbackURL: url) { (token, response) in
//                        UserDefaults.standard.set(token?.key, forKey: "twitterOAuthToken")
//                        UserDefaults.standard.set(token?.secret, forKey: "twitterOAuthSecret")
//
//
//                        self.updateTwitter(id: token?.userID ?? "", Username: token?.screenName ?? "") {res in
//                            DispatchQueue.main.async {
//                                self.hideLoadingIndicator()
//                                if res["CODE"] as! Int == 514{
//                                    DispatchQueue.main.async {
//                                        self.showToast(message: "The Twitter ID you have entered is already registered with another user. Please choose a different account and try again.".localize())
//                                        return
//                                    }
//                                }
//                                self.getProfile { profile in
//                                    DispatchQueue.main.async { [self] in
//                                        hideLoadingIndicator()
//                                        UserDefaults.standard.set(token?.screenName, forKey: "twittername")
//                                        swifter = Swifter(consumerKey: "7dfm8RoglWqOMUbgz3ebbz38g", consumerSecret: "SuFQzZoF8DTX4YUi1HShqxZhyLDKKMoWPapeEcmathY6QkqxCj", oauthToken: UserDefaults.standard.string(forKey: "twitterOAuthToken") ?? "", oauthTokenSecret: UserDefaults.standard.string(forKey: "twitterOAuthSecret") ?? "")
//                                        userProfile = profile
//                                        if let refCode = userProfile?.referal_code {
//                                            if var user = getRetrievedUser(), user.referal_code != "" {
//                                                user.referal_code = refCode
//                                                if let userData = try? JSONEncoder().encode(user)  {
//                                                    UserDefaults.standard.set(userData, forKey: "User")
//                                                }
//                                            }
//                                        }
//                                        if let urlStr = userProfile?.user_image {
//                                            downloadImageFrom(urlString: urlStr) { downloadedImage in
//                                                DispatchQueue.main.async {
//                                                    self.imageView.image = downloadedImage
//                                                }
//                                            }
//                                        }
//                                        //                name.text = profile.full_name
//                                        updateUserDefaults(name: profile.full_name, imageStr: nil)
//                                        contactNo.text = profile.phone_number
//                                        tableView.reloadData()
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }else{
//                print("else")
//            }
//        }
    }
}

extension ProfileView: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showAlert() {
        let alert = UIAlertController(title: "Add Photo!".localize(), message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Take Photo".localize(), style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Choose from Library".localize(), style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Do it Later".localize(), style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.contentMode = .scaleToFill
            imageView.image = pickedImage
            self.uploadImage(pickedImage)
         }

         dismiss(animated: true, completion: nil)
    }
    
    private func uploadImage(_ img: UIImage) {
        img.uploadImageToS3 { uploaded, imgUrl in
            if uploaded {
                self.addEditProfilePicture(imgUrl) {
                    DispatchQueue.main.async {
                        //
                    }
                }
            }
        } progress: { progressVal in
            //
        } size: { imgSize in
            //
        } failure: { error in
            print(error)
        }

    }
    
    func openCamera() {
        if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }

    func openGallery() {
        imagePicker.sourceType = .photoLibrary
        imagePicker.modalPresentationStyle = .fullScreen
        present(imagePicker, animated: true, completion: nil)
    }
}

extension Notification.Name {
    static let twitterCallback = Notification.Name(rawValue: "Twitter.CallbackNotification.Name")
}

