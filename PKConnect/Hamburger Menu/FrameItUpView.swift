//
//  FrameItUpView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit

class FrameItUpView: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet private var birthdayWishView: UIView!
    @IBOutlet weak var enterNameField: CustomTextField!
    
    @IBOutlet weak var viewTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        enterNameField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        self.view.addSubview(backgroundView)
        backgroundView.frame = self.view.frame
        
        let alertView = UINib(nibName: "BirthdayWishView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BirthdayWishView
        alertView.layer.cornerRadius = 4
        alertView.translatesAutoresizingMaskIntoConstraints = false
//        alertView.delegate = self
        self.view.addSubview(alertView)
        NSLayoutConstraint.activate([
            alertView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor, constant: -20),
            alertView.heightAnchor.constraint(equalToConstant: 340),
            alertView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 1),
            alertView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 1)
        ])
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            let yVal = birthdayWishView.frame.size.height - enterNameField.frame.maxY + view.safeAreaInsets.bottom
            viewTopConstraint.constant -= (keyboardSize.height - yVal)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        viewTopConstraint.constant = 20
    }
    
}
