//
//  LogOutVC.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit

class LogOutView: UIViewController {
    
    @IBOutlet weak var contentView: CustomView!
    @IBOutlet weak var yesButton: RoundedButton!
    @IBOutlet weak var noButton: RoundedButton!
    
    var yesButtonAction: (()->())?
    var noButtonAction: (()->())?

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func yesButtonTapped(_ sender: Any) {
        self.yesButtonAction?()
    }
    
    @IBAction func noButtonTapped(_ sender: Any) {
        self.noButtonAction?()
    }
    
}
