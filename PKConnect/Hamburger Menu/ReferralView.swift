//
//  ReferralView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit
import FirebaseDynamicLinks

class ReferralView: BaseViewController {
    
    @IBOutlet weak var viewreferal: UIView!
    @IBOutlet private var inviteSegment: UIButton!
    @IBOutlet private var inviteView: UIView!
    @IBOutlet private var referralCodeLabel: UILabel!
    
    @IBOutlet private var referredFriendsView: UIView!
    @IBOutlet private var tableView: UITableView!
    
    @IBOutlet private var underlineViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet private var underlineViewLeadingConstraint: NSLayoutConstraint!
    
    private var referralCode = "",
                referredList = [Referral]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewreferal.isHidden = true
        tableView.tableFooterView = UIView()
        
        view.layoutIfNeeded()
        self.inviteSegmentSelected(inviteSegment)
        if let user = getRetrievedUser(), let refCode = user.referal_code {
            self.referralCode = refCode
            var newStr = ""
            for ch in refCode {
                newStr.append("\(ch) ")
            }
            referralCodeLabel.text = newStr
        }
    }
    
    @IBAction func inviteSegmentSelected(_ sender: UIButton) {
        underlineViewWidthConstraint.constant = sender.titleLabel?.intrinsicContentSize.width ?? 0
        underlineViewLeadingConstraint.constant = sender.frame.origin.x + (sender.titleLabel?.frame.origin.x ?? 0)
        
        inviteView.isHidden = false
        referredFriendsView.isHidden = true
    }
    
    @IBAction func referredFriendsSegmentSelected(_ sender: UIButton) {
        underlineViewWidthConstraint.constant = sender.titleLabel?.intrinsicContentSize.width ?? 0
        underlineViewLeadingConstraint.constant = sender.frame.origin.x + (sender.titleLabel?.frame.origin.x ?? 0)
        
        referredFriendsView.isHidden = false
        inviteView.isHidden = true
        setUpReferredFriendsView()
    }
    @IBAction func inviteFriendTapped(_ sender: Any) {
        let link = "\(DynamicLinkParams.dynamicLinkDomain)?apn=\(DynamicLinkParams.packageName)&ibi=\(DynamicLinkParams.packageName)&isi=1530520823&link=https%3A%2F%2Fpkconnect.page.link%2F%3Freferral_code%3D\(referralCode)&ofl=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3D\(DynamicLinkParams.packageName)"

        print("", link)

        let textToShare = "Show your support to Prashant Kishor by registering on PK Connect App\n\nTo download the app, click on the link -\n\(link)"

        let didiLogoLogo = UIImage(named: "PKConnectLogo")!
        let activities = [didiLogoLogo, textToShare] as [Any]
        let activityVC = UIActivityViewController(activityItems: activities, applicationActivities: nil)
        activityVC.excludedActivityTypes = [.addToReadingList, .assignToContact]

        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
//    @IBAction func inviteFriendTapped(_ sender: Any) {
//        //&_imcp=1
//
//        let link = "\(DynamicLinkParams.dynamicLinkDomain)?apn=\(DynamicLinkParams.packageName)&ibi=\(DynamicLinkParams.packageName)&isi=1530520823&link=https%3A%2F%2Fpkconnect.page.link%2F%3Freferral_code%3D\(referralCode)&ofl=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3D\(DynamicLinkParams.packageName)"
//
//        print("",link)
//
//        DynamicLinkComponents.shortenURL(URL(string: link)!, options: nil) { url, warnings, error in
//            print("The short URL is: \(url?.absoluteString ?? "")")
//
//            let textToShare = "Show your support to Prashant Kishor by registering on PK Connect App\n\nTo download the app click on the link-\n \(url?.absoluteString ?? link)"
//
//            let didiLogoLogo = UIImage(named: "PKConnectLogo")!
//            let activities = [didiLogoLogo, textToShare] as [Any]
//            let activityVC = UIActivityViewController(activityItems: activities, applicationActivities: nil)
//            activityVC.excludedActivityTypes = [.addToReadingList, .assignToContact]
//
//            activityVC.popoverPresentationController?.sourceView = self.view
//            self.present(activityVC, animated: true, completion: nil)
//
//        }
//    }
    private func setUpReferredFriendsView() {
        getReferralList { referredList in
            self.referredList = referredList
            DispatchQueue.main.async { [self] in
                referredList.count == 0 ? addNoRecordsLabel("SemiBold", 20, to: referredFriendsView) : tableView.reloadData()
            }
        }
    }
}

extension ReferralView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        referredList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "referralCell", for: indexPath) as! ReferralTableCell
        
        let referredUser = referredList[indexPath.row]
        cell.fullName.text = referredUser.full_name
        cell.signUpDate.text = "Joined on \(getFormattedDate(from: referredUser.created_date, inputFormat: "yyyy-MM-dd HH:mm:ss", outputFormat: 4))"
        
        return cell
    }
    
}
