//
//  ProfilePhotoVC.swift
//  PKConnect
//
//  Created by c on 10/04/23.
//

//import UIKit
//
//class ProfilePhotoVC: UIViewController {
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Do any additional setup after loading the view.
//    }
//
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destination.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//}




import UIKit

class ProfilePhotoVC: BaseViewController {
    @IBOutlet weak var imgProfile: UIImageView!
    private let imagePicker = UIImagePickerController()

    var userProfile: UserProfile? {
        didSet {
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        callProfileAPI()
        // Do any additional setup after loading the view.
    }
    private func callProfileAPI() {
        showLoadingIndicator(in: view)
        getProfile { profile in
            DispatchQueue.main.async { [self] in
                hideLoadingIndicator()
                userProfile = profile
                if let imgUrl = userProfile?.user_image {
                    imgProfile.setLoadedImage(imgUrl)
                }
                //                name.text = profile.full_name
            }
        }
    }
    @IBAction private func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction private func changeLangTapped(_ sender: Any) {
        changeAppLanguage()
    }

    @IBAction func btnEdit(_ sender: UIButton) {
        showAlert()
    }
    
}
extension ProfilePhotoVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showAlert() {
        let alert = UIAlertController(title: "Add Photo!".localize(), message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Take Photo".localize(), style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Choose from Library".localize(), style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Do it Later".localize(), style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            imgProfile.contentMode = .scaleAspectFit
            imgProfile.image = pickedImage
            self.uploadImage(pickedImage)
         }
         dismiss(animated: true, completion: nil)
    }
    
    private func uploadImage(_ img: UIImage) {
        img.uploadImageToS3 { uploaded, imgUrl in
            if uploaded {
                self.addEditProfilePicture(imgUrl) {
                    DispatchQueue.main.async {
                        self.getProfile { profile in
                            DispatchQueue.main.async { [self] in
                                if var user = getRetrievedUser() {
                                    if let refCode = profile.referal_code { user.referal_code = refCode }
                                    if let imageUrl = profile.user_image { user.user_image = imageUrl }
                                    if let userData = try? JSONEncoder().encode(user)  {
                                        UserDefaults.standard.set(userData, forKey: "User")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } progress: { progressVal in
            //
        } size: { imgSize in
            //
        } failure: { error in
            print(error)
        }
        
    }
    
    func openCamera() {
        if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }

    func openGallery() {
        imagePicker.sourceType = .photoLibrary
        imagePicker.modalPresentationStyle = .fullScreen
        present(imagePicker, animated: true, completion: nil)
    }
}
