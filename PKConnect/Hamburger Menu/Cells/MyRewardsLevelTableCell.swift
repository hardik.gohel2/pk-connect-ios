//
//  MyRewardsLevelTableCell.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit

class MyRewardsLevelTableCell: UITableViewCell {

    @IBOutlet weak var levelName: UILabel!
    @IBOutlet weak var levelNumber: UILabel!
    @IBOutlet weak var levelBackground: UIImageView!
    
    
    @IBOutlet weak var levelIMg: UIImageView!
    
    @IBOutlet weak var dottedLineTop: UIImageView!
    @IBOutlet weak var dottedLineBottom: UIImageView!
    
    
    
    @IBOutlet weak var lockUnlockView: UIImageView!
    @IBOutlet weak var levelDescription: UILabel!
    @IBOutlet weak var polygonImage: UIImageView!
    
    @IBOutlet var keyImageCenterConstraint: NSLayoutConstraint!
    @IBOutlet var keyImageLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var levelViewHeight: NSLayoutConstraint!
    @IBOutlet weak var levelNameViewHeight: NSLayoutConstraint!
    @IBOutlet weak var levelDescriptionViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }


}
