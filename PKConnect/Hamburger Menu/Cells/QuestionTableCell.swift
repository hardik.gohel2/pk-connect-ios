//
//  QuestionTableCell.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit

class QuestionTableCell: UITableViewCell {

    @IBOutlet weak var queCardView: UIView!
    @IBOutlet weak var ansCardView: UIView!
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    
    @IBOutlet var ansCardHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        queCardView.layer.cornerRadius = 5
        queCardView.layer.borderColor = UIColor(red: 252/255, green: 189/255, blue: 217/255, alpha: 1).cgColor
        queCardView.layer.borderWidth = 1
        
        ansCardView.layer.cornerRadius = 5
        ansCardView.layer.borderWidth = 1
        ansCardView.layer.borderColor = UIColor.white.cgColor
    }

}
//252, 189, 217
