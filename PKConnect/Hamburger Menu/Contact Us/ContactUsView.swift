//
//  ContactUsView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit

class ContactUsView: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    private var categories = [ContactUsCategory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getContactUsCategories { contactUsCategories in
            let categories = contactUsCategories
            self.categories = categories.sorted(by: {
                return $0.category_id < $1.category_id
            })
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        categories.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactUsTableCell", for: indexPath) as! ContactUsTableCell
        
        let category = categories[indexPath.row]
        cell.label.text = (appLang == "en" ? category.category_name : category.category_name_tn)
        cell.tag = Int(category.category_id) ?? 99
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ContactUsTableCell
        
        let storyBoard = UIStoryboard(name: "Other", bundle: nil)
        let contactUsDetailView = storyBoard.instantiateViewController(withIdentifier: "contactUsDetailView") as! ContactUsDetailView
        contactUsDetailView.categoryId = cell.tag
        contactUsDetailView.categoryTitle = cell.label.text ?? ""
        self.navigationController?.pushViewController(contactUsDetailView, animated: true)
    }
    
}
