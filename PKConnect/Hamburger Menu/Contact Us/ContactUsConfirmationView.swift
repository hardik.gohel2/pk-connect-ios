//
//  ContactUsConfirmationView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/19/22.
//

import UIKit

class ContactUsConfirmationView: BaseViewController {

    @IBOutlet private var categoryTitle: UILabel!
    @IBOutlet private var issueDescription: UILabel!
    
    var issueTitle = "",
        issueDetail = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoryTitle.text = issueTitle
        issueDescription.text = issueDetail
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                if vc.isKind(of: SlidingViewController.self) {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }
    }

    @IBAction private func changeLangTapped(_ sender: Any) {
        changeAppLanguage()
    }
    
}
