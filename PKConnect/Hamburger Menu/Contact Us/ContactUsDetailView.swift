

import UIKit

class ContactUsDetailView: BaseViewController, UITextViewDelegate {

    @IBOutlet private var issueLabel: UILabel!
    @IBOutlet private var sendButton: UIButton!
    @IBOutlet private var textView: UITextView!
    
    var categoryTitle = ""
    var categoryId = 99
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sendButton.layer.cornerRadius = 20
        issueLabel.text = categoryTitle
    }
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func sendButtonTapped(_ sender: Any) {
        if textView.text == "" || textView.text.count < 10 {
            view.endEditing(true)
            self.showToast(message: "Please write at least 10 characters about your request".localize())
           // showFloatingLabel()
        } else {
            showLoadingIndicator(in: view)
            let id = "\(categoryId)"
            postContactUsRequest(id, description: textView.text) {
                DispatchQueue.main.async {
                    self.hideLoadingIndicator()
                    self.showConfirmationScreen()
                }
            }
        }
    }
    
    private func showConfirmationScreen() {
        let confirmationVC = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "contactUsConfirmationView") as! ContactUsConfirmationView
        confirmationVC.modalPresentationStyle = .fullScreen
        confirmationVC.issueTitle = categoryTitle
        confirmationVC.issueDetail = textView.text
        self.navigationController?.pushViewController(confirmationVC, animated: true)
    }
    
    func showFloatingLabel() {
        let label = UILabel()
        label.textAlignment = .center
        label.text = "Please write at least 10 characters about your request".localize()
//        label.attributedText = NSAttributedString(string: "Please write at least 10 characters about your request".localize(), attributes: [.font: UIFont(name: "NunitoSans-Regular", size: 16)!, .foregroundColor: UIColor.label])
        label.backgroundColor = .white
        label.layer.cornerRadius = 20
        label.layer.masksToBounds = true
        label.numberOfLines = 0
        view.addSubview(label)
        label.frame = CGRect(x: 20, y: view.frame.size.height - 150, width: view.frame.size.width - 40, height: 60)
        view.bringSubviewToFront(label)
        DispatchQueue.main.asyncAfter(deadline: .now()+5) {
            label.removeFromSuperview()
        }
    }
    
    @IBAction private func changeLangTapped(_ sender: Any) {
        changeAppLanguage()
    }
    
}
