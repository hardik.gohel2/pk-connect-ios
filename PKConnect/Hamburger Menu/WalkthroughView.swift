//
//  WalkthroughView.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/5/22.
//

import UIKit
import AVKit

class WalkthroughView: UIViewController {

    @IBOutlet weak var videoView: UIView!
    
    var player: AVPlayer!
    var playerViewController: AVPlayerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let videoURL = URL(string:
                            "https://kcr-sainyam.s3.ap-south-1.amazonaws.com/ipac/62a1d1b5437cb.62373c8939f2e_3rd.mp4")
//    "https://s3.ap-south-1.amazonaws.com/goa-deserve-better/android1664121406750.mp4")
        self.player = AVPlayer(url: videoURL!)
        self.playerViewController = AVPlayerViewController()
        playerViewController.player = self.player
        playerViewController.view.frame = self.videoView.bounds
        playerViewController.player?.play()
        self.videoView.addSubview(playerViewController.view)
    }
    
}
