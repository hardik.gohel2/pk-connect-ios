//
//  HamburgerViewController.swift
//  PKConnect
//
//  Created by Poshitha Gajjala on 11/1/22.
//

import UIKit

protocol HamburgerToMaster {
    func menuItemSelected(index: Int)
}

class HamburgerViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet private var userImage: UIImageView!
    @IBOutlet private var userName: UILabel!
    @IBOutlet private var userContact: UILabel!
    @IBOutlet private var sideMenuTableView: UITableView!
    
    var hamburgerToMaster: HamburgerToMaster?
    private var selectedIndex = 0,
                prevSelectedIndex = 0
    private let menuItems = ["Home", "Profile", "Task", "My Rewards","Bookmarks", "Referral", "FAQs", "Contact Us", "Privacy Policy", "Log Out"],

        menuIcons = ["home_icon", "profile_icon", "task_icon", "myrewards_icon", "bookmarks_icon","referral_icon", "faqs_icon", "contactus_icon", "privacypolicy_icon", "logout_icon"]
    
//    private let menuItems = ["Home", "Profile", "Task", "My Rewards", "Frame It Up", "Bookmarks", "Walkthrough", "Referral", "FAQs", "Contact Us", "Privacy Policy", "Log Out"],
//                menuIcons = ["home_icon", "profile_icon", "task_icon", "myrewards_icon", "frameitup_icon", "bookmarks_icon", "padyatra", "referral_icon", "faqs_icon", "contactus_icon", "privacypolicy_icon", "logout_icon"]
    var retrievedUser: VerifiedUser? {
        didSet {
            userName.text = retrievedUser?.user_name
            userContact.text = retrievedUser?.contactNumber
            userImage.sd_setImage(with: URL(string:retrievedUser?.user_image ?? ""), placeholderImage: UIImage(named: "user_44"))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userImage.layer.cornerRadius = 30
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userName.text = retrievedUser?.user_name
        userContact.text = retrievedUser?.contactNumber
        if let urlStr = retrievedUser?.user_image {
            userImage.setLoadedImage(urlStr)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        10
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = sideMenuTableView.dequeueReusableCell(withIdentifier: "sideMenuTableCell", for: indexPath) as! SideMenuTableCell
        
        let img = UIImage(named: menuIcons[indexPath.row])
        cell.itemImage.image = (selectedIndex == indexPath.row) ? img?.withTintColor(.pkConnectYellow) : img
        cell.itemLabel.text = menuItems[indexPath.row].localize()
        cell.itemLabel.textColor = (selectedIndex == indexPath.row) ? .pkConnectYellow : .label
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tableView.reloadRows(at: [IndexPath(row: prevSelectedIndex, section: 0), IndexPath(row: selectedIndex, section: 0)], with: UITableView.RowAnimation.none)
        prevSelectedIndex = selectedIndex
        
        if let delegate = hamburgerToMaster {
            delegate.menuItemSelected(index: indexPath.row)
        }
    }
    
    
    @IBAction func ProfilePicClick(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Other", bundle: nil)
        let ProfilePhotoView = storyBoard.instantiateViewController(withIdentifier: "ProfilePhotoVC") as! ProfilePhotoVC
        self.navigationController?.pushViewController(ProfilePhotoView, animated: false)    }
    
}
