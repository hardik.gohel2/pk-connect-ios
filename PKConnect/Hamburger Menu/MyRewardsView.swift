

import UIKit

class MyRewardsView: BaseViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    private var VerifyPopUpView: VerifyPopUpView!
    @IBOutlet weak var levelsTable: UITableView!
    var leaderboard: LeaderboardResult?

    private let array1 = ["level1_bg", "level2_bg", "level3_bg", "level4_bg", "level5_bg"],
                array2 = ["polygon1", "polygon2", "polygon3", "polygon4", "polygon5"],
                array3 = ["LockOne", "LockTwo", "LockThree", "LockFour", "LockFive"]
               
    
    private var backgroundImageArray = [String](),
                polygonImageArray = [String](),
                lockImageArray = [String](),
                levelData = [Level](),
              
                tappedIndex: Int?
    var profileTopVC: LeaderboardProfileView!,
        topViewYConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundImageArray = array1 + array1 + array1
        polygonImageArray = array2 + array2 + array2
        lockImageArray = array3 + array3 + array3
       
        
        callLevelsAPI()
        //DispatchQueue.main.async {
            self.setUpLeaderboardProfileView()
      //  }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.value(forKey: "UserVerification") != nil, UserDefaults.standard.value(forKey: "UserVerification") as! String == "0" || UserDefaults.standard.value(forKey: "UserVerification") as! String == "2" || UserDefaults.standard.value(forKey: "UserVerification") as! String == "3"{
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            self.VerifyPopUpView = storyBoard.instantiateViewController(withIdentifier: "VerifyPopUpView") as? VerifyPopUpView
            self.addChild(self.VerifyPopUpView)
            self.view.addSubview(self.VerifyPopUpView.view)
            self.VerifyPopUpView.view.frame = self.view.bounds

            self.view.bringSubviewToFront(self.VerifyPopUpView.view)
        }
    }
    
    private func callLevelsAPI() {
        getLevelData { levelData, points  in
            self.levelData = levelData
            DispatchQueue.main.async {
                self.levelsTable.reloadData()
                if let vc = self.children.first as? LeaderboardProfileView
                {
                    vc.leaderBoardProfileDelegate = self
                    let pointsEarned = Int(Double(points) ?? 0)
                    var userImage = "", fullName = ""
                    if let savedUserInfo = self.getRetrievedUser() {
                        userImage = savedUserInfo.user_image ?? ""
                        fullName = savedUserInfo.user_name ?? ""
                    }
                    vc.userInfo = UserInfo(full_name: fullName, user_image: userImage, registered_on: nil, points_earned: "\(pointsEarned)", rank: 0)
                }
            }
        }
    }
    
    private func setUpLeaderboardProfileView() {
   
        profileTopVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "leaderboardProfileView") as? LeaderboardProfileView
        addChild(profileTopVC)
        levelsTable.addSubview(profileTopVC.view)
        profileTopVC.view.translatesAutoresizingMaskIntoConstraints = false
        topViewYConstraint = NSLayoutConstraint(item: profileTopVC.view!, attribute: .top, relatedBy: .equal, toItem: levelsTable, attribute: .top, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([
            profileTopVC.view.heightAnchor.constraint(equalToConstant: 340),
            profileTopVC.view.leftAnchor.constraint(equalTo: self.levelsTable.leftAnchor),
            profileTopVC.view.widthAnchor.constraint(equalTo: self.levelsTable.widthAnchor, constant: 0),
            topViewYConstraint
        ])
        
        profileTopVC.didMove(toParent: self)
        profileTopVC.levelsPinnedView.isHidden = false
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        if(offset > 300){
            topViewYConstraint.constant = offset - 300
        } else {
            topViewYConstraint.constant = 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        levelData.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        340
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myRewardsLevelCell", for: indexPath) as! MyRewardsLevelTableCell
        
        let levelObj = levelData[indexPath.row]
        
        print("........",levelObj)
        if indexPath.row == 0{
            cell.dottedLineTop.isHidden = true
            cell.dottedLineBottom.isHidden = false
        }else if indexPath.row == levelData.count - 1{
            cell.dottedLineTop.isHidden = false
            cell.dottedLineBottom.isHidden = true
        }else{
            cell.dottedLineTop.isHidden = false
            cell.dottedLineBottom.isHidden = false
        }
        cell.levelName.text = (appLang == "en") ? levelObj.name : levelObj.name_tn
        cell.levelNumber.text = levelObj.level_tag_name
      
        cell.levelDescription.text = "\(levelObj.description)\n\(levelObj.incentive)"
        cell.levelBackground.image = UIImage(named: backgroundImageArray[indexPath.row])
        cell.polygonImage.image = UIImage(named: polygonImageArray[indexPath.row])
        cell.lockUnlockView.image = UIImage(named: (levelObj.is_unlocked ? "unlock" : lockImageArray[indexPath.row]))
        
        print(levelObj)
        cell.levelIMg.sd_setImage(with: URL(string: levelObj.image), placeholderImage: UIImage(named: ""))

        
        if tappedIndex != nil && tappedIndex == indexPath.row {
            cell.levelViewHeight.constant = 150
            cell.levelNameViewHeight.constant = 60
            cell.levelDescriptionViewHeight.constant = 90
            cell.keyImageCenterConstraint.isActive = false
            cell.keyImageLeadingConstraint.isActive = true
        } else {
            cell.levelViewHeight.constant = 100
            cell.levelNameViewHeight.constant = 60
            cell.levelDescriptionViewHeight.constant = 40
            cell.keyImageCenterConstraint.isActive = true
            cell.keyImageLeadingConstraint.isActive = false
        }
        
        if indexPath.row % 2 != 0 {
            cell.transform = CGAffineTransform(scaleX: -1, y: 1);
            cell.levelName.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell.levelNumber.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell.levelDescription.transform = CGAffineTransform(scaleX: -1, y: 1)
        } else {
            cell.transform = CGAffineTransform(scaleX: 1, y: 1)
            cell.levelName.transform = CGAffineTransform(scaleX: 1, y: 1)
            cell.levelNumber.transform = CGAffineTransform(scaleX: 1, y: 1)
            cell.levelDescription.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tappedIndex != nil, tappedIndex == indexPath.row {
            tappedIndex = nil
        } else {
            tappedIndex = indexPath.row
        }
        tableView.reloadData()
    }
    
}

extension MyRewardsView: LeaderBoardProfileDelegate {
    func showRewardTransactions() {
        let storyBoard = UIStoryboard(name: "Other", bundle: nil)
        let rewardTransactionVC = storyBoard.instantiateViewController(withIdentifier: "rewardTransactionsView")
        rewardTransactionVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(rewardTransactionVC, animated: false)
    }
}
