//
//  newViewController.swift
//  PKConnect
//
//  Created by c on 19/05/23.
//

import UIKit
import WebKit


class newViewController: UIViewController, WKNavigationDelegate {
    private var webView: WKWebView!
    var campaign: Campaign?
    var url = ""

    @IBAction func BackButtonPress(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    

    @IBOutlet weak var lblName: CustomLabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print(campaign)
        guard let unwrappedCampaign = campaign else {
            return
        }
        lblName.text = unwrappedCampaign.title
        url = unwrappedCampaign.url
        print("url is >>>>>>>>>>>>>",url)
        webView = WKWebView()
        webView = WKWebView(frame: view.bounds)
              webView.navigationDelegate = self
              view.addSubview(webView)
               
        webView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 60),
            webView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -10)
        ])
        
            let url = URL(string: url)
            let request = URLRequest(url: url!)
            webView.load(request)
    }

    override func loadView() {
        super.loadView()

        webView = WKWebView()
        webView.navigationDelegate = self
        view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 200),
            webView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -10)
        ])
    }
}










